package aurum.deco;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        RenderingRegistry.registerBlockHandler(new RenderBookShelf());
        aurumBookShelfRenderType = RenderingRegistry.getNextAvailableRenderId();
    }

    public static int aurumBookShelfRenderType;
}