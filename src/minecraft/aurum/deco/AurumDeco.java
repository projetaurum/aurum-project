package aurum.deco;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFence;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.server.MinecraftServer;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;
import aurum.deco.blocks.BlockArdoise;
import aurum.deco.blocks.BlockAurumBookShelf;
import aurum.deco.blocks.BlockAurumPane;
import aurum.deco.blocks.BlockAurumSandstone;
import aurum.deco.blocks.BlockAurumStairs;
import aurum.deco.blocks.BlockAurumVitraux;
import aurum.deco.blocks.BlockAurumWall;
import aurum.deco.blocks.BlockCarpet;
import aurum.deco.blocks.BlockHardenedClay;
import aurum.deco.blocks.BlockStainedClay;
import aurum.deco.items.ItemArdoise;
import aurum.deco.items.ItemBlockArdoise;
import aurum.deco.items.ItemBlockAurumSandstone;
import aurum.deco.items.ItemBlockAurumWall;
import aurum.deco.items.ItemBlockCarpet;
import aurum.deco.items.ItemBlockStainedClay;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumDeco.ID, version = AurumDeco.VERSION, name = "AurumDeco", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumDeco
{
    @Instance("AurumDeco")
    public static AurumDeco decoInstance;

    @SidedProxy(clientSide = "aurum.deco.ClientProxy", serverSide = "aurum.deco.CommonProxy")
    public static CommonProxy proxy;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumDecoMod";

    public static final CreativeTabs aurumTab = AurumCore.aurumDecoTab;

    public static Logger logger;

    public static MinecraftServer server;

    public static final Block blockStoneBrickWall = new BlockAurumWall(2001, Block.stoneBrick)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("StoneBrickWall")
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockBrownPlanksFence = new BlockFence(2002, "wood_spruce", Material.wood)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BrownPlanksFence")
    .setHardness(2.0F).setResistance(5.0F)
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockWhitePlanksFence = new BlockFence(2003, "wood_birch", Material.wood)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("WhitePlanksFence")
    .setHardness(2.0F).setResistance(5.0F)
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockPinkPlanksFence = new BlockFence(2004, "wood_jungle", Material.wood)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("PinkPlanksFence")
    .setHardness(2.0F).setResistance(5.0F)
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockVitraux = new BlockAurumVitraux(2005)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("AurumVitraux")
    .setHardness(0.3F)
    .setStepSound(Block.soundGlassFootstep);

    public static final Block blockAurumSandstone = new BlockAurumSandstone(2006)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("Sandstone")
    .setHardness(0.8F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockArdoise = new BlockArdoise(2007)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockArdoise")
    .setResistance(10.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockArdoiseStairsGray = new BlockAurumStairs(2008, blockArdoise, 6)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockArdoiseStairsGray")
    .setHardness(1.0F)
    .setResistance(10.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockArdoiseStairsBlack = new BlockAurumStairs(2009, blockArdoise, 3)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockArdoiseStairsBlack")
    .setHardness(2.0F)
    .setResistance(10.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockSandstoneBurnedStairs = new BlockAurumStairs(2010, blockAurumSandstone, 3)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockSandstoneStairsBurned")
    .setHardness(2.0F)
    .setResistance(10.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockTuileStairs = new BlockAurumStairs(2011, blockArdoise, 4)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockTuileStairs")
    .setHardness(1.8F)
    .setResistance(10.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockAurumBookShelf = new BlockAurumBookShelf(2100, Material.wood)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("BlockAurumBookShelf")
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockBronzeBars = new BlockAurumPane(2101, "aurum/deco:bars_bronze", "aurum/deco:bars_bronze", Material.iron, true)
    .setUnlocalizedName("BronzeBars")
    .setCreativeTab(aurumTab);

    public static final Block blockHardenedClay = new BlockHardenedClay(2102)
    .setUnlocalizedName("Hardened Clay")
    .setCreativeTab(aurumTab)
    .setHardness(2.5F).setResistance(7.5F);

    public static final Block blockStainedClay = new BlockStainedClay(2103)
    .setUnlocalizedName("Stained Clay")
    .setCreativeTab(aurumTab)
    .setHardness(2.5F).setResistance(7.5F);

    public static final Block blockCarpet = new BlockCarpet(2104)
    .setUnlocalizedName("Wool Carpet")
    .setCreativeTab(aurumTab)
    .setStepSound(Block.soundClothFootstep);

    public static final Block blockChoco = new Block(2107, Material.rock)
    .setUnlocalizedName("aurum/deco:block_chocolate")
    .setCreativeTab(aurumTab);

    public static final Item itemArdoise = new ItemArdoise(2160)
    .setUnlocalizedName("ItemArdoise")
    .setCreativeTab(aurumTab);

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        decoInstance = this;
        proxy.registerRendering();
    }

    @Init
    public void init(FMLInitializationEvent event)
    {
        //Blocks
        AurumRegistry.registerBlock(blockBrownPlanksFence, "Spruce Planks Fence");
        AurumRegistry.registerBlock(blockPinkPlanksFence, "Jungle Planks Fence");
        AurumRegistry.registerBlock(blockWhitePlanksFence, "Birch Planks Fence");
        AurumRegistry.registerBlock(blockArdoiseStairsGray, "Gray Slate Stairs");
        AurumRegistry.registerBlock(blockArdoiseStairsBlack, "Dark Ardonite Stairs");
        AurumRegistry.registerBlock(blockTuileStairs, "Tile Stairs");
        AurumRegistry.registerBlock(blockAurumBookShelf, "BookShelf");
        AurumRegistry.registerBlock(blockBronzeBars, "Bronze Bars");
        AurumRegistry.registerBlock(blockHardenedClay, "Hardened Clay");
        AurumRegistry.registerBlock(blockChoco, "Chocolate Block");
        //ItemBlocks & Blocks
        //AurumRegistry.registerBlock(blockVitraux, ItemBlockAurumVitraux.class, "Vitraux");
        AurumRegistry.registerBlock(blockStoneBrickWall, ItemBlockAurumWall.class, "StoneBrick Wall");
        AurumRegistry.registerBlock(blockAurumSandstone, ItemBlockAurumSandstone.class, "Sandstone");
        AurumRegistry.registerBlock(blockArdoise, ItemBlockArdoise.class, "Ardoise");
        AurumRegistry.registerBlock(blockStainedClay, ItemBlockStainedClay.class, "Stained Clay");
        AurumRegistry.registerBlock(blockCarpet, ItemBlockCarpet.class, "Wool Carpet");
        //Adding TileEntities
        //Adding Items
        AurumRegistry.registerItem(itemArdoise, "ItemArdoise");
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabDeco", "en_US", "Aurum Deco");
        //Recipes
        registerRecipes();
    }

    public final void registerRecipes()
    {
        AurumRegistry.removeRecipe(new ItemStack(Block.sandStone, 1));
        /*
         * Adding Furnace Recipes
         */
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 0, new ItemStack(AurumDeco.blockAurumSandstone, 1, 8), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 1, new ItemStack(AurumDeco.blockAurumSandstone, 1, 10), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 7, new ItemStack(AurumDeco.blockAurumSandstone, 1, 4), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 9, new ItemStack(AurumDeco.blockAurumSandstone, 1, 5), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 10, new ItemStack(AurumDeco.blockAurumSandstone, 1, 2), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 11, new ItemStack(AurumDeco.blockAurumSandstone, 1, 6), 0.0F);
        FurnaceRecipes.smelting().addSmelting(Block.sandStone.blockID, 0, new ItemStack(AurumDeco.blockAurumSandstone, 1, 9), 0.0F);
        FurnaceRecipes.smelting().addSmelting(Block.sandStone.blockID, 1, new ItemStack(AurumDeco.blockAurumSandstone, 1, 7), 0.0F);
        FurnaceRecipes.smelting().addSmelting(Block.sandStone.blockID, 2, new ItemStack(AurumDeco.blockAurumSandstone, 1, 11), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumDeco.blockAurumSandstone.blockID, 8, new ItemStack(AurumDeco.blockAurumSandstone, 1, 3), 0.0F);
        FurnaceRecipes.smelting().addSmelting(Block.blockClay.blockID, 0, new ItemStack(AurumDeco.blockHardenedClay.blockID, 1, 0), 0.0F);
        /*
         * Adding Crafting Table Recipes
         */
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockAurumSandstone, 1, 0),
                               new Object[] {"XX", "XX", 'X', Block.sand});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 0),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.stoneBrick, 1, 1)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 1),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.stoneBrick, 1, 0)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 9),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.wood, 1, 0)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 10),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.wood, 1, 1)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 11),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.wood, 1, 2)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStoneBrickWall, 6, 12),
                               new Object[] {"   ", "XXX", "XXX", 'X', new ItemStack(Block.wood, 1, 3)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockAurumSandstone, 4, 1),
                               new Object[] {"XX", "XX", 'X', new ItemStack(AurumDeco.blockAurumSandstone, 1, 0)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockAurumSandstone, 4, 2),
                               new Object[] {"XX", "XX", 'X', new ItemStack(AurumDeco.blockAurumSandstone, 1, 3)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockAurumSandstone, 4, 10),
                               new Object[] {"XX", "XX", 'X', new ItemStack(AurumDeco.blockAurumSandstone, 1, 8)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockWhitePlanksFence, 4, 0),
                               new Object[] {"   ", "XOX", "XOX",
                                             'X', new ItemStack(Block.wood, 1, 1),
                                             'O', Item.stick
                                            });
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockArdoise, 4, 2),
                new Object[] {"XX", "XX", 'X', new ItemStack(AurumDeco.blockArdoise, 1, 0)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockArdoise, 4, 3),
                new Object[] {"XX", "XX", 'X', new ItemStack(AurumDeco.blockArdoise, 1, 1)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockArdoiseStairsBlack,4,0),
        		new Object[] {"X  ","XX ","XXX", 'X', new ItemStack(AurumDeco.blockArdoise,1,3)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockArdoiseStairsGray,4,0),
        		new Object[] {"X  ","XX ","XXX", 'X', new ItemStack(AurumDeco.blockArdoise,1,2)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockTuileStairs,4,0),
        		new Object[] {"X  ","XX ","XXX", 'X', new ItemStack(AurumDeco.blockArdoise,1,4)});
        
        GameRegistry.addRecipe(new ItemStack(Item.silk,4),
        		new Object[] {"X", 'X', new ItemStack(Block.cloth,1)});
        GameRegistry.addRecipe(new ItemStack(AurumDeco.blockArdoise,4,4),
        		new Object[] {"XX","XX", 'X', Block.hardenedClay});
        

        for (int i = 0; i < 16; i++)
        {
            GameRegistry.addRecipe(new ItemStack(AurumDeco.blockStainedClay, 8, +i),
                                   new Object[] {"XXX", "XOX", "XXX",
                                                 'O', new ItemStack(Item.dyePowder, 1, +i),
                                                 'X', new ItemStack(AurumDeco.blockHardenedClay, 1, 0)
                                                });
            GameRegistry.addRecipe(new ItemStack(AurumDeco.blockCarpet, 3, +i),
                                   new Object[] {"XX",
                                                 'X', new ItemStack(Block.cloth, 1, +i),
                                                });
        }
    }
}