package aurum.deco.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockAurumWall extends ItemBlock
{
    public ItemBlockAurumWall(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockAurumWall");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        if (stack.getItemDamage() == 0)
        {
            return "Mossy Stone Bricks Wall";
        }

        if (stack.getItemDamage() == 1)
        {
            return "Stone Bricks Wall";
        }

        if (stack.getItemDamage() == 2)
        {
            return "Quartz Wall";
        }

        if (stack.getItemDamage() == 3)
        {
            return "Polished Sandstone Wall";
        }

        if (stack.getItemDamage() == 4)
        {
            return "Cooked Polished Sandstone Wall";
        }

        if (stack.getItemDamage() == 5)
        {
            return "Burned Polished Sandstone Wall";
        }

        if (stack.getItemDamage() == 6)
        {
            return "Sandstone Brick Wall";
        }

        if (stack.getItemDamage() == 7)
        {
            return "Cooked Sandstone Brick Wall";
        }

        if (stack.getItemDamage() == 8)
        {
            return "Burned Sandstone Brick Wall";
        }

        if (stack.getItemDamage() == 9)
        {
            return "Oak Wood Wall";
        }

        if (stack.getItemDamage() == 10)
        {
            return "Spruce Wood Wall";
        }

        if (stack.getItemDamage() == 11)
        {
            return "Birch Wood Wall";
        }

        if (stack.getItemDamage() == 12)
        {
            return "Jungle Wood Wall";
        }

        return "";
    }
}