package aurum.deco.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockCarpet extends ItemBlock
{
    public ItemBlockCarpet(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockCarpet");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "White Wool Carpet";
                break;

            case 1:
                name = "Orange Wool Carpet";
                break;

            case 2:
                name = "Purple Wool Carpet";
                break;

            case 3:
                name = "Light Bleu Wool Carpet";
                break;

            case 4:
                name = "Yellow Wool Carpet";
                break;

            case 5:
                name = "Lime Wool Carpet";
                break;

            case 6:
                name = "Pink Wool Carpet";
                break;

            case 7:
                name = "Gray Wool Carpet";
                break;

            case 8:
                name = "Silver Wool Carpet";
                break;

            case 9:
                name = "Cyan Wool Carpet";
                break;

            case 10:
                name = "Magenta Wool Carpet";
                break;

            case 11:
                name = "Blue Wool Carpet";
                break;

            case 12:
                name = "Brown Wool Carpet";
                break;

            case 13:
                name = "Green Wool Carpet";
                break;

            case 14:
                name = "Red Wool Carpet";
                break;

            case 15:
                name = "Black Wool Carpet";
                break;
        }

        return name;
    }
}
