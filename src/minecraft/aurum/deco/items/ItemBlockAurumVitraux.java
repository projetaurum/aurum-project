package aurum.deco.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockAurumVitraux extends ItemBlock
{
    public ItemBlockAurumVitraux(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockAurumVitraux");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        if (stack.getItemDamage() == 0)
        {
            return "Empty Glass";
        }

        if (stack.getItemDamage() == 1)
        {
            return "Cyan Glass";
        }

        if (stack.getItemDamage() == 2)
        {
            return "Yellow Glass";
        }

        if (stack.getItemDamage() == 3)
        {
            return "Red Glass";
        }

        if (stack.getItemDamage() == 4)
        {
            return "Cyan & Red Glass";
        }

        if (stack.getItemDamage() == 5)
        {
            return "Cyan & Yellow Glass";
        }

        if (stack.getItemDamage() == 6)
        {
            return "Red & Yellow Glass";
        }

        return "";
    }
}