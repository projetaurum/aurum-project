package aurum.deco.items;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemBlock;

public class ItemBlockHardenedClay extends ItemBlock
{
    public ItemBlockHardenedClay(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockHardenedClay");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "Stained Clay Black";
                break;

            case 1:
                name = "Stained Clay Red";
                break;

            case 2:
                name = "Stained Clay Green";
                break;

            case 3:
                name = "Stained Clay Brown";
                break;

            case 4:
                name = "Stained Clay Blue";
                break;

            case 5:
                name = "Stained Clay Purple";
                break;

            case 6:
                name = "Stained Clay Cyan";
                break;

            case 7:
                name = "Stained Clay Silver";
                break;

            case 8:
                name = "Stained Clay Gray";
                break;

            case 9:
                name = "Stained Clay Pink";
                break;

            case 10:
                name = "Stained Clay Lime";
                break;

            case 11:
                name = "Stained Clay Yellow";
                break;

            case 12:
                name = "Stained Clay Light Blue";
                break;

            case 13:
                name = "Stained Clay Magenta";
                break;

            case 14:
                name = "Stained Clay Orange";
                break;

            case 15:
                name = "Stained Clay White";
                break;

            case 16:
                name = "Hardened Clay";
                break;
        }

        return name;
    }
}
