package aurum.deco.items;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemArdoise extends Item
{
    Icon iconBuffer[];

    public ItemArdoise(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubItems(int par1, CreativeTabs par2, List par3)
    {
        par3.add(new ItemStack(par1, 1, 0));
        par3.add(new ItemStack(par1, 1, 1));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        iconBuffer = new Icon[2];
        iconBuffer[0] = register.registerIcon("aurumdeco:ardoise");
        iconBuffer[1] = register.registerIcon("aurumdeco:ardonite");
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Gray Slate Parts";
                break;

            case 1:
                name = "Dark Ardonite Parts";
                break;
        }

        return name;
    }
}