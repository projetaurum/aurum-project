package aurum.deco.items;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemBlock;

public class ItemBlockStainedClay extends ItemBlock
{
    public ItemBlockStainedClay(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockHardenedClay");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "Black Stained Clay";
                break;

            case 1:
                name = "Red Stained Clay";
                break;

            case 2:
                name = "Green Stained Clay";
                break;

            case 3:
                name = "Brown Stained Clay";
                break;

            case 4:
                name = "Blue Stained Clay";
                break;

            case 5:
                name = "Purple Stained Clay";
                break;

            case 6:
                name = "Cyan Stained Clay";
                break;

            case 7:
                name = "Silver Stained Clay";
                break;

            case 8:
                name = "Gray Stained Clay";
                break;

            case 9:
                name = "Pink Stained Clay";
                break;

            case 10:
                name = "Lime Stained Clay";
                break;

            case 11:
                name = "Yellow Stained Clay";
                break;

            case 12:
                name = "Light Blue Stained Clay";
                break;

            case 13:
                name = "Magenta Stained Clay";
                break;

            case 14:
                name = "Orange Stained Clay";
                break;

            case 15:
                name = "White Stained Clay";
                break;
        }

        return name;
    }
}
