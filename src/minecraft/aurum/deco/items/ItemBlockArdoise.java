package aurum.deco.items;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockArdoise extends ItemBlock
{
    public ItemBlockArdoise(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockArdoise");
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4)
    {
        switch (stack.getItemDamage())
        {
            case 0:
                list.add("Ardoise Grise");
                break;

            case 1:
                list.add("Ardonite Sombre");
                break;

            case 2:
                list.add("Ardoise Grise Taillee");
                break;

            case 3:
                list.add("Ardonite Sombre Taillee");
                break;

            case 4:
                list.add("Tuile");
                break;
        }
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "Raw Gray Slate";
                break;

            case 1:
                name = "Raw Dark Ardonite";
                break;

            case 2:
                name = "Ciseled Gray Slate";
                break;

            case 3:
                name = "Ciseled Dark Ardonite";
                break;

            case 4:
                name = "Tile";
                break;
        }

        return name;
    }
}