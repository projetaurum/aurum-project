package aurum.deco.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockAurumSandstone extends ItemBlock
{
    public ItemBlockAurumSandstone(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockAurumSandstone");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Sandstone";
                break;

            case 1:
                name = "Sandstone Bricks";
                break;

            case 2:
                name = "Burned Sandstone Bricks";
                break;

            case 3:
                name = "Burned Sandstone";
                break;

            case 4:
                name = "Burned Chiseled Sandstone";
                break;

            case 5:
                name = "Burned Polished Sandstone";
                break;

            case 6:
                name = "Burned Smooth Sandstone";
                break;

            case 7:
                name = "Cooked Chiseled Sandstone";
                break;

            case 8:
                name = "Cooked Sandstone";
                break;

            case 9:
                name = "Cooked Polished Sandstone";
                break;

            case 10:
                name = "Cooked Sandstone Bricks";
                break;

            case 11:
                name = "Cooked Smooth Sandstone";
                break;
        }

        return name;
    }
}