package aurum.deco.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockStainedClay extends Block
{
    Icon iconBuffer[];

    public BlockStainedClay(int par1)
    {
        super(par1, Material.rock);
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
        list.add(new ItemStack(this.blockID, 1, 5));
        list.add(new ItemStack(this.blockID, 1, 6));
        list.add(new ItemStack(this.blockID, 1, 7));
        list.add(new ItemStack(this.blockID, 1, 8));
        list.add(new ItemStack(this.blockID, 1, 9));
        list.add(new ItemStack(this.blockID, 1, 10));
        list.add(new ItemStack(this.blockID, 1, 11));
        list.add(new ItemStack(this.blockID, 1, 12));
        list.add(new ItemStack(this.blockID, 1, 13));
        list.add(new ItemStack(this.blockID, 1, 14));
        list.add(new ItemStack(this.blockID, 1, 15));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[16];
        iconBuffer[0] = register.registerIcon("aurumdeco:hardened_clay_stained_black");
        iconBuffer[1] = register.registerIcon("aurumdeco:hardened_clay_stained_red");
        iconBuffer[2] = register.registerIcon("aurumdeco:hardened_clay_stained_green");
        iconBuffer[3] = register.registerIcon("aurumdeco:hardened_clay_stained_brown");
        iconBuffer[4] = register.registerIcon("aurumdeco:hardened_clay_stained_blue");
        iconBuffer[5] = register.registerIcon("aurumdeco:hardened_clay_stained_purple");
        iconBuffer[6] = register.registerIcon("aurumdeco:hardened_clay_stained_cyan");
        iconBuffer[7] = register.registerIcon("aurumdeco:hardened_clay_stained_silver");
        iconBuffer[8] = register.registerIcon("aurumdeco:hardened_clay_stained_gray");
        iconBuffer[9] = register.registerIcon("aurumdeco:hardened_clay_stained_pink");
        iconBuffer[10] = register.registerIcon("aurumdeco:hardened_clay_stained_lime");
        iconBuffer[11] = register.registerIcon("aurumdeco:hardened_clay_stained_yellow");
        iconBuffer[12] = register.registerIcon("aurumdeco:hardened_clay_stained_light_blue");
        iconBuffer[13] = register.registerIcon("aurumdeco:hardened_clay_stained_magenta");
        iconBuffer[14] = register.registerIcon("aurumdeco:hardened_clay_stained_orange");
        iconBuffer[15] = register.registerIcon("aurumdeco:hardened_clay_stained_white");
    }
}
