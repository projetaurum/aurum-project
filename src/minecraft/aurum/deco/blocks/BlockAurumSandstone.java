package aurum.deco.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockAurumSandstone extends Block
{
    Icon iconBuffer[];

    public BlockAurumSandstone(int par1)
    {
        super(par1, Material.rock);
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        if (meta == 1)
        {
            return iconBuffer[1];
        }

        if (meta == 2)
        {
            return iconBuffer[2];
        }

        if (meta == 10)
        {
            return iconBuffer[3];
        }

        if (meta == 3)
        {
            return iconBuffer[4];
        }

        if (meta == 8)
        {
            return iconBuffer[6];
        }

        if (meta == 4)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[5];
            }

            return iconBuffer[8];
        }

        if (meta == 7)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[7];
            }

            return iconBuffer[9];
        }

        if (meta == 5)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[5];
            }

            return iconBuffer[10];
        }

        if (meta == 9)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[7];
            }

            return iconBuffer[11];
        }

        if (meta == 6)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[5];
            }

            return iconBuffer[12];
        }

        if (meta == 11)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[7];
            }

            return iconBuffer[13];
        }

        return iconBuffer[0];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
        list.add(new ItemStack(this.blockID, 1, 5));
        list.add(new ItemStack(this.blockID, 1, 6));
        list.add(new ItemStack(this.blockID, 1, 7));
        list.add(new ItemStack(this.blockID, 1, 8));
        list.add(new ItemStack(this.blockID, 1, 9));
        list.add(new ItemStack(this.blockID, 1, 10));
        list.add(new ItemStack(this.blockID, 1, 11));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[14];
        iconBuffer[0] = register.registerIcon("aurumdeco:sandstone");
        iconBuffer[1] = register.registerIcon("aurumdeco:sandstone_brick");
        iconBuffer[2] = register.registerIcon("aurumdeco:sandstone_brick_burned");
        iconBuffer[3] = register.registerIcon("aurumdeco:sandstone_brick_cuite");
        iconBuffer[4] = register.registerIcon("aurumdeco:sandstone_burned");
        iconBuffer[5] = register.registerIcon("aurumdeco:sandstone_burned_top");
        iconBuffer[6] = register.registerIcon("aurumdeco:sandstone_cuite");
        iconBuffer[7] = register.registerIcon("aurumdeco:sandstone_cuite_top");
        iconBuffer[8] = register.registerIcon("aurumdeco:sandstone_grave_burned");
        iconBuffer[9] = register.registerIcon("aurumdeco:sandstone_grave_cuite");
        iconBuffer[10] = register.registerIcon("aurumdeco:sandstone_polie_burned");
        iconBuffer[11] = register.registerIcon("aurumdeco:sandstone_polie_cuite");
        iconBuffer[12] = register.registerIcon("aurumdeco:sandstone_smooth_burned");
        iconBuffer[13] = register.registerIcon("aurumdeco:sandstone_smooth_cuite");
    }
}