package aurum.deco.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockArdoise extends Block
{
    Icon iconBuffer[];

    public BlockArdoise(int par1)
    {
        super(par1, Material.rock);
    }

    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public float getBlockHardness(World par1World, int par2, int par3, int par4)
    {
        if (par1World.getBlockMetadata(par2, par3, par4) == 0)
        {
            return 1.0F;
        }
        else if (par1World.getBlockMetadata(par2, par3, par4) == 1)
        {
            return 2.0F;
        }
        else if (par1World.getBlockMetadata(par2, par3, par4) == 4)
        {
            return 1.8F;
        }

        return 1.5F;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        if (meta == 0)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[0];
            }

            return iconBuffer[1];
        }

        if (meta == 1)
        {
            if (i == 0 || i == 1)
            {
                return iconBuffer[2];
            }

            return iconBuffer[3];
        }

        if (meta == 2)
        {
            return iconBuffer[4];
        }

        if (meta == 3)
        {
            return iconBuffer[5];
        }

        if (meta == 4)
        {
            return iconBuffer[6];
        }

        return iconBuffer[0];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[7];
        iconBuffer[0] = register.registerIcon("aurumdeco:ardoise_grise");
        iconBuffer[1] = register.registerIcon("aurumdeco:ardoise_grise_cotes");
        iconBuffer[2] = register.registerIcon("aurumdeco:ardoise_noire");
        iconBuffer[3] = register.registerIcon("aurumdeco:ardoise_noire_cotes");
        iconBuffer[4] = register.registerIcon("aurumdeco:ardoise_grise_polished");
        iconBuffer[5] = register.registerIcon("aurumdeco:ardoise_noire_polished");
        iconBuffer[6] = register.registerIcon("aurumdeco:tuile");
    }
}