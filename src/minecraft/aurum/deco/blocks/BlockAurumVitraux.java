package aurum.deco.blocks;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockAurumVitraux extends Block
{
    Icon iconBuffer[];

    public BlockAurumVitraux(int par1)
    {
        super(par1, Material.glass);
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(int par1, CreativeTabs tab, List subItems)
    {
        subItems.add(new ItemStack(this, 1, 0));
        subItems.add(new ItemStack(this, 1, 1));
        subItems.add(new ItemStack(this, 1, 2));
        subItems.add(new ItemStack(this, 1, 3));
        subItems.add(new ItemStack(this, 1, 4));
        subItems.add(new ItemStack(this, 1, 5));
        subItems.add(new ItemStack(this, 1, 6));
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        iconBuffer = new Icon[7];
        iconBuffer[0] = register.registerIcon("aurumdeco:Empty_Stained_glass");
        iconBuffer[1] = register.registerIcon("aurumdeco:Stained_glass_cyan");
        iconBuffer[2] = register.registerIcon("aurumdeco:Stained_glass_yellow");
        iconBuffer[3] = register.registerIcon("aurumdeco:Stained_glass_red");
        iconBuffer[4] = register.registerIcon("aurumdeco:Stained_glass_cyan_red");
        iconBuffer[5] = register.registerIcon("aurumdeco:Stained_glass_cyan_yellow");
        iconBuffer[6] = register.registerIcon("aurumdeco:Stained_glass_red_yellow");
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }
}