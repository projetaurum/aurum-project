package aurum.deco.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockCarpet extends Block
{
    Icon iconBuffer[];

    public BlockCarpet(int par1)
    {
        super(par1, Material.grass);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
        list.add(new ItemStack(this.blockID, 1, 5));
        list.add(new ItemStack(this.blockID, 1, 6));
        list.add(new ItemStack(this.blockID, 1, 7));
        list.add(new ItemStack(this.blockID, 1, 8));
        list.add(new ItemStack(this.blockID, 1, 9));
        list.add(new ItemStack(this.blockID, 1, 10));
        list.add(new ItemStack(this.blockID, 1, 11));
        list.add(new ItemStack(this.blockID, 1, 12));
        list.add(new ItemStack(this.blockID, 1, 13));
        list.add(new ItemStack(this.blockID, 1, 14));
        list.add(new ItemStack(this.blockID, 1, 15));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[16];

        for (int i = 0; i < this.iconBuffer.length; ++i)
        {
            this.iconBuffer[i] = register.registerIcon("cloth_" + i);
        }
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }
}
