package aurum.deco.blocks;

import java.util.List;

import aurum.deco.AurumDeco;
import net.minecraft.block.Block;
import net.minecraft.block.BlockWall;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockAurumWall extends BlockWall
{
    public BlockAurumWall(int par1, Block par2Block)
    {
        super(par1, par2Block);
    }

    @Override
    public Icon getIcon(int par1, int par2)
    {
        if (par2 == 0)
        {
            return Block.stoneBrick.getIcon(par1, 1);
        }
        else if (par2 == 1)
        {
            return Block.stoneBrick.getIcon(par1, 0);
        }
        else if (par2 == 2)
        {
            return Block.blockNetherQuartz.getIcon(par1, 0);
        }
        else if (par2 == 3)
        {
            return Block.sandStone.getIcon(par1, 0);
        }
        else if (par2 == 4)
        {
            return AurumDeco.blockAurumSandstone.getIcon(par1, 9);
        }
        else if (par2 == 5)
        {
            return AurumDeco.blockAurumSandstone.getIcon(par1, 5);
        }
        else if (par2 == 6)
        {
            return AurumDeco.blockAurumSandstone.getIcon(par1, 1);
        }
        else if (par2 == 7)
        {
            return AurumDeco.blockAurumSandstone.getIcon(par1, 10);
        }
        else if (par2 == 8)
        {
            return AurumDeco.blockAurumSandstone.getIcon(par1, 2);
        }
        else if (par2 == 9)
        {
            return Block.wood.getIcon(par1, 0);
        }
        else if (par2 == 10)
        {
            return Block.wood.getIcon(par1, 1);
        }
        else if (par2 == 11)
        {
            return Block.wood.getIcon(par1, 2);
        }
        else if (par2 == 12)
        {
            return Block.wood.getIcon(par1, 3);
        }

        return null;
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List par3List)
    {
        par3List.add(new ItemStack(par1, 1, 0));
        par3List.add(new ItemStack(par1, 1, 1));
        par3List.add(new ItemStack(par1, 1, 2));
        par3List.add(new ItemStack(par1, 1, 3));
        par3List.add(new ItemStack(par1, 1, 4));
        par3List.add(new ItemStack(par1, 1, 5));
        par3List.add(new ItemStack(par1, 1, 6));
        par3List.add(new ItemStack(par1, 1, 7));
        par3List.add(new ItemStack(par1, 1, 8));
        par3List.add(new ItemStack(par1, 1, 9));
        par3List.add(new ItemStack(par1, 1, 10));
        par3List.add(new ItemStack(par1, 1, 11));
        par3List.add(new ItemStack(par1, 1, 12));
    }
}