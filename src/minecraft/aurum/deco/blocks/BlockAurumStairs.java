package aurum.deco.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockStairs;

public class BlockAurumStairs extends BlockStairs
{
    public BlockAurumStairs(int par1, Block par2Block, int par3)
    {
        super(par1, par2Block, par3);
        useNeighborBrightness[par1] = true;
    }
}