package aurum.deco.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockHardenedClay extends Block
{
    Icon iconBuffer[];

    public BlockHardenedClay(int par1)
    {
        super(par1, Material.rock);
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[0];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[1];
        iconBuffer[0] = register.registerIcon("aurumdeco:hardened_clay");
    }
}
