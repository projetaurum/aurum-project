package aurum.deco.blocks;

import net.minecraft.block.BlockPane;
import net.minecraft.block.material.Material;

public class BlockAurumPane extends BlockPane
{
    public BlockAurumPane(int par1, String par2Str, String par3Str,
                          Material par4Material, boolean par5)
    {
        super(par1, par2Str, par3Str, par4Material, par5);
    }
}