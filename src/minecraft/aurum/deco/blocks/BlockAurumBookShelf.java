package aurum.deco.blocks;

import aurum.deco.ClientProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockAurumBookShelf extends Block
{
    Icon iconBuffer[];

    public BlockAurumBookShelf(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    public int getRenderType()
    {
        return ClientProxy.aurumBookShelfRenderType;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        iconBuffer = new Icon[2];
        iconBuffer[0] = register.registerIcon("aurumdeco:bookshelf_wood");
        iconBuffer[1] = register.registerIcon("aurumdeco:bookshelf_book");
    }

    @Override
    public Icon getIcon(int i, int m)
    {
        if (m == 0)
        {
            if (i == 0)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }
        else if (m == 1)
        {
            if (i == 1)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }
        else if (m == 2)
        {
            if (i == 2)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }
        else if (m == 3)
        {
            if (i == 3)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }
        else if (m == 4)
        {
            if (i == 4)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }
        else if (m == 5)
        {
            if (i == 5)
            {
                return iconBuffer[1];
            }

            return iconBuffer[0];
        }

        return iconBuffer[0];
    }

    public int onBlockPlaced(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8, int par9)
    {
        switch (par5)
        {
            case 0:
                return 0;

            case 1:
                return 1;

            case 2:
                return 2;

            case 3:
                return 3;

            case 4:
                return 4;

            case 5:
                return 5;
        }

        return 0;
    }
}