package aurum.deco;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class RenderBookShelf implements ISimpleBlockRenderingHandler
{
    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID,
                                     RenderBlocks renderer)
    {
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
                                    Block block, int modelId, RenderBlocks renderer)
    {
        int l = renderer.blockAccess.getBlockMetadata(x, y, z);
        int i1 = l & 12;

        if (i1 == 4)
        {
            renderer.uvRotateTop = 0;
            renderer.uvRotateBottom = 0;
        }
        else if (l == 3 || l == 2)
        {
            renderer.uvRotateTop = 1;
            renderer.uvRotateBottom = 1;
        }

        boolean flag = renderer.renderStandardBlock(block, x, y, z);
        renderer.uvRotateSouth = 0;
        renderer.uvRotateEast = 0;
        renderer.uvRotateWest = 0;
        renderer.uvRotateNorth = 0;
        renderer.uvRotateTop = 0;
        renderer.uvRotateBottom = 0;
        return flag;
    }

    @Override
    public boolean shouldRender3DInInventory()
    {
        return false;
    }

    @Override
    public int getRenderId()
    {
        return ClientProxy.aurumBookShelfRenderType;
    }
}
