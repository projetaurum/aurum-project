package aurum.steampunk;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;

public class AurumKeyHandler extends KeyHandler
{
	public AurumKeyHandler(KeyBinding[] keyBindings) 
	{
		super(keyBindings);
	}

	@Override
	public String getLabel() 
	{
		return "Synapse Key";
	}

	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb,
			boolean tickEnd, boolean isRepeat) 
	{
		Minecraft.getMinecraft().thePlayer.openGui(AurumSteamPunk.steamPunkInstance, AurumSteamPunk.GUI_SYNAPSE, Minecraft.getMinecraft().theWorld, 0, 0, 0);
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) 
	{
		
	}

	@Override
	public EnumSet<TickType> ticks() 
	{
		return null;
	}
	
}