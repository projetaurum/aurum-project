package aurum.steampunk.fonderie;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelFonderie extends ModelBase
{
    ModelRenderer Block1;
    ModelRenderer Block11;
    ModelRenderer Block12;
    ModelRenderer Block13;
    ModelRenderer Block14;
    ModelRenderer Block15;
    ModelRenderer Block16;
    ModelRenderer Block17;
    ModelRenderer Block18;
    ModelRenderer Block19;
    ModelRenderer Block110;
    ModelRenderer Block111;
    ModelRenderer Block112;
    ModelRenderer Block113;
    ModelRenderer Block114;
    ModelRenderer Block115;
    ModelRenderer Block116;
    ModelRenderer Block117;
    ModelRenderer Block118;
    ModelRenderer Block119;
    ModelRenderer Block120;
    ModelRenderer Block121;
    ModelRenderer Block122;
    ModelRenderer Block123;
    ModelRenderer Block124;
    ModelRenderer Block125;
    ModelRenderer Block126;
    ModelRenderer Block127;
    ModelRenderer Block128;
    ModelRenderer Block129;
    ModelRenderer Block130;
    ModelRenderer Block131;
    ModelRenderer Block132;
    ModelRenderer Block133;
    ModelRenderer tabli;
    ModelRenderer tabli1;
    ModelRenderer tabli2;
    ModelRenderer tabli3;
    ModelRenderer tabli4;
    ModelRenderer tabli5;
    ModelRenderer tabli6;
    ModelRenderer tabli7;
    ModelRenderer tabli8;
    ModelRenderer tabli9;
    ModelRenderer tabli10;
    ModelRenderer tabli11;
    ModelRenderer tabli12;
    ModelRenderer tabli13;
    ModelRenderer tabli14;
    ModelRenderer tabli15;
    ModelRenderer tabli16;
    ModelRenderer tabli17;
    ModelRenderer Block2;
    ModelRenderer Block21;
    ModelRenderer Block22;
    ModelRenderer Block23;
    ModelRenderer Block24;
    ModelRenderer Block25;
    ModelRenderer Block26;
    ModelRenderer Block27;
    ModelRenderer Block28;
    ModelRenderer Block29;
    ModelRenderer Block210;
    ModelRenderer Block211;
    ModelRenderer Block212;
    ModelRenderer Block213;

    public ModelFonderie()
    {
        this( 0.0f );
    }

    public ModelFonderie( float par1 )
    {
        Block1 = new ModelRenderer( this, 5, 124 );
        Block1.setTextureSize( 256, 256 );
        Block1.addBox( -8F, -8F, -24F, 16, 16, 48);
        Block1.setRotationPoint( 0F, 16F, -1F );
        Block11 = new ModelRenderer( this, 5, 188 );
        Block11.setTextureSize( 256, 256 );
        Block11.addBox( -8F, -8F, -24F, 16, 16, 48);
        Block11.setRotationPoint( -16F, 16F, -1F );
        Block12 = new ModelRenderer( this, 134, 149 );
        Block12.setTextureSize( 256, 256 );
        Block12.addBox( -1F, -7F, -24F, 2, 14, 48);
        Block12.setRotationPoint( -23F, 1F, -1F );
        Block13 = new ModelRenderer( this, 109, 140 );
        Block13.setTextureSize( 256, 256 );
        Block13.addBox( -7F, -7F, -1F, 14, 14, 2);
        Block13.setRotationPoint( -1F, 1F, -24F );
        Block14 = new ModelRenderer( this, 144, 150 );
        Block14.setTextureSize( 256, 256 );
        Block14.addBox( -7F, -7F, -1F, 14, 14, 2);
        Block14.setRotationPoint( -15F, 1F, -24F );
        Block15 = new ModelRenderer( this, 139, 170 );
        Block15.setTextureSize( 256, 256 );
        Block15.addBox( -7F, -7F, -1F, 14, 14, 2);
        Block15.setRotationPoint( -15F, 1F, 22F );
        Block16 = new ModelRenderer( this, 86, 123 );
        Block16.setTextureSize( 256, 256 );
        Block16.addBox( -8F, -7F, -1F, 16, 14, 2);
        Block16.setRotationPoint( 0F, 1F, 22F );
        Block17 = new ModelRenderer( this, 189, 178 );
        Block17.setTextureSize( 256, 256 );
        Block17.addBox( -7F, -7F, -1F, 14, 14, 2);
        Block17.setRotationPoint( -1F, 1F, -10F );
        Block18 = new ModelRenderer( this, 189, 178 );
        Block18.setTextureSize( 256, 256 );
        Block18.addBox( -7F, -7F, -1F, 14, 14, 2);
        Block18.setRotationPoint( -15F, 1F, -10F );
        Block19 = new ModelRenderer( this, 92, 101 );
        Block19.setTextureSize( 256, 256 );
        Block19.addBox( -1F, -7F, -3F, 3, 14, 6);
        Block19.setRotationPoint( 6F, 1F, -6F );
        Block110 = new ModelRenderer( this, 124, 103 );
        Block110.setTextureSize( 256, 256 );
        Block110.addBox( -1F, -7F, -2F, 3, 14, 4);
        Block110.setRotationPoint( 6F, 1F, 3F );
        Block111 = new ModelRenderer( this, 3, 127 );
        Block111.setTextureSize( 256, 256 );
        Block111.addBox( -4F, 0F, -1F, 9, 1, 2);
        Block111.setRotationPoint( 7F, 6F, -1F );
        Block112 = new ModelRenderer( this, 4, 131 );
        Block112.setTextureSize( 256, 256 );
        Block112.addBox( -4F, -1F, 0F, 9, 2, 1);
        Block112.setRotationPoint( 7F, 6F, -3F );
        Block113 = new ModelRenderer( this, 4, 131 );
        Block113.setTextureSize( 256, 256 );
        Block113.addBox( -4F, -1F, -1F, 9, 2, 1);
        Block113.setRotationPoint( 7F, 6F, 1F );
        Block114 = new ModelRenderer( this, 110, 109 );
        Block114.setTextureSize( 256, 256 );
        Block114.addBox( -1F, -4F, -2F, 3, 8, 4);
        Block114.setRotationPoint( 6F, -2F, -1F );
        Block115 = new ModelRenderer( this, 0, 79 );
        Block115.setTextureSize( 256, 256 );
        Block115.addBox( -11F, -1F, -13F, 23, 2, 26);
        Block115.setRotationPoint( -9F, 6F, 6F );
        Block116 = new ModelRenderer( this, 1, 111 );
        Block116.setTextureSize( 256, 256 );
        Block116.addBox( 0F, -1F, -13F, 1, 2, 26);
        Block116.setRotationPoint( -20F, 4F, 6F );
        Block117 = new ModelRenderer( this, 9, 119 );
        Block117.setTextureSize( 256, 256 );
        Block117.addBox( 0F, -1F, -9F, 1, 2, 18);
        Block117.setRotationPoint( 2F, 4F, 10F );
        Block118 = new ModelRenderer( this, 23, 133 );
        Block118.setTextureSize( 256, 256 );
        Block118.addBox( 0F, -1F, -2F, 1, 2, 4);
        Block118.setRotationPoint( 2F, 4F, -5F );
        Block119 = new ModelRenderer( this, 0, 107 );
        Block119.setTextureSize( 256, 256 );
        Block119.addBox( -10F, -1F, 0F, 21, 2, 1);
        Block119.setRotationPoint( -9F, 4F, -7F );
        Block120 = new ModelRenderer( this, 0, 107 );
        Block120.setTextureSize( 256, 256 );
        Block120.addBox( -10F, -1F, -1F, 21, 2, 1);
        Block120.setRotationPoint( -9F, 4F, 19F );
        Block121 = new ModelRenderer( this, 123, 107 );
        Block121.setTextureSize( 256, 256 );
        Block121.addBox( -1F, -3F, -8F, 3, 6, 16);
        Block121.setRotationPoint( 6F, 5F, 13F );
        Block122 = new ModelRenderer( this, 146, 104 );
        Block122.setTextureSize( 256, 256 );
        Block122.addBox( -1F, -1F, -8F, 3, 2, 16);
        Block122.setRotationPoint( 6F, -5F, 13F );
        Block123 = new ModelRenderer( this, 94, 41 );
        Block123.setTextureSize( 256, 256 );
        Block123.addBox( 0F, -2F, -1F, 1, 4, 2);
        Block123.setRotationPoint( 6F, -1F, 6F );
        Block124 = new ModelRenderer( this, 80, 27 );
        Block124.setTextureSize( 256, 256 );
        Block124.addBox( 0F, 0F, -8F, 1, 1, 16);
        Block124.setRotationPoint( 6F, -4F, 13F );
        Block125 = new ModelRenderer( this, 80, 27 );
        Block125.setTextureSize( 256, 256 );
        Block125.addBox( 0F, 0F, -8F, 1, 1, 16);
        Block125.setRotationPoint( 6F, 1F, 13F );
        Block126 = new ModelRenderer( this, 104, 39 );
        Block126.setTextureSize( 256, 256 );
        Block126.addBox( 0F, -2F, -2F, 1, 4, 4);
        Block126.setRotationPoint( 6F, -1F, 19F );
        Block127 = new ModelRenderer( this, 107, 41 );
        Block127.setTextureSize( 256, 256 );
        Block127.addBox( 0F, -2F, -1F, 1, 4, 2);
        Block127.setRotationPoint( 6F, -1F, 15F );
        Block128 = new ModelRenderer( this, 107, 41 );
        Block128.setTextureSize( 256, 256 );
        Block128.addBox( 0F, -2F, -1F, 1, 4, 2);
        Block128.setRotationPoint( 6F, -1F, 12F );
        Block129 = new ModelRenderer( this, 107, 41 );
        Block129.setTextureSize( 256, 256 );
        Block129.addBox( 0F, -2F, -1F, 1, 4, 2);
        Block129.setRotationPoint( 6F, -1F, 9F );
        Block130 = new ModelRenderer( this, 91, 40 );
        Block130.setTextureSize( 256, 256 );
        Block130.addBox( 0F, -2F, -1F, 1, 4, 2);
        Block130.setRotationPoint( 7F, -1F, 19F );
        Block131 = new ModelRenderer( this, 133, 86 );
        Block131.setTextureSize( 256, 256 );
        Block131.addBox( -8F, -1F, -24F, 16, 2, 48);
        Block131.setRotationPoint( 0F, -7F, -1F );
        Block132 = new ModelRenderer( this, 133, 86 );
        Block132.setTextureSize( 256, 256 );
        Block132.addBox( -8F, -1F, -24F, 16, 2, 48);
        Block132.setRotationPoint( -16F, -7F, -1F );
        Block133 = new ModelRenderer( this, 87, 141 );
        Block133.setTextureSize( 256, 256 );
        Block133.addBox( -1F, -7F, -8F, 2, 14, 16);
        Block133.setRotationPoint( 7F, 1F, -17F );
        tabli = new ModelRenderer( this, 136, 24 );
        tabli.setTextureSize( 256, 256 );
        tabli.addBox( -1F, -2F, -2F, 2, 4, 4);
        tabli.setRotationPoint( 9F, 11F, -7F );
        tabli1 = new ModelRenderer( this, 157, 23 );
        tabli1.setTextureSize( 256, 256 );
        tabli1.addBox( -6F, -1F, -4F, 12, 2, 8);
        tabli1.setRotationPoint( 16F, 12F, 7F );
        tabli2 = new ModelRenderer( this, 114, 23 );
        tabli2.setTextureSize( 256, 256 );
        tabli2.addBox( -1F, -2F, -8F, 2, 4, 16);
        tabli2.setRotationPoint( 9F, 11F, 3F );
        tabli3 = new ModelRenderer( this, 130, 10 );
        tabli3.setTextureSize( 256, 256 );
        tabli3.addBox( -1F, -2F, -4F, 2, 4, 8);
        tabli3.setRotationPoint( 23F, 11F, 7F );
        tabli4 = new ModelRenderer( this, 154, 7 );
        tabli4.setTextureSize( 256, 256 );
        tabli4.addBox( -8F, -2F, -2F, 16, 4, 3);
        tabli4.setRotationPoint( 16F, 11F, 13F );
        tabli5 = new ModelRenderer( this, 155, 35 );
        tabli5.setTextureSize( 256, 256 );
        tabli5.addBox( -7F, 0F, -4F, 14, 1, 8);
        tabli5.setRotationPoint( 17F, 12F, -1F );
        tabli6 = new ModelRenderer( this, 149, 19 );
        tabli6.setTextureSize( 256, 256 );
        tabli6.addBox( -1F, -2F, -2F, 2, 4, 4);
        tabli6.setRotationPoint( 23F, 11F, -7F );
        tabli7 = new ModelRenderer( this, 159, 14 );
        tabli7.setTextureSize( 256, 256 );
        tabli7.addBox( -6F, -2F, -1F, 12, 4, 2);
        tabli7.setRotationPoint( 16F, 11F, -8F );
        tabli8 = new ModelRenderer( this, 192, 12 );
        tabli8.setTextureSize( 256, 256 );
        tabli8.addBox( -6F, -1F, -1F, 12, 2, 2);
        tabli8.setRotationPoint( 16F, 12F, -6F );
        tabli9 = new ModelRenderer( this, 180, 48 );
        tabli9.setTextureSize( 256, 256 );
        tabli9.addBox( -8F, -2F, -1F, 16, 4, 2);
        tabli9.setRotationPoint( 13F, 16F, 21F );
        tabli10 = new ModelRenderer( this, 180, 48 );
        tabli10.setTextureSize( 256, 256 );
        tabli10.addBox( -8F, -2F, -1F, 16, 4, 2);
        tabli10.setRotationPoint( 13F, 16F, -8F );
        tabli11 = new ModelRenderer( this, 186, 62 );
        tabli11.setTextureSize( 256, 256 );
        tabli11.addBox( -3F, -3F, -3F, 6, 6, 6);
        tabli11.setRotationPoint( 15F, 17F, 17F );
        tabli12 = new ModelRenderer( this, 186, 60 );
        tabli12.setTextureSize( 256, 256 );
        tabli12.addBox( -3F, -3F, -3F, 6, 6, 6);
        tabli12.setRotationPoint( 16F, 14F, 17F );
        tabli13 = new ModelRenderer( this, 186, 60 );
        tabli13.setTextureSize( 256, 256 );
        tabli13.addBox( -3F, -4F, -3F, 6, 8, 6);
        tabli13.setRotationPoint( 11F, 18F, 17F );
        tabli14 = new ModelRenderer( this, 154, 0 );
        tabli14.setTextureSize( 256, 256 );
        tabli14.addBox( -8F, -2F, -1F, 16, 4, 3);
        tabli14.setRotationPoint( 16F, 11F, 21F );
        tabli15 = new ModelRenderer( this, 129, 0 );
        tabli15.setTextureSize( 256, 256 );
        tabli15.addBox( -3F, -2F, -3F, 5, 4, 6);
        tabli15.setRotationPoint( 22F, 11F, 17F );
        tabli16 = new ModelRenderer( this, 195, 0 );
        tabli16.setTextureSize( 256, 256 );
        tabli16.addBox( -2F, -2F, -3F, 5, 4, 6);
        tabli16.setRotationPoint( 10F, 11F, 17F );
        tabli17 = new ModelRenderer( this, 94, 44 );
        tabli17.setTextureSize( 256, 256 );
        tabli17.addBox( -3F, -0.5F, -3F, 6, 1, 6);
        tabli17.setRotationPoint( 16F, 10F, 17F );
        Block2 = new ModelRenderer( this, 114, 46 );
        Block2.setTextureSize( 256, 256 );
        Block2.addBox( -6F, -10F, -6F, 12, 20, 12);
        Block2.setRotationPoint( 16F, -2F, -16F );
        Block21 = new ModelRenderer( this, 73, 83 );
        Block21.setTextureSize( 256, 256 );
        Block21.addBox( -8F, -1F, -1F, 16, 2, 2);
        Block21.setRotationPoint( 6.999996F, -19F, -16F );
        Block22 = new ModelRenderer( this, 112, 80 );
        Block22.setTextureSize( 256, 256 );
        Block22.addBox( -6F, -3F, -1F, 12, 6, 2);
        Block22.setRotationPoint( 16F, -15F, -23F );
        Block23 = new ModelRenderer( this, 105, 75 );
        Block23.setTextureSize( 256, 256 );
        Block23.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block23.setRotationPoint( 23F, -19F, -23F );
        Block24 = new ModelRenderer( this, 105, 75 );
        Block24.setTextureSize( 256, 256 );
        Block24.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block24.setRotationPoint( 23F, -19F, -9F );
        Block25 = new ModelRenderer( this, 112, 80 );
        Block25.setTextureSize( 256, 256 );
        Block25.addBox( -6F, -3F, -1F, 12, 6, 2);
        Block25.setRotationPoint( 8.999998F, -15F, -16F );
        Block26 = new ModelRenderer( this, 112, 80 );
        Block26.setTextureSize( 256, 256 );
        Block26.addBox( -6F, -3F, -1F, 12, 6, 2);
        Block26.setRotationPoint( 23F, -15F, -16F );
        Block27 = new ModelRenderer( this, 112, 80 );
        Block27.setTextureSize( 256, 256 );
        Block27.addBox( -6F, -3F, -1F, 12, 6, 2);
        Block27.setRotationPoint( 16F, -15F, -9F );
        Block28 = new ModelRenderer( this, 73, 83 );
        Block28.setTextureSize( 256, 256 );
        Block28.addBox( -8F, -1F, -1F, 16, 2, 2);
        Block28.setRotationPoint( 16F, -19F, -25F );
        Block29 = new ModelRenderer( this, 73, 83 );
        Block29.setTextureSize( 256, 256 );
        Block29.addBox( -8F, -1F, -1F, 16, 2, 2);
        Block29.setRotationPoint( 16F, -19F, -7F );
        Block210 = new ModelRenderer( this, 73, 83 );
        Block210.setTextureSize( 256, 256 );
        Block210.addBox( -8F, -1F, -1F, 16, 2, 2);
        Block210.setRotationPoint( 25F, -19F, -16F );
        Block211 = new ModelRenderer( this, 105, 75 );
        Block211.setTextureSize( 256, 256 );
        Block211.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block211.setRotationPoint( 8.999997F, -19F, -9F );
        Block212 = new ModelRenderer( this, 105, 75 );
        Block212.setTextureSize( 256, 256 );
        Block212.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block212.setRotationPoint( 8.999997F, -19F, -23F );
        Block213 = new ModelRenderer( this, 131, 79 );
        Block213.setTextureSize( 256, 256 );
        Block213.addBox( -6F, -6F, -5.5F, 12, 12, 11);
        Block213.setRotationPoint( 13F, 1F, -16F );
    }

   public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
   {
        Block1.rotateAngleX = 0F;
        Block1.rotateAngleY = 0F;
        Block1.rotateAngleZ = 0F;
        Block1.renderWithRotation(par7);

        Block11.rotateAngleX = 0F;
        Block11.rotateAngleY = -3.141592F;
        Block11.rotateAngleZ = 0F;
        Block11.renderWithRotation(par7);

        Block12.rotateAngleX = 0F;
        Block12.rotateAngleY = 0F;
        Block12.rotateAngleZ = 0F;
        Block12.renderWithRotation(par7);

        Block13.rotateAngleX = 0F;
        Block13.rotateAngleY = 0F;
        Block13.rotateAngleZ = 0F;
        Block13.renderWithRotation(par7);

        Block14.rotateAngleX = 0F;
        Block14.rotateAngleY = 0F;
        Block14.rotateAngleZ = 0F;
        Block14.renderWithRotation(par7);

        Block15.rotateAngleX = 0F;
        Block15.rotateAngleY = 0F;
        Block15.rotateAngleZ = 0F;
        Block15.renderWithRotation(par7);

        Block16.rotateAngleX = 0F;
        Block16.rotateAngleY = 0F;
        Block16.rotateAngleZ = 0F;
        Block16.renderWithRotation(par7);

        Block17.rotateAngleX = 0F;
        Block17.rotateAngleY = 0F;
        Block17.rotateAngleZ = 0F;
        Block17.renderWithRotation(par7);

        Block18.rotateAngleX = 0F;
        Block18.rotateAngleY = 0F;
        Block18.rotateAngleZ = 0F;
        Block18.renderWithRotation(par7);

        Block19.rotateAngleX = 0F;
        Block19.rotateAngleY = 0F;
        Block19.rotateAngleZ = 0F;
        Block19.renderWithRotation(par7);

        Block110.rotateAngleX = 0F;
        Block110.rotateAngleY = 0F;
        Block110.rotateAngleZ = 0F;
        Block110.renderWithRotation(par7);

        Block111.rotateAngleX = 0F;
        Block111.rotateAngleY = 0F;
        Block111.rotateAngleZ = 0.2617994F;
        Block111.renderWithRotation(par7);

        Block112.rotateAngleX = 0F;
        Block112.rotateAngleY = 0F;
        Block112.rotateAngleZ = 0.2617994F;
        Block112.renderWithRotation(par7);

        Block113.rotateAngleX = 0F;
        Block113.rotateAngleY = 0F;
        Block113.rotateAngleZ = 0.2617994F;
        Block113.renderWithRotation(par7);

        Block114.rotateAngleX = 0F;
        Block114.rotateAngleY = 0F;
        Block114.rotateAngleZ = 0F;
        Block114.renderWithRotation(par7);

        Block115.rotateAngleX = 0F;
        Block115.rotateAngleY = 0F;
        Block115.rotateAngleZ = 0F;
        Block115.renderWithRotation(par7);

        Block116.rotateAngleX = 0F;
        Block116.rotateAngleY = 0F;
        Block116.rotateAngleZ = 0F;
        Block116.renderWithRotation(par7);

        Block117.rotateAngleX = 0F;
        Block117.rotateAngleY = 0F;
        Block117.rotateAngleZ = 0F;
        Block117.renderWithRotation(par7);

        Block118.rotateAngleX = 0F;
        Block118.rotateAngleY = 0F;
        Block118.rotateAngleZ = 0F;
        Block118.renderWithRotation(par7);

        Block119.rotateAngleX = 0F;
        Block119.rotateAngleY = 0F;
        Block119.rotateAngleZ = 0F;
        Block119.renderWithRotation(par7);

        Block120.rotateAngleX = 0F;
        Block120.rotateAngleY = 0F;
        Block120.rotateAngleZ = 0F;
        Block120.renderWithRotation(par7);

        Block121.rotateAngleX = 0F;
        Block121.rotateAngleY = 0F;
        Block121.rotateAngleZ = 0F;
        Block121.renderWithRotation(par7);

        Block122.rotateAngleX = 0F;
        Block122.rotateAngleY = 0F;
        Block122.rotateAngleZ = 0F;
        Block122.renderWithRotation(par7);

        Block123.rotateAngleX = 0F;
        Block123.rotateAngleY = 0F;
        Block123.rotateAngleZ = 0F;
        Block123.renderWithRotation(par7);

        Block124.rotateAngleX = 0F;
        Block124.rotateAngleY = 0F;
        Block124.rotateAngleZ = 0F;
        Block124.renderWithRotation(par7);

        Block125.rotateAngleX = 0F;
        Block125.rotateAngleY = 0F;
        Block125.rotateAngleZ = 0F;
        Block125.renderWithRotation(par7);

        Block126.rotateAngleX = 0F;
        Block126.rotateAngleY = 0F;
        Block126.rotateAngleZ = 0F;
        Block126.renderWithRotation(par7);

        Block127.rotateAngleX = 0F;
        Block127.rotateAngleY = 0F;
        Block127.rotateAngleZ = 0F;
        Block127.renderWithRotation(par7);

        Block128.rotateAngleX = 0F;
        Block128.rotateAngleY = 0F;
        Block128.rotateAngleZ = 0F;
        Block128.renderWithRotation(par7);

        Block129.rotateAngleX = 0F;
        Block129.rotateAngleY = 0F;
        Block129.rotateAngleZ = 0F;
        Block129.renderWithRotation(par7);

        Block130.rotateAngleX = 0F;
        Block130.rotateAngleY = 0F;
        Block130.rotateAngleZ = 0F;
        Block130.renderWithRotation(par7);

        Block131.rotateAngleX = 0F;
        Block131.rotateAngleY = 0F;
        Block131.rotateAngleZ = 0F;
        Block131.renderWithRotation(par7);

        Block132.rotateAngleX = 0F;
        Block132.rotateAngleY = -3.141592F;
        Block132.rotateAngleZ = 0F;
        Block132.renderWithRotation(par7);

        Block133.rotateAngleX = 0F;
        Block133.rotateAngleY = 0F;
        Block133.rotateAngleZ = 0F;
        Block133.renderWithRotation(par7);

        tabli.rotateAngleX = 0F;
        tabli.rotateAngleY = 0F;
        tabli.rotateAngleZ = 0F;
        tabli.renderWithRotation(par7);

        tabli1.rotateAngleX = 0F;
        tabli1.rotateAngleY = 0F;
        tabli1.rotateAngleZ = 0F;
        tabli1.renderWithRotation(par7);

        tabli2.rotateAngleX = 0F;
        tabli2.rotateAngleY = 0F;
        tabli2.rotateAngleZ = 0F;
        tabli2.renderWithRotation(par7);

        tabli3.rotateAngleX = 0F;
        tabli3.rotateAngleY = 0F;
        tabli3.rotateAngleZ = 0F;
        tabli3.renderWithRotation(par7);

        tabli4.rotateAngleX = 0F;
        tabli4.rotateAngleY = 0F;
        tabli4.rotateAngleZ = 0F;
        tabli4.renderWithRotation(par7);

        tabli5.rotateAngleX = 0F;
        tabli5.rotateAngleY = 0F;
        tabli5.rotateAngleZ = 0F;
        tabli5.renderWithRotation(par7);

        tabli6.rotateAngleX = 0F;
        tabli6.rotateAngleY = 0F;
        tabli6.rotateAngleZ = 0F;
        tabli6.renderWithRotation(par7);

        tabli7.rotateAngleX = 0F;
        tabli7.rotateAngleY = 0F;
        tabli7.rotateAngleZ = 0F;
        tabli7.renderWithRotation(par7);

        tabli8.rotateAngleX = 0F;
        tabli8.rotateAngleY = 0F;
        tabli8.rotateAngleZ = 0F;
        tabli8.renderWithRotation(par7);

        tabli9.rotateAngleX = 0F;
        tabli9.rotateAngleY = 0F;
        tabli9.rotateAngleZ = -0.7299843F;
        tabli9.renderWithRotation(par7);

        tabli10.rotateAngleX = 0F;
        tabli10.rotateAngleY = 0F;
        tabli10.rotateAngleZ = -0.7299843F;
        tabli10.renderWithRotation(par7);

        tabli11.rotateAngleX = 0F;
        tabli11.rotateAngleY = 0F;
        tabli11.rotateAngleZ = 0.7853982F;
        tabli11.renderWithRotation(par7);

        tabli12.rotateAngleX = 0F;
        tabli12.rotateAngleY = 0F;
        tabli12.rotateAngleZ = -1.889617E-08F;
        tabli12.renderWithRotation(par7);

        tabli13.rotateAngleX = 0F;
        tabli13.rotateAngleY = 0F;
        tabli13.rotateAngleZ = 1.570796F;
        tabli13.renderWithRotation(par7);

        tabli14.rotateAngleX = 0F;
        tabli14.rotateAngleY = 0F;
        tabli14.rotateAngleZ = 0F;
        tabli14.renderWithRotation(par7);

        tabli15.rotateAngleX = 0F;
        tabli15.rotateAngleY = 0F;
        tabli15.rotateAngleZ = 0F;
        tabli15.renderWithRotation(par7);

        tabli16.rotateAngleX = 0F;
        tabli16.rotateAngleY = 0F;
        tabli16.rotateAngleZ = 0F;
        tabli16.renderWithRotation(par7);

        tabli17.rotateAngleX = 0F;
        tabli17.rotateAngleY = 0F;
        tabli17.rotateAngleZ = 0F;
        tabli17.renderWithRotation(par7);

        Block2.rotateAngleX = 0F;
        Block2.rotateAngleY = 0F;
        Block2.rotateAngleZ = -3.371748E-07F;
        Block2.renderWithRotation(par7);

        Block21.rotateAngleX = 3.604254E-07F;
        Block21.rotateAngleY = 1.570796F;
        Block21.rotateAngleZ = -1.411519E-13F;
        Block21.renderWithRotation(par7);

        Block22.rotateAngleX = 0F;
        Block22.rotateAngleY = 0F;
        Block22.rotateAngleZ = -6.554533E-07F;
        Block22.renderWithRotation(par7);

        Block23.rotateAngleX = 0F;
        Block23.rotateAngleY = 0F;
        Block23.rotateAngleZ = -6.554533E-07F;
        Block23.renderWithRotation(par7);

        Block24.rotateAngleX = -4.258229E-07F;
        Block24.rotateAngleY = -1.570796F;
        Block24.rotateAngleZ = -2.282656E-13F;
        Block24.renderWithRotation(par7);

        Block25.rotateAngleX = 3.604254E-07F;
        Block25.rotateAngleY = 1.570796F;
        Block25.rotateAngleZ = -1.411519E-13F;
        Block25.renderWithRotation(par7);

        Block26.rotateAngleX = -2.761318E-07F;
        Block26.rotateAngleY = -1.570796F;
        Block26.rotateAngleZ = -1.168925E-13F;
        Block26.renderWithRotation(par7);

        Block27.rotateAngleX = 0F;
        Block27.rotateAngleY = 0F;
        Block27.rotateAngleZ = -6.554533E-07F;
        Block27.renderWithRotation(par7);

        Block28.rotateAngleX = 0F;
        Block28.rotateAngleY = 0F;
        Block28.rotateAngleZ = -6.554533E-07F;
        Block28.renderWithRotation(par7);

        Block29.rotateAngleX = 0F;
        Block29.rotateAngleY = 0F;
        Block29.rotateAngleZ = -6.554533E-07F;
        Block29.renderWithRotation(par7);

        Block210.rotateAngleX = -2.761318E-07F;
        Block210.rotateAngleY = -1.570796F;
        Block210.rotateAngleZ = -1.168925E-13F;
        Block210.renderWithRotation(par7);

        Block211.rotateAngleX = 0F;
        Block211.rotateAngleY = 0F;
        Block211.rotateAngleZ = -6.554533E-07F;
        Block211.renderWithRotation(par7);

        Block212.rotateAngleX = 3.604254E-07F;
        Block212.rotateAngleY = 1.570796F;
        Block212.rotateAngleZ = -1.411519E-13F;
        Block212.renderWithRotation(par7);

        Block213.rotateAngleX = 0F;
        Block213.rotateAngleY = 0F;
        Block213.rotateAngleZ = -3.371748E-07F;
        Block213.renderWithRotation(par7);

    }

}
