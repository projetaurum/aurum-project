package aurum.steampunk.render;

import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.steampunk.tileentity.TileEntityMovingCarpet;

public class TileEntityMovingCarpetSpecialRenderer
    extends TileEntitySpecialRenderer
{
    RenderItem item;
    float itemPos = -0.41f;

    @Override
    public void renderTileEntityAt(TileEntity tileentity, double d0, double d1,
                                   double d2, float f) {
        TileEntityMovingCarpet tile = (TileEntityMovingCarpet) tileentity;
        GL11.glPushMatrix();
        GL11.glTranslatef((float)d0 + 0.5f, (float)d1 + 1.5f, (float)d2 + 0.5f);
        GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
        bindTexture(new ResourceLocation("aurumsteampunk","textures/blocks/movingcarpet.png"));
        new ModelMovingCarpet().render(tileentity, 0.0625F);
        GL11.glPushMatrix();
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        item = new RenderItem() {
            public byte getMiniBlockCountForItemStack(ItemStack stack) {
                return 1;
            }
            
            public byte getMiniItemCountForItemStack(ItemStack stack) {
                return 1;
            }
            
            @Override
            public boolean shouldBob() {
                return false;
            }
            
            @Override
            public boolean shouldSpreadItems() {
                return false;
            }
        };
        item.setRenderManager(RenderManager.instance);
        EntityItem customitem = new EntityItem(tile.worldObj);
        customitem.hoverStart = 0f;

        for (int i = 0; i < tile.getItemCount(); i++) {
            if (tile.getItem(i) != null && tile.isLaunched(i)) {
                customitem.setEntityItemStack(tile.getItem(i));
                item.doRenderItem(customitem, tile.getItemX(i), tile.getItemZ(i), 0.60, 50.156F, 0);
                /*if (tile.getItemZ(i) >= 0.2f) {
                    tile.transmitItem(i);
                } else {
                    tile.setItemZ(i, tile.getItemZ(i) + (0.016f / tile.getSpeed()));
                }*/
            }
        }

        GL11.glPopMatrix();
        GL11.glPopMatrix();
    }
}