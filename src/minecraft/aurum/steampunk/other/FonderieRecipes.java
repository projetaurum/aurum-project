package aurum.steampunk.other;

import java.util.ArrayList;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import aurum.steampunk.AurumSteamPunk;
import aurum.stockage.AurumStockage;

public class FonderieRecipes
{
    private static final FonderieRecipes smeltingBase = new FonderieRecipes();

    public final ArrayList<ItemStack> smeltingMaterial1 = new ArrayList<ItemStack>();
    public final ArrayList<ItemStack> smeltingMaterial2 = new ArrayList<ItemStack>();
    public final ArrayList<ItemStack> smeltingTemplate  = new ArrayList<ItemStack>();
    public final ArrayList<ItemStack> smeltingResults   = new ArrayList<ItemStack>();
    public final ArrayList<Integer> smeltingTemps       = new ArrayList<Integer>();
    public final ArrayList<Integer> smeltingTime        = new ArrayList<Integer>(); 

    public static final FonderieRecipes smelting()
    {
        return smeltingBase;
    }

    private FonderieRecipes()
    {
        this.addSmelting(new ItemStack(Item.ingotIron), null, new ItemStack(AurumSteamPunk.itemTemplateKey), new ItemStack(AurumStockage.itemIronKey, 4, 0), 300, 700);
        this.addSmelting(new ItemStack(Item.ingotGold), null, new ItemStack(AurumSteamPunk.itemTemplateKey), new ItemStack(AurumStockage.itemIronKey, 4, 0), 300, 1200);
    }

    /**
     * Adds a smelting recipe.
     */
    public void addSmelting(ItemStack material1, ItemStack material2, ItemStack template, ItemStack resultat, int temperature, int time)
    {
        this.smeltingMaterial1.add(material1);
        this.smeltingMaterial2.add(material2);
        this.smeltingTemplate.add(template);
        this.smeltingResults.add(resultat);
        this.smeltingTemps.add(temperature * 100);
        this.smeltingTime.add(time);
    }

    public ItemStack getSmeltingResult(ItemStack item1, ItemStack item2, ItemStack template, int heat)
    {
    	int i = 0;
    	if( (item1 != null || item2 != null) && template != null) {
    		for(ItemStack temp : this.smeltingTemplate){
    			if(temp.itemID ==  template.itemID){
    				ItemStack mat1   = this.smeltingMaterial1.get(i);
    				ItemStack mat2   = this.smeltingMaterial2.get(i);
    				ItemStack result = this.smeltingResults.get(i);
    				int temperature  = this.smeltingTemps.get(i);
    				
    				if(isValide(item1, item2, this.smeltingMaterial1.get(i), this.smeltingMaterial2.get(i))){
    					if(heat >= temperature && heat <= (temperature + 5000)) return result;
    				}
    			}
    			i++;
    		}
    	}
        return null;
    }
    
    private boolean isValide(ItemStack item1, ItemStack item2, ItemStack material1, ItemStack material2){
    	ArrayList<Integer> material  = new ArrayList<Integer>();
    	if(material1 != null) material.add(material1.itemID);
    	if(material2 != null) material.add(material2.itemID);
    	
    	
    	ArrayList<Integer> itemstack = new ArrayList<Integer>();
    	if(item1 != null) itemstack.add(item1.itemID);
    	if(item2 != null) itemstack.add(item2.itemID);

    	if(itemstack.size() == material.size()){
    		return itemstack.containsAll(material);
    	}
    	return false;
    }

	public int getSmeltingTime(ItemStack itemStack) {
		return this.smeltingTime.get(this.smeltingResults.indexOf(itemStack));
	}
}