package aurum.steampunk.enaria;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntitySynapsePortal extends Entity
{
	private byte orientation;
	private UUID attachedDoor;
	private int lifeTime = 0;
	
	public EntitySynapsePortal(World par1World) 
	{
		super(par1World);
	}
	
	public EntitySynapsePortal(World world, byte orientation, UUID attachedDoor, int lifeTime)
	{
		super(world);
		this.orientation = orientation;
		this.attachedDoor = attachedDoor;
		this.lifeTime = lifeTime;
	}
	
	public byte getOrientation()
	{
		return this.orientation;
	}
	
	public void setOrientation(byte orien)
	{
		this.orientation = orien;
	}
	
	public UUID getAttachedDoor()
	{
		return this.attachedDoor;
	}
	
	public void setAttachedDoor(UUID uuid)
	{
		this.attachedDoor = uuid;
	}

	@Override
	protected void entityInit() { }

	@Override
	protected void readEntityFromNBT(NBTTagCompound tag) 
	{
		this.orientation = tag.getByte("Orientation");
		this.lifeTime = tag.getInteger("LifeTime");
		
        if (tag.hasKey("DoorMost") && tag.hasKey("DoorLeast"))
        {
            this.attachedDoor = new UUID(tag.getLong("DoorMost"), tag.getLong("DoorLeast"));
        }
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound tag) 
	{
		tag.setByte("Orientation", orientation);
        tag.setLong("DoorMost", attachedDoor.getMostSignificantBits());
        tag.setLong("DoorLeast", attachedDoor.getLeastSignificantBits());
		tag.setInteger("LifeTime", lifeTime);
	}
	
	@Override
	public void onEntityUpdate()
	{
		super.onEntityUpdate();
		
		if(this.ticksExisted >= this.lifeTime)
			this.setDead();
		
		List<?> list = this.worldObj.getEntitiesWithinAABB(Entity.class, this.boundingBox.expand(1.0D, 1.0D, 1.0D));
        Iterator<?> iterator = list.iterator();

        while (iterator.hasNext())
        {
        	Entity e = (Entity) iterator.next();
        	
        	if(e != null)
        	{
        		if(!(e instanceof EntitySynapsePortal))
            		e.setPosition(this.posX, this.posY+30, this.posZ+20);
        	}
        }
	}
}