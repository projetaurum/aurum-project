package aurum.steampunk.enaria;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderDrone extends RenderLiving
{
	public static final ResourceLocation texture = new ResourceLocation("aurumsteampunk:textures/blocks/block_cuivre.png");
	
	public RenderDrone(ModelBase par1ModelBase, float par2) 
	{
		super(par1ModelBase, par2);
	}

	protected ResourceLocation getDroneTextures(EntityDrone drone)
    {
        return texture;
    }

    protected ResourceLocation getEntityTexture(Entity par1Entity)
    {
        return this.getDroneTextures((EntityDrone)par1Entity);
    }
}