package aurum.steampunk.enaria.bolts;

import java.util.Random;
import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import aurum.steampunk.enaria.EntitySynapsePortal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class EntityTeleportBolt extends EntityThrowable
{
	public EntityTeleportBolt(World par1World) 
	{
		super(par1World);
	}

	public EntityTeleportBolt(World par1World, EntityLivingBase shootingEntity)
	{
		super(par1World, shootingEntity);

		Vec3 direction = shootingEntity.getLookVec().normalize();
		double speed = 1.0;
		this.motionX = direction.xCoord * speed;
		this.motionY = direction.yCoord * speed;
		this.motionZ = direction.zCoord * speed;
		double r = 0.4375;
		double xoffset = 0.1;
		double yoffset = 0;
		double zoffset = 0;
		double horzScale = Math.sqrt(direction.xCoord * direction.xCoord + direction.zCoord * direction.zCoord);
		double horzx = direction.xCoord / horzScale;
		double horzz = direction.zCoord / horzScale;
		this.posX = shootingEntity.posX + direction.xCoord * xoffset - direction.yCoord * horzx * yoffset - horzz * zoffset;
		this.posY = shootingEntity.posY + shootingEntity.getEyeHeight() + direction.yCoord * xoffset + (1 - Math.abs(direction.yCoord)) * yoffset;
		this.posZ = shootingEntity.posZ + direction.zCoord * xoffset - direction.yCoord * horzz * yoffset + horzx * zoffset;
		this.boundingBox.setBounds(posX - r, posY - 0.0625, posZ - r, posX + r, posY + 0.0625, posZ + r);
	}

	public EntityTeleportBolt(World par1World, double par2, double par4, double par6)
	{
		super(par1World, par2, par4, par6);
	}

	@Override
	protected float func_70182_d()
	{
		return 3.5F;
	}

	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		return 0.0F;
	}

	public int getMaxLifetime() 
	{
		return 50;
	}

	public void onUpdate()
	{
		if(new Random().nextInt(3) == 2)
			this.worldObj.spawnParticle("lava", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);

		if (this.ticksExisted > this.getMaxLifetime()) 
		{
        	createPortal();
        	this.setDead();
		}
		
		System.out.println(motionX+" | "+motionY+" | "+motionZ);

		super.onUpdate();
	}

	protected float getGravityVelocity()
	{
		return 0F;
	}

	protected void onImpact(MovingObjectPosition mov)
	{
		if(mov.entityHit != null)
			mov.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 2);
		createPortal();

		this.setDead();
	}
	
	private void createPortal()
	{
		byte orientation = 0;
		
		if(motionX < 0 || motionZ < 0)
		{
			if(motionX > motionZ)
				orientation = 0;
			else
				orientation = 1;
		}
		else
		{
			if(motionX > motionZ)
				orientation = 1;
		}
		
    	EntitySynapsePortal portal = new EntitySynapsePortal(this.worldObj, orientation, UUID.randomUUID(), 800);
    	
    	portal.setPosition(posX, posY, posZ);
    	this.worldObj.spawnEntityInWorld(portal);
	}
}