package aurum.steampunk.enaria.bolts;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderPlasmaBolt extends Render
{
    private float size;
    
	public static final ResourceLocation texture = new ResourceLocation("aurumsteampunk:textures/items/raw_gold.png");

    public RenderPlasmaBolt(float size)
    {
        this.size = size;
    }

    @Override
    public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9)
    {
        this.doRenderLaserBolt((EntityPlasmaBolt)var1, var2, var4, var6, var8, var9);
    }

    public void doRenderLaserBolt(EntityPlasmaBolt plasmabolt, double par2, double par4, double par6, float par8, float par9)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef((float)par2, (float)par4, (float)par6);
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        float var10 = this.size;
        GL11.glScalef(var10 / 1.0F, var10 / 1.0F, var10 / 1.0F);
        byte var11 = 0;
        this.bindTexture(texture);
        Tessellator var12 = Tessellator.instance;
        float var13 = (float)(var11 % 16 * 16 + 0) / 16.0F;
        float var14 = (float)(var11 % 16 * 16 + 16) / 16.0F;
        float var15 = (float)(var11 / 16 * 16 + 0) / 16.0F;
        float var16 = (float)(var11 / 16 * 16 + 16) / 16.0F;
        float var17 = 1.0F;
        float var18 = 0.5F;
        float var19 = 0.25F;
        GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
        GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
        var12.startDrawingQuads();
        var12.setNormal(0.0F, 1.0F, 0.0F);
        var12.addVertexWithUV((double)(0.0F - var18), (double)(0.0F - var19), 0.0D, (double)var13, (double)var16);
        var12.addVertexWithUV((double)(var17 - var18), (double)(0.0F - var19), 0.0D, (double)var14, (double)var16);
        var12.addVertexWithUV((double)(var17 - var18), (double)(1.0F - var19), 0.0D, (double)var14, (double)var15);
        var12.addVertexWithUV((double)(0.0F - var18), (double)(1.0F - var19), 0.0D, (double)var13, (double)var15);
        var12.draw();
        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        GL11.glPopMatrix();
    }

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) 
	{
		return texture;
	}
}