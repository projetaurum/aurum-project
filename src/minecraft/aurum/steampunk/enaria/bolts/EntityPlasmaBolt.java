package aurum.steampunk.enaria.bolts;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class EntityPlasmaBolt extends EntityThrowable
{
	public EntityPlasmaBolt(World par1World) 
	{
		super(par1World);
	}

	public EntityPlasmaBolt(World par1World, EntityLiving shootingEntity)
	{
		super(par1World, shootingEntity);

		Vec3 direction = shootingEntity.getLookVec().normalize();
		double speed = 1.0;
		this.motionX = direction.xCoord * speed;
		this.motionY = direction.yCoord * speed;
		this.motionZ = direction.zCoord * speed;
		double r = 0.4375;
		double xoffset = 0.1;
		double yoffset = 0;
		double zoffset = 0;
		double horzScale = Math.sqrt(direction.xCoord * direction.xCoord + direction.zCoord * direction.zCoord);
		double horzx = direction.xCoord / horzScale;
		double horzz = direction.zCoord / horzScale;
		this.posX = shootingEntity.posX + direction.xCoord * xoffset - direction.yCoord * horzx * yoffset - horzz * zoffset;
		this.posY = shootingEntity.posY + shootingEntity.getEyeHeight() + direction.yCoord * xoffset + (1 - Math.abs(direction.yCoord)) * yoffset;
		this.posZ = shootingEntity.posZ + direction.zCoord * xoffset - direction.yCoord * horzz * yoffset + horzx * zoffset;
		this.boundingBox.setBounds(posX - r, posY - 0.0625, posZ - r, posX + r, posY + 0.0625, posZ + r);
	}

	public EntityPlasmaBolt(World par1World, double par2, double par4, double par6)
	{
		super(par1World, par2, par4, par6);
	}

	@Override
	protected float func_70182_d()
	{
		return 1F;
	}

	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		return 0.0F;
	}

	public int getMaxLifetime() 
	{
		return 400;
	}

	public void onUpdate()
	{
		if(new Random().nextInt(3) == 2)
			this.worldObj.spawnParticle("lava", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);

		if (this.ticksExisted > this.getMaxLifetime()) 
		{
			this.setDead();
		}

		super.onUpdate();
	}

	protected float getGravityVelocity()
	{
		return 0F;
	}

	protected void onImpact(MovingObjectPosition mov)
	{
		if(mov.entityHit != null)
		{
            mov.entityHit.setFire(5);
			mov.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 8);
		}
		this.worldObj.newExplosion((Entity)null, this.posX, this.posY, this.posZ, (float)2, true, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
		this.setDead();
	}
}