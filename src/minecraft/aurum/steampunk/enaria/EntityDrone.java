package aurum.steampunk.enaria;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import aurum.steampunk.enaria.bolts.EntityLaserBolt;
import aurum.steampunk.enaria.bolts.EntityPlasmaBolt;
import aurum.steampunk.enaria.bolts.EntityTeleportBolt;

public class EntityDrone extends EntityMob
{
    private int field_70846_g;
	
	public EntityDrone(World par1World) 
	{
		super(par1World);

        /*this.tasks.addTask(2, new EntityAIWander(this, 1.0D));
        this.tasks.addTask(3, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(4, new EntityAILookIdle(this));*/
        this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityLiving.class, 0, true, false, IMob.mobSelector));
		
		this.isImmuneToFire = true;
	}
	
    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(16, new Byte((byte)0));
    }
	
	public void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(40D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setAttribute(80.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(1.5D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setAttribute(6.0D);
    }
	
	@Override
    protected boolean canDespawn()
    {
        return false;
    }
	
   @Override
   protected Entity findPlayerToAttack()
   {
       EntityPlayer entityplayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 48.0D);
       return entityplayer != null && this.canEntityBeSeen(entityplayer) ? entityplayer : null;
   }
   
   @Override
   public void setTarget(Entity par1Entity)
   {
	   if(par1Entity instanceof EntityMob || par1Entity instanceof EntityPlayer)
		   this.entityToAttack = par1Entity;
   }

    /**
     * Basic mob attack. Default to touch of death in EntityCreature. Overridden by each mob to define their attack.
     */
	@Override
	public void attackEntity(Entity par1Entity, float par2)
    {
    	if(par1Entity instanceof EntityPlayer)
    	{
            if (this.attackTime <= 0 && par2 < 2.0F && par1Entity.boundingBox.maxY > this.boundingBox.minY && par1Entity.boundingBox.minY < this.boundingBox.maxY)
            {
                this.attackTime = 20;
                this.attackEntityAsMob(par1Entity);
            }
            else if (par2 < 30.0F)
            {
                double d0 = par1Entity.posX - this.posX;
                double d2 = par1Entity.posZ - this.posZ;

                if (this.attackTime == 0)
                {
                    ++this.field_70846_g;

                    if (this.field_70846_g == 1)
                    {
                        this.attackTime = 60;
                    }
                    else if (this.field_70846_g <= 40)
                    {
                        this.attackTime = 6;
                    }
                    else
                    {
                        this.attackTime = 100;
                        this.field_70846_g = 0;
                    }

                    if (this.field_70846_g > 1)
                    {
                        this.worldObj.playAuxSFXAtEntity((EntityPlayer)null, 1009, (int)this.posX, (int)this.posY, (int)this.posZ, 0);

                        for (int i = 0; i < 1; ++i)
                        {
                            EntityLaserBolt entitybolt = new EntityLaserBolt(this.worldObj, this);
                            this.worldObj.spawnEntityInWorld(entitybolt);
                            
                            if(new Random().nextInt(4) == 2)
                            {
                            	EntityPlasmaBolt entitypbolt = new EntityPlasmaBolt(this.worldObj, this);
                            	this.worldObj.spawnEntityInWorld(entitypbolt);
                            }
                            
                            if(new Random().nextInt(10) == 2)
                            {
                            	System.out.println("Teleport");
                            	EntityTeleportBolt teleport = new EntityTeleportBolt(this.worldObj, this);
                            	this.worldObj.spawnEntityInWorld(teleport);
                            }
                        }
                    }
                }

                this.rotationYaw = (float)(Math.atan2(d2, d0) * 180.0D / Math.PI) - 90.0F;
                this.hasAttacked = true;
            }
    	}
    }
	
	@Override
    public boolean attackEntityFrom(DamageSource source, float par2)
    {
		if(source.getEntity() != null)
		{
			if(source.getEntity() instanceof EntityDrone)
				return false;
		}
        return super.attackEntityFrom(source, par2);
    }
}