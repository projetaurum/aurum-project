package aurum.steampunk.enaria;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderSynapsePortal extends Render
{
	public static final ResourceLocation texture = new ResourceLocation("aurumsteampunk:textures/items/raw_copper.png");

    public RenderSynapsePortal()
    {
    }

    @Override
    public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9)
    {
        this.doRenderSynapsePortal((EntitySynapsePortal)var1, var2, var4, var6, var8, var9);
    }

    public void doRenderSynapsePortal(EntitySynapsePortal portal, double x, double y, double z, float par8, float par9)
    {
		GL11.glPushMatrix();
		GL11.glTranslatef((float)x, (float)y, (float)z);
		GL11.glDisable(GL11.GL_LIGHTING);
		
		if(portal.getOrientation() == 1)
			GL11.glRotatef(90f, 0f, 1f, 0f);
		
        Tessellator t = Tessellator.instance;
		t.startDrawingQuads();
		bindTexture( new ResourceLocation("aurumsteampunk","textures/blocks/coal_block.png"));
		t.addVertexWithUV(-1.5, -1.5, -1.5, 0, 0);
		t.addVertexWithUV(-1.5, 1.5, -1.5, 0, 1);
		t.addVertexWithUV(1.5, 1.5, -1.5, 1, 1);
		t.addVertexWithUV(1.5, -1.5, -1.5, 1, 0);

		t.addVertexWithUV(-1.5, -1.5, -1.5, 0, 0);
		t.addVertexWithUV(1.5, -1.5, -1.5, 0, 1);
		t.addVertexWithUV(1.5, 1.5, -1.5, 1, 1);
		t.addVertexWithUV(-1.5, 1.5, -1.5, 1, 0);
		t.draw();
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();
    }

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) 
	{
		return texture;
	}
}