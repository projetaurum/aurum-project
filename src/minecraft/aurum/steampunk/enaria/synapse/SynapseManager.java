package aurum.steampunk.enaria.synapse;

import java.util.ArrayList;

public class SynapseManager
{
	//A Synapse node transmit synapses datas, revert virtual-material energy between different spaces
	public static final ArrayList<SynapseNode> nodes = new ArrayList<SynapseNode>();
	
	//A Synapse port is an access to the synapse network by a human, a drone, an other thing
	public static final ArrayList<SynapsePort> ports = new ArrayList<SynapsePort>();
	
	public static final void tickSynapse()
	{
		
	}
}