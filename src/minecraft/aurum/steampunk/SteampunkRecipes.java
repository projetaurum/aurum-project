package aurum.steampunk;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import cpw.mods.fml.common.registry.GameRegistry;

public class SteampunkRecipes
{
    public static final void registerRecipes()
    {
        //Furnace recipes
        FurnaceRecipes.smelting().addSmelting(AurumSteamPunk.itemRawOre.itemID, 0, new ItemStack(AurumSteamPunk.itemIngot, 1, 0), 0.0F);
        FurnaceRecipes.smelting().addSmelting(AurumSteamPunk.itemRawOre.itemID, 1, new ItemStack(AurumSteamPunk.itemIngot, 1, 1), 0.0F);
        //Crafting Table
        GameRegistry.addRecipe(new ItemStack(AurumSteamPunk.blockOreSteamPunk, 1, 1),
                               new Object[] {"XXX", "XXX", "XXX", 'X', new ItemStack(AurumSteamPunk.itemIngot, 1, 2)});
        GameRegistry.addRecipe(new ItemStack(AurumSteamPunk.itemIngot, 9, 2),
                               new Object[] {"X", 'X', new ItemStack(AurumSteamPunk.blockOreSteamPunk, 1, 1)});
    }
}