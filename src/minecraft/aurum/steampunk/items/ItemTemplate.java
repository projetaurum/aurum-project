package aurum.steampunk.items;

import aurum.core.AurumCore;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.Icon;

public class ItemTemplate extends Item
{
    String texture;

    public ItemTemplate(int par1, String texture)
    {
        super(par1);
        this.setMaxStackSize(1);
        this.texture = texture;
        this.setMaxDamage(64);
        this.setCreativeTab(AurumCore.aurumSteampunkTab);
    }

    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        this.itemIcon = iconRegister.registerIcon("aurumsteampunk:" + this.texture);
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return itemIcon;
    }
}