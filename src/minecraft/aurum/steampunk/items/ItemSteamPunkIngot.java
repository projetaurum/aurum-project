package aurum.steampunk.items;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemSteamPunkIngot extends Item
{
    Icon iconBuffer[];

    public ItemSteamPunkIngot(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubItems(int par1, CreativeTabs par2, List par3)
    {
        par3.add(new ItemStack(par1, 1, 0));
        par3.add(new ItemStack(par1, 1, 1));
        par3.add(new ItemStack(par1, 1, 2));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[3];
        iconBuffer[0] = register.registerIcon("aurumsteampunk:ingot_copper");
        iconBuffer[1] = register.registerIcon("aurumsteampunk:ingot_mithril");
        iconBuffer[2] = register.registerIcon("aurumsteampunk:ingot_bronze");
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "Copper Ingot";
                break;

            case 1:
                name = "Mithril Ingot";
                break;

            case 2:
                name = "Bronze Ingot";
                break;
        }

        return name;
    }
}
