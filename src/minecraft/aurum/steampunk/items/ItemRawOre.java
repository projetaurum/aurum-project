package aurum.steampunk.items;

import java.util.List;

import aurum.steampunk.enaria.bolts.EntityTeleportBolt;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class ItemRawOre extends Item
{
    Icon iconBuffer[];

    public ItemRawOre(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
    }
    
    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
    {
    	EntityTeleportBolt teleport = new EntityTeleportBolt(world, player);
    	world.spawnEntityInWorld(teleport);
    	return stack;
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubItems(int par1, CreativeTabs par2, List par3)
    {
        par3.add(new ItemStack(par1, 1, 0));
        par3.add(new ItemStack(par1, 1, 1));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[2];
        iconBuffer[0] = register.registerIcon("aurumsteampunk:raw_copper");
        iconBuffer[1] = register.registerIcon("aurumsteampunk:raw_mithril");
    }
    				
    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name = "Raw Copper";
                break;

            case 1:
                name = "Raw Mithril";
                break;
        }

        return name;
    }
}
