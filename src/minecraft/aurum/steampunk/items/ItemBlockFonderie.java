package aurum.steampunk.items;

import net.minecraft.item.ItemBlock;

public class ItemBlockFonderie extends ItemBlock
{
    public ItemBlockFonderie(int par1)
    {
        super(par1);
        setHasSubtypes(true);
        this.setUnlocalizedName("Fonderie");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }
}