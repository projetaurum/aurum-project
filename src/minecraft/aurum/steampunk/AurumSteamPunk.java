package aurum.steampunk;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;
import aurum.steampunk.blocks.BlockCoal;
import aurum.steampunk.blocks.BlockCopperOre;
import aurum.steampunk.blocks.BlockFonderie;
import aurum.steampunk.blocks.BlockMithrilOre;
import aurum.steampunk.blocks.BlockMovingCarpet;
import aurum.steampunk.blocks.BlockSteamPump;
import aurum.steampunk.blocks.BlockSteampunkOre;
import aurum.steampunk.blocks.SteampunkItemBlockOre;
import aurum.steampunk.enaria.EntityDrone;
import aurum.steampunk.enaria.EntitySynapsePortal;
import aurum.steampunk.enaria.bolts.EntityLaserBolt;
import aurum.steampunk.enaria.bolts.EntityPlasmaBolt;
import aurum.steampunk.enaria.bolts.EntityTeleportBolt;
import aurum.steampunk.items.ItemBlockFonderie;
import aurum.steampunk.items.ItemBlockSteamPump;
import aurum.steampunk.items.ItemRawOre;
import aurum.steampunk.items.ItemSteamPunkIngot;
import aurum.steampunk.items.ItemTemplate;
import aurum.steampunk.tileentity.TileEntityFonderie;
import aurum.steampunk.tileentity.TileEntityMovingCarpet;
import aurum.steampunk.tileentity.TileEntitySteamPump;

import com.google.common.primitives.Ints;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.IFuelHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = AurumSteamPunk.ID, version = AurumSteamPunk.VERSION, name = "AurumSteamPunk", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumSteamPunk implements IFuelHandler
{
    @Instance("AurumSteamPunk")
    public static AurumSteamPunk steamPunkInstance;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumSteamPunkMod";
    
	//Gui Id
	private static int guiid   			  = 0;
	public static final int GUI_FONDERIE  = guiid++;
	public static final int GUI_STEAMPUMP = guiid++;
	public static final int GUI_SYNAPSE = guiid++;
	
    public static final CreativeTabs steamPunkTab = AurumCore.aurumSteampunkTab;

    public static Logger logger;

    public static MinecraftServer server;

    @SidedProxy(clientSide = "aurum.steampunk.ClientProxy", serverSide = "aurum.steampunk.CommonProxy")
    public static CommonProxy proxy;

    public static final Block blockCopperOre = new BlockCopperOre(1750)
    .setUnlocalizedName("Copper Ore")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockMithrilOre = new BlockMithrilOre(1751)
    .setUnlocalizedName("Mithril Ore")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockCoal = new BlockCoal(1752, Material.rock)
    .setUnlocalizedName("CoalBlock")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundStoneFootstep);
    
    public static final Block blockOreSteamPunk = new BlockSteampunkOre(1753)
    .setUnlocalizedName("SteampunkOre")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundMetalFootstep);

    public static final Block blockMovingCarpet = new BlockMovingCarpet(1754, Material.iron)
    .setUnlocalizedName("MovingCarpet")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundMetalFootstep);
    
    public static final Block blockFonderie = new BlockFonderie(1755)
    .setUnlocalizedName("Fonderie")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundMetalFootstep);
    
    public static final Block blockSteamPump = new BlockSteamPump(1756, Material.rock)
    .setUnlocalizedName("Steam Pump")
    .setCreativeTab(steamPunkTab)
    .setStepSound(Block.soundStoneFootstep);

    public static final Item itemRawOre = new ItemRawOre(1900)
    .setCreativeTab(steamPunkTab)
    .setUnlocalizedName("Raw Ore");

    public static final Item itemIngot = new ItemSteamPunkIngot(1901)
    .setCreativeTab(steamPunkTab)
    .setUnlocalizedName("AurumIngot");
    
    public static final Item itemTemplate = new ItemTemplate(10500, "template")
    .setUnlocalizedName("Template");

    public static final Item itemTemplateKey = new ItemTemplate(10501, "template_key")
    .setUnlocalizedName("Key Template");

    public static final Item itemTemplateCadenas = new ItemTemplate(10502, "template_cadenas")
    .setUnlocalizedName("Cadenas Template");

    public static final Item itemTemplateIngot = new ItemTemplate(10503, "template_ingot")
    .setUnlocalizedName("Ingot Template");
    
    //LIST OF ALL FUEL
    private static int[] fuel = new int[] { Item.coal.itemID };

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        steamPunkInstance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        //Blocks
        AurumRegistry.registerBlock(blockCoal, "Coal Block");
        AurumRegistry.registerBlock(blockCopperOre, "Copper Ore");
        AurumRegistry.registerBlock(blockMithrilOre, "Mithril Ore");
        //AurumRegistry.registerBlock(blockMovingCarpet, "Moving Carpet");
        
        //ItemBlocks & Blocks
        AurumRegistry.registerBlock(blockOreSteamPunk, SteampunkItemBlockOre.class, "Steampunk Ore");
        AurumRegistry.registerBlock(blockFonderie, ItemBlockFonderie.class, "Fonderie");
        AurumRegistry.registerBlock(blockSteamPump, ItemBlockSteamPump.class, "Steam Pump");
        
        //Adding TileEntities
        GameRegistry.registerTileEntity(TileEntityMovingCarpet.class, "TileEntityMovingCarpet");
        GameRegistry.registerTileEntity(TileEntityFonderie.class, "TileEntityFonderie");
        GameRegistry.registerTileEntity(TileEntitySteamPump.class, "TileEntitySteamPump");
        
        //Adding Items
        AurumRegistry.registerItem(itemRawOre, "Raw Ore");
        AurumRegistry.registerItem(itemIngot, "Ingot");
        AurumRegistry.registerItem(itemTemplate, "Template");
        AurumRegistry.registerItem(itemTemplateCadenas, "Lock Template");
        AurumRegistry.registerItem(itemTemplateKey, "Key Template");
        AurumRegistry.registerItem(itemTemplateIngot, "Ingot Template");
        
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabSteampunk", "en_US", "Aurum SteamPunk");
        //Other
        
        GameRegistry.registerFuelHandler(this);
        //Our GuiHandler Register
        
        NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
        //Recipes
        
        SteampunkRecipes.registerRecipes();
        
        EntityRegistry.registerGlobalEntityID(EntityDrone.class, "EnarianDrone", EntityRegistry.findGlobalUniqueEntityId(), 24, 30);
        EntityRegistry.registerModEntity(EntityDrone.class, "EnarianDrone", 250, this, 40, 1, true);
        LanguageRegistry.instance().addStringLocalization("entity.EnarianDrone.name", "Enarian Drone");
        
        EntityRegistry.registerGlobalEntityID(EntityLaserBolt.class, "Laser Bolt", EntityRegistry.findGlobalUniqueEntityId());
        EntityRegistry.registerModEntity(EntityLaserBolt.class, "Laser Bolt", 251, this, 64, 1, true);
        
        EntityRegistry.registerGlobalEntityID(EntityPlasmaBolt.class, "Plasma Bolt", EntityRegistry.findGlobalUniqueEntityId());
        EntityRegistry.registerModEntity(EntityPlasmaBolt.class, "Plasma Bolt", 252, this, 64, 1, true);
        
        EntityRegistry.registerGlobalEntityID(EntityTeleportBolt.class, "Teleport Bolt", EntityRegistry.findGlobalUniqueEntityId());
        EntityRegistry.registerModEntity(EntityTeleportBolt.class, "Teleport Bolt", 253, this, 64, 1, true);
        
        EntityRegistry.registerGlobalEntityID(EntitySynapsePortal.class, "Synapse Portal", EntityRegistry.findGlobalUniqueEntityId());
        EntityRegistry.registerModEntity(EntitySynapsePortal.class, "Synapse Portal", 300, this, 64, 1, true);
    }
    
    @EventHandler
    public void onServerStarting(FMLServerStartingEvent event)
    {
    	TickRegistry.registerTickHandler(new SteampunkTickHandler(), Side.SERVER);
    }
    
    @Override
    public int getBurnTime(ItemStack fuel) {
        int itemID = fuel.itemID;
        if (itemID == AurumSteamPunk.blockCoal.blockID)
            return 14400;
        return 0;
    }
    
    //Return item is fuel
    public static boolean isFuel(int id){
 	   return Ints.contains(fuel, id);
    }
}