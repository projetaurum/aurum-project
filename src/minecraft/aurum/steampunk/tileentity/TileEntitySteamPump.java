package aurum.steampunk.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntitySteamPump extends TileEntity implements IInventory{

	//Magical Variable
	public static int SIZE = 1;
	public static int SLOT_FUEL = 0;
	
	//Inventory
	public ItemStack inventory[];
	
	//Water Level
	public int water_maximal = 5;
	public int water_current = 0;
	
	//Steam Level
	public int steam_maximal = 1500;
	public int steam_current = 0;
	
	//Burn time
	public int burn_time = 0;
	public int burn_item = 0;
	
	//Time between update
	private int lastUpdate = 40;
	
	//Liquid Id
	public int liquidId = 0;
	
	public TileEntitySteamPump(){
		inventory = new ItemStack[SIZE];
	}
	
	@Override
	public int getSizeInventory() {
		return inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return inventory[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
		 ItemStack stack = getStackInSlot(i);
         if (stack != null) {
                 if (stack.stackSize <= j) {
                         setInventorySlotContents(i, null);
                 } else {
                         stack = stack.splitStack(j);
                         if (stack.stackSize == 0) {
                                 setInventorySlotContents(i, null);
                         }
                 }
         }
         return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
        ItemStack stack = getStackInSlot(i);
        if(stack != null) setInventorySlotContents(i, null);
        return stack;
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
        inventory[i] = itemstack;
        if (itemstack != null && itemstack.stackSize > getInventoryStackLimit()) {
        	itemstack.stackSize = getInventoryStackLimit();
        }
	}

	@Override
	public String getInvName() {
		return "Steam Pump";
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) == this && entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
	}

	@Override
	public void openChest() {}

	@Override
	public void closeChest() {}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
    	if(i < this.getSizeInventory() && (this.getStackInSlot(i) == null || this.getStackInSlot(i).getItem() == itemstack.getItem())){
			return true;
		}
		return false;
	}
	
	public boolean isBurning(){
		return this.burn_time > 0;
	}

	public int getBurnLevel(int i) {
        if (this.burn_item == 0)
            this.burn_item = 400;

        return this.burn_time * i / this.burn_item;
	}

	public int getWaterLevel(int i) {
        return this.water_current * i / this.water_maximal;
	}

	public int getSteamLevel(int i) {
        return this.steam_current * i / this.steam_maximal;
	}
	
	/*// NBT EDIT //*/
    @Override
    public void readFromNBT(NBTTagCompound tagCompound) {
        super.readFromNBT(tagCompound);
        
        NBTTagList tagList = tagCompound.getTagList("Inventory");
        for (int i = 0; i < tagList.tagCount(); ++i) {
            NBTTagCompound tag = (NBTTagCompound)tagList.tagAt(i);
            byte b0 = tag.getByte("Slot");

            if (b0 >= 0 && b0 < this.inventory.length) {
                this.inventory[b0] = ItemStack.loadItemStackFromNBT(tag);
            }
        }

        this.water_current = tagCompound.getShort("Water");
        this.steam_current = tagCompound.getInteger("Steam");
        this.burn_item     = tagCompound.getShort("BurnItem");
        this.burn_time     = tagCompound.getShort("BurnTime");
    }
    
    @Override
    public void writeToNBT(NBTTagCompound tagCompound) {
        super.writeToNBT(tagCompound);
        
        NBTTagList itemList = new NBTTagList();
        for (int i = 0; i < this.inventory.length; ++i) {
        	ItemStack item = this.inventory[i];
            if (item != null) {
                NBTTagCompound tag = new NBTTagCompound();
                tag.setByte("Slot", (byte)i);
                item.writeToNBT(tag);
                itemList.appendTag(tag);
            }
        }

        tagCompound.setTag("Inventory", itemList);
        tagCompound.setShort("Water", (short)this.water_current);
        tagCompound.setInteger("Steam", (short)this.steam_current);
        tagCompound.setShort("BurnItem", (short)this.burn_item);
        tagCompound.setShort("BurnTime", (short)this.burn_time);
    }
}
