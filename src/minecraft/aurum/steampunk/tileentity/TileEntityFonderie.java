package aurum.steampunk.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.blocks.BlockFonderie;
import aurum.steampunk.other.FonderieRecipes;

import com.google.common.primitives.Ints;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityFonderie extends TileEntity implements IInventory {

	//Magical Variable
	public static int SIZE 			= 16;
	public static int SLOT_STOCK 	= 0;
	public static int SLOT_MATERIAL	= 8;
	public static int SLOT_FUEL 	= 10;
	public static int SLOT_RESULT 	= 14;
	public static int SLOT_TEMPLATE	= 15;
	
	//ALL ITEM IN TILEENTITY
    ItemStack items[];

    //BURN TIME VARIABLES
    public int fonderieBurnTime 	= 0;
    public int currentItemBurnTime 	= 0;
    
    //COOK TIME VARIABLES
    public int fonderieCookTime 	= 0;
    public int currentItemCookTime 	= 0;
    
    //TEMPERATURE VARIABLES
    public int temperature 		= 0;
    public int temperatureMax 	= 100000;
    
    //CORE BLOCK
    public int[] core = null;
    public boolean isCore = false;

    //PATERN BY HEIGHT LEVEL
    private int[][][] patern = new int[][][] {	{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, 
												{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 
												{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}}};

    public TileEntityFonderie() {
        items = new ItemStack[SIZE];
    }

    @Override
    public int getSizeInventory() {
        return items.length;
    }

    @Override
    public ItemStack getStackInSlot(int i) {
        return items[i];
    }
    
    @Override
    public void setInventorySlotContents(int i, ItemStack itemstack) {
        items[i] = itemstack;
        if (itemstack != null && itemstack.stackSize > getInventoryStackLimit()) {
        	itemstack.stackSize = getInventoryStackLimit();
        } 
    }

    @Override
    public ItemStack decrStackSize(int i, int j) {
		 ItemStack stack = getStackInSlot(i);
         if (stack != null) {
                 if (stack.stackSize <= j) {
                         setInventorySlotContents(i, null);
                 } else {
                         stack = stack.splitStack(j);
                         if (stack.stackSize == 0) {
                                 setInventorySlotContents(i, null);
                         }
                 }
         }
         return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int i) {
        ItemStack stack = getStackInSlot(i);
        if(stack != null) setInventorySlotContents(i, null);
        return stack;
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }
    
	@Override
	public String getInvName() {
		return "Fonderie";
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}
    
    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer) {
        return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) == this && entityplayer.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
    }

    @Override
    public void openChest() {}

    @Override
    public void closeChest() {}

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemstack) { 
    	if(i < this.getSizeInventory() && (this.getStackInSlot(i) == null || this.getStackInSlot(i).getItem() == itemstack.getItem())){
			return true;
		}
		return false;
	}
    
	/*// UPDATE ENTITY //*/
    public void updateEntity() {
        boolean flag  = this.fonderieBurnTime > 0;
        boolean flag1 = false;

        if (this.fonderieBurnTime > 0) {
            --this.fonderieBurnTime;
            if(this.temperature < this.temperatureMax){
            	this.temperature++;
            }
        } else if(this.temperature > 0){
        	this.temperature--;
        }

        if (!this.worldObj.isRemote) {
        	if(this.fonderieBurnTime == 0 && this.asFuel()){
        		this.fonderieBurnTime    = getFuelBurnTime();
        		this.currentItemBurnTime = getFuelBurnTime();
        		
        		if (this.fonderieBurnTime > 0) {
                    flag1 = true;

                    this.useFuel();
                }
        	}
        	
        	ItemStack item = this.getSmelt();
            if (item != null) {
                ++this.fonderieCookTime;

                int time = this.getTimeToMelt(item);
                if(this.currentItemCookTime != time)
                	this.currentItemCookTime = time;
                
                if (this.fonderieCookTime == time) {
                    this.fonderieCookTime = 0;
                    this.smeltItem(item);
                    flag1 = true;
                }
            } else {
                this.fonderieCookTime = 0;
            }

            if (flag != this.fonderieBurnTime > 0) {
                flag1 = true;
                BlockFonderie.updateFonderieBlockState(this.fonderieBurnTime > 0, this.worldObj, this.xCoord, this.yCoord, this.zCoord);
            }
        }

        if (flag1) {
            this.onInventoryChanged();
        }
    }
   
   public boolean asFuel(){
	   return (this.items[10] != null || this.items[11] != null || this.items[12] != null || this.items[13] != null);
   }
   
   public boolean useFuel(){
	   for(int i = 10; i <= 13; i++){
		   if(this.items[i] != null){
			   this.items[i].stackSize--;
			   if(this.items[i].stackSize == 0)
				   this.items[i] = null;
			   return true;
		   }
	   }
	   return false;
   }
   
   public int getFuelBurnTime(){
	   for(int i = 10; i <= 13; i++){
		   if(this.items[i] != null){
			   return getItemBurnTime(this.items[i]);
		   }
	   }
	   return 0;
   	}

	public static int getItemBurnTime(ItemStack stack) {
        if(stack != null){
	        if (stack.itemID == Item.coal.itemID)
	            return 800;
        }
        return 0;
    }

	/*// BURNING AND ACTIVE //*/
    public boolean isBurning()
    {
        return this.fonderieBurnTime > 0;
    }
    
    public boolean isActive(){
    	 return this.fonderieCookTime > 0;
    }
    
	/*// SMELTING //*/
    private boolean canSmelt() {
    	if(this.items[15] != null && (this.items[8] != null || this.items[9] != null)){
            ItemStack itemstack = FonderieRecipes.smelting().getSmeltingResult(this.items[8], this.items[9], this.items[15], this.temperature);
            if(itemstack == null) return false;
            
            if (this.items[14] != null){
	            if (!this.items[14].isItemEqual(itemstack))
	                return false;
	            
	            if (this.items[14] == null)
	                return true;
	
	            int result = items[14].stackSize + itemstack.stackSize;
	            return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
            }
            return true;
        }
        return false;
    }
    
    private ItemStack getSmelt(){
    	if(this.items[15] != null && (this.items[8] != null || this.items[9] != null)){
            ItemStack itemstack = FonderieRecipes.smelting().getSmeltingResult(this.items[8], this.items[9], this.items[15], this.temperature);
            if(itemstack == null) return null;
            
            if (this.items[14] != null){
	            if (!this.items[14].isItemEqual(itemstack))
	                return null;
	            
	            if (this.items[14] == null)
	                return itemstack;
	
	            int result = items[14].stackSize + itemstack.stackSize;
	            return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize()) ? itemstack : null;
            }
            return itemstack;
        }
        return null;
    }
    
    public int getTimeToMelt(ItemStack itemstack){
    	return FonderieRecipes.smelting().getSmeltingTime(itemstack);
    }

    public void smeltItem(ItemStack itemstack) {
        if(itemstack != null){
        	this.items[15].setItemDamage(this.items[15].getItemDamage() - 1);
            if (this.items[15].getItemDamage() == 1) {
                this.decrStackSize(9, 1);
            }

            if (this.items[14] == null)
                this.items[14] = itemstack.copy();
            else if (this.items[14].isItemEqual(itemstack))
                items[14].stackSize += itemstack.stackSize;

            if(this.items[8] != null){
            	--this.items[8].stackSize;
	            if (this.items[8].stackSize <= 0)
	                this.items[8] = null;
            }
            
            if(this.items[9] != null){
            	--this.items[9].stackSize;
	            if (this.items[9].stackSize <= 0)
	                this.items[9] = null;
            }
            
        }
    }
    
	/*// MULTIBLOCK //*/
    public boolean isAttached() {
        if (this.getWorldObj().getBlockId(xCoord, yCoord - 1, zCoord) == AurumSteamPunk.blockFonderie.blockID
                && (this.getWorldObj().getBlockMetadata(xCoord, yCoord - 1, zCoord) == 0 || this.getWorldObj().getBlockMetadata(xCoord, yCoord - 1, zCoord) == 2))
            return true;
        if(this.core != null) return true;
        return false;
    }
    
    public TileEntityFonderie getBottomTile(boolean center){
    	TileEntityFonderie lastTile = this;
    	while(true){
    		TileEntityFonderie tile = getTileAt(lastTile, 0 , -1 , 0);
    		if(tile != null){
    			lastTile = tile;
    		} else {
    			break;
    		}
    	}
    	
    	if(center){
    		if(getPosition(lastTile) != 4){
    			lastTile = getCenter(lastTile);
    		}
    	}
    	
    	return lastTile;
    }
     
    // 0:Alone | 1:Line | 2:Corner | 3:Side | 4:Center
    public int getPosition(TileEntityFonderie tile){
    	int result = 0;
    	if(getTileAt(tile, -1, 0,  0) != null) result++;
    	if(getTileAt(tile,  1, 0,  0) != null) result++;
    	if(getTileAt(tile,  0, 0,  1) != null) result++;
    	if(getTileAt(tile,  0, 0, -1) != null) result++;
    	return result;
    }
    
    public TileEntityFonderie getCenter(TileEntityFonderie tile){
    	for(int i = -1; i <= 1; i++){
    		for(int j = -1; j <= 1; j++){
    			TileEntityFonderie tileat = getTileAt(tile, i, 0, j);
    			if(tileat != null && getPosition(tileat) == 4) return tileat;
    		}
    	}
    	return null;
    }
    
    public boolean isMultiBlock(){
    	TileEntityFonderie lastTile = getBottomTile(true);
    	if(lastTile == null) return false;
    	
    	boolean core = false;
    	int[][][] pat = new int[3][3][3];
				
		//Check if patern is ok
    	for(int i = 0; i <= 2; i++){
        	for(int k = -1; k <= 1; k++){
        		for(int j = -1; j <= 1; j++){
        			TileEntityFonderie tileat = getTileAt(lastTile, k, i, j);
        			if(tileat == null) return false;

        			pat[1+k][i][1+j] = this.worldObj.getBlockMetadata(tileat.xCoord, tileat.yCoord, tileat.zCoord);
        			if(i == 1 && ((k == -1 && j == 0) || (k == 1 && j == 0) || (k == 0 && j == -1) || (k == 0 && j == 1))){
	        			if(!core && tileat.isCore) core = true;
	        			else if(core && tileat.isCore) return false;
        			}
        		}
        	}
    	}
    	
    	return core ? setCore() : false;
    }
    
    //SET CORE OF STRUCTURE
    public boolean setCore(){
    	TileEntityFonderie lastTile = getBottomTile(true);
    	if(lastTile == null) return false;
    	
    	TileEntityFonderie core = getCore(lastTile);
    	if(core == null) return false;
    	
    	//Set core to all Element
    	for(int i = 0; i <= 2; i++){
        	for(int k = -1; k <= 1; k++){
        		for(int j = -1; j <= 1; j++){
        			TileEntityFonderie tileat = getTileAt(lastTile, k, i, j);
        			if(tileat == null) return false;
        			tileat.core = new int[]{ core.xCoord, core.yCoord, core.zCoord };
        		}
        	}
    	}
    	
    	return true;
    }
    
  //RETURN CORE AT STRUCTURE
    public TileEntityFonderie getCore(TileEntityFonderie lastTile){
		//Search at X
		for(int i = -1; i <= 1; i++){
			TileEntityFonderie core = getTileAt(lastTile, i, 1, 0);
			if(core != null && core.isCore) return core;
		}
		
		//Search at Z
		for(int i = -1; i <= 1; i++){
			TileEntityFonderie core = getTileAt(lastTile, 0, 1, i);
			if(core != null && core.isCore) return core;
		}

		//Nothing Found
    	return null;
    }
    
    //RETURN TILE AT COORD
    public TileEntityFonderie getTileAt(TileEntityFonderie tile, int x, int y, int z){
    	TileEntity tileat = tile.worldObj.getBlockTileEntity(tile.xCoord + x, tile.yCoord + y, tile.zCoord + z);
    	if(tileat instanceof TileEntityFonderie){
    		return (TileEntityFonderie)tileat;
    	}
    	return null;
    }
    
    //NOT USED YET
    public boolean isPatern(int[][][] pat){
        for(int i = 0; i < 3; i++){
        	for(int j = 0; j < 3; j++){
        		for(int k = 0; k < 3; k++){
        			if(pat[i][j][k] != this.patern[i][j][k]) return false;
        		}
        	}
        }
        return true;
    }
    
	/*// NBT EDIT //*/
    @Override
    public void readFromNBT(NBTTagCompound tagCompound) {
        super.readFromNBT(tagCompound);
        
        NBTTagList tagList = tagCompound.getTagList("Inventory");
        for (int i = 0; i < tagList.tagCount(); ++i) {
            NBTTagCompound tag = (NBTTagCompound)tagList.tagAt(i);
            byte b0 = tag.getByte("Slot");

            if (b0 >= 0 && b0 < this.items.length) {
                this.items[b0] = ItemStack.loadItemStackFromNBT(tag);
            }
        }

        this.temperature         = tagCompound.getInteger("Temperature");
        this.fonderieBurnTime    = tagCompound.getShort("BurnTime");
        this.fonderieCookTime    = tagCompound.getShort("CookTime");
        this.currentItemBurnTime = tagCompound.getShort("CurrentBurnTime");
        this.currentItemCookTime = tagCompound.getShort("CurrentCookTime");
        this.isCore 			 = tagCompound.getBoolean("isCore");
        this.core				 = tagCompound.getIntArray("Core");
    }
    
    @Override
    public void writeToNBT(NBTTagCompound tagCompound) {
        super.writeToNBT(tagCompound);
        
        NBTTagList itemList = new NBTTagList();
        for (int i = 0; i < this.items.length; ++i) {
        	ItemStack item = this.items[i];
            if (item != null) {
                NBTTagCompound tag = new NBTTagCompound();
                tag.setByte("Slot", (byte)i);
                item.writeToNBT(tag);
                itemList.appendTag(tag);
            }
        }

        tagCompound.setTag("Inventory", itemList);
        tagCompound.setShort("CookTime", (short)this.fonderieCookTime);
        tagCompound.setShort("BurnTime", (short)this.fonderieBurnTime);
        tagCompound.setShort("CurrentBurnTime", (short)this.currentItemBurnTime);
        tagCompound.setShort("CurrentCookTime", (short)this.currentItemCookTime);
        tagCompound.setInteger("Temperature", this.temperature);
        tagCompound.setBoolean("isCore", this.isCore);
        
        if(this.core != null &&this.core.length == 3)
        	tagCompound.setIntArray("Core", this.core);
    }
    
	/*// GUI DISPLAY //*/
	@SideOnly(Side.CLIENT)
    public int getCookProgressScaled(int par1)
    {
        if (this.currentItemCookTime == 0)
            this.currentItemCookTime = 400;

        return this.fonderieCookTime * par1 / this.currentItemCookTime;
    }

    @SideOnly(Side.CLIENT)
    public int getBurnTimeRemainingScaled(int par1)
    {
        if (this.currentItemBurnTime == 0)
            this.currentItemBurnTime = 400;

        return this.fonderieBurnTime * par1 / this.currentItemBurnTime;
    }
    
    @SideOnly(Side.CLIENT)
    public int getHeatProgressScaled(int par1){
        return this.temperature * par1 / this.temperatureMax;
    }
}