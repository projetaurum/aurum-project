package aurum.steampunk.tileentity;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;
import aurum.core.AurumDirection;
import aurum.core.packets.MovingCarpetDrop01Packet;
import aurum.core.packets.MovingCarpetSetItem02Packet;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;

public class TileEntityMovingCarpet extends TileEntity
{
    private ItemStack[] items;
    private boolean[] isLaunched = new boolean[] {false, false};

    private Float[] itemsXPos;
    private Float[] itemsZPos;
    private Integer carpetSpeed;
    private AurumDirection carpetDir;

    public TileEntityMovingCarpet()
    {
        this.items = new ItemStack[2];
        this.itemsXPos = new Float[2];
        this.itemsZPos = new Float[2];
        this.itemsXPos[0] = 1.0F;
        this.itemsXPos[1] = 1.0F;
        this.itemsZPos[0] = 0.0F;
        this.itemsZPos[1] = -0.525F;
        this.carpetSpeed = 1;
        this.carpetDir = new AurumDirection(5);
    }

    @Override
    public void updateEntity()
    {
        super.updateEntity();
        Side side = FMLCommonHandler.instance().getEffectiveSide();

        if (side.isServer())
        {
            if (this.worldObj != null)
            {
                if (this.worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
                {
                    for (int i = 0; i < getItemCount(); i++)
                    {
                        if (getItem(i) != null)
                        {
                            if (!isLaunched[i])
                            {
                                launchItemMovement(i, true);
                            }

                            if (getItemZ(i) >= 0.2f)
                            {
                                transmitItem(i);
                            }
                            else
                            {
                                setItemZPacket(i, getItemZ(i) + (0.016f / getSpeed()));
                            }
                        }
                    }
                }
            }
        }
        else
        {
        }
    }

    public boolean isLaunched(int i)
    {
        return this.isLaunched[i];
    }

    /**
     * Vitesse en int
     * 1-20
     * 1 Vitesse grande
     * 20 Vitesse faible
     */
    public void setSpeed(int i)
    {
        this.carpetSpeed = i;
    }

    public int getSpeed()
    {
        return this.carpetSpeed;
    }

    public void setCarpetDirection(int i)
    {
        this.carpetDir.setOrientation(i);
    }

    public AurumDirection getCarpetDir()
    {
        return this.carpetDir;
    }

    public ItemStack[] getItems()
    {
        return this.items;
    }

    public ItemStack getItem(int i)
    {
        return this.items[i];
    }

    public Float getItemX(int i)
    {
        return this.itemsXPos[i];
    }

    public Float getItemZ(int i)
    {
        return this.itemsZPos[i];
    }

    public void setItemX(int i, float x)
    {
        this.itemsXPos[i] = x;
    }

    public void setItemZ(int i, float z)
    {
        this.itemsZPos[i] = z;
    }

    public void setItemZPacket(int i, float z)
    {
        this.itemsZPos[i] = z;
        PacketDispatcher.sendPacketToAllAround(xCoord, yCoord, zCoord, 50d, worldObj.getWorldInfo().getVanillaDimension(),
                                               new MovingCarpetSetItem02Packet(xCoord, yCoord, zCoord, 0, 0, z, i).makePacket());
    }

    public int getItemCount()
    {
        return this.items.length;
    }

    public void setItem(ItemStack stack, float x, float z, int i)
    {
        if (FMLCommonHandler.instance().getEffectiveSide().isServer())
        {
        }

        this.items[i] = stack;
        this.setItemX(i, x);
        this.setItemZ(i, z);
    }

    @Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet)
    {
        readFromNBT(packet.data);
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        tag.setInteger("Speed", this.carpetSpeed);
        tag.setInteger("Direction", this.carpetDir.getOrientation());
        //Items
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.items.length; ++i)
        {
            if (this.items[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                nbttagcompound1.setFloat("XPos", this.itemsXPos[i]);
                nbttagcompound1.setFloat("ZPos", this.itemsXPos[i]);
                this.items[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        super.writeToNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        this.carpetSpeed = tag.getInteger("Speed");
        this.carpetDir = new AurumDirection(tag.getInteger("Direction"));
        //Items
        NBTTagList nbttaglist = tag.getTagList("Items");

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.items.length)
            {
                this.items[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
                this.itemsXPos[b0] = nbttagcompound1.getFloat("XPos");
                this.itemsZPos[b0] = nbttagcompound1.getFloat("ZPos");
            }
        }

        super.readFromNBT(tag);
    }

    /*
     * Interactions relatives a un pseudo inventaire
     */

    public boolean canAcceptItem()
    {
        if (this.getItemMinXPos() >= 0.3f)
        {
            return true;
        }

        return false;
    }

    public float getItemMinXPos()
    {
        if (this.itemsXPos[0] > this.itemsXPos[1])
        {
            return this.itemsXPos[1];
        }
        else
        {
            return this.itemsXPos[0];
        }
    }

    /**
     * Envoyer l'item vers un autre tapis
     */
    public void transmitItem(int i)
    {
        System.out.println("Transmit");

        if (this.checkAvailableCarpet(carpetDir))
        {
            this.setItem(null, 1.0f, 0.0f, i);
            TileEntityMovingCarpet carpet = null;

            if (carpetDir.getOrientation() == 6)
            {
                carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1);
            }
            else if (carpetDir.getOrientation() == 5)
            {
                carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1);
            }
            else if (carpetDir.getOrientation() == 1)
            {
                carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord);
            }
            else if (carpetDir.getOrientation() == 2)
            {
                carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord);
            }

            carpet.setItem(this.items[i], 0.0f, -0.4f, i);
        }
        else
        {
            System.out.println("Transmit : DROP " + FMLCommonHandler.instance().getEffectiveSide());

            if (FMLCommonHandler.instance().getEffectiveSide().isServer())
            {
                dropItem(i);
            }
            else
            {
                this.sendServerDrop(i);
            }
        }

        this.items[i] = null;
        this.itemsXPos[i] = 1.0F;
        this.itemsZPos[i] = 0.0F;
    }

    /**
     * Begin the item moving action
     * @param i = Location in ItemStack array
     */
    public void launchItemMovement(int i, boolean packet)
    {
        this.isLaunched[i] = true;

        if (packet)
        {
            PacketDispatcher.sendPacketToAllAround(xCoord, yCoord, zCoord, 50d, worldObj.getWorldInfo().getVanillaDimension(),
                                                   new MovingCarpetSetItem02Packet(xCoord, yCoord, zCoord, 0, 2, 0, i).makePacket());
        }
    }

    public boolean checkAvailableCarpet(AurumDirection dir)
    {
        if (dir.getOrientation() == 6)
        {
            if (this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1) != null)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1);

                if (carpet.canAcceptItem())
                {
                    return true;
                }
            }
        }
        else if (dir.getOrientation() == 5)
        {
            if (this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1) != null)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1);

                if (carpet.canAcceptItem())
                {
                    return true;
                }
            }
        }
        else if (dir.getOrientation() == 1)
        {
            if (this.worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord) != null)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord);

                if (carpet.canAcceptItem())
                {
                    return true;
                }
            }
        }
        else if (dir.getOrientation() == 2)
        {
            if (this.worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord) != null)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) this.worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord);

                if (carpet.canAcceptItem())
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void sendServerDrop(int i)
    {
        PacketDispatcher.sendPacketToServer(new MovingCarpetDrop01Packet
                                            (this.xCoord, this.yCoord, this.zCoord, i, Minecraft.getMinecraft().thePlayer.username).makePacket());
    }

    public void dropItem(int i)
    {
        ItemStack itemstack = this.getItem(i);
        this.items[i] = null;
        this.itemsXPos[i] = 1.0F;
        this.itemsZPos[i] = 0.0F;
        this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
        Random rand = new Random();

        if (itemstack != null)
        {
            float f = rand.nextFloat() * 0.8F + 0.1F;
            float f1 = rand.nextFloat() * 0.8F + 0.1F;
            EntityItem entityitem;

            for (float f2 = rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; this.worldObj.spawnEntityInWorld(entityitem))
            {
                int k1 = rand.nextInt(21) + 10;

                if (k1 > itemstack.stackSize)
                {
                    k1 = itemstack.stackSize;
                }

                itemstack.stackSize -= k1;
                entityitem = new EntityItem(this.worldObj, (double)((float)this.xCoord + f), (double)((float)this.yCoord + f1), (double)((float)this.zCoord + f2), new ItemStack(itemstack.itemID, k1, itemstack.getItemDamage()));
                float f3 = 0.05F;
                entityitem.motionX = (double)((float)rand.nextGaussian() * f3);
                entityitem.motionY = (double)((float)rand.nextGaussian() * f3 + 0.2F);
                entityitem.motionZ = (double)((float)rand.nextGaussian() * f3);

                if (itemstack.hasTagCompound())
                {
                    entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                }
            }
        }
    }
}