package aurum.steampunk;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import aurum.steampunk.enaria.GuiSynapse;
import aurum.steampunk.gui.ContainerFonderie;
import aurum.steampunk.gui.ContainerSteamPump;
import aurum.steampunk.gui.GuiFonderie;
import aurum.steampunk.gui.GuiSteamPump;
import aurum.steampunk.tileentity.TileEntityFonderie;
import aurum.steampunk.tileentity.TileEntitySteamPump;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{
	
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if (tileEntity != null) {
        	if(id == AurumSteamPunk.GUI_FONDERIE) 
        		return new ContainerFonderie((TileEntityFonderie) tileEntity, player.inventory, world);
        	if(id == AurumSteamPunk.GUI_STEAMPUMP)
        		return new ContainerSteamPump((TileEntitySteamPump) tileEntity, player.inventory, world);
        }
        else
        {

        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if (tileEntity != null) {
        	if(id == AurumSteamPunk.GUI_FONDERIE)
        		return new GuiFonderie(new ContainerFonderie((TileEntityFonderie) tileEntity, player.inventory, world), (TileEntityFonderie) world.getBlockTileEntity(x, y, z));
        	if(id == AurumSteamPunk.GUI_STEAMPUMP)
        		return new GuiSteamPump(new ContainerSteamPump((TileEntitySteamPump) tileEntity, player.inventory, world), (TileEntitySteamPump) world.getBlockTileEntity(x, y, z));
        	if(id == AurumSteamPunk.GUI_SYNAPSE)
        		return new GuiSynapse(player);
        }
        return null;
    }
}
