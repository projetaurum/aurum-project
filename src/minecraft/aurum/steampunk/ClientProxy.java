package aurum.steampunk;

import aurum.steampunk.enaria.EntityDrone;
import aurum.steampunk.enaria.EntitySynapsePortal;
import aurum.steampunk.enaria.ModelDrone;
import aurum.steampunk.enaria.RenderDrone;
import aurum.steampunk.enaria.RenderSynapsePortal;
import aurum.steampunk.enaria.bolts.EntityLaserBolt;
import aurum.steampunk.enaria.bolts.EntityPlasmaBolt;
import aurum.steampunk.enaria.bolts.EntityTeleportBolt;
import aurum.steampunk.enaria.bolts.RenderLaserBolt;
import aurum.steampunk.enaria.bolts.RenderPlasmaBolt;
import aurum.steampunk.enaria.bolts.RenderTeleportBolt;
import aurum.steampunk.render.TileEntityMovingCarpetSpecialRenderer;
import aurum.steampunk.tileentity.TileEntityMovingCarpet;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMovingCarpet.class, new TileEntityMovingCarpetSpecialRenderer());
        RenderingRegistry.registerEntityRenderingHandler(EntityDrone.class, new RenderDrone(new ModelDrone(), 0.5F));
        RenderingRegistry.registerEntityRenderingHandler(EntityLaserBolt.class, new RenderLaserBolt(0.5f));
        RenderingRegistry.registerEntityRenderingHandler(EntityPlasmaBolt.class, new RenderPlasmaBolt(0.5f));
        RenderingRegistry.registerEntityRenderingHandler(EntityTeleportBolt.class, new RenderTeleportBolt(0.5f));
        RenderingRegistry.registerEntityRenderingHandler(EntitySynapsePortal.class, new RenderSynapsePortal());
    }
}