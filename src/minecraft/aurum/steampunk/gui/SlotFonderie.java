package aurum.steampunk.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.items.ItemTemplate;
import aurum.steampunk.tileentity.TileEntityFonderie;

public class SlotFonderie extends Slot
{
    byte type = 0;
    /*
     * Type 0 : template
     * Type 1 : osef
     * Type 2 : osef
     * Type 3 : resultat
     * Type 4 : template interdit
     */

    public SlotFonderie(IInventory par1iInventory, int par2, int par3, int par4, byte type) {
        super(par1iInventory, par2, par3, par4);
        this.type = type;
        //this.setBackgroundIconTexture("mods/aurum/textures/items/ecu");
    }

    @Override
    public boolean canTakeStack(EntityPlayer player) {
        TileEntityFonderie fonderie = (TileEntityFonderie) this.inventory;

        if (this.type == 4 && fonderie.isBurning()) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isItemValid(ItemStack par1)
    {
        if (!((TileEntityFonderie) this.inventory).isAttached())
            return false;
        
        if (this.getSlotIndex() <= 7) {
        	return par1.getItem() instanceof ItemTemplate;
        } else if (this.type == 0 || this.type == 4) {
            return par1.getItem() instanceof ItemTemplate;
        } else if (this.type == 1) {
        	return true;
        } else if (this.type == 2) {
            return true;
        }

        return false;
    }
}