package aurum.steampunk.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.steampunk.tileentity.TileEntitySteamPump;

public class GuiSteamPump extends GuiContainer {

	private TileEntitySteamPump tileEntity;
	
	public GuiSteamPump(Container par1Container, TileEntitySteamPump tileEntity) {
		super(par1Container);
		this.tileEntity = tileEntity;
		
		this.xSize = 176;
		this.ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) {
        this.fontRenderer.drawString("Steam Pump", 8, this.ySize - 159, 4210752);
        this.fontRenderer.drawString("Water : " + Math.round(this.tileEntity.water_current/100), 8, this.ySize - 107, 4210752);
        this.fontRenderer.drawString("Steam : " + Math.round(this.tileEntity.water_current/100), 8, this.ySize - 95, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        mc.renderEngine.bindTexture(new ResourceLocation("aurumsteampunk","/mods/aurum/steampunk/textures/gui/steampump.png"));
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        int i1;

        if (this.tileEntity.isBurning()) {
        	i1 = this.tileEntity.getBurnLevel(12);
        	this.drawTexturedModalRect(x + 63 + 9, y + 36 + 34 - i1, 176, 12 - i1, 14, i1 + 2);
        }
        
        if( this.tileEntity.water_current > 0){
        	i1 = this.tileEntity.getWaterLevel(69);
        	this.drawTexturedModalRect(x + 143, y + 78 - i1, 191, 70 - i1, 13, i1);
        }
        
        if( this.tileEntity.steam_current > 0){
        	i1 = this.tileEntity.getSteamLevel(69);
        	this.drawTexturedModalRect(x + 114, y + 78 - i1, 204, 70 - i1, 18, i1);
        }
	}

}
