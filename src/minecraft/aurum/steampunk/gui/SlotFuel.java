package aurum.steampunk.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import aurum.steampunk.AurumSteamPunk;

public class SlotFuel extends Slot{
	
	TileEntity tile;
	
	public SlotFuel(IInventory par1iInventory, int par2, int par3, int par4) {
        super(par1iInventory, par2, par3, par4);
    }

    @Override
    public boolean canTakeStack(EntityPlayer player) {
        //TileEntityFonderie fonderie = (TileEntityFonderie) this.inventory;
        return true;
    }

    @Override
    public boolean isItemValid(ItemStack par1) {
    	if(AurumSteamPunk.isFuel(par1.itemID))
    		return true;
        return false;
    }
}
