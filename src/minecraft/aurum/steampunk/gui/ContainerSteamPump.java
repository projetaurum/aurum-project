package aurum.steampunk.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemCoal;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.tileentity.TileEntitySteamPump;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerSteamPump extends Container{

	//Magical Variable
	private static final int INV_START = TileEntitySteamPump.SIZE , INV_END = INV_START+26, 
			HOTBAR_START = INV_END+1, HOTBAR_END = HOTBAR_START+8;
	
	//Save the TileEntity parent
    protected TileEntitySteamPump tileEntity;

    //TileEntity cache
    private int lastWaterLevel   = 0;
    private int lastSteamLevel   = 0;
    private int lastBurnTime     = 0;
    private int lastItemBurnTime = 0;
    
    public ContainerSteamPump(TileEntitySteamPump tileEntity, InventoryPlayer player, World world){
    	this.tileEntity = tileEntity;
        binSteamPumpInventory(tileEntity);
        bindPlayerInventory(player);
    }
    
	private void bindPlayerInventory(InventoryPlayer player) {
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 9; j++){
				addSlotToContainer(new Slot(player, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
		for (int i = 0; i < 9; i++){
			addSlotToContainer(new Slot(player, i, 8 + i * 18, 142));
		}
	}

	private void binSteamPumpInventory(TileEntitySteamPump tileEntity2) {
        addSlotToContainer(new SlotFuel(tileEntity, 0, 80, 61));
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.tileEntity.isUseableByPlayer(entityplayer);
	}
	
	public String getName(){
		return this.tileEntity.getInvName();
	}
	
    public void addCraftingToCrafters(ICrafting par1ICrafting) {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.tileEntity.water_current);
        par1ICrafting.sendProgressBarUpdate(this, 1, this.tileEntity.steam_current);
        par1ICrafting.sendProgressBarUpdate(this, 2, this.tileEntity.burn_item);
        par1ICrafting.sendProgressBarUpdate(this, 3, this.tileEntity.burn_time);
    }

    public void detectAndSendChanges() {
        for (int var1 = 0; var1 < this.crafters.size(); ++var1) {
            ICrafting var2 = (ICrafting)this.crafters.get(var1);

            if (this.lastBurnTime != this.tileEntity.burn_time) {
                var2.sendProgressBarUpdate(this, 0, this.tileEntity.burn_time);
                this.lastBurnTime = this.tileEntity.burn_time;
            }

            if (this.lastItemBurnTime != this.tileEntity.burn_item) {
                var2.sendProgressBarUpdate(this, 1, this.tileEntity.burn_item);
                this.lastItemBurnTime = this.tileEntity.burn_item;
            }

            if (this.lastSteamLevel != this.tileEntity.steam_current) {
                var2.sendProgressBarUpdate(this, 2, this.tileEntity.steam_current);
                this.lastSteamLevel = this.tileEntity.steam_current;
            }

            if (this.lastWaterLevel != this.tileEntity.water_current) {
                var2.sendProgressBarUpdate(this, 3, this.tileEntity.water_current);
                this.lastWaterLevel = this.tileEntity.water_current;
            }
        }
        
        super.detectAndSendChanges();
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2) {
        if (par1 == 0)
            this.tileEntity.burn_time = par2;

        if (par1 == 1)
            this.tileEntity.burn_item = par2;

        if (par1 == 2)
            this.tileEntity.steam_current = par2;
        
        if (par1 == 3)
            this.tileEntity.water_current = par2;
    }
    
    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1, int par2) {
    	ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);
	
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2 < INV_START) {
				if (!this.mergeItemStack(itemstack1, INV_START, HOTBAR_END + 1, true))
					return null;
				slot.onSlotChange(itemstack1, itemstack);
			} else {
				if (AurumSteamPunk.isFuel(itemstack1.itemID)) {
					if (!this.mergeItemStack(itemstack1, TileEntitySteamPump.SLOT_FUEL, TileEntitySteamPump.SLOT_FUEL+1, false))
						return null;
				}
			}
			
			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}
			
			if (itemstack1.stackSize == itemstack.stackSize)
				return null;
			
			slot.onPickupFromSlot(par1, itemstack1);
		}
		return itemstack;
    }

}
