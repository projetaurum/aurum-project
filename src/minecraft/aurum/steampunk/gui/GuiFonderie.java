package aurum.steampunk.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.steampunk.tileentity.TileEntityFonderie;

public class GuiFonderie extends GuiContainer {
    private TileEntityFonderie tileEntity;

    public GuiFonderie(Container par1Container, TileEntityFonderie tileEntity)
    {
        super(par1Container);
        this.tileEntity = tileEntity;
        
        this.xSize = 176;
        this.ySize = 199;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int i, int j)
    {
        this.fontRenderer.drawString("Fonderie", 8, this.ySize - 193, 4210752);
        this.fontRenderer.drawString(Math.round(this.tileEntity.temperature/100) + " C", 15, this.ySize - 100, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        mc.renderEngine.bindTexture(new ResourceLocation("aurumsteampunk","textures/gui/fonderie.png"));
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        int i1;

        if (this.tileEntity.isActive()) {
            i1 = this.tileEntity.getCookProgressScaled(24);
            this.drawTexturedModalRect(x + 84 + 15, y + 34 - 7, 176, 14, i1, 16);
        }
        
        if (this.tileEntity.isBurning()) {
        	i1 = this.tileEntity.getBurnTimeRemainingScaled(12);
        	this.drawTexturedModalRect(x + 63 + 9, y + 36 + 34 - i1, 176, 12 - i1, 14, i1 + 2);
        }
        
        if( this.tileEntity.temperature > 0){
        	i1 = this.tileEntity.getHeatProgressScaled(58);
        	this.drawTexturedModalRect(x + 139, y + 110 - i1, 213, 58 - i1, 13, i1);
        }
    }
}