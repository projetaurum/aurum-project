package aurum.steampunk.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.items.ItemTemplate;
import aurum.steampunk.tileentity.TileEntityFonderie;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerFonderie extends Container {
	
	//Magical Variable
	private static final int INV_START = TileEntityFonderie.SIZE , INV_END = INV_START+26, 
			HOTBAR_START = INV_END+1, HOTBAR_END = HOTBAR_START+8;
	
    protected TileEntityFonderie tileEntity;

    private int lastCookTime     = 0;
    private int lastBurnTime     = 0;
    private int lastItemBurnTime = 0;
    private int lastItemCookTime = 0;
    private int lastTemperature  = 0;

    public ContainerFonderie(TileEntityFonderie tileEntity, InventoryPlayer player, World world) {
        this.tileEntity = tileEntity;
        bindFonderieInventory(tileEntity);
        bindPlayerInventory(player);
    }

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.tileEntity.isUseableByPlayer(entityplayer);
	}
	
	public String getName(){
		return this.tileEntity.getInvName();
	}

    protected void bindFonderieInventory(TileEntityFonderie tileEntity) {
        addSlotToContainer(new SlotFonderie(tileEntity, 0, 8, 18, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 1, 26, 18, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 2, 8, 36, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 3, 26, 36, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 4, 8, 54, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 5, 26, 54, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 6, 8, 72, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 7, 26, 72, (byte) 0));
        addSlotToContainer(new SlotFonderie(tileEntity, 8, 61, 18, (byte) 2));
        addSlotToContainer(new SlotFonderie(tileEntity, 9, 81, 18, (byte) 2));
        addSlotToContainer(new SlotFuel(tileEntity, 10, 62, 76));
        addSlotToContainer(new SlotFuel(tileEntity, 11, 80, 76));
        addSlotToContainer(new SlotFuel(tileEntity, 12, 62, 94));
        addSlotToContainer(new SlotFuel(tileEntity, 13, 80, 94));
        addSlotToContainer(new SlotFonderie(tileEntity, 14, 129, 28, (byte) 3));
        addSlotToContainer(new SlotFonderie(tileEntity, 15, 71, 38, (byte) 4));
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 9; j++){
				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 117 + i * 18));
			}
		}
		for (int i = 0; i < 9; i++){
			addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 175));
		}
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1, int par2) {
    	ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);
	
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (par2 < INV_START) {
				if (!this.mergeItemStack(itemstack1, INV_START, HOTBAR_END + 1, true))
					return null;
				slot.onSlotChange(itemstack1, itemstack);
			} else {
				if (itemstack1.getItem() instanceof ItemTemplate) {
					if (!this.mergeItemStack(itemstack1, TileEntityFonderie.SLOT_STOCK, TileEntityFonderie.SLOT_MATERIAL, false))
						return null;
				} else if (AurumSteamPunk.isFuel(itemstack1.itemID)) {
					if (!this.mergeItemStack(itemstack1, TileEntityFonderie.SLOT_FUEL, TileEntityFonderie.SLOT_RESULT, false))
						return null;
				} else {
					if (!this.mergeItemStack(itemstack1, TileEntityFonderie.SLOT_MATERIAL, TileEntityFonderie.SLOT_FUEL, false))
						return null;
				}
			}
			
			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}
			
			if (itemstack1.stackSize == itemstack.stackSize)
				return null;
			
			slot.onPickupFromSlot(par1, itemstack1);
		}
		return itemstack;
    }

    public void addCraftingToCrafters(ICrafting par1ICrafting) {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.tileEntity.fonderieCookTime);
        par1ICrafting.sendProgressBarUpdate(this, 1, this.tileEntity.fonderieBurnTime);
        par1ICrafting.sendProgressBarUpdate(this, 2, this.tileEntity.currentItemBurnTime);
        par1ICrafting.sendProgressBarUpdate(this, 3, this.tileEntity.currentItemCookTime);
        par1ICrafting.sendProgressBarUpdate(this, 4, this.tileEntity.temperature);
    }

    public void detectAndSendChanges() {

        for (int var1 = 0; var1 < this.crafters.size(); ++var1) {
            ICrafting var2 = (ICrafting)this.crafters.get(var1);

            if (this.lastCookTime != this.tileEntity.fonderieCookTime) {
                var2.sendProgressBarUpdate(this, 0, this.tileEntity.fonderieCookTime);
                this.lastCookTime     = this.tileEntity.fonderieCookTime;
            }

            if (this.lastBurnTime != this.tileEntity.fonderieBurnTime) {
                var2.sendProgressBarUpdate(this, 1, this.tileEntity.fonderieBurnTime);
                this.lastBurnTime     = this.tileEntity.fonderieBurnTime;
            }

            if (this.lastItemBurnTime != this.tileEntity.currentItemBurnTime) {
                var2.sendProgressBarUpdate(this, 2, this.tileEntity.currentItemBurnTime);
                this.lastItemBurnTime = this.tileEntity.currentItemBurnTime;
            }

            if (this.lastItemCookTime != this.tileEntity.currentItemCookTime) {
                var2.sendProgressBarUpdate(this, 3, this.tileEntity.currentItemCookTime);
                this.lastItemCookTime = this.tileEntity.currentItemCookTime;
            }
            
            if (this.lastTemperature != this.tileEntity.temperature){
            	var2.sendProgressBarUpdate(this, 4, this.tileEntity.temperature);
                this.lastTemperature  = this.tileEntity.temperature;
            }
        }
        
        super.detectAndSendChanges();
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2) {
        if (par1 == 0) {
            this.tileEntity.fonderieCookTime = par2;
        }

        if (par1 == 1) {
            this.tileEntity.fonderieBurnTime = par2;
        }

        if (par1 == 2) {
            this.tileEntity.currentItemBurnTime = par2;
        }
        
        if (par1 == 3) {
            this.tileEntity.currentItemCookTime = par2;
        } 
        
        if (par1 == 4){
        	this.tileEntity.temperature = par2;
        }
    }
}