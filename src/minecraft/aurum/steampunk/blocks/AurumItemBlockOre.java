package aurum.steampunk.blocks;

import aurum.steampunk.AurumSteamPunk;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class AurumItemBlockOre extends ItemBlock
{
    public AurumItemBlockOre(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setCreativeTab(AurumSteamPunk.steamPunkTab);
        this.setUnlocalizedName("AurumItemBlockOre");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Orichalque";
                break;

            case 1:
                name = "Neptunite";
                break;

            case 2:
                name = "Steam Vapora";
                break;

            case 3:
                name = "Astria";
                break;
        }

        return name;
    }
}
