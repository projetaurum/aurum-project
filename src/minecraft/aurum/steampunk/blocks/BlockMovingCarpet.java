package aurum.steampunk.blocks;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.steampunk.tileentity.TileEntityMovingCarpet;

public class BlockMovingCarpet extends BlockContainer
{
    public BlockMovingCarpet(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    @Override
    public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer entity, int side, float hitX, float hitY, float hitZ)
    {
        TileEntityMovingCarpet tile = (TileEntityMovingCarpet) w.getBlockTileEntity(x, y, z);
        System.out.println("Side : " + FMLCommonHandler.instance().getEffectiveSide());
        System.out.println("Items 1: " + tile.getItems()[0] + " Position X : " + tile.getItemX(0) + " Position Z : " + tile.getItemZ(0));
        System.out.println("Items 2: " + tile.getItems()[1] + " Position X : " + tile.getItemX(1) + " Position Z : " + tile.getItemZ(1));

        if (entity.getCurrentEquippedItem() != null && !entity.isSneaking())
        {
            ItemStack stack = entity.inventory.getCurrentItem();

            if (tile.canAcceptItem())
            {
                tile.setItem(new ItemStack(stack.itemID, 1, stack.getItemDamage()), 0.0f, -0.4f, 0);
                entity.inventory.setInventorySlotContents(entity.inventory.currentItem, new ItemStack(stack.itemID, stack.stackSize - 1, stack.getItemDamage()));
                return false;
            }
        }

        return super.onBlockActivated(w, x, y, z, entity, side, hitX, hitY, hitZ);
    }

    @Override
    public void onBlockAdded(World w, int x, int y, int z)
    {
        //TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) w.getBlockTileEntity(x, y, z);
        //carpet.setCarpetDirection(ForgeDirection.getOrientation(0));
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        return false;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityMovingCarpet();
    }
}