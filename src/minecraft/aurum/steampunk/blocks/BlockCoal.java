package aurum.steampunk.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

public class BlockCoal extends Block
{
    public BlockCoal(int par1, Material par2Material)
    {
        super(par1, Material.wood);
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurumsteampunk:coal_block");
    }
}
