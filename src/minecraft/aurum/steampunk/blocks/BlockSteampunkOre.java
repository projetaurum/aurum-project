package aurum.steampunk.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockSteampunkOre extends Block
{
    Icon[] iconBuffer;
    public BlockSteampunkOre(int par1)
    {
        super(par1, Material.iron);
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[2];
        this.iconBuffer[0] = register.registerIcon("aurumsteampunk:block_cuivre");
        this.iconBuffer[1] = register.registerIcon("aurumsteampunk:block_bronze");
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        return iconBuffer[meta];
    }
}