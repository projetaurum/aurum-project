package aurum.steampunk.blocks;

import java.util.Random;

import aurum.steampunk.AurumSteamPunk;
import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.texture.IconRegister;

public class BlockCopperOre extends BlockOre
{
    public BlockCopperOre(int par1)
    {
        super(par1);
    }

    @Override
    public int idDropped(int par1, Random fortune, int par3)
    {
        return AurumSteamPunk.itemRawOre.itemID;
    }

    @Override
    public int quantityDropped(int id, int meta, Random fortune)
    {
        if (fortune.nextInt(20) == 16)
        {
            return 3;
        }

        return 2;
    }

    @Override
    public int damageDropped(int par1)
    {
        return 0;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurumsteampunk:ore_copper");
    }
}
