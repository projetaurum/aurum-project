package aurum.steampunk.blocks;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class SteampunkItemBlockOre extends ItemBlock
{
    public SteampunkItemBlockOre(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockSteampunkOre");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Copper Block";
                break;

            case 1:
                name = "Bronze Block";
                break;
        }

        return name;
    }
}