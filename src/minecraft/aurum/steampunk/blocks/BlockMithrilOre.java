package aurum.steampunk.blocks;

import java.util.Random;

import aurum.steampunk.AurumSteamPunk;
import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.texture.IconRegister;

public class BlockMithrilOre extends BlockOre
{
    public BlockMithrilOre(int par1)
    {
        super(par1);
    }

    @Override
    public int idDropped(int par1, Random fortune, int par3)
    {
        return AurumSteamPunk.itemRawOre.itemID;
    }

    @Override
    public int quantityDropped(int id, int meta, Random fortune)
    {
        if (fortune.nextInt(20) == 16)
        {
            return 2;
        }

        return 1;
    }

    @Override
    public int damageDropped(int par1)
    {
        return 1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurumsteampunk:ore_mithril");
    }
}
