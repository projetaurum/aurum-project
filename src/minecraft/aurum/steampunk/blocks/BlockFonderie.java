package aurum.steampunk.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.tileentity.TileEntityFonderie;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockFonderie extends BlockContainer {
    public static Icon iconBuffer[];

    public BlockFonderie(int par1) {
        super(par1, Material.rock);
		this.setHardness(1.5F);
		this.setResistance(10.0F);
    }
    
    public void onPostBlockPlaced(World par1World, int par2, int par3, int par4, int par5) {
        super.onPostBlockPlaced(par1World, par2, par3, par4, par5);
        
        TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
        if(tile != null && tile instanceof TileEntityFonderie){
        	TileEntityFonderie fonderie = (TileEntityFonderie) tile;
        	int meta = par1World.getBlockMetadata(par2, par3, par4);
        	if(meta == 0) fonderie.isCore = true;
        	fonderie.isMultiBlock();
        }
    }

    @Override
    public void onBlockDestroyedByExplosion(World world, int x, int y, int z, Explosion exp)
    {
        int metadata = world.getBlockMetadata(x, y, z);
        this.destroy(world, x, y, z, metadata);
        super.onBlockDestroyedByExplosion(world, x, y, z, exp);
    }

    public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int metadata)
    {
        this.destroy(world, x, y, z, metadata);
        super.onBlockDestroyedByPlayer(world, x, y, z, metadata);
    }
    
    private void destroy(World world, int x, int y, int z, int metadata){
    	if (metadata == 0 || metadata == 2) {
        	if((world.getBlockId(x, y + 1, z) == this.blockID && (world.getBlockMetadata(x, y + 1, z) == 1 || world.getBlockMetadata(x, y + 1, z) == 3))){
        		TileEntityFonderie fonderie = (TileEntityFonderie) world.getBlockTileEntity(x, y + 1, z);
        		if(fonderie != null){
        			for (int i = 0; i < 8; i++){
                        ItemStack itemstack = fonderie.getStackInSlot(i);
                        if (itemstack != null)
                            this.dropBlockAsItem_do(world, x, y, z, itemstack);
                        fonderie.setInventorySlotContents(i, null);
                    }
        			for (int i = 0; i < 4; i++){
                        ItemStack itemstack = fonderie.getStackInSlot(i + 10);
    	                if (itemstack != null){
    		                System.out.println("Drop ? " + itemstack);
    	                    this.dropBlockAsItem_do(world, x, y, z, itemstack);
    	                }
                        fonderie.setInventorySlotContents(i + 10, null);
                    }
        		}
        	}
        } else {
        	TileEntityFonderie fonderie = (TileEntityFonderie) world.getBlockTileEntity(x, y, z);
        	
        	if(fonderie != null){
	            for (int i = 0; i < 15; i++) {
	                ItemStack itemstack = fonderie.getStackInSlot(i);
	                if (itemstack != null){
		                System.out.println("Drop ? " + itemstack);
	                    this.dropBlockAsItem_do(world, x, y, z, itemstack);
	                }
	                fonderie.setInventorySlotContents(i + 10, null);
	            }
        	}
        }
    }
    
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
    	if(!world.isRemote) {
	    	int data = world.getBlockMetadata(x, y, z);
	    	if(data == 0 || data == 2){
	    		int dataO = world.getBlockMetadata(x, y + 1, z);
	    		if(world.getBlockId(x, y + 1, z) == AurumSteamPunk.blockFonderie.blockID && (dataO == 1 || dataO == 3)){
	    			player.openGui(AurumSteamPunk.steamPunkInstance, AurumSteamPunk.GUI_FONDERIE, world, x, y + 1, z);
	    		}
	    	} else if(data == 1 || data == 3){
	    		int dataO = world.getBlockMetadata(x, y - 1, z);
	    		if(world.getBlockId(x, y - 1, z) == AurumSteamPunk.blockFonderie.blockID && (dataO == 0 || dataO == 2)){
	    			player.openGui(AurumSteamPunk.steamPunkInstance, AurumSteamPunk.GUI_FONDERIE, world, x, y, z);
	    		}
	    	}
	    	
	    	TileEntity tile = world.getBlockTileEntity(x, y, z);
	    	if(tile instanceof TileEntityFonderie){
	    		TileEntityFonderie fonderie = (TileEntityFonderie) tile;
	    		if(fonderie.core != null && fonderie.core.length == 3) player.openGui(AurumSteamPunk.steamPunkInstance, AurumSteamPunk.GUI_FONDERIE, world, fonderie.core[0], fonderie.core[1], fonderie.core[2]);
	    	}
    	}
    	
        return true;
    }
    
    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        Block block = blocksList[world.getBlockId(x, y, z)];
        if (block != null) {
        	int meta = world.getBlockMetadata(x, y, z);
            if(meta == 2 || meta == 3)
            	return 15;
        }
        return 0;
    }

    @Override
    public void registerIcons(IconRegister par1)
    {
        iconBuffer = new Icon[12];
        iconBuffer[0] = par1.registerIcon("aurumsteampunk:fonderie1");
        iconBuffer[1] = par1.registerIcon("aurumsteampunk:fonderie2");
        iconBuffer[2] = par1.registerIcon("aurumsteampunk:fonderie3");
        iconBuffer[3] = par1.registerIcon("aurumsteampunk:fonderie4");
        iconBuffer[4] = par1.registerIcon("aurumsteampunk:fonderie5");
        iconBuffer[5] = par1.registerIcon("aurumsteampunk:fonderie6");
        iconBuffer[6] = par1.registerIcon("aurumsteampunk:fonderie7");
        iconBuffer[7] = par1.registerIcon("aurumsteampunk:fonderie8");
        iconBuffer[8] = par1.registerIcon("aurumsteampunk:fonderie9");
        iconBuffer[9] = par1.registerIcon("aurumsteampunk:fonderie10");
        iconBuffer[10] = par1.registerIcon("aurumsteampunk:fonderie11");
        iconBuffer[11] = par1.registerIcon("aurumsteampunk:fonderie12");
    }

    @SuppressWarnings("static-access")
    @Override
    public Icon getIcon(int par1, int par2)
    {
        if (par2 == 0)
        {
            if (par1 == 1)
            {
                return iconBuffer[1];
            }

            if (par1 == 0)
            {
                return iconBuffer[0];
            }

            if (par1 == 4)
            {
                return iconBuffer[4];
            }

            return iconBuffer[2];
        }

        if (par2 == 1)
        {
            if (par1 == 1)
            {
                return iconBuffer[8];
            }

            if (par1 == 0)
            {
                return iconBuffer[1];
            }

            if (par1 == 4)
            {
                return iconBuffer[6];
            }

            return iconBuffer[11];
        }

        if (par2 == 2)
        {
            if (par1 == 1)
            {
                return iconBuffer[5];
            }

            if (par1 == 0)
            {
                return iconBuffer[0];
            }

            if (par1 == 4)
            {
                return iconBuffer[4];
            }

            return iconBuffer[3];
        }

        if (par2 == 3)
        {
            if (par1 == 1)
            {
                return iconBuffer[9];
            }

            if (par1 == 0)
            {
                return iconBuffer[5];
            }

            if (par1 == 4)
            {
                return iconBuffer[7];
            }

            return iconBuffer[10];
        }

        return this.iconBuffer[0];
    }

    @Override
    public int damageDropped(int metadata) {
        return metadata;
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(int par1, CreativeTabs tab, List subItems) {
        subItems.add(new ItemStack(this, 1, 0));
        subItems.add(new ItemStack(this, 1, 1));
        //subItems.add(new ItemStack(this, 1, 2));
        //subItems.add(new ItemStack(this, 1, 3));
    }

    @Override
    public TileEntity createNewTileEntity(World world) {
        return new TileEntityFonderie();
    }

    public static void updateFonderieBlockState(boolean b, World worldObj,
            int xCoord, int yCoord, int zCoord) {
        if (b) {
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 3, 2);
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord - 1, zCoord, 2, 2);
        } else {
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, 2);
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord - 1, zCoord, 0, 2);
        }
    }

    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
        if (par1World.getBlockMetadata(par2, par3, par4) == 3) {
            par1World.getBlockMetadata(par2, par3, par4);
            float f = (float)par2 + 0.5F;
            float f1 = (float)par3 + 0.0F + par5Random.nextFloat() * 6.0F / 16.0F;
            float f2 = (float)par4 + 0.5F;
            float f4 = par5Random.nextFloat() * 0.6F - 0.3F;
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f), (double)f1 + 1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1 + 0.1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f), (double)f1 + 1 + 0.1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1 + 0.2, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("flame", (double)(f), (double)f1 + 1 + 0.2, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
        }
    }
}