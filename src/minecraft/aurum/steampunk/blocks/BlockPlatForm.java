package aurum.steampunk.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import aurum.steampunk.tileentity.TileEntityPlatForm;

public class BlockPlatForm extends BlockContainer
{
    public BlockPlatForm(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityPlatForm();
    }
}