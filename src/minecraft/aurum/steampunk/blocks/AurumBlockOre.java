package aurum.steampunk.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class AurumBlockOre extends BlockOre
{
    Icon iconBuffer[];

    public AurumBlockOre(int par1)
    {
        super(par1);
    }

    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public Icon getIcon(int par1, int par2)
    {
        return iconBuffer[par2];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        iconBuffer[0] = register.registerIcon("aurumsteampunk:orichalque_ore");
        iconBuffer[1] = register.registerIcon("aurumsteampunk:neptunite_ore");
        iconBuffer[2] = register.registerIcon("aurumsteampunk:steamvapora_ore");
        iconBuffer[3] = register.registerIcon("aurumsteampunk:astria_ore");
    }

    public int idDropped(int par1, Random par2Random, int par3, int m)
    {
        switch (m)
        {
            case 0:
                return m;
        }

        return par1;
    }

    public int quantityDroppedWithBonus(int par1, Random par2Random)
    {
        return this.quantityDropped(par2Random) + par2Random.nextInt(par1 + 1);
    }

    public int quantityDropped(Random par1Random)
    {
        return 4 + par1Random.nextInt(2);
    }
}