package aurum.steampunk.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.steampunk.AurumSteamPunk;
import aurum.steampunk.tileentity.TileEntitySteamPump;

public class BlockSteamPump extends BlockContainer{
	
	private Icon iconBuffer[];
	private int lastUpdate = 0;
	private int update     = 40; //Time between update
	
	public BlockSteamPump(int par1, Material par2Material) {
		super(par1, par2Material);
		this.setHardness(1.5F);
		this.setResistance(10.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntitySteamPump();
	}
	
    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
    	if(!world.isRemote)
    		player.openGui(AurumSteamPunk.steamPunkInstance, AurumSteamPunk.GUI_STEAMPUMP, world, x, y, z);
        return true;
    }
    
    @Override
    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
    	System.out.println("Block here !!!");
    	TileEntitySteamPump pump = (TileEntitySteamPump) par1World.getBlockTileEntity(par2, par3, par4);
		if(pump != null){
			for (int i = 0; i < pump.getSizeInventory(); i++){
                ItemStack itemstack = pump.getStackInSlot(i);
                if (itemstack != null)
                    this.dropBlockAsItem_do(par1World, par2, par3, par4, itemstack);
                pump.setInventorySlotContents(i, null);
            }
		}
    	super.breakBlock(par1World, par2, par3, par4, par5, par6);
    }
	
    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        Block block = blocksList[world.getBlockId(x, y, z)];
        if (block != null) {
        	int meta = world.getBlockMetadata(x, y, z);
            if(meta == 1)
            	return 7;
        }
        return 0;
    }
    
    @Override
    public void registerIcons(IconRegister par1) {
        iconBuffer = new Icon[2];
        iconBuffer[0] = par1.registerIcon("aurumsteampunk:fonderie1");
        iconBuffer[1] = par1.registerIcon("aurumsteampunk:fonderie3");
    }
    
    @Override
    public Icon getIcon(int par1, int par2) {
        if (par1 == 0 || par1 == 1)
            return iconBuffer[0];
        else
        	return iconBuffer[1];
    }
    
    @Override
    public int damageDropped(int metadata) {
        return 0;
    }
    
    public static void updateBlockState(boolean b, World worldObj, int xCoord, int yCoord, int zCoord) {
        if (b) {
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 1, 2);
        } else {
            worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, 0, 2);
        }
    }
    
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random) {
        if (par1World.getBlockMetadata(par2, par3, par4) == 1) {
            par1World.getBlockMetadata(par2, par3, par4);
            float f = (float)par2 + 0.5F;
            float f1 = (float)par3 + 0.0F + par5Random.nextFloat() * 6.0F / 16.0F;
            float f2 = (float)par4 + 0.5F;
            float f4 = par5Random.nextFloat() * 0.6F - 0.3F;
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1 + 0.1, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
            par1World.spawnParticle("smoke", (double)(f), (double)f1 + 1 + 0.2, (double)(f2 + f4), 0.0D, 0.0D, 0.0D);
        }
    }
}
