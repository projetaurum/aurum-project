package aurum.steampunk;

public class WeightManager
{
    public static final float PLANKS = 2.0f;
    public static final float LOG = 8.0f;
    public static final float STONE = 10.0f;
    public static final float COBBLESTONE = 9.0f;
    public static final float BRICKSTONE = 10.5f;
    public static final float CIRCLESTONE = 11.0f;
    public static final float DIRT = 4.0f;
    public static final float SAND = 3.0f;
    public static final float SANDSTONE = 12.0f;
    public static final float SNOWBLOCK = 5.0f;
    public static final float BRICK = 8.0f;
    public static final float HARDENEDCLAY = 7.5f;
    public static final float CLAY = 8.0f;
}