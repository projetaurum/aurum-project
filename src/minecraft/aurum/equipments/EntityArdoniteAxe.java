package aurum.equipments;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityArdoniteAxe extends EntityThrowable
{
    public EntityArdoniteAxe(World par1World)
    {
        super(par1World);
    }

    public EntityArdoniteAxe(World par1World, EntityPlayer par3EntityPlayer)
    {
        super(par1World, par3EntityPlayer);
    }

    public EntityArdoniteAxe(World par1World, double par2, double par4, double par6)
    {
        super(par1World, par2, par4, par6);
    }

    @Override
    protected void onImpact(MovingObjectPosition object)
    {
        if (object.entityHit != null)
        {
            object.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 4);
        }
    }
}