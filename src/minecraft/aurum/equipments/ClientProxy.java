package aurum.equipments;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        RenderingRegistry.registerEntityRenderingHandler(EntityArdoniteAxe.class, new RenderArdoniteAxe(0.5F));
    }
}