package aurum.equipments;

import net.minecraft.entity.Entity;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ArmorBase extends ItemArmor
{
    public ArmorBase(int par1, EnumArmorMaterial par2Enumarmormaterial, int par3)
    {
        super(par1, par2Enumarmormaterial, 0, par3);
    }

	@Override
	public String getArmorTexture(ItemStack itemstack, Entity entity, int slot, int layer) 
	{
        if (itemstack.itemID == AurumEquipments.bronzeHelmet.itemID || itemstack.itemID == AurumEquipments.bronzeChestPlate.itemID || itemstack.itemID == AurumEquipments.bronzeBoots.itemID)
            return "/mods/aurum/equipments/textures/items/bronze_layer_1.png";
        else
            return "/mods/aurum/equipments/textures/items/bronze_layer_2.png";
	}
}
