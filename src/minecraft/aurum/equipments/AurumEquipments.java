package aurum.equipments;

import java.util.logging.Logger;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.EnumHelper;
import aurum.core.AurumCore;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumEquipments.ID, version = AurumEquipments.VERSION, name = "AurumEquipments")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumEquipments
{
    @Instance("AurumEquipments")
    public static AurumEquipments equipmentsInstance;

    @SidedProxy(clientSide = "aurum.equipments.ClientProxy", serverSide = "aurum.equipments.CommonProxy")
    public static CommonProxy proxy;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumEquipmentsMod";

    public static final CreativeTabs equipmentsTab = AurumCore.aurumEquipmentsTab;

    public static Logger logger;

    public static MinecraftServer server;
    
    public static Item bronzeHelmet, bronzeChestPlate, bronzeLeggings, bronzeBoots;
    
    
    static EnumArmorMaterial BronzeArmor = EnumHelper.addArmorMaterial("Bronze", 13, new int[]{1, 5, 4, 1}, 18);

    public static EnumArmorMaterial bronzeArmor = EnumHelper.addArmorMaterial("bronze", 13, new int[] {1, 5, 4, 1}, 18);
    public static EnumToolMaterial bronze = EnumHelper.addToolMaterial("bronze", 2, 200, 6.0F, 3, 20);

    public static final Item itemArdoniteAxeCopper = new ItemArdoniteAxe(2401, 100, "copper")
    .setUnlocalizedName("ArdoniteAxeCopper")
    .setCreativeTab(equipmentsTab);

    // 1 .6 A corriger !
    /*public static final Item itemBronzeHelmet = new ArmorBase(2402, bronzeArmor, 0)
    .setUnlocalizedName("aurum/equipments:bronze_helmet");

    public static final Item itemBronzeChestPlate = new ArmorBase(2403, bronzeArmor, 1)
    .setUnlocalizedName("aurum/equipments:bronze_chestplate");

    public static final Item itemBronzeLeggings = new ArmorBase(2404, bronzeArmor, 2)
    .setUnlocalizedName("aurum/equipments:bronze_leggings");

    public static final Item itemBronzeBoots = new ArmorBase(2405, bronzeArmor, 3)
    .setUnlocalizedName("aurum/equipments:bronze_boots");*/

    public static final Item itemBronzeSword = new ItemSword(2406, bronze)
    .setUnlocalizedName("aurum/equipments:bronze_sword");

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
        equipmentsInstance = this;
    }

    @Init
    public void init(FMLInitializationEvent event)
    {
    	//Names
    	//LanguageRegistry.addName(itemBronzeBoots, "Bronze Boots");
    	//LanguageRegistry.addName(itemBronzeChestPlate, "Bronze Chestplate");
    	//LanguageRegistry.addName(itemBronzeHelmet, "Bronze Helmet");
    	//LanguageRegistry.addName(itemBronzeLeggings, "Bronze Leggings");
    	
    	LanguageRegistry.addName(itemBronzeSword, "Bronze Sword");
    	LanguageRegistry.addName(itemArdoniteAxeCopper, "Ardonite Copper Axe");
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabEquipments", "en_US", "Aurum Equipments");
        //Adding Entities
        EntityRegistry.registerGlobalEntityID(EntityArdoniteAxe.class, "Ardonite Axe", EntityRegistry.findGlobalUniqueEntityId());
    }
}