package aurum.backpack;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBag extends Item
{
    protected Icon[] icons;

    public ItemBag(int id)
    {
        super(id);
        setMaxStackSize(1);
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int indexInInventory, boolean isCurrentItem)
    {
        if (world.isRemote || !isCurrentItem)
        {
            return;
        }

        checkForSizeUpdate(itemStack, entity);

        if (((EntityPlayer) entity).openContainer == null || ((EntityPlayer) entity).openContainer instanceof ContainerPlayer)
        {
            return;
        }

        int containerType = containerMatchesItem(((EntityPlayer) entity).openContainer);

        if (containerType == 0)
        {
            ContainerBag myContainer = (ContainerBag)((EntityPlayer) entity).openContainer;

            if (myContainer.updateNotification)
            {
                myContainer.saveToNBT(itemStack);
                myContainer.updateNotification = false;
            }
        }
    }

    public void checkForSizeUpdate(ItemStack itemStack, Entity entity)
    {
        NBTTagCompound nbtTagCompound = itemStack.getTagCompound();

        if (nbtTagCompound == null)
        {
            nbtTagCompound = new NBTTagCompound();
            itemStack.setTagCompound(nbtTagCompound);
        }
        else if (nbtTagCompound.getInteger("increaseSize") > 0)
        {
            InventoryBag tempInv = new InventoryBag(itemStack, (EntityPlayer) entity, 0);
            tempInv.writeToNBT(nbtTagCompound);
            nbtTagCompound.setInteger("increaseSize", 0);
        }
    }

    protected int containerMatchesItem(Container openContainer)
    {
        if (ContainerBag.class.isAssignableFrom(openContainer.getClass()))
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister iconRegister)
    {
        icons = new Icon[2];
        icons[0] = iconRegister.registerIcon("");
        icons[1] = iconRegister.registerIcon("");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses()
    {
        return true;
    }
}
