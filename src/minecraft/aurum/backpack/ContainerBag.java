package aurum.backpack;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ContainerBag extends Container
{
    int invRow, invCol, colPlayer, invSize, rest;
    InventoryBag containerInv;
    boolean updateNotification;

    public ContainerBag(InventoryBag myInv, InventoryPlayer myPlayerInv)
    {
        updateNotification = false;
        containerInv = myInv;
        invSize = containerInv.getSizeInventory();

        if (invSize < 55)
        {
            if (invSize < 9)
            {
                invCol = invSize;
            }
            else
            {
                invCol = 9;
            }

            invRow = (containerInv.getSizeInventory() / invCol);
        }

        bindPlayerInventory(myPlayerInv);
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
                                            8 + j * 18, 84 + i * 18));
            }
        }

        for (int i = 0; i < 9; i++)
        {
            addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    public void setSlotLine(IInventory targetInv, int startSlot, int slots, int x, int y)
    {
        int currentX = x;

        for (int i = 0; i < slots; i++)
        {
            this.addSlotToContainer(new Slot(targetInv, startSlot + i, currentX, y));
            currentX = currentX + 18;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer myPlayer)
    {
        return true;
    }

    public void saveToNBT(ItemStack itemStack)
    {
        if (!itemStack.hasTagCompound())
        {
            itemStack.setTagCompound(new NBTTagCompound());
        }

        containerInv.writeToNBT(itemStack.getTagCompound());
    }

    @Override
    public ItemStack slotClick(int slotID, int buttonPressed, int flag, EntityPlayer player)
    {
        Slot tmpSlot;

        if (slotID >= 0 && slotID < inventorySlots.size())
        {
            tmpSlot = (Slot) inventorySlots.get(slotID);
        }
        else
        {
            tmpSlot = null;
        }

        if (tmpSlot != null && tmpSlot.isSlotInInventory(player.inventory, player.inventory.currentItem))
        {
            return tmpSlot.getStack();
        }

        updateNotification = true;
        return super.slotClick(slotID, buttonPressed, flag, player);
    }
}