package aurum.backpack;

import java.util.UUID;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryBag implements IInventory
{
    protected ItemStack[] myInventory;
    protected int size;
    protected String uniqueID;

    public InventoryBag(ItemStack itemStack, EntityPlayer myPlayer, int invSize)
    {
        uniqueID = "";

        if (!itemStack.hasTagCompound())
        {
            itemStack.stackTagCompound = new NBTTagCompound();
            uniqueID = UUID.randomUUID().toString();
        }

        size = readInvSizeFromNBT(itemStack.getTagCompound());
        myInventory = new ItemStack[size];
        readFromNBT(itemStack.getTagCompound());
    }

    @Override
    public String getInvName()
    {
        return this.uniqueID;
    }

    public int readInvSizeFromNBT(NBTTagCompound myCompound)
    {
        if (myCompound != null)
        {
            NBTTagCompound contentTag = ((NBTTagCompound) myCompound.getTag("abminventory"));

            if (contentTag == null)
            {
                return 0;
            }
        }

        return  myCompound.getInteger("invSize");
    }

    public void readFromNBT(NBTTagCompound myCompound)
    {
        NBTTagCompound contentTag = ((NBTTagCompound) myCompound.getTag("abminventory"));

        if (contentTag == null)
        {
            return;
        }

        if ("".equals(uniqueID))
        {
            uniqueID = myCompound.getString("uniqueID");

            if ("".equals(uniqueID))
            {
                uniqueID = UUID.randomUUID().toString();
            }
        }

        NBTTagList myList = contentTag.getTagList("indexList");

        for (int i = 0; i < myList.tagCount() && i < myInventory.length; i++)
        {
            NBTTagCompound indexTag = (NBTTagCompound) myList.tagAt(i);
            int index = indexTag.getInteger("index");

            try
            {
                myInventory[index] = ItemStack.loadItemStackFromNBT(indexTag);
            }
            catch (NullPointerException npe)
            {
                myInventory[index] = null;
            }
        }
    }

    public void writeToNBT(NBTTagCompound myCompound)
    {
        NBTTagList myList = new NBTTagList();

        for (int i = 0; i < this.myInventory.length; i++)
        {
            if (this.myInventory[i] != null && this.myInventory[i].stackSize > 0)
            {
                NBTTagCompound indexTag = new NBTTagCompound();
                myList.appendTag(indexTag);
                indexTag.setInteger("index", i);
                myInventory[i].writeToNBT(indexTag);
            }
        }

        NBTTagCompound contentTag = new NBTTagCompound();
        contentTag.setTag("indexList", myList);
        myCompound.setTag("abminventory", contentTag);
        myCompound.setString("uniqueID", uniqueID);
        myCompound.setInteger("invSize", size);
    }

    @Override
    public int getSizeInventory()
    {
        return this.myInventory.length;
    }

    @Override
    public ItemStack getStackInSlot(int var1)
    {
        return myInventory[var1];
    }

    @Override
    public ItemStack decrStackSize(int slot, int number)
    {
        if (myInventory[slot] == null)
        {
            return null;
        }

        ItemStack returnStack;

        if (myInventory[slot].stackSize > number)
        {
            returnStack = myInventory[slot].splitStack(number);
        }
        else
        {
            returnStack = myInventory[slot];
            myInventory[slot] = null;
        }

        onInventoryChanged();
        return returnStack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot)
    {
        ItemStack returnStack = getStackInSlot(slot);
        setInventorySlotContents(slot, null);
        return returnStack;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemStack)
    {
        if (0 <= slot && slot < size)
        {
            myInventory[slot] = itemStack;
        }
    }

    @Override
    public void onInventoryChanged()
    {
        for (int i = 0; i < size; i++)
        {
            ItemStack tempStack = getStackInSlot(i);

            if (tempStack != null && tempStack.stackSize == 0)
            {
                setInventorySlotContents(i, null);
            }
        }
    }

    public void increaseSize(int i)
    {
        ItemStack[] newInventory = new ItemStack[size + i];
        System.arraycopy(myInventory, 0, newInventory, 0, size);
        myInventory = newInventory;
        size = size + i;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer myPlayer)
    {
        return true;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public void openChest()
    { }

    @Override
    public void closeChest()
    { }

    @Override
    public boolean isInvNameLocalized()
    {
        return false;
    }

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) 
	{
		return true;
	}
}
