package aurum.sylve.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemBlockFlowers extends ItemBlock
{
    Icon iconBuffer[];

    public ItemBlockFlowers(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockFlowers");
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        iconBuffer[0] = register.registerIcon("aurum/sylve:azurin");
        iconBuffer[1] = register.registerIcon("aurum/sylve:poppy");
        iconBuffer[2] = register.registerIcon("aurum/sylve:whitepoppy");
        iconBuffer[3] = register.registerIcon("aurum/sylve:violet");
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Azurin";
                break;

            case 1:
                name = "Poppy";
                break;

            case 2:
                name = "White Poppy";
                break;

            case 3:
                name = "Violet";
                break;
        }

        return name;
    }
}
