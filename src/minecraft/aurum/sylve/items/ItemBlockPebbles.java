package aurum.sylve.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.Icon;

public class ItemBlockPebbles extends ItemBlock
{
    Icon iconBuffer[];

    public ItemBlockPebbles(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockPebbles");
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        iconBuffer[0] = register.registerIcon("aurum/sylve:pebbles1");
        iconBuffer[1] = register.registerIcon("aurum/sylve:pebbles2");
        iconBuffer[2] = register.registerIcon("aurum/sylve:pebbles3");
        iconBuffer[3] = register.registerIcon("aurum/sylve:pebbles4");
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }
}
