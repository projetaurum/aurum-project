package aurum.sylve.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemBlockBerryBush extends ItemBlock
{
    Icon iconBuffer[];

    public ItemBlockBerryBush(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("aurum:ItemBlockBerryBush");
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[6];
        this.iconBuffer[0] = register.registerIcon("aurum/sylve:bush");
        this.iconBuffer[1] = register.registerIcon("aurum/sylve:berry_black");
        this.iconBuffer[2] = register.registerIcon("aurum/sylve:berry_blue");
        this.iconBuffer[3] = register.registerIcon("aurum/sylve:berry_red");
        this.iconBuffer[4] = register.registerIcon("aurum/sylve:berry_white");
        this.iconBuffer[5] = register.registerIcon("aurum/sylve:berry_yellow");
    }

    @Override
    public Icon getIconFromDamage(int meta)
    {
        return iconBuffer[meta];
    }
    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        String name = "";

        switch (stack.getItemDamage())
        {
            case 0:
                name =  "Berry Bush";
                break;

            case 1:
                name = "Black Berry Bush";
                break;

            case 2:
                name = "Blue Berry Bush";
                break;

            case 3:
                name = "Red Berry Bush";
                break;

            case 4:
                name = "White Berry Bush";
                break;

            case 5:
                name = "Yellow Berry Bush";
                break;
        }

        return name;
    }
}