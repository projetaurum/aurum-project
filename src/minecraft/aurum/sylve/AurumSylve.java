package aurum.sylve;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;
import aurum.sylve.blocks.BlockBerryBush;
import aurum.sylve.blocks.BlockFlowers;
import aurum.sylve.blocks.BlockHay;
import aurum.sylve.blocks.BlockPebbles;
import aurum.sylve.items.ItemBlockBerryBush;
import aurum.sylve.items.ItemBlockFlowers;
import aurum.sylve.items.ItemBlockPebbles;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumSylve.ID, version = AurumSylve.VERSION, name = "AurumSylve", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumSylve
{
    @Instance("AurumSylve")
    public static AurumSylve SylveInstance;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumSylveMod";

    @SidedProxy(clientSide = "aurum.sylve.ClientProxy", serverSide = "aurum.sylve.CommonProxy")
    public static CommonProxy proxy;

    public static final CreativeTabs sylveTab = AurumCore.aurumSylveTab;

    public static Logger logger;

    public static MinecraftServer server;

    public static final Block blockBerryBush = new BlockBerryBush(2201)
    .setCreativeTab(sylveTab)
    .setStepSound(Block.soundGrassFootstep);

    public static final Block blockPebbles = new BlockPebbles(2202)
    .setCreativeTab(sylveTab)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockFlowers = new BlockFlowers(2203)
    .setCreativeTab(sylveTab)
    .setStepSound(Block.soundGrassFootstep);

    public static final Block blockHay = new BlockHay(2204)
    .setCreativeTab(sylveTab)
    .setStepSound(Block.soundGrassFootstep)
    .setUnlocalizedName("BlockHay");

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
    }

    @Init
    public void init(FMLInitializationEvent event)
    {
        //Blocks
        AurumRegistry.registerBlock(blockHay, "Hay Block");
        //ItemBlocks & Blocks
        AurumRegistry.registerBlock(blockBerryBush, ItemBlockBerryBush.class, "Berry Bush");
        AurumRegistry.registerBlock(blockFlowers, ItemBlockFlowers.class, "Flowers");
        AurumRegistry.registerBlock(blockPebbles, ItemBlockPebbles.class, "Pebbles");
        //Adding TileEntities
        //Adding Items
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabSylve", "en_US", "Aurum Sylve");
        //Adding Entities
        //Other
        MinecraftForge.addGrassPlant(blockFlowers, 0, 10);
        MinecraftForge.addGrassPlant(blockFlowers, 1, 10);
        MinecraftForge.addGrassPlant(blockFlowers, 2, 2);
        MinecraftForge.addGrassPlant(blockFlowers, 3, 15);
        //Recipes
        registerRecipes();
    }

    public final void registerRecipes()
    {
        GameRegistry.addRecipe(new ItemStack(Item.dyePowder, 2, 5),
                               new Object[] {"X", 'X', new ItemStack(AurumSylve.blockFlowers, 1, 3)});
        GameRegistry.addRecipe(new ItemStack(AurumSylve.blockHay, 1),
                               new Object[] {"XXX", "XXX", "XXX", 'X', Item.wheat});
        GameRegistry.addRecipe(new ItemStack(Item.wheat, 9),
                               new Object[] {"X", 'X', new ItemStack(AurumSylve.blockHay, 1)});
    }
}