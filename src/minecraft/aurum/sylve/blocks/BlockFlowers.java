package aurum.sylve.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockFlowers extends BlockFlower
{
    Icon iconBuffer[];

    public static BlockFlowers instance;

    public BlockFlowers(int par1)
    {
        super(par1, Material.plants);
        instance = this;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        iconBuffer[0] = register.registerIcon("aurumsylve:azurin");
        iconBuffer[1] = register.registerIcon("aurumsylve:poppy");
        iconBuffer[2] = register.registerIcon("aurumsylve:whitepoppy");
        iconBuffer[3] = register.registerIcon("aurumsylve:violet");
    }

    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public int quantityDropped(int meta, int fortune, Random rand)
    {
        if (meta == 0)
        {
            return 1;
        }

        if (meta == 3)
        {
            return 1;
        }

        return 0;
    }
}
