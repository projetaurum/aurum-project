package aurum.sylve.blocks;

import java.util.Random;

import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockPebbles extends BlockFlower
{
    Icon iconBuffer[];

    public BlockPebbles(int par1)
    {
        super(par1, Material.rock);
        float f = 0.5F;
        float f1 = 0.015625F;
        this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f1, 0.5F + f);
        this.setUnlocalizedName("aurumsylve:pebbles");
    }

    @Override
    public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
    {
        return true;
    }

    @Override
    public void onPostBlockPlaced(World w, int x, int y, int z, int meta)
    {
        Random rand = new Random();
        int alea = rand.nextInt(9);
        w.setBlockMetadataWithNotify(x, y, z, alea / 3, 2);
        super.onPostBlockPlaced(w, x, y, z, meta);
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        iconBuffer[0] = register.registerIcon("aurumsylve:pebbles1");
        iconBuffer[1] = register.registerIcon("aurumsylve:pebbles2");
        iconBuffer[2] = register.registerIcon("aurumsylve:pebbles3");
        iconBuffer[3] = register.registerIcon("aurumsylve:pebbles4");
    }

    @Override
    public int quantityDropped(int meta, int fortune, Random rand)
    {
        return 0;
    }

    public int getRenderType()
    {
        return 23;
    }
}