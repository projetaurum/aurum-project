package aurum.sylve.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockHay extends Block
{
    Icon iconBuffer[];

    public BlockHay(int par1)
    {
        super(par1, Material.grass);
    }

    public int getRenderType()
    {
        return 31;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        iconBuffer = new Icon[2];
        iconBuffer[0] = register.registerIcon("aurumsylve:hay_block_top");
        iconBuffer[1] = register.registerIcon("aurumsylve:hay_block_side");
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        int k = meta & 12;
        return k == 0 && (side == 1 || side == 0) ? this.iconBuffer[0] :
               (k == 4 && (side == 5 || side == 4) ? this.iconBuffer[0] :
                (k == 8 && (side == 2 || side == 3) ? this.iconBuffer[0] :
                 this.iconBuffer[1]));
    }

    public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata)
    {
        byte b0 = 0;

        switch (side)
        {
            case 0:
            case 1:
                b0 = 0;
                break;

            case 2:
            case 3:
                b0 = 8;
                break;

            case 4:
            case 5:
                b0 = 4;
        }

        return b0;
    }
}
