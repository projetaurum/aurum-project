package aurum.sylve.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class BlockBerryBush extends Block
{
    Icon iconBuffer[];

    public BlockBerryBush(int par1)
    {
        super(par1, Material.cactus);
        setUnlocalizedName("aurum/sylve:berrybush");
        float f = 0.4F;
        this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
    }

    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[6];
        this.iconBuffer[0] = register.registerIcon("aurumsylve:bush");
        this.iconBuffer[1] = register.registerIcon("aurumsylve:berry_black");
        this.iconBuffer[2] = register.registerIcon("aurumsylve:berry_blue");
        this.iconBuffer[3] = register.registerIcon("aurumsylve:berry_red");
        this.iconBuffer[4] = register.registerIcon("aurumsylve:berry_white");
        this.iconBuffer[5] = register.registerIcon("aurumsylve:berry_yellow");
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
        list.add(new ItemStack(this.blockID, 1, 5));
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int getRenderType()
    {
        return 1;
    }
}