package aurum.stockage;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;
import aurum.core.ItemAurum;
import aurum.monnaie.BlockEcuEchangeur;
import aurum.monnaie.ItemBourse;
import aurum.stockage.block.BlockCaisse;
import aurum.stockage.block.BlockCaisseGag;
import aurum.stockage.block.BlockPiedestal;
import aurum.stockage.gui.StockageGuiHandler;
import aurum.stockage.items.ItemBlockCaisse;
import aurum.stockage.items.ItemBlockPiedestal;
import aurum.stockage.items.ItemKey;
import aurum.stockage.items.ItemLock;
import aurum.stockage.tileentity.AurumChunkWatchEvent;
import aurum.stockage.tileentity.TileEntityCaisse;
import aurum.stockage.tileentity.TileEntityCaisseGag;
import aurum.stockage.tileentity.TileEntityPiedestal;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumStockage.ID, version = AurumStockage.VERSION, name = "AurumStockage", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumStockage
{
    @Instance("AurumStockage")
    public static AurumStockage stockageInstance;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumStockageMod";

    public static final CreativeTabs stockageTab = AurumCore.aurumStockageTab;

    @SidedProxy(clientSide = "aurum.stockage.ClientProxy", serverSide = "aurum.stockage.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    public static MinecraftServer server;

    public static final Block blockPiedestal = new BlockPiedestal(3100)
    .setUnlocalizedName("Piedestal")
    .setCreativeTab(stockageTab)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockCaisse = new BlockCaisse(3101)
    .setCreativeTab(stockageTab)
    .setUnlocalizedName("Caisse")
    .setHardness(2.5F)
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockEcuEchangeur = new BlockEcuEchangeur(3103, Material.wood)
    .setCreativeTab(stockageTab)
    .setUnlocalizedName("EcuEchangeur")
    .setHardness(2.0F)
    .setStepSound(Block.soundWoodFootstep);

    public static final Block blockCaisseGag = new BlockCaisseGag(3104)
    .setHardness(2.5f)
    .setStepSound(Block.soundWoodFootstep);

    public static final Item itemBourse = new ItemBourse(10001)
    .setCreativeTab(stockageTab)
    .setUnlocalizedName("Bourse");

    public static final Item itemIronKey = new ItemKey(10002, 256, "key_iron")
    .setUnlocalizedName("Iron Key");

    public static final Item itemGoldKey = new ItemKey(10003, 256, "key_gold")
    .setUnlocalizedName("Gold Key");

    public static final Item itemIronLock = new ItemLock(10004, 256, "lock_iron")
    .setUnlocalizedName("Iron Lock");

    public static final Item itemGoldLock = new ItemLock(10005, 256, "lock_gold")
    .setUnlocalizedName("Gold Lock");

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        stockageInstance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
    }

    @Init
    public void init(FMLInitializationEvent event)
    {
        //Blocks
        AurumRegistry.registerBlock(blockEcuEchangeur, "Ecu Echangeur");
        AurumRegistry.registerBlock(blockCaisseGag, "Caisse Gag");
        
        //ItemBlocks & Blocks
        AurumRegistry.registerBlock(blockCaisse, ItemBlockCaisse.class, "Caisse");
        AurumRegistry.registerBlock(blockPiedestal, ItemBlockPiedestal.class, "Piedestal");
        
        //Adding TileEntities
        GameRegistry.registerTileEntity(TileEntityCaisse.class, "Caisse");
        GameRegistry.registerTileEntity(TileEntityPiedestal.class, "TileEntityPiedestal");
        GameRegistry.registerTileEntity(TileEntityCaisseGag.class, "TileEntityCaisseGag");
        //Adding Items
        AurumRegistry.registerItem(itemBourse, "Bourse");
        AurumRegistry.registerItem(itemIronKey, "Iron Key");
        AurumRegistry.registerItem(itemGoldKey, "Gold Key");
        AurumRegistry.registerItem(itemIronLock, "Iron Lock");
        AurumRegistry.registerItem(itemGoldLock, "Gold Lock");
        
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabStockage", "en_US", "Aurum Stockage");
        
        //GuiHandler
        NetworkRegistry.instance().registerGuiHandler(this, new StockageGuiHandler());
        MinecraftForge.EVENT_BUS.register(new AurumChunkWatchEvent());
        
        //Recipes
        registerRecipes();
    }

    public final void registerRecipes()
    {
        GameRegistry.addRecipe(new ItemStack(AurumStockage.blockCaisse, 1),
                               new Object[] {"UXU", "XOX", "UXU",
                                             'X', Block.planks,
                                             'U', Block.fenceIron,
                                             'O', Block.chest
                                            });
        GameRegistry.addRecipe(new ItemStack(AurumStockage.blockCaisse, 1, 1),
                new Object[] {"X","X",
                              'X', new ItemStack(AurumStockage.blockCaisse, 1)
                             });
        GameRegistry.addRecipe(new ItemStack(AurumStockage.blockCaisse, 1, 2),
                new Object[] {"XX","XX",
                              'X', new ItemStack(AurumStockage.blockCaisse, 1)
                             });
        GameRegistry.addRecipe(new ItemStack(AurumStockage.blockCaisse, 1, 2),
                new Object[] {"XX",
                              'X', new ItemStack(AurumStockage.blockCaisse, 1, 1)
                             });
        GameRegistry.addRecipe(new ItemStack(AurumStockage.itemBourse, 1),
                               new Object[] {"XXX", "X X", "XXX", 'X', Item.leather});
    }
}