package aurum.stockage.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.stockage.tileentity.TileEntityCaisse;

public class GuiCaisse extends GuiContainer
{
    private TileEntityCaisse tile;

    public GuiCaisse(Container par1Container, TileEntity entity)
    {
        super(par1Container);
        this.tile = (TileEntityCaisse) entity;

        if (tile.getCaisseType() == 0)
        {
            this.xSize += 8;
            this.ySize += 20;
        }
        else if (tile.getCaisseType() == 1)
        {
            this.xSize += 27;
            this.ySize += 82;
        }
        else if (tile.getCaisseType() == 2)
        {
            this.xSize += 72;
            this.ySize += 90;
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int i, int j)
    {
        if (tile.getCaisseType() == 0)
        {
            this.fontRenderer.drawString("Caisse", 5, this.ySize - 190, 4210752);
        }
        else if (tile.getCaisseType() == 1)
        {
            this.fontRenderer.drawString("Caisse Double", 5, this.ySize - 241, 4210752);
        }
        else if (tile.getCaisseType() == 2)
        {
            this.fontRenderer.drawString("Caisse Quad", -3, this.ySize - 252, 4210752);
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;

        if (tile.getCaisseType() == 0)
        {
            this.mc.renderEngine.bindTexture(new ResourceLocation("aurumstockage","textures/gui/caisse.png"));
            this.drawTexturedModalRect(x - 4, y - 11, 0, 0, xSize, ySize);
        }
        else if (tile.getCaisseType() == 1)
        {
            this.mc.renderEngine.bindTexture(new ResourceLocation("aurumstockage","textures/gui/caisse2.png"));
            this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
        }
        else if (tile.getCaisseType() == 2)
        {
            this.mc.renderEngine.bindTexture(new ResourceLocation("aurumstockage","textures/gui/caisse3.png"));
            this.drawTexturedModalRect(x - 10/*-36*/, y/*-48*/, 0, 0, xSize, ySize);
            //drawTexturedQuadFit(x+4,y-3,256,254,0);
        }
    }

    public static void drawTexturedQuadFit(double x, double y, double width, double height, double zLevel)
    {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV(x + 0, y + height, zLevel, 0, 1);
        tessellator.addVertexWithUV(x + width, y + height, zLevel, 1, 1);
        tessellator.addVertexWithUV(x + width, y + 0, zLevel, 1, 0);
        tessellator.addVertexWithUV(x + 0, y + 0, zLevel, 0, 0);
        tessellator.draw();
    }
}