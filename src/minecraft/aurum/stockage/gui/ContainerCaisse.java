package aurum.stockage.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.stockage.tileentity.TileEntityCaisse;

public class ContainerCaisse extends Container
{
    protected TileEntityCaisse tileEntity;
    protected int nbrSlot = 0;

    public ContainerCaisse(TileEntityCaisse tileEntityCaisse, InventoryPlayer player, World world)
    {
        this.tileEntity = tileEntityCaisse;

        if (tileEntity.getCaisseType() == 0)
        {
            this.nbrSlot = 32;
            bindPlayerInventory(player, 0, -10);
            int i;

            for (i = 0; i < 4; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    this.addSlotToContainer(new Slot(tileEntityCaisse, j + i * 8, 17 + j * 18, 7 + i * 18));
                }
            }
        }
        else if (tileEntity.getCaisseType() == 1)
        {
            this.nbrSlot = 64;
            bindPlayerInventory(player, 13, 73);
            int i;

            for (i = 0; i < 8; ++i)
            {
                for (int j = 0; j < 8; ++j)
                {
                    this.addSlotToContainer(new Slot(tileEntityCaisse, j + i * 8, 30 + j * 18, 18 + i * 18));
                }
            }
        }
        else if (tileEntity.getCaisseType() == 2)
        {
            this.nbrSlot = 117;
            bindPlayerInventory(player, 26, 84);
            int i;

            for (i = 0; i < 9; ++i)
            {
                for (int j = 0; j < 13; ++j)
                {
                    this.addSlotToContainer(new Slot(tileEntityCaisse, j + i * 13, -2 + j * 18, 13 + i * 18));
                }
            }
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer var1)
    {
        return tileEntity.isUseableByPlayer(var1);
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer, int sourceX, int sourceY)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
                                            8 + j * 18 + sourceX, 84 + 9 + i * 18 + sourceY));
            }
        }

        for (int i = 0; i < 9; i++)
        {
            addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18 + sourceX, 142 + 9 + sourceY));
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack stack = null;
        Slot slotObject = (Slot) inventorySlots.get(slot);

        if (slotObject != null && slotObject.getHasStack())
        {
            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();

            if (slot >= 36)
            {
                if (!this.mergeItemStack(stackInSlot, 0, 36, true))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(stackInSlot, 36, nbrSlot + 36, false))
            {
                return null;
            }

            if (stackInSlot.stackSize == 0)
            {
                slotObject.putStack(null);
            }
            else
            {
                slotObject.onSlotChanged();
            }

            if (stackInSlot.stackSize == stack.stackSize)
            {
                return null;
            }

            slotObject.onPickupFromSlot(player, stackInSlot);
        }

        return stack;
    }
}