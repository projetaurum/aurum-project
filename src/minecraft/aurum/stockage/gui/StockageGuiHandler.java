package aurum.stockage.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import aurum.stockage.tileentity.TileEntityCaisse;
import cpw.mods.fml.common.network.IGuiHandler;

public class StockageGuiHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

        if (id == 0)
        {
            return new ContainerCaisse((TileEntityCaisse) tileEntity, player.inventory, world);
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

        if (id == 0)
        {
            return new GuiCaisse(new ContainerCaisse((TileEntityCaisse) tileEntity, player.inventory, world), tileEntity);
        }

        return null;
    }
}