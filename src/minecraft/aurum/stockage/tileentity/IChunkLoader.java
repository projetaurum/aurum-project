package aurum.stockage.tileentity;

import cpw.mods.fml.common.network.Player;

public interface IChunkLoader
{
    void onChunkLoad(Player player);
}