package aurum.stockage.tileentity;

import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.ChunkWatchEvent;
import cpw.mods.fml.common.network.Player;
import java.util.HashMap;

public class AurumChunkWatchEvent
{
    @SuppressWarnings("rawtypes")
    @ForgeSubscribe
    public void chunkLoaded(ChunkWatchEvent event)
    {
        HashMap teMap = (HashMap)event.player.worldObj.getChunkFromChunkCoords(event.chunk.chunkXPos, event.chunk.chunkZPos).chunkTileEntityMap;

        for (Object tileEntity: teMap.values())
        {
            if (tileEntity instanceof IChunkLoader)
            {
                System.out.println(tileEntity.getClass());
                IChunkLoader clTileEntity = (IChunkLoader) tileEntity;
                Player chunkLoadingPlayer = (Player)event.player;
                clTileEntity.onChunkLoad(chunkLoadingPlayer);
            }
        }
    }
}