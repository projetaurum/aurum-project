package aurum.stockage.block;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.core.ClientProxy;
import aurum.stockage.AurumStockage;
import aurum.stockage.tileentity.TileEntityCaisse;

public class BlockCaisse extends BlockContainer
{
	private final Random random = new Random();

	Icon iconBuffer[];

	/*
	 * Metadata :
	 * 	0 Caisse simple
	 * 	1 Caisse double bas/haut
	 * 		4 Caisse double devant/derriere
	 * 		5 Caisse double gauche/droite
	 * 	2 Caisse quad plat
	 * 		6 Caisse quad mur devant/derriere
	 * 		7 Caisse quad mur gauche/droite
	 */

	public BlockCaisse(int par1)
	{
		super(par1, Material.wood);
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
	{
		return false;
	}
	
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ClientProxy.renderInventoryTESRID;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@SuppressWarnings( { "unchecked", "rawtypes" })
	@Override
	public void getSubBlocks(int par1, CreativeTabs tab, List list)
	{
		list.add(new ItemStack(par1, 1, 0));
		list.add(new ItemStack(par1, 1, 1));
		list.add(new ItemStack(par1, 1, 2));
	}

	@Override
	public void registerIcons(IconRegister register)
	{
		this.blockIcon = register.registerIcon("aurumstockage:caisse");
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
	{
		if (player.isSneaking())
		{
			return true;
		}

		player.openGui(AurumStockage.stockageInstance, 0, world, x, y, z);
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return new TileEntityCaisse();
	}

	public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
	{
		TileEntityCaisse tileentitycaisse = (TileEntityCaisse)par1World.getBlockTileEntity(par2, par3, par4);

		if (tileentitycaisse != null)
		{
			for (int j1 = 0; j1 < tileentitycaisse.getSizeInventory(); ++j1)
			{
				ItemStack itemstack = tileentitycaisse.getStackInSlot(j1);

				if (itemstack != null)
				{
					float f = this.random.nextFloat() * 0.8F + 0.1F;
					float f1 = this.random.nextFloat() * 0.8F + 0.1F;
					EntityItem entityitem;

					for (float f2 = this.random.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; par1World.spawnEntityInWorld(entityitem))
					{
						int k1 = this.random.nextInt(21) + 10;

						if (k1 > itemstack.stackSize)
						{
							k1 = itemstack.stackSize;
						}

						itemstack.stackSize -= k1;
						entityitem = new EntityItem(par1World, (double)((float)par2 + f), (double)((float)par3 + f1), (double)((float)par4 + f2), new ItemStack(itemstack.itemID, k1, itemstack.getItemDamage()));
						float f3 = 0.05F;
						entityitem.motionX = (double)((float)this.random.nextGaussian() * f3);
						entityitem.motionY = (double)((float)this.random.nextGaussian() * f3 + 0.2F);
						entityitem.motionZ = (double)((float)this.random.nextGaussian() * f3);

						if (itemstack.hasTagCompound())
						{
							entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
						}
					}
				}
			}

			par1World.func_96440_m(par2, par3, par4, par5);
		}

		super.breakBlock(par1World, par2, par3, par4, par5, par6);
	}
}