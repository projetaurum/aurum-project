package aurum.stockage.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.stockage.AurumStockage;
import aurum.stockage.tileentity.TileEntityCaisse;
import aurum.stockage.tileentity.TileEntityCaisseGag;

public class BlockCaisseGag extends BlockContainer
{
    public BlockCaisseGag(int par1)
    {
        super(par1, Material.wood);
        this.setUnlocalizedName("aurum/stockage:caisse");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        if (player.isSneaking())
        {
            return true;
        }

        TileEntityCaisseGag gag = (TileEntityCaisseGag) world.getBlockTileEntity(x, y, z);
        
        if(world.getBlockTileEntity(gag.getSourceX(), gag.getSourceY(), gag.getSourceZ()) instanceof TileEntityCaisse)
        	player.openGui(AurumStockage.stockageInstance, 0, world, gag.getSourceX(), gag.getSourceY(), gag.getSourceZ());
        return true;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int par5, int par6)
    {
        TileEntityCaisseGag tileEntity = (TileEntityCaisseGag)world.getBlockTileEntity(x, y, z);

        if (tileEntity != null)
        {
            world.destroyBlock(tileEntity.getSourceX(), tileEntity.getSourceY(), tileEntity.getSourceZ(), false);
            world.removeBlockTileEntity(tileEntity.getSourceX(), tileEntity.getSourceY(), tileEntity.getSourceZ());
        }

        world.removeBlockTileEntity(x, y, z);
    }

    @Override
    public void onNeighborBlockChange(World world, int i, int j, int k, int par5)
    {
        TileEntityCaisseGag tileEntity = (TileEntityCaisseGag)world.getBlockTileEntity(i, j, k);

        if (tileEntity != null)
        {
            if (world.getBlockId(tileEntity.getSourceX(), tileEntity.getSourceY(),
                                 tileEntity.getSourceZ()) != AurumStockage.blockCaisse.blockID)
            {
                world.destroyBlock(i, j, k, false);
                world.removeBlockTileEntity(i, j, k);
            }
        }
    }

    @Override
    public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        return false;
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityCaisseGag();
    }
}