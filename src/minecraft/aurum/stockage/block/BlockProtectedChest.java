package aurum.stockage.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;

public class BlockProtectedChest extends BlockContainer
{
    public BlockProtectedChest(int par1)
    {
        super(par1, Material.wood);
    }

    @Override
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1, int par2, int par3, int par4)
    {
        return null;
    }

    public void onBlockHighlight(DrawBlockHighlightEvent event)
    {
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return null;
    }
}