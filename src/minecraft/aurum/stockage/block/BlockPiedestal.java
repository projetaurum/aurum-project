package aurum.stockage.block;

import java.util.List;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.core.ClientProxy;
import aurum.stockage.tileentity.TileEntityPiedestal;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockPiedestal extends BlockContainer
{
    public BlockPiedestal(int par1)
    {
        super(par1, Material.rock);
    }
    
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ClientProxy.renderInventoryTESRID;
	}

    @Override
    public void onBlockDestroyedByExplosion(World world, int x, int y, int z, Explosion exp)
    {
        TileEntityPiedestal entity = (TileEntityPiedestal) world.getBlockTileEntity(x, y, z);

        if (entity.getStackInSlot(0) != null)
        {
            this.dropBlockAsItem_do(world, x, y, z, entity.getStackInSlot(0));
        }

        super.onBlockDestroyedByExplosion(world, x, y, z, exp);
    }

    public int damageDropped(int par1)
    {
        /*
         * INCOMPLET !!
         */
        return par1;
    }

    @Override
    public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int metadata)
    {
        TileEntityPiedestal entity = (TileEntityPiedestal) world.getBlockTileEntity(x, y, z);

        if (entity != null && entity.getStackInSlot(0) != null)
        {
            this.dropBlockAsItem_do(world, x, y, z, entity.getStackInSlot(0));
        }

        super.onBlockDestroyedByPlayer(world, x, y, z, metadata);
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        TileEntityPiedestal entity = (TileEntityPiedestal) world.getBlockTileEntity(x, y, z);
        ItemStack stack = player.getCurrentEquippedItem();
        ItemStack contains = entity.getStackInSlot(0);

        if (stack == null)
        {
            if (contains != null)
            {
                contains.stackSize = 1;
                player.inventory.addItemStackToInventory(contains);
                entity.setInventorySlotContents(0, null, true);
                return true;
            }
        }

        if (contains == null)
        {
            if (stack == null || (stack.itemID < 255) || (stack.itemID > 3000 && stack.itemID < 4096))
            {
                return true;
            }

            ItemStack stack2 = ItemStack.copyItemStack(stack);
            stack2.stackSize = 1;
            entity.setInventorySlotContents(0, stack2, true);

            if (player.inventory.getStackInSlot(player.inventory.currentItem).stackSize == 1)
            {
                player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
                return true;
            }

            player.inventory.decrStackSize(player.inventory.currentItem, 1);
        }

        return true;
    }

    public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        return false;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    @Override
    public void onBlockPlacedBy(World world, int i, int j, int k, EntityLivingBase entityliving, ItemStack stack)
    {
        int rotation = MathHelper.floor_double((double)((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
        int metadata = world.getBlockMetadata(i, j, k) & 4;
        int sens = 0;

        if (rotation == 0)
        {
            sens = 0;
        }

        if (rotation == 1)
        {
            sens = 1;
        }

        if (rotation == 2)
        {
            sens = 2;
        }

        if (rotation == 3)
        {
            sens = 3;
        }

        world.setBlock(i, j, k, this.blockID, sens | metadata , 3);
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityPiedestal();
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(int par1, CreativeTabs tab, List subItems)
    {
        subItems.add(new ItemStack(this, 1, 0));
        subItems.add(new ItemStack(this, 1, 4));
        subItems.add(new ItemStack(this, 1, 8));
    }
}