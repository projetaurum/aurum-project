package aurum.stockage.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;

public class ModelPiedestal extends ModelBase
{
    ModelRenderer Base;
    ModelRenderer Pilier;
    ModelRenderer Plaque;
    RenderItem Item;

    public ModelPiedestal()
    {
        textureWidth = 64;
        textureHeight = 64;
        Base = new ModelRenderer(this, 0, 0);
        Base.addBox(0F, 0F, 0F, 10, 2, 10);
        Base.setRotationPoint(-5F, 22F, -5F);
        Base.setTextureSize(64, 64);
        Base.mirror = true;
        setRotation(Base, 0F, 0F, 0F);
        Pilier = new ModelRenderer(this, 0, 22);
        Pilier.addBox(0F, 0F, 0F, 4, 12, 4);
        Pilier.setRotationPoint(-2F, 10F, -2F);
        Pilier.setTextureSize(64, 64);
        Pilier.mirror = true;
        setRotation(Pilier, 0F, 0F, 0F);
        Plaque = new ModelRenderer(this, 0, 12);
        Plaque.addBox(0F, 0F, 0F, 8, 1, 9);
        Plaque.setRotationPoint(-4F, 9F, -4F);
        Plaque.setTextureSize(64, 64);
        Plaque.mirror = true;
        setRotation(Plaque, -0.1570796F, 0F, 0F);
    }

    public void render(TileEntity tileentity, float f5)
    {
        Base.render(f5);
        Pilier.render(f5);
        Plaque.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }

    public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
    {
        super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    }
}