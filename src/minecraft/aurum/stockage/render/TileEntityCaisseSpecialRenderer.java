package aurum.stockage.render;

import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.core.multiblocks.IInventoryRenderer;

public class TileEntityCaisseSpecialRenderer extends TileEntitySpecialRenderer implements IInventoryRenderer
{
    public TileEntityCaisseSpecialRenderer() 
    {
		this.setTileEntityRenderer(TileEntityRenderer.instance);
    }

    @Override
    public void renderTileEntityAt(TileEntity tileentity, double x, double y,
                                   double z, float f)
    {
        renderCaisse(tileentity.getBlockMetadata(), x, y, z, f, false);
    }

    public void renderCaisse(int meta, double x, double y, double z, float f, boolean isInventory)
    {
        if (meta == 0)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x + 0.5f, (float)y - 0.5f, (float)z + 0.5f);
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_model.png"));
            new ModelCube().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 1)
        {
            GL11.glPushMatrix();
            
            if(!isInventory)
            	GL11.glTranslatef((float)x + 0.5f, (float)y - 0.5f, (float)z + 0.5f);
            else
            {
            	GL11.glTranslatef((float)x + 0.5f, (float)y + 0.25f, (float)z + 0.5f);
            	GL11.glScalef(1f, .5f, 1f);
            }
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_double_model.png"));
            new ModelDoubleCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 2)
        {
            GL11.glPushMatrix();
            
            if(!isInventory)
            	GL11.glTranslatef((float)x + 0.5f, (float)y - 0.5f, (float)z + 0.5f);
            else
            {
            	GL11.glTranslatef((float)x + 0.5f, (float)y + 0.25f, (float)z + 0.5f);
            	GL11.glScalef(.5f, .5f, .5f);
            }
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_quad_model.png"));
            new ModelQuadCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 4)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f);
            GL11.glRotatef(90.0f, -1.0f, 0.0f, 0.0f);
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_double_model.png"));
            new ModelDoubleCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 5)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x + 0.5f, (float)y + 0.5f, (float)z + 0.5f);
            GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_double_model.png"));
            new ModelDoubleCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 6)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x - 0.5f, (float)y + 1.5f, (float)z - 0.5f);
            GL11.glRotatef(90f, 1.0f, 0.0f, 0.0f);
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_quad_model.png"));
            new ModelQuadCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
        else if (meta == 7)
        {
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x + 1.5f, (float)y + 1.5f, (float)z - 0.5f);
            GL11.glRotatef(90f, 1.0f, 0.0f, 0.0f);
            GL11.glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/caisse_quad_model.png"));
            new ModelQuadCaisse().render(0.0625F);
            GL11.glPopMatrix();
        }
    }

	@Override
	public void renderInventory(double x, double y, double z, int metadata) 
	{
		renderCaisse(metadata,x,y,z,0.0f, true);
	}
}