package aurum.stockage.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.tileentity.TileEntity;

public class ModelDoubleCaisse extends ModelBase
{
    ModelRenderer DoubleCaisse;

    public ModelDoubleCaisse()
    {
        textureWidth = 64;
        textureHeight = 64;
        DoubleCaisse = new ModelRenderer(this, 0, 0);
        DoubleCaisse.addBox(0F, 0F, 0F, 16, 32, 16);
        DoubleCaisse.setRotationPoint(-8F, -8F, -8F);
        DoubleCaisse.setTextureSize(128, 128);
        DoubleCaisse.mirror = true;
        setRotation(DoubleCaisse, 0F, 0F, 0F);
    }

    public void render(float f5)
    {
        DoubleCaisse.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}