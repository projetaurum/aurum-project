package aurum.stockage.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.tileentity.TileEntity;

public class ModelQuadCaisse extends ModelBase
{
    ModelRenderer CaisseQuad;

    public ModelQuadCaisse()
    {
        textureWidth = 128;
        textureHeight = 64;
        CaisseQuad = new ModelRenderer(this, 0, 0);
        CaisseQuad.addBox(0F, 0F, 0F, 32, 16, 32);
        CaisseQuad.setRotationPoint(-8F, 8F, -8F);
        CaisseQuad.setTextureSize(256, 128);
        CaisseQuad.mirror = true;
        setRotation(CaisseQuad, 0F, 0F, 0F);
    }

    public void render(float f5)
    {
        CaisseQuad.render(f5);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
        model.rotateAngleX = x;
        model.rotateAngleY = y;
        model.rotateAngleZ = z;
    }
}