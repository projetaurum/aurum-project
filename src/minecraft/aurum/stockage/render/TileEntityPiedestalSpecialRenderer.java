package aurum.stockage.render;

import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.core.multiblocks.IInventoryRenderer;
import aurum.stockage.AurumStockage;
import aurum.stockage.tileentity.TileEntityPiedestal;

public class TileEntityPiedestalSpecialRenderer
    extends TileEntitySpecialRenderer implements IInventoryRenderer
{
    RenderItem item;
    
	public TileEntityPiedestalSpecialRenderer()
	{
		this.setTileEntityRenderer(TileEntityRenderer.instance);
	}

    @Override
    public void renderTileEntityAt(TileEntity tileentity, double d0, double d1,
                                   double d2, float f)
    {
    	renderPiedestal(tileentity,d0,d1,d2,f,-1);
    }
    
    public void renderPiedestal(TileEntity tileentity, double x, double y, double z, float f, int metadata)
    {
    	int orientation = 0;
    	if(metadata != -1)
    		orientation = metadata;
    	else
    		orientation = tileentity.getBlockMetadata();
    	
    	if(tileentity != null)
    		orientation = tileentity.worldObj.getBlockMetadata(tileentity.xCoord, tileentity.yCoord, tileentity.zCoord);
        GL11.glPushMatrix();
        AurumStockage.blockPiedestal.setBlockBounds(0.060F, 0F, 0.060F, 0.940F, 1F, 0.940F);
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);

        if (orientation  == 0 || orientation == 4 || orientation == 8)
        {
            GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
        }
        else if (orientation == 1 || orientation == 5 || orientation == 9)
        {
            GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
        }
        else if (orientation == 2 || orientation == 6 || orientation == 10)
        {
            GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
        }
        else if (orientation == 3 || orientation == 7 || orientation == 11)
        {
            GL11.glRotatef(180F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(90.0F, 0.0F, -1.0F, 0.0F);
        }

        if (orientation >= 0 && orientation < 4)
        {
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/piedestal.png"));
        }

        if (orientation >= 4 && orientation < 8)
        {
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/piedestal_stonebrick.png"));
        }

        if (orientation >= 8 && orientation < 12)
        {
            bindTexture(new ResourceLocation("aurumstockage","textures/blocks/piedestal_planks.png"));
        }

        new ModelPiedestal().render(tileentity, 0.0625F);
        
        if(tileentity != null)
        {
            TileEntityPiedestal piedestal = (TileEntityPiedestal) tileentity;

            if (piedestal.getStackInSlot(0) != null)
            {
                GL11.glPushMatrix();
                GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
                GL11.glRotatef(101.0F, 1.0F, 0.0F, 0.0F);
                GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
                item = new RenderItem()
                {
                    public byte getMiniBlockCountForItemStack(ItemStack stack)
                    {
                        return 1;
                    }
                    public byte getMiniItemCountForItemStack(ItemStack stack)
                    {
                        return 1;
                    }
                    @Override
                    public boolean shouldBob()
                    {
                        return false;
                    }
                    @Override
                    public boolean shouldSpreadItems()
                    {
                        return false;
                    }
                };
                item.setRenderManager(RenderManager.instance);
                EntityItem customitem = new EntityItem(tileentity.worldObj);
                customitem.hoverStart = 0f;
                customitem.setEntityItemStack(piedestal.getStackInSlot(0));
                item.doRenderItem(customitem, 0, -0.23, 0.56, 50.156F, 0);
                GL11.glPopMatrix();
            }
        }
        GL11.glPopMatrix();
    }
    
	@Override
	public void renderInventory(double x, double y, double z, int metadata) 
	{
		this.renderPiedestal(null, x, y, z, 0.0f, metadata);
	}
}