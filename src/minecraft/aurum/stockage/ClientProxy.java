package aurum.stockage;

import aurum.core.multiblocks.TESRInventoryRenderer;
import aurum.core.multiblocks.TESRInventoryRenderer.TESRIndex;
import aurum.magie.AurumMagie;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumnSpecialRenderer;
import aurum.stockage.render.TileEntityCaisseSpecialRenderer;
import aurum.stockage.render.TileEntityPiedestalSpecialRenderer;
import aurum.stockage.tileentity.TileEntityCaisse;
import aurum.stockage.tileentity.TileEntityPiedestal;
import cpw.mods.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy
{
	@Override
	public void registerRendering()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPiedestal.class, new TileEntityPiedestalSpecialRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCaisse.class, new TileEntityCaisseSpecialRenderer());

		TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumStockage.blockPiedestal, 0), new TileEntityPiedestalSpecialRenderer());
		TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumStockage.blockPiedestal, 4), new TileEntityPiedestalSpecialRenderer());
		TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumStockage.blockPiedestal, 8), new TileEntityPiedestalSpecialRenderer());

		TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumStockage.blockCaisse, 1), new TileEntityCaisseSpecialRenderer());
		TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumStockage.blockCaisse, 2), new TileEntityCaisseSpecialRenderer());
	}
}