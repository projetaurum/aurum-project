package aurum.stockage.items;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockPiedestal extends ItemBlock
{
    public ItemBlockPiedestal(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("PiedestalItemBlock");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        if (stack.getItemDamage() >= 0 && stack.getItemDamage() < 4)
        {
            return "Quartz Piedestal";
        }

        if (stack.getItemDamage() >= 4 && stack.getItemDamage() < 8)
        {
            return "Brickstone Piedestal";
        }

        if (stack.getItemDamage() >= 8 && stack.getItemDamage() < 12)
        {
            return "Planks Piedestal";
        }

        return null;
    }
}