package aurum.stockage.items;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.core.PLocation;
import aurum.core.PLocationWorld;
import aurum.stockage.AurumStockage;
import aurum.stockage.tileentity.TileEntityCaisse;
import aurum.stockage.tileentity.TileEntityCaisseGag;

public class ItemBlockCaisse extends ItemBlock
{
    public ItemBlockCaisse(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockCaisse");
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        int meta = stack.getItemDamage();

        if (meta == 0)
        {
            return "Caisse";
        }
        else if (meta == 1)
        {
            return "Double Caisse";
        }
        else if (meta == 2)
        {
            return "Quadruple Caisse";
        }

        return "";
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }

    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata)
    {
        if (!world.setBlock(x, y, z, AurumStockage.blockCaisse.blockID, metadata, 3))
        {
            return false;
        }

        if (metadata == 0)
        {
            TileEntityCaisse caisse = (TileEntityCaisse) world.getBlockTileEntity(x, y, z);
            caisse.setCaisseType((byte)0);
        }

        if (metadata == 1)
        {
            PLocation loc0 = new PLocation(x, y, z);
            PLocation loc1 = null;
            TileEntityCaisse caisse = null;
            TileEntityCaisseGag caisse2 = null;
            int data = 0;

            if (side == 0)
            {
                loc1 = new PLocation(x, y - 1, z);
                data = 1;
            }
            else if (side == 1)
            {
                loc1 = new PLocation(x, y, z);
                loc0 = new PLocation(x, y + 1, z);
                data = 1;
            }
            else if (side == 2)
            {
                loc1 = new PLocation(x, y, z - 1);
                data = 4;
            }
            else if (side == 3)
            {
                loc1 = new PLocation(x, y, z);
                loc0 = new PLocation(x, y, z + 1);
                data = 4;
            }
            else if (side == 4)
            {
                loc1 = new PLocation(x - 1, y, z);
                data = 5;
            }
            else if (side == 5)
            {
                loc1 = new PLocation(x, y, z);
                loc0 = new PLocation(x + 1, y, z);
                data = 5;
            }

            if (loc0.getPosX() != x || loc0.getPosY() != y || loc0.getPosZ() != z)
            {
                world.setBlockToAir(loc1.getPosX(), loc1.getPosY(), loc1.getPosZ());

                if (!PLocationWorld.isAirBlock(world, loc0))
                {
                    world.setBlockToAir(loc0.getPosX(), loc0.getPosY(), loc0.getPosZ());
                    return false;
                }
            }

            PLocationWorld.setBlock(world, loc0, AurumStockage.blockCaisse.blockID, data);
            caisse = (TileEntityCaisse) PLocationWorld.getTileEntity(world, loc0);
            caisse.setCaisseType((byte)1);

            if (!PLocationWorld.isAirBlock(world, loc1))
            {
                world.setBlockToAir(loc0.getPosX(), loc0.getPosY(), loc0.getPosZ());
                return false;
            }

            PLocationWorld.setBlock(world, loc1, AurumStockage.blockCaisseGag.blockID, 0);
            caisse2 = (TileEntityCaisseGag) PLocationWorld.getTileEntity(world, loc1);
            caisse2.setSourceX(caisse.xCoord);
            caisse2.setSourceY(caisse.yCoord);
            caisse2.setSourceZ(caisse.zCoord);
        }
        else if (metadata == 2)
        {
            TileEntityCaisse caisse = (TileEntityCaisse) world.getBlockTileEntity(x, y, z);
            TileEntityCaisseGag caisse2 = null;
            TileEntityCaisseGag caisse3 = null;
            TileEntityCaisseGag caisse4 = null;
            PLocation loc0 = new PLocation(x, y, z);
            PLocation loc1 = null;
            PLocation loc2 = null;
            PLocation loc3 = null;

            if (side == 0 || side == 1)
            {
                loc1 = new PLocation(x + 1, y, z);
                loc2 = new PLocation(x + 1, y, z + 1);
                loc3 = new PLocation(x, y, z + 1);
            }
            else if (side == 2)
            {
                world.setBlockMetadataWithNotify(x, y, z, 6, 3);
                loc1 = new PLocation(x - 1, y, z);
                loc2 = new PLocation(x - 1, y + 1, z);
                loc3 = new PLocation(x, y + 1, z);
            }
            else if (side == 3)
            {
                loc0 = new PLocation(x + 1, y, z);
                world.setBlockToAir(x, y, z);
                world.setBlock(x + 1, y, z, AurumStockage.blockCaisse.blockID, 6, 3);
                caisse = (TileEntityCaisse) world.getBlockTileEntity(x + 1, y, z);
                loc1 = new PLocation(x, y, z);
                loc2 = new PLocation(x, y + 1, z);
                loc3 = new PLocation(x + 1, y + 1, z);
            }
            else if (side == 4)
            {
                world.setBlockMetadataWithNotify(x, y, z, 7, 3);
                loc1 = new PLocation(x, y, z + 1);
                loc2 = new PLocation(x, y + 1, z + 1);
                loc3 = new PLocation(x, y + 1, z);
            }
            else if (side == 5)
            {
                world.setBlockMetadataWithNotify(x, y, z, 7, 3);
                loc1 = new PLocation(x, y, z - 1);
                loc2 = new PLocation(x, y + 1, z - 1);
                loc3 = new PLocation(x, y + 1, z);
            }

            if (loc0.getPosX() != x || loc0.getPosY() != y || loc0.getPosZ() != z)
            {
                if (!PLocationWorld.isAirBlock(world, loc0))
                {
                    world.setBlockToAir(loc0.getPosX(), loc0.getPosY(), loc0.getPosZ());
                    return false;
                }
            }

            if (!PLocationWorld.isAirBlock(world, loc1) ||
                    !PLocationWorld.isAirBlock(world, loc3) ||
                    !PLocationWorld.isAirBlock(world, loc3))
            {
                world.setBlockToAir(loc0.getPosX(), loc0.getPosY(), loc0.getPosZ());
                return false;
            }

            caisse.setCaisseType((byte)2);
            PLocationWorld.setBlock(world, loc1, AurumStockage.blockCaisseGag.blockID, 0);
            caisse2 = (TileEntityCaisseGag) PLocationWorld.getTileEntity(world, loc1);
            caisse2.setSourceX(caisse.xCoord);
            caisse2.setSourceY(caisse.yCoord);
            caisse2.setSourceZ(caisse.zCoord);
            PLocationWorld.setBlock(world, loc2, AurumStockage.blockCaisseGag.blockID, 0);
            caisse3 = (TileEntityCaisseGag) PLocationWorld.getTileEntity(world, loc2);
            caisse3.setSourceX(caisse.xCoord);
            caisse3.setSourceY(caisse.yCoord);
            caisse3.setSourceZ(caisse.zCoord);
            PLocationWorld.setBlock(world, loc3, AurumStockage.blockCaisseGag.blockID, 0);
            caisse4 = (TileEntityCaisseGag) PLocationWorld.getTileEntity(world, loc3);
            caisse4.setSourceX(caisse.xCoord);
            caisse4.setSourceY(caisse.yCoord);
            caisse4.setSourceZ(caisse.zCoord);
        }

        if (world.getBlockId(x, y, z) == AurumStockage.blockCaisse.blockID)
        {
            Block.blocksList[AurumStockage.blockCaisse.blockID].onBlockPlacedBy(world, x, y, z, player, stack);
            Block.blocksList[AurumStockage.blockCaisse.blockID].onPostBlockPlaced(world, x, y, z, metadata);
        }

        return true;
    }
}