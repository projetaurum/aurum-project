package aurum.stockage.items;

import aurum.stockage.AurumStockage;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;

public class ItemKey extends Item
{
    String texture;

    public ItemKey(int id, int damage, String texture)
    {
        super(id);
        this.texture = texture;
        this.setMaxStackSize(1);
        this.setMaxDamage(damage);
        this.setCreativeTab(AurumStockage.stockageTab);
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.itemIcon = register.registerIcon("aurumstockage:" + texture);
    }
}