package aurum.core.tabs;

import aurum.equipments.AurumEquipments;
import net.minecraft.creativetab.CreativeTabs;

public class AurumEquipmentsTab extends CreativeTabs
{
    public AurumEquipmentsTab(String par2Str)
    {
        super(par2Str);
    }

    public int getTabIconItemIndex()
    {
        return AurumEquipments.itemArdoniteAxeCopper.itemID;
    }
}
