package aurum.core.tabs;

import aurum.core.AurumCore;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class AurumCoreTab extends CreativeTabs
{
    public AurumCoreTab(String par2Str)
    {
        super(par2Str);
    }

    public Item getTabIconItem()
    {
        return AurumCore.itemEcu;
    }
}