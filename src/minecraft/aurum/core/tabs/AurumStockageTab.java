package aurum.core.tabs;

import aurum.stockage.AurumStockage;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class AurumStockageTab extends CreativeTabs
{
    public AurumStockageTab(String par2Str)
    {
        super(par2Str);
    }

    public ItemStack getIconItemStack()
    {
        return new ItemStack(AurumStockage.blockCaisse.blockID, 1, 0);
    }
}