package aurum.core.tabs;

import aurum.deco.AurumDeco;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class AurumDecoTab extends CreativeTabs
{
    public AurumDecoTab(String label)
    {
        super(label);
    }

    public ItemStack getIconItemStack()
    {
        return new ItemStack(AurumDeco.blockArdoise.blockID, 1, 0);
    }
}
