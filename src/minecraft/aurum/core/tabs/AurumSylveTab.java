package aurum.core.tabs;

import aurum.sylve.AurumSylve;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class AurumSylveTab extends CreativeTabs
{
    public AurumSylveTab(String label)
    {
        super(label);
    }

    public ItemStack getIconItemStack()
    {
        return new ItemStack(AurumSylve.blockBerryBush.blockID, 1, 0);
    }
}
