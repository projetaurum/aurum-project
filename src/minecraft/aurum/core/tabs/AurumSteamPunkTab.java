package aurum.core.tabs;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class AurumSteamPunkTab extends CreativeTabs
{
    public AurumSteamPunkTab(String par2Str)
    {
        super(par2Str);
    }

    public ItemStack getIconItemStack()
    {
        return new ItemStack(Block.blockIron.blockID, 1, 0);
    }
}
