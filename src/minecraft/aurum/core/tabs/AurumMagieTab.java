package aurum.core.tabs;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import aurum.magie.AurumMagie;

public class AurumMagieTab extends CreativeTabs
{
    public AurumMagieTab(String label)
    {
        super(label);
    }

    public ItemStack getIconItemStack()
    {
        return new ItemStack(AurumMagie.itemSoulStealer, 1, 0);
    }
}