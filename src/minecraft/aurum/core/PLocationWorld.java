package aurum.core;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class PLocationWorld
{
    public static final void setBlock(World w, PLocation loc, int blockID, int meta)
    {
        w.setBlock(loc.getPosX(), loc.getPosY(), loc.getPosZ(), blockID, meta, 3);
    }

    public static final void setBlockMeta(World w, PLocation loc, int meta)
    {
        w.setBlockMetadataWithNotify(loc.getPosX(), loc.getPosY(), loc.getPosZ(), meta, 3);
    }

    public static final TileEntity getTileEntity(World w, PLocation loc)
    {
        return w.getBlockTileEntity(loc.getPosX(), loc.getPosY(), loc.getPosZ());
    }

    public static final boolean isAirBlock(World w, PLocation loc)
    {
        return w.isAirBlock(loc.getPosX(), loc.getPosY(), loc.getPosZ());
    }
}