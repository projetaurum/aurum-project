package aurum.core;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import aurum.monnaie.gui.ContainerBourse;
import aurum.monnaie.gui.GuiBourse;
import aurum.monnaie.gui.InventoryBourse;
import aurum.npc.entity.EntityMarchand;
import aurum.npc.gui.ContainerMarchand;
import aurum.npc.gui.GuiMarchand;
import aurum.npc.gui.InventoryMarchand;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        if (id == 0)
        {
            return new ContainerBourse(new InventoryBourse(player, player.inventory.getStackInSlot(player.inventory.currentItem)), player.inventory);
        }
        else if(id == 1)
        {
        	return new ContainerMarchand(new InventoryMarchand((EntityMarchand) world.getEntityByID(x)),player.inventory,player);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        if (id == 0)
        {
            return new GuiBourse(new ContainerBourse(new InventoryBourse(player, player.inventory.getStackInSlot(player.inventory.currentItem)), player.inventory));
        }
        else if(id == 1)
        {
        	return new GuiMarchand(new ContainerMarchand(new InventoryMarchand((EntityMarchand) world.getEntityByID(x)),player.inventory,player));
        }
        return null;
    }
}