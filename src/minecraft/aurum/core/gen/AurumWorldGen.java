package aurum.core.gen;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import aurum.deco.AurumDeco;
import aurum.steampunk.AurumSteamPunk;
import aurum.sylve.AurumSylve;
import cpw.mods.fml.common.IWorldGenerator;

public class AurumWorldGen implements IWorldGenerator
{
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world,
                         IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
    {
        switch (world.provider.dimensionId)
        {
            case -1:
                generateNether(world, random, chunkX * 16, chunkZ * 16);

            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);

            case 1:
                generateEnd(world, random, chunkX * 16, chunkZ * 16);
        }
    }

    private void generateEnd(World world, Random random, int x, int z) {}

    private void generateSurface(World world, Random random, int x, int z)
    {
        String biomeName = world.getBiomeGenForCoords(x, z).biomeName;

        if (biomeName.equals("Extreme Hills") || biomeName.equals("Forest") || biomeName.equals("ForestHills") || biomeName.equals("River"))
        {
            this.addOreSpawn(AurumSteamPunk.blockCopperOre, 0, world, random, x, z, 16, 16, 2 + random.nextInt(6), 10, 30, 60);
        }

        this.addOreSpawn(AurumSteamPunk.blockMithrilOre, 0, world, random, x, z, 16, 16, 5 + random.nextInt(5), 1, 6, 10);
        this.addOreSpawn(AurumDeco.blockArdoise, 0, world, random, x, z, 16, 16, 30 + random.nextInt(10), 1, 30, 70);
        this.addOreSpawn(AurumDeco.blockArdoise, 1, world, random, x, z, 16, 16, 10 + random.nextInt(5), 3, 8, 30);
        int xCh = x + random.nextInt(16);
        int yCh = random.nextInt(128);
        int zCh = z + random.nextInt(16);

        if (biomeName.equals("Plains") || biomeName.equals("Forest"))
        {
            //Azurin
            generateFlower(world, random, xCh, yCh + 64, zCh, 0, 32);
            //Poppy
            generateFlower(world, random, xCh, yCh + 64, zCh, 1, 32);
            //WhitePoppy
            generateFlower(world, random, xCh, yCh + 64, zCh, 2, 2);
            //Violet
            generateFlower(world, random, xCh, yCh + 64, zCh, 3, 32);
        }
    }

    private void generateNether(World world, Random random, int x, int z) {}

    /**
     * Adds an Ore Spawn to Minecraft. Simply register all Ores to spawn with this method in your Generation method in your IWorldGeneration extending Class
     *
     * @param The Block to spawn
     * @param The World to spawn in
     * @param A Random object for retrieving random positions within the world to spawn the Block
     * @param An int for passing the X-Coordinate for the Generation method
     * @param An int for passing the Z-Coordinate for the Generation method
     * @param An int for setting the maximum X-Coordinate values for spawning on the X-Axis on a Per-Chunk basis
     * @param An int for setting the maximum Z-Coordinate values for spawning on the Z-Axis on a Per-Chunk basis
     * @param An int for setting the maximum size of a vein
     * @param An int for the Number of chances available for the Block to spawn per-chunk
     * @param An int for the minimum Y-Coordinate height at which this block may spawn
     * @param An int for the maximum Y-Coordinate height at which this block may spawn
     **/
    public void addOreSpawn(Block block, int meta, World world, Random random, int blockXPos, int blockZPos, int maxX, int maxZ, int maxVeinSize, int chancesToSpawn, int minY, int maxY)
    {
        int maxPossY = minY + (maxY - 1);
        assert maxY > minY: "The maximum Y must be greater than the Minimum Y";
        assert maxX > 0 && maxX <= 16: "addOreSpawn: The Maximum X must be greater than 0 and less than 16";
        assert minY > 0: "addOreSpawn: The Minimum Y must be greater than 0";
        assert maxY < 256 && maxY > 0: "addOreSpawn: The Maximum Y must be less than 256 but greater than 0";
        assert maxZ > 0 && maxZ <= 16: "addOreSpawn: The Maximum Z must be greater than 0 and less than 16";
        int diffBtwnMinMaxY = maxY - minY;

        for (int x = 0; x < chancesToSpawn; x++)
        {
            int posX = blockXPos + random.nextInt(maxX);
            int posY = minY + random.nextInt(diffBtwnMinMaxY);
            int posZ = blockZPos + random.nextInt(maxZ);
            (new WorldGenMinable(block.blockID, meta, maxVeinSize, Block.stone.blockID)).generate(world, random, posX, posY, posZ);
        }
    }

    public boolean generateFlower(World world, Random random, int x, int y, int z, int meta, int rarity)
    {
        for (int tries = 0; tries < rarity; tries++)
        {
            int i1 = (x + random.nextInt(8)) - random.nextInt(8);
            int j1 = (y + random.nextInt(4)) - random.nextInt(4);
            int k1 = (z + random.nextInt(8)) - random.nextInt(8);

            if (world.isAirBlock(i1, j1, k1) && AurumSylve.blockFlowers.canBlockStay(world, i1, j1, k1))
            {
                world.setBlock(i1, j1, k1, AurumSylve.blockFlowers.blockID, meta, 2);
            }
        }

        return true;
    }
}