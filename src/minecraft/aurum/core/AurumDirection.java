package aurum.core;

public class AurumDirection
{
    /*
     * 0 = xyz
     * 1 = +x
     * 2 = -x
     * 3 = +y
     * 4 = -y
     * 5 = +z
     * 6 = -z
     */
    private int orientation;

    public AurumDirection(int i)
    {
        this.orientation = i;
    }

    /**
     * 0 = xyz
     * 1 = +x
     * 2 = -x
     * 3 = +y
     * 4 = -y
     * 5 = +z
     * 6 = -z
     */
    public int getOrientation()
    {
        return this.orientation;
    }

    public void setOrientation(int i)
    {
        this.orientation = i;
    }
}