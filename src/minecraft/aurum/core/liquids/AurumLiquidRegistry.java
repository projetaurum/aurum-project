package aurum.core.liquids;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFluid;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraftforge.liquids.LiquidContainerData;
import net.minecraftforge.liquids.LiquidContainerRegistry;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;

public class AurumLiquidRegistry
{
    public static final void registerLiquids()
    {
        registerLiquid("honey", 3003, AurumCore.aurumDecoTab);
        registerLiquid("chocolate", 3000, AurumCore.aurumDecoTab);
    }

    /**
     * @param String liquidName
     * @param Integer startID
     * @param CreativeTabs tabCreative
     */
    public static final void registerLiquid(String liquidName, int startID, CreativeTabs tab)
    {
        String liquidName2 = AurumRegistry.capitalizeFirstLetter(liquidName);
        Block liquidFlow = new BlockAurumLiquidFlow(startID, liquidName)
        .setUnlocalizedName("aurumdeco:" + liquidName + "_flow");
        Block liquidStill = new BlockAurumLiquidStill(startID + 1, liquidName)
        .setUnlocalizedName("aurumdeco:" + liquidName);
        Item bucket = new ItemBucket(startID + 2, liquidFlow.blockID)
        .setUnlocalizedName("aurumdeco:bucket_" + liquidName)
        .setCreativeTab(tab);
        AurumRegistry.registerBlock(liquidFlow, liquidName2 + " Flow");
        AurumRegistry.registerBlock(liquidStill, liquidName2 + " Still");
        AurumRegistry.registerItem(bucket, liquidName2 + " Bucket");
        BucketHandler.liquids.put((BlockFluid) liquidStill, (ItemBucket) bucket);
        LiquidDictionary.getOrCreateLiquid(liquidName2, new LiquidStack(liquidStill, LiquidContainerRegistry.BUCKET_VOLUME));
        LiquidContainerRegistry.registerLiquid(new LiquidContainerData(new LiquidStack(liquidStill, LiquidContainerRegistry.BUCKET_VOLUME),  new ItemStack(bucket), new ItemStack(Item.bucketEmpty)));
    }
}