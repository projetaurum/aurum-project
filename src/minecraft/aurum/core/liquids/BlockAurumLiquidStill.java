package aurum.core.liquids;

import net.minecraft.block.BlockStationary;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public class BlockAurumLiquidStill extends BlockStationary
{
    private String type = "";

    public BlockAurumLiquidStill(int par1, String type)
    {
        super(par1, Material.water);
        this.type = type;
        this.blockHardness = 100.0F;
        this.setLightOpacity(3);
        this.disableStats();
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurumdeco:" + type);
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        return this.blockIcon;
    }
}