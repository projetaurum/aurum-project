package aurum.core.liquids;

import net.minecraft.block.BlockFlowing;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public class BlockAurumLiquidFlow extends BlockFlowing
{
    private String type = "";

    public BlockAurumLiquidFlow(int par1, String type)
    {
        super(par1, Material.water);
        this.type = type;
        this.blockHardness = 100.0F;
        this.setLightOpacity(3);
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurumdeco:" + type + "_flow");
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        return this.blockIcon;
    }
}