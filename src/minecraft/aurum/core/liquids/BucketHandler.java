package aurum.core.liquids;

import java.util.HashMap;

import net.minecraft.block.BlockFluid;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.event.Event.Result;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.FillBucketEvent;

public class BucketHandler
{
    public static HashMap<BlockFluid, ItemBucket> liquids = new HashMap<BlockFluid, ItemBucket>();

    @ForgeSubscribe
    public void onBucketFill(FillBucketEvent event)
    {
        ItemStack result = attemptFill(event.world, event.target);

        if (result != null)
        {
            event.result = result;
            event.setResult(Result.ALLOW);
        }
    }

    private ItemStack attemptFill(World world, MovingObjectPosition p)
    {
        int id = world.getBlockId(p.blockX, p.blockY, p.blockZ);

        for (BlockFluid liquid : liquids.keySet())
        {
            if (id == liquid.blockID)
            {
                world.setBlock(p.blockX, p.blockY, p.blockZ, 0);
                return new ItemStack(liquids.get(liquid));
            }
        }

        return null;
    }
}