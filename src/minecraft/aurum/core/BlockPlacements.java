package aurum.core;

import net.minecraft.block.material.Material;
import net.minecraft.world.World;

public class BlockPlacements
{
    public static final boolean genLineX(World w, int xMin, int xMax, PLocation start, int id, int meta, boolean replace)
    {
        for (int i = 0; i < xMax - xMin; i++)
        {
            if (replace)
            {
                w.setBlock(xMin + i, start.getPosY(), start.getPosZ(), id, meta, 2);
            }
            else
            {
                if (w.getBlockId(xMin + i, start.getPosY(), start.getPosZ()) == 0)
                {
                    w.setBlock(xMin + i, start.getPosY(), start.getPosZ(), id, meta, 2);
                }
            }
        }

        return true;
    }

    public static final boolean genLineZ(World w, int zMin, int zMax, int id, int meta, boolean replace)
    {
        return true;
    }

    public static final boolean genPlateForme(World w, int xMin, int xMax, int zMin, int zMax, int id, int meta, boolean replace)
    {
        return true;
    }

    public static final boolean genWall(World w, boolean xz, int xzMin, int xzMax, int yMin, int yMax, int id, int meta, boolean replace)
    {
        return true;
    }

    public static final boolean genCube(World w, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax, int id, int meta, boolean replace)
    {
        return true;
    }

    public static final boolean genEmptyCube(World w, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax, int id, int meta, boolean replace)
    {
        return true;
    }

    public static final boolean spaceIsFree(World w, int x, int y, int z)
    {
        if (w.getBlockMaterial(x, y, z).equals(Material.air))
        {
            return true;
        }

        return false;
    }
}