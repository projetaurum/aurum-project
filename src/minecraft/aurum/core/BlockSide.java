package aurum.core;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockSide extends Block
{
    public BlockSide(int par1, Material par2Material)
    {
        super(par1, par2Material);
        this.setUnlocalizedName("AurumBlockSide");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        if (player.isSneaking())
        {
            return true;
        }

        /*String host = "ftp.projet-aurum.fr";
        String username = "";
        String password = "";

        try
        {
            FTPUploader ftp = new FTPUploader(host, username, password);
            ftp.uploadFile("C:\\Users\\Choucroute\\Desktop\\Repos\\aurum-project\\jars\\options.txt", "options.txt", "wwwstockagezones");
            ftp.disconnect();
            System.out.println("Done");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }*/

        return true;
    }

    Icon[] iconBuffer;

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[6];
        this.iconBuffer[0] = register.registerIcon("aurumcore:side0");
        this.iconBuffer[1] = register.registerIcon("aurumcore:side1");
        this.iconBuffer[2] = register.registerIcon("aurumcore:side2");
        this.iconBuffer[3] = register.registerIcon("aurumcore:side3");
        this.iconBuffer[4] = register.registerIcon("aurumcore:side4");
        this.iconBuffer[5] = register.registerIcon("aurumcore:side5");
    }

    @Override
    public Icon getIcon(int side, int meta)
    {
        return iconBuffer[side];
    }
}