package aurum.core;

import net.minecraft.nbt.NBTTagCompound;

public class PLocation
{
    protected double posX;
    protected double posY;
    protected double posZ;

    protected int worldName;

    public PLocation(int posX, int posY, int posZ, int worldName)
    {
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.worldName = worldName;
    }

    public PLocation(int posX, int posY, int posZ)
    {
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.worldName = 0;
    }
    
    public PLocation(double posX, double posY, double posZ, int worldName)
    {
    	this.posX = posX;
    	this.posY = posY;
    	this.posZ = posZ;
    	this.worldName = worldName;
    }
    
    public PLocation(double posX, double posY, double posZ)
    {
    	this.posX = posX;
    	this.posY = posY;
    	this.posZ = posZ;
    	this.worldName = 0;
    }

    public int getPosX()
    {
        return (int)this.posX;
    }

    public int getPosY()
    {
        return (int)this.posY;
    }

    public int getPosZ()
    {
        return (int)this.posZ;
    }

    public int getWorld()
    {
        return this.worldName;
    }

    public void setPosX(int posX)
    {
        this.posX = posX;
    }
    public void setPosY(int posY)
    {
        this.posY = posY;
    }
    public void setPosZ(int posZ)
    {
        this.posZ = posZ;
    }

    public static final PLocation loadFromNBT(NBTTagCompound tag, String key)
    {
        PLocation ploc = new PLocation(tag.getInteger(key + "PosX"), tag.getInteger(key + "PosY"),
                                       tag.getInteger(key + "PosZ"), tag.getInteger(key + "World"));
        return ploc;
    }

    public void writeToNBT(NBTTagCompound tag, String key)
    {
        tag.setInteger(key + "PosX", (int)this.posX);
        tag.setInteger(key + "PosY", (int)this.posY);
        tag.setInteger(key + "PosZ", (int)this.posZ);
        tag.setInteger(key + "World", this.worldName);
    }
    
    // DECIMALS LOCATION SYSTEM
    
    public double getPosXd()
    {
    	return this.posX;
    }
    
    public double getPosYd()
    {
    	return this.posY;
    }
    
    public double getPosZd()
    {
    	return this.posZ;
    }
    
    public void setPosXd(double posX)
    {
    	this.posX = posX;
    }
    
    public void setPosYd(double posY)
    {
    	this.posY = posY;
    }
    
    public void setPosZd(double posZ)
    {
    	this.posZ = posZ;
    }
    
    public static final PLocation loadFromNBTd(NBTTagCompound tag, String key)
    {
        PLocation ploc = new PLocation(tag.getDouble(key + "PosX"), tag.getDouble(key + "PosY"),
                tag.getDouble(key + "PosZ"), tag.getInteger(key + "World"));
return ploc;
    }
    
    public void writeToNBTd(NBTTagCompound tag, String key)
    {
        tag.setDouble(key + "PosX", this.posX);
        tag.setDouble(key + "PosY", this.posY);
        tag.setDouble(key + "PosZ", this.posZ);
        tag.setInteger(key + "World", this.worldName);
    }
}