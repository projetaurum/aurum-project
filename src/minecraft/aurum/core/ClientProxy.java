package aurum.core;

import net.minecraft.client.Minecraft;
import net.minecraftforge.common.MinecraftForge;
import aurum.core.events.zones.GuiZoneIndicator;
import aurum.core.multiblocks.TESRInventoryRenderer;
import aurum.monnaie.RenderEcuEchangeur;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        MinecraftForge.EVENT_BUS.register(new SoundEventManager());
        MinecraftForge.EVENT_BUS.register(new GuiZoneIndicator(Minecraft.getMinecraft()));
        ecuEchangeurRenderType = RenderingRegistry.getNextAvailableRenderId();
        renderInventoryTESRID = RenderingRegistry.getNextAvailableRenderId();
        RenderingRegistry.registerBlockHandler(new RenderEcuEchangeur());
        RenderingRegistry.registerBlockHandler(new TESRInventoryRenderer());
    }
    public static int ecuEchangeurRenderType;
    public static int renderInventoryTESRID;
}