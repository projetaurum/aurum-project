package aurum.core;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class AurumRegistry
{
    public static final void registerBlock(Block block, String name)
    {
        GameRegistry.registerBlock(block, name);
        LanguageRegistry.addName(block, name);
    }

    public static final void registerBlock(Block block, Class <? extends ItemBlock > itemclass, String name)
    {
        GameRegistry.registerBlock(block, itemclass, name);
        LanguageRegistry.addName(block, name);
    }

    public static final void registerItem(Item item, String name)
    {
        GameRegistry.registerItem(item, name);
        LanguageRegistry.addName(item, name);
    }

    public static final void removeRecipe(ItemStack resultItem)
    {
        @SuppressWarnings("unchecked")
		List<IRecipe> recipes = CraftingManager.getInstance().getRecipeList();

        for (int i = 0; i < recipes.size(); i++)
        {
            IRecipe tmpRecipe = recipes.get(i);

            if (tmpRecipe instanceof ShapedRecipes)
            {
                ShapedRecipes recipe = (ShapedRecipes)tmpRecipe;
                ItemStack recipeResult = recipe.getRecipeOutput();

                if (ItemStack.areItemStacksEqual(resultItem, recipeResult))
                {
                    recipes.remove(i--);
                }
            }
        }
    }

    public static final String capitalizeFirstLetter(String value)
    {
        if (value == null)
        {
            return null;
        }

        if (value.length() == 0)
        {
            return value;
        }

        StringBuilder result = new StringBuilder(value);
        result.replace(0, 1, result.substring(0, 1).toUpperCase());
        return result.toString();
    }
}