package aurum.core.memory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import aurum.core.PLocation;
import aurum.magie.monolithes.MonolithesManager;

public class SaveData
{
    public static final void saveMonolithes()
    {
        File dir = new File("AurumCore");
        dir.mkdirs();
        File file = new File(dir + "/monolithes.aur");

        if (!file.exists())
            try
            {
                file.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        try
        {
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("#####################");
            bw.newLine();
            bw.write("Pourquoi tu lis mon fichier de sauvegarde ? :p");
            bw.newLine();
            bw.write("#####################");
            bw.newLine();
            bw.write(String.valueOf(MonolithesManager.locs.size()));
            bw.newLine();

            for (PLocation loc : MonolithesManager.locs)
            {
                bw.write(String.valueOf(loc.getWorld()) +
                         "#" + String.valueOf(loc.getPosX()) +
                         "#" + String.valueOf(loc.getPosY()) +
                         "#" + String.valueOf(loc.getPosZ()));
                bw.newLine();
            }

            bw.close();
            fw.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static final void savePlayerOverlayData()
    {
        File dir = new File("AurumCore/PlayerData");
        dir.mkdirs();
        File file = new File(dir + "/event-overlay.aur");

        if (!file.exists())
            try
            {
                file.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

        try
        {
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("#####################");
            bw.newLine();
            bw.write("Pourquoi tu lis mon fichier de sauvegarde ? :p");
            bw.newLine();
            bw.write("#####################");
            bw.newLine();
            bw.write(String.valueOf(MonolithesManager.locs.size()));
            bw.newLine();

            for (PLocation loc : MonolithesManager.locs)
            {
                bw.write(String.valueOf(loc.getWorld()) +
                         "#" + String.valueOf(loc.getPosX()) +
                         "#" + String.valueOf(loc.getPosY()) +
                         "#" + String.valueOf(loc.getPosZ()));
                bw.newLine();
            }

            bw.close();
            fw.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}