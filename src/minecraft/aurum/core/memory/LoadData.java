package aurum.core.memory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import aurum.core.AurumCore;
import aurum.magie.monolithes.MonolithesManager;

public class LoadData
{
    public static final void loadMonolithes()
    {
        File dir = new File("AurumCore");
        dir.mkdirs();
        File file = new File(dir + "/monolithes.aur");

        if (!file.exists())
        {
            return;
        }

        try
        {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            br.readLine();
            br.readLine();
            br.readLine();
            int b = Integer.parseInt(br.readLine());

            for (int d = 0; d < b; d++)
            {
                String loc[] = br.readLine().split("#");
                int dimensionID = Integer.parseInt(loc[0]);
                double posX = Integer.parseInt(loc[1]);
                double posY = Integer.parseInt(loc[2]);
                double posZ = Integer.parseInt(loc[3]);
                MonolithesManager.addSpawner(AurumCore.server.worldServerForDimension(dimensionID), (int)posX, (int)posY, (int)posZ);
            }

            br.close();
            fr.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static final void loadVendeur()
    {
    }
}