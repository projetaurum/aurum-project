package aurum.core.memory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import aurum.core.AurumCore;

public class SynapseMemory 
{
	public static final ArrayList<UUID> synapsePortals = new ArrayList<UUID>();
	
	public static final void saveSynapsePortals()
	{
		File file = new File(AurumCore.server.getFile(AurumCore.server.getWorldName())+"/AURsyn/portals.aur");
		
		if(!file.exists())
			file.mkdirs();
		
		System.out.println("SERVER DIRECTORY : "+file.getPath());
		
		try 
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(synapsePortals.size());
			bw.newLine();
			for(UUID id : synapsePortals)
			{
				bw.write(id.toString());
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {e.printStackTrace();}
	}
	
	public static final void loadSynapsePortals()
	{
		
	}
}