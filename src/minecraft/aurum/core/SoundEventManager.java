package aurum.core;

import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.EventPriority;
import net.minecraftforge.event.ForgeSubscribe;

public class SoundEventManager
{
    @ForgeSubscribe(priority = EventPriority.NORMAL)
    public void eventHandler(RenderGameOverlayEvent event) {}

    @ForgeSubscribe
    public void onSoundLoad(SoundLoadEvent event)
    {
        try
        {
            event.manager.soundPoolSounds.addSound(AurumCore.class.getResource("/aurum/core/monolithe.ogg").toString());
            System.out.println("[AurumCoreMod] Successfully loaded monolithe.ogg");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}