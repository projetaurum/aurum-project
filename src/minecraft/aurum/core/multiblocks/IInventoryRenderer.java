package aurum.core.multiblocks;

public interface IInventoryRenderer 
{
	public void renderInventory(double x, double y, double z, int metadata);
}
