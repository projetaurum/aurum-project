package aurum.core.multiblocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockGagBase extends BlockContainer
{
	public BlockGagBase(int par1, Material par2Material) 
	{
		super(par1, par2Material);
	}
	
	@Override
    public MovingObjectPosition collisionRayTrace(World world, int x, int y, int z, Vec3 start, Vec3 end)
    {
		if(isPresent(world,x,y,z))
		{
			TileEntityGagBase gag = (TileEntityGagBase) world.getBlockTileEntity(x, y, z);
			return gag.getMainBlock().collisionRayTrace(world, gag.getSourceX(), gag.getSourceY(), gag.getSourceZ(), start, end);
		}
		return null;
    }
	
    @Override
    public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        return false;
    }

    @Override
    public boolean isOpaqueCube()
    {
        return false;
    }

	@Override
	public TileEntity createNewTileEntity(World world) 
	{
		return new TileEntityGagBase();
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
	{
		if (player.isSneaking())
			return true;

		if(isPresent(world,x,y,z))
		{
			TileEntityGagBase gag = (TileEntityGagBase) world.getBlockTileEntity(x, y, z);
			return gag.getMainBlock().onBlockActivated(world, gag.getSourceX(), gag.getSourceY(), gag.getSourceZ(), player, par6, par7, par8, par9);
		}
		return true;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, int par5, int par6)
	{
		TileEntityGagBase tileEntity = (TileEntityGagBase)world.getBlockTileEntity(x, y, z);

		if (tileEntity != null)
		{
			world.destroyBlock(tileEntity.getSourceX(), tileEntity.getSourceY(), tileEntity.getSourceZ(), false);
			world.removeBlockTileEntity(tileEntity.getSourceX(), tileEntity.getSourceY(), tileEntity.getSourceZ());
		}

		world.removeBlockTileEntity(x, y, z);
	}

	@Override
	public void onNeighborBlockChange(World world, int i, int j, int k, int par5)
	{
		if(!isPresent(world,i,j,k))
		{
			world.destroyBlock(i, j, k, false);
			world.removeBlockTileEntity(i, j, k);
		}
	}

	public boolean isPresent(World w, int x, int y, int z)
	{
		TileEntityGagBase gag = (TileEntityGagBase) w.getBlockTileEntity(x, y, z);

		if (gag != null && gag.getMainBlock() != null)
		{
			if (w.getBlockId(gag.getSourceX(), gag.getSourceY(), gag.getSourceZ()) != gag.getMainBlock().blockID)
				return false;
		}
		return true;
	}
}