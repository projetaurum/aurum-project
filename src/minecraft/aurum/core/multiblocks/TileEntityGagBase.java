package aurum.core.multiblocks;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityGagBase extends TileEntity
{
    private int sourceX = 0;
    private int sourceY = 0;
    private int sourceZ = 0;
    
    private Block mainBlock;

    public TileEntityGagBase() {}

    public Block getMainBlock()
    {
    	return this.mainBlock;
    }
    
    public void setMainBlock(Block block)
    {
    	this.mainBlock = block;
    }
    
    public Integer getSourceX()
    {
        return this.sourceX;
    }

    public Integer getSourceY()
    {
        return this.sourceY;
    }

    public Integer getSourceZ()
    {
        return this.sourceZ;
    }

    public void setSourceX(int x)
    {
        this.sourceX = x;
    }

    public void setSourceY(int y)
    {
        this.sourceY = y;
    }

    public void setSourceZ(int z)
    {
        this.sourceZ = z;
    }

    @Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet)
    {
        readFromNBT(packet.data);
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setInteger("SourceX", this.sourceX);
        tag.setInteger("SourceY", this.sourceY);
        tag.setInteger("SourceZ", this.sourceZ);
        tag.setInteger("BlockID", this.mainBlock.blockID);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.setSourceX(tag.getInteger("SourceX"));
        this.setSourceY(tag.getInteger("SourceY"));
        this.setSourceZ(tag.getInteger("SourceZ"));
        this.mainBlock = Block.blocksList[tag.getInteger("BlockID")];
    }
}