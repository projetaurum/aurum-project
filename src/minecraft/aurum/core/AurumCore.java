package aurum.core;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.command.ICommandManager;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import aurum.core.commands.CommandMonoGen;
import aurum.core.events.zones.BlockZoneWall;
import aurum.core.gen.AurumWorldGen;
import aurum.core.liquids.AurumLiquidRegistry;
import aurum.core.liquids.BucketHandler;
import aurum.core.memory.LoadData;
import aurum.core.memory.SaveData;
import aurum.core.memory.SynapseMemory;
import aurum.core.multiblocks.BlockGagBase;
import aurum.core.multiblocks.TileEntityGagBase;
import aurum.core.packets.AurumPacket;
import aurum.core.packets.PacketHandler;
import aurum.core.tabs.AurumCoreTab;
import aurum.core.tabs.AurumDecoTab;
import aurum.core.tabs.AurumEquipmentsTab;
import aurum.core.tabs.AurumMagieTab;
import aurum.core.tabs.AurumSteamPunkTab;
import aurum.core.tabs.AurumStockageTab;
import aurum.core.tabs.AurumSylveTab;
import aurum.monnaie.ItemEcu;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumCore.ID, version = AurumCore.VERSION, name = "AurumCore")
@NetworkMod(clientSideRequired = true, serverSideRequired = false,
            channels = {AurumPacket.CHANNEL}, packetHandler = PacketHandler.class)
public class AurumCore
{
    @Instance(value = "AurumCoreMod")
    public static AurumCore instance;

    public static final String ID = "AurumCoreMod";
    public static final String VERSION = "1.0";

    @SidedProxy(clientSide = "aurum.core.ClientProxy", serverSide = "aurum.core.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    public static MinecraftServer server;

    AurumWorldGen aurumGen = new AurumWorldGen();

    public static final CreativeTabs aurumTab = new AurumCoreTab("tabAurum");
    public static final CreativeTabs aurumDecoTab = new AurumDecoTab("tabDeco");
    public static final CreativeTabs aurumEquipmentsTab = new AurumEquipmentsTab("tabEquipments");
    public static final CreativeTabs aurumMagieTab = new AurumMagieTab("tabMagie");
    public static final CreativeTabs aurumSteampunkTab = new AurumSteamPunkTab("tabSteampunk");
    public static final CreativeTabs aurumStockageTab = new AurumStockageTab("tabStockage");
    public static final CreativeTabs aurumSylveTab = new AurumSylveTab("tabSylve");

    public static final Item itemEcu = new ItemEcu(10000)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("Ecu");

    public static final Block blockSide = new BlockSide(1600, Material.rock)
    .setBlockUnbreakable()
    .setCreativeTab(aurumTab);

    public static final Block blockZoneWall = new BlockZoneWall(1601)
    .setCreativeTab(aurumTab)
    .setUnlocalizedName("ZoneWall")
    .setStepSound(Block.soundStoneFootstep);
    
    public static final Block blockGagStone = new BlockGagBase(1602, Material.rock)
    .setUnlocalizedName("GagBaseStone")
    .setStepSound(Block.soundStoneFootstep);
    
    public static final Block blockGagWood = new BlockGagBase(1603, Material.wood)
    .setUnlocalizedName("GagBaseWood")
    .setStepSound(Block.soundWoodFootstep);
    
    public static final Block blockGagGlass = new BlockGagBase(1604, Material.glass)
    .setUnlocalizedName("GagBaseGlass")
    .setStepSound(Block.soundGlassFootstep);

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        instance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        //Adding Blocks
        AurumRegistry.registerBlock(blockSide, "Side Block Indicator");
        AurumRegistry.registerBlock(blockZoneWall, "Zone Wall");
        //Adding ItemBlock & Block
        AurumRegistry.registerBlock(blockGagStone, "GagBlockStone");
        AurumRegistry.registerBlock(blockGagWood, "GagBlockWood");
        AurumRegistry.registerBlock(blockGagGlass, "GagBlockGlass");
        //AddingItems
        AurumRegistry.registerItem(itemEcu, "Ecu");
        //Our GuiHandler Register
        NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
        //NAME of this tab
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabAurum", "en_US", "Aurum");
        //Register WorldGenerator
        GameRegistry.registerWorldGenerator(aurumGen);
        //Register BucketHandler
        MinecraftForge.EVENT_BUS.register(new BucketHandler());
        //Register Liquids
        AurumLiquidRegistry.registerLiquids();
        //Register TileEntities
        GameRegistry.registerTileEntity(TileEntityGagBase.class, "TileEntityGagBase");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event)
    {
        server = FMLCommonHandler.instance().getMinecraftServerInstance();
        ICommandManager commandManager = server.getCommandManager();
        ServerCommandManager serverCommandManager = ((ServerCommandManager) commandManager);
        registerCommands(serverCommandManager);
        logger.log(Level.INFO, "Loading Monolithes");
        LoadData.loadMonolithes();
        SynapseMemory.saveSynapsePortals();
    }

    @EventHandler
    public void serverStopping(FMLServerStoppingEvent event)
    {
        logger.log(Level.INFO, "Saving Monolithes");
        SaveData.saveMonolithes();
    }

    public static final void registerCommands(ServerCommandManager manager)
    {
        manager.registerCommand(new CommandMonoGen());
    }
    
    public static final boolean isOp(String user)
    {
    	return AurumCore.server.getConfigurationManager().getOps().contains(user.toLowerCase());
    }
}