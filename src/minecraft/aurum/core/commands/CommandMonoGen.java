package aurum.core.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import aurum.magie.monolithes.MonolithesManager;

public class CommandMonoGen extends CommandBase
{
    @Override
    public String getCommandName()
    {
        return "monolithe";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] astring)
    {
        System.out.println("Generation de monolithe!");
        MonolithesManager.launchSpawner();
    }

	@Override
	public String getCommandUsage(ICommandSender icommandsender) 
	{
		return "";
	}
}