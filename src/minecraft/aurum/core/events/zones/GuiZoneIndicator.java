package aurum.core.events.zones;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.EventPriority;
import net.minecraftforge.event.ForgeSubscribe;

import org.lwjgl.opengl.GL11;

public class GuiZoneIndicator extends Gui
{
    private Minecraft mc;
    private boolean event = false;

    public GuiZoneIndicator(Minecraft mc)
    {
        super();
        this.mc = mc;
    }

    @ForgeSubscribe(priority = EventPriority.NORMAL)
    public void onRenderExperienceBar(RenderGameOverlayEvent event)
    {
        if (this.event)
        {
            ScaledResolution scaledresolution = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
            int xPos = scaledresolution.getScaledWidth() - 128;
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            this.mc.renderEngine.bindTexture(new ResourceLocation("/mods/aurum/core/textures/gui/zone.png"));
            GL11.glDisable(GL11.GL_LIGHTING);
            this.drawTexturedModalRect(xPos / 2, 0, 0, 0, 128, 64);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.mc.renderEngine.bindTexture(new ResourceLocation("/gui/icons.png"));
        }
    }

    @ForgeSubscribe(priority = EventPriority.NORMAL)
    public void onClientChatReceived(ClientChatReceivedEvent event)
    {
            if (event.message.endsWith("evsr1r2"))
            {
                this.event = true;
            }
            else if (event.message.endsWith("evfr1r2"))
            {
                this.event = false;
            }
            else
            {
                return;
            }
    }
}