package aurum.core.events.zones;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.core.BlockPlacements;
import aurum.core.PLocation;

public class BlockZoneWall extends Block/*Container*/
{
    public BlockZoneWall(int par1)
    {
        super(par1, Material.air);
        this.setBlockUnbreakable();
    }

    @Override
    public void onBlockPlacedBy(World w, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
        super.onBlockAdded(w, x, y, z);
        BlockPlacements.genLineX(w, x, x + 20, new PLocation(x, y, z), this.blockID, 0, false);
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurum/core:zone_wall");
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean canCollideCheck(int par1, boolean par2)
    {
        return false;
    }

    public int getRenderBlockPass()
    {
        return 1;
    }

    public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
    {
        return super.shouldSideBeRendered(par1IBlockAccess, par2, par3, par4, 1 - par5);
    }

    /*@Override
    public TileEntity createNewTileEntity(World world)
    {
    	return null;
    }*/
}