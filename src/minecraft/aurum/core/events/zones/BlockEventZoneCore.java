package aurum.core.events.zones;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockEventZoneCore extends BlockContainer
{
    public BlockEventZoneCore(int par1)
    {
        super(par1, Material.rock);
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityEventZoneCore();
    }
}