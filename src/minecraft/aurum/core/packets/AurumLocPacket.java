package aurum.core.packets;

import net.minecraft.entity.player.EntityPlayer;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class AurumLocPacket extends AurumPacket
{
    protected int xCoord;
    protected int yCoord;
    protected int zCoord;
    protected int dimensionID;

    public AurumLocPacket(int xCoord, int yCoord, int zCoord, int dimensionID)
    {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.zCoord = zCoord;
        this.dimensionID = dimensionID;
    }

    public AurumLocPacket() {}

    @Override
    public void write(ByteArrayDataOutput out)
    {
        out.writeInt(xCoord);
        out.writeInt(yCoord);
        out.writeInt(zCoord);
        out.writeInt(dimensionID);
    }

    @Override
    public void read(ByteArrayDataInput in) throws ProtocolException
    {
        this.xCoord = in.readInt();
        this.yCoord = in.readInt();
        this.zCoord = in.readInt();
        this.dimensionID = in.readInt();
    }

    @Override
    public void execute(EntityPlayer player, Side side) throws ProtocolException {}
}