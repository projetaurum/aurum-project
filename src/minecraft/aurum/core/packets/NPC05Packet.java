package aurum.core.packets;

import net.minecraft.entity.player.EntityPlayer;
import aurum.core.AurumCore;
import aurum.npc.gui.ContainerMarchand;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class NPC05Packet extends AurumPacket
{
	/**
	 * 0 = Name
	 * 1 = Phrase
	 */
	private byte type;
	private String value = "";

	public NPC05Packet(int type, String value)
	{
		this.type = (byte) type;
		this.value = value;
	}

	public NPC05Packet(){}

	@Override
	public void write(ByteArrayDataOutput out) 
	{
		out.writeByte(type);
		out.writeUTF(value);
	}

	@Override
	public void read(ByteArrayDataInput in) throws ProtocolException 
	{
		this.type = in.readByte();
		this.value = in.readUTF();
	}

	@Override
	public void execute(EntityPlayer player, Side side) throws ProtocolException 
	{
		if(side.isServer())
		{
			ContainerMarchand marchandC = (ContainerMarchand) player.openContainer;
			
			if(marchandC.inv.marchand.getOwner().equals(player.username.toLowerCase()) || AurumCore.isOp(player.username))
			{
				if(this.type == 0)
				{
					marchandC.inv.marchand.setPersonnalName(value);
				}
				else if(this.type == 1)
				{
					
				}
			}
			else
				AurumCore.logger.severe("Le joueur "+player.username+" a usurper la possession du PNJ "+marchandC.inv.marchand.getPersonnalName()+" par envoi d'un packet errone !");
		}
		else
		{
			if(this.type == 1 && this.value.contains("#*%"))
			{
				ContainerMarchand marchandC = (ContainerMarchand) player.openContainer;
				
				for(String str : this.value.split("#*%"))
					marchandC.inv.marchand.addPhrase(str.replace("#*", ""));
			}
		}
	}
}