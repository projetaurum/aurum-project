package aurum.core.packets;

import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumn;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class SupColumn07Packet extends AurumLocPacket
{
	private boolean isActivated;
	private boolean hasColumn;
	
	public SupColumn07Packet(){}
	
	public SupColumn07Packet(int x, int y, int z, int dimensionID, boolean isActivated, boolean hasColumn)
	{
		super(x,y,z,dimensionID);
		this.isActivated = isActivated;
		this.hasColumn = hasColumn;
	}
	
	@Override
	public void write(ByteArrayDataOutput out) 
	{
		super.write(out);
		out.writeBoolean(isActivated);
		out.writeBoolean(hasColumn);
	}

	@Override
	public void read(ByteArrayDataInput in) throws ProtocolException 
	{
		super.read(in);
		this.isActivated = in.readBoolean();
		this.hasColumn = in.readBoolean();
	}

	@Override
	public void execute(EntityPlayer player, Side side) throws ProtocolException 
	{
		if(side.isServer())
		{
			EntityPlayerMP playerMP = (EntityPlayerMP)player;
            World world = playerMP.worldObj;
            
            if (world != null)
            {
            	TileEntitySuperpositionColumn column = (TileEntitySuperpositionColumn) world.getBlockTileEntity(xCoord, yCoord, zCoord);
            	
            	if(column != null)
            	{
            		column.setActivated(isActivated);
            		column.setColumn(hasColumn);
            		world.markBlockForUpdate(xCoord, yCoord, zCoord);
            	}
            	else
            		AurumCore.logger.severe("TileEntity on packet SupColumn07Packet is null !");
            }
            else
            	AurumCore.logger.severe("World on packet SupColumn07Packet is null !");
		}
	}
}
