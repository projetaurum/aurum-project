package aurum.core.packets;

import net.minecraft.entity.player.EntityPlayer;
import aurum.npc.gui.ContainerMarchand;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class SlotRemoved04Packet extends AurumPacket
{
	private boolean remove;

	public SlotRemoved04Packet(boolean remove)
	{
		this.remove = remove;
	}

	public SlotRemoved04Packet(){}

	@Override
	public void write(ByteArrayDataOutput out) 
	{
		out.writeBoolean(remove);
	}

	@Override
	public void read(ByteArrayDataInput in) throws ProtocolException 
	{
		this.remove = in.readBoolean();
	}

	@Override
	public void execute(EntityPlayer player, Side side) throws ProtocolException 
	{
		if(side.isServer())
		{
			if(player.openContainer instanceof ContainerMarchand)
			{
				if(remove)
				{
					((ContainerMarchand)player.openContainer).removeTab0();
				}
				else
				{
					((ContainerMarchand)player.openContainer).setTab0();
				}
			}
		}
		else
		{
			throw new ProtocolException("Cannot send this packet to the server!");
		}
	}
}