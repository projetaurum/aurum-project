package aurum.core.packets;

import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.steampunk.tileentity.TileEntityMovingCarpet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class MovingCarpetSetItem02Packet extends AurumLocPacket
{
    protected float value;
    protected int itemID;

    protected ItemStack item;

    /**
     * Values :
     * 0 ~ setItemZ
     * 1 ~ setItemX
     * 2 ~ launchItemMovement
     * 3 ~ setItem
     */
    protected int type;

    /**
     *
     * @param xCoord
     * @param yCoord
     * @param zCoord
     * @param dimensionID
     * @param type
     * @param value
     * @param itemID
     */
    public MovingCarpetSetItem02Packet(int xCoord, int yCoord, int zCoord, int dimensionID, int type, float value, int itemID)
    {
        super(xCoord, yCoord, zCoord, dimensionID);
        this.itemID = itemID;
        this.value = value;
        this.type = type;
    }

    public MovingCarpetSetItem02Packet(int xCoord, int yCoord, int zCoord, int dimensionID, ItemStack item, int itemID)
    {
        super(xCoord, yCoord, zCoord, dimensionID);
        this.itemID = itemID;
        this.item = item;
        this.type = 3;
    }

    public MovingCarpetSetItem02Packet() {}

    @SuppressWarnings("static-access")
    @Override
    public void write(ByteArrayDataOutput out)
    {
        super.write(out);
        out.writeByte(type);
        out.writeByte(itemID);

        if (type == 0)
        {
            out.writeFloat(value);
        }
        else if (type == 2)
        {
            return;
        }
        else if (type == 3)
            try
            {
                this.writeItemStack(item, out);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
    }

    @SuppressWarnings("static-access")
    @Override
    public void read(ByteArrayDataInput in) throws ProtocolException
    {
        super.read(in);
        this.type = in.readByte();
        this.itemID = in.readByte();

        if (type == 0)
        {
            this.value = in.readFloat();
        }
        else if (type == 2)
        {
            return;
        }
        else if (type == 3)
            try
            {
                this.item = this.readItemStack(in);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
    }

    @Override
    public void execute(EntityPlayer player, Side side) throws ProtocolException
    {
        if (side.isClient())
        {
            World w = player.worldObj;

            if (w.getBlockTileEntity(xCoord, yCoord, zCoord) != null && w.getBlockTileEntity(xCoord, yCoord, zCoord) instanceof TileEntityMovingCarpet)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) w.getBlockTileEntity(xCoord, yCoord, zCoord);

                if (type == 0)
                {
                    carpet.setItemZ(itemID, value);
                }
                else if (type == 2)
                {
                    carpet.launchItemMovement(this.itemID, false);
                }
                else if (type == 3)
                {
                }
            }
        }
        else
        {
            throw new ProtocolException("Cannot send this packet to the server!");
        }
    }
}