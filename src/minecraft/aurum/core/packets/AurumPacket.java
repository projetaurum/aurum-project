package aurum.core.packets;

import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;

public abstract class AurumPacket
{
    public static final String CHANNEL = "AurumChannel";
    private static final BiMap < Integer, Class <? extends AurumPacket >> idMap;

    static
    {
        ImmutableBiMap.Builder < Integer, Class <? extends AurumPacket >> builder = ImmutableBiMap.builder();
        builder.put(Integer.valueOf(0), AurumLocPacket.class);
        builder.put(Integer.valueOf(1), MovingCarpetDrop01Packet.class);
        builder.put(Integer.valueOf(2), MovingCarpetSetItem02Packet.class);
        builder.put(Integer.valueOf(3), Piedestal03Packet.class);
        builder.put(Integer.valueOf(4), SlotRemoved04Packet.class);
        builder.put(Integer.valueOf(5), NPC05Packet.class);
        builder.put(Integer.valueOf(6), BourseInv06Packet.class);
        builder.put(Integer.valueOf(7), SupColumn07Packet.class);
        idMap = builder.build();
    }

    public static AurumPacket constructPacket(int packetId) throws ProtocolException, ReflectiveOperationException
    {
        Class <? extends AurumPacket > clazz = idMap.get(Integer.valueOf(packetId));

        if (clazz == null)
        {
            throw new ProtocolException("Unknown Packet Id!");
        }
        else
        {
            return clazz.newInstance();
        }
    }

    @SuppressWarnings("serial")
    public static class ProtocolException extends Exception
    {
        public ProtocolException() {}

        public ProtocolException(String message, Throwable cause)
        {
            super(message, cause);
        }
        public ProtocolException(String message)
        {
            super(message);
        }
        public ProtocolException(Throwable cause)
        {
            super(cause);
        }
    }

    public final int getPacketId()
    {
        if (idMap.inverse().containsKey(getClass()))
        {
            return idMap.inverse().get(getClass()).intValue();
        }
        else
        {
            throw new RuntimeException("Packet " + getClass().getSimpleName() + " is missing a mapping!");
        }
    }

    public final Packet makePacket()
    {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeByte(getPacketId());
        write(out);
        return PacketDispatcher.getPacket(CHANNEL, out.toByteArray());
    }

    /**
     * Reads a ItemStack from the InputStream
     */
    public static ItemStack readItemStack(ByteArrayDataInput in) throws IOException
    {
        ItemStack itemstack = null;
        short short1 = in.readShort();

        if (short1 >= 0)
        {
            byte b0 = in.readByte();
            short short2 = in.readShort();
            itemstack = new ItemStack(short1, b0, short2);
            itemstack.stackTagCompound = readNBTTagCompound(in);
        }

        return itemstack;
    }

    /**
     * Writes the ItemStack's ID (short), then size (byte), then damage. (short)
     */
    public static void writeItemStack(ItemStack par0ItemStack, ByteArrayDataOutput out) throws IOException
    {
        if (par0ItemStack == null)
        {
            out.writeShort(-1);
        }
        else
        {
            out.writeShort(par0ItemStack.itemID);
            out.writeByte(par0ItemStack.stackSize);
            out.writeShort(par0ItemStack.getItemDamage());
            NBTTagCompound nbttagcompound = null;

            if (par0ItemStack.getItem().isDamageable() || par0ItemStack.getItem().getShareTag())
            {
                nbttagcompound = par0ItemStack.stackTagCompound;
            }

            writeNBTTagCompound(nbttagcompound, out);
        }
    }

    protected static void writeNBTTagCompound(NBTTagCompound par0NBTTagCompound, ByteArrayDataOutput out) throws IOException
    {
        if (par0NBTTagCompound == null)
        {
            out.writeShort(-1);
        }
        else
        {
            byte[] abyte = CompressedStreamTools.compress(par0NBTTagCompound);
            out.writeShort((short)abyte.length);
            out.write(abyte);
        }
    }

    public static NBTTagCompound readNBTTagCompound(ByteArrayDataInput in) throws IOException
    {
        short short1 = in.readShort();

        if (short1 < 0)
        {
            return null;
        }
        else
        {
            byte[] abyte = new byte[short1];
            in.readFully(abyte);
            return CompressedStreamTools.decompress(abyte);
        }
    }

    public abstract void write(ByteArrayDataOutput out);

    public abstract void read(ByteArrayDataInput in) throws ProtocolException;

    public abstract void execute(EntityPlayer player, Side side) throws ProtocolException;
}