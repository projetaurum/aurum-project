package aurum.core.packets;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.steampunk.tileentity.TileEntityMovingCarpet;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.relauncher.Side;

public class MovingCarpetDrop01Packet extends AurumPacket
{
    private String player;
    private int xCoord;
    private int yCoord;
    private int zCoord;
    private int item;

    public MovingCarpetDrop01Packet(int xCoord, int yCoord, int zCoord, int item, String player)
    {
        this.player = player;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.zCoord = zCoord;
        this.item = item;
    }

    public MovingCarpetDrop01Packet() {}

    @Override
    public void write(ByteArrayDataOutput out)
    {
        out.writeUTF(player);
        out.writeInt(xCoord);
        out.writeInt(yCoord);
        out.writeInt(zCoord);
        out.writeInt(item);
    }

    @Override
    public void read(ByteArrayDataInput in) throws ProtocolException
    {
        this.player = in.readUTF();
        this.xCoord = in.readInt();
        this.yCoord = in.readInt();
        this.zCoord = in.readInt();
        this.item = in.readInt();
    }

    @Override
    public void execute(EntityPlayer player, Side side) throws ProtocolException
    {
        if (side.isServer())
        {
            EntityPlayerMP playerMP = MinecraftServer.getServerConfigurationManager(AurumCore.server).getPlayerForUsername(this.player);
            World w = playerMP.worldObj;

            if (w.getBlockTileEntity(xCoord, yCoord, zCoord) != null && w.getBlockTileEntity(xCoord, yCoord, zCoord) instanceof TileEntityMovingCarpet)
            {
                TileEntityMovingCarpet carpet = (TileEntityMovingCarpet) w.getBlockTileEntity(xCoord, yCoord, zCoord);
                carpet.dropItem(item);
            }
        }
        else
        {
            throw new ProtocolException("Cannot send this packet to the client!");
        }
    }
}