package aurum.core.packets;

import net.minecraft.entity.player.EntityPlayer;
import aurum.npc.gui.ContainerMarchand;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;

public class BourseInv06Packet extends AurumPacket
{
	public BourseInv06Packet() {}
	
	@Override
	public void write(ByteArrayDataOutput out) 
	{

	}

	@Override
	public void read(ByteArrayDataInput in) throws ProtocolException 
	{

	}

	@Override
	public void execute(EntityPlayer player, Side side) throws ProtocolException 
	{
		if(side.isClient())
		{
			if(player.openContainer instanceof ContainerMarchand)
			{
				System.out.println("CLIENT RECEIVE");
				((ContainerMarchand)player.openContainer).updateBourse();
				PacketDispatcher.sendPacketToServer(new BourseInv06Packet().makePacket());
			}
		}
		else
		{
			if(player.openContainer instanceof ContainerMarchand)
			{
				System.out.println("SERVER RECEIVE");
				((ContainerMarchand)player.openContainer).updateBourse();
			}
		}
	}
}