package aurum.core.packets;

import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.stockage.tileentity.TileEntityPiedestal;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;

public class Piedestal03Packet extends AurumLocPacket
{
    private ItemStack stack;

    public Piedestal03Packet(int xCoord, int yCoord, int zCoord, int dimensionID, ItemStack stack)
    {
        super(xCoord, yCoord, zCoord, dimensionID);
        this.stack = stack;
    }

    public Piedestal03Packet() {}

    @Override
    public void write(ByteArrayDataOutput out)
    {
        super.write(out);

        try
        {
            writeItemStack(stack, out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void read(ByteArrayDataInput in) throws ProtocolException
    {
        super.read(in);

        try
        {
            this.stack = readItemStack(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(EntityPlayer player, Side side) throws ProtocolException
    {
        if (side.isServer())
        {
            World world = AurumCore.server.worldServerForDimension(dimensionID);

            if (world != null)
            {
                TileEntityPiedestal piedestal = (TileEntityPiedestal) world.getBlockTileEntity(xCoord, yCoord, zCoord);

                if (piedestal != null)
                {
                    piedestal.setInventorySlotContents(0, stack);
                    PacketDispatcher.sendPacketToAllAround(xCoord, yCoord, zCoord, 30.0d, dimensionID, new Piedestal03Packet(xCoord, yCoord, zCoord, dimensionID, stack).makePacket());
                }
                else
                {
                    AurumCore.logger.log(Level.SEVERE, "World tileEntityPiedestal is null ! Its a big error !");
                }
            }
            else
            {
                AurumCore.logger.log(Level.SEVERE, "World object is null ! Its a big error !");
            }
        }
        else
        {
            World world = Minecraft.getMinecraft().theWorld;
            TileEntityPiedestal piedestal = (TileEntityPiedestal) world.getBlockTileEntity(xCoord, yCoord, zCoord);

            if (piedestal != null)
            {
                if (stack != null)
                {
                    stack.stackSize = 1;
                }

                piedestal.setInventorySlotContents(0, stack);
            }
            else
            {
                AurumCore.logger.log(Level.SEVERE, "World tileEntityPiedestal is null ! Its a big error !");
            }
        }
    }

    public void dropItem(World world, ItemStack itemstack)
    {
        world.markBlockForUpdate(xCoord, yCoord, zCoord);
        Random rand = new Random();

        if (itemstack != null)
        {
            float f = rand.nextFloat() * 0.8F + 0.1F;
            float f1 = rand.nextFloat() * 0.8F + 0.1F;
            EntityItem entityitem;

            for (float f2 = rand.nextFloat() * 0.8F + 0.1F; itemstack.stackSize > 0; world.spawnEntityInWorld(entityitem))
            {
                int k1 = rand.nextInt(21) + 10;

                if (k1 > itemstack.stackSize)
                {
                    k1 = itemstack.stackSize;
                }

                itemstack.stackSize -= k1;
                entityitem = new EntityItem(world, (double)((float)this.xCoord + f), (double)((float)this.yCoord + f1), (double)((float)this.zCoord + f2), new ItemStack(itemstack.itemID, k1, itemstack.getItemDamage()));
                float f3 = 0.05F;
                entityitem.motionX = (double)((float)rand.nextGaussian() * f3);
                entityitem.motionY = (double)((float)rand.nextGaussian() * f3 + 0.2F);
                entityitem.motionZ = (double)((float)rand.nextGaussian() * f3);

                if (itemstack.hasTagCompound())
                {
                    entityitem.getEntityItem().setTagCompound((NBTTagCompound)itemstack.getTagCompound().copy());
                }
            }
        }
    }
}