package aurum.core;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public class ItemAurum extends Item
{
    String texture;

    String[] textures;
    String[] names;

    Icon iconBuffer[];

    int subDamage = 0;

    public ItemAurum(int id, String texture)
    {
        super(id);
        this.texture = texture;
        this.setHasSubtypes(false);
    }

    public ItemAurum(int id, String[] textures, int damage, String[] names)
    {
        super(id);
        this.textures = textures;
        this.subDamage = damage;
        this.names = names;
        this.setHasSubtypes(true);
    }

    @Override
    public String getItemDisplayName(ItemStack stack)
    {
        return names[stack.getItemDamage()];
    }

    @Override
    public Icon getIconFromDamage(int i)
    {
        return iconBuffer[i];
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubItems(int par1, CreativeTabs tab, List list)
    {
        if (this.hasSubtypes)
        {
            for (int i = 0; i < this.subDamage; i++)
            {
                list.add(new ItemStack(this.itemID, 1, i));
            }
        }
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[this.subDamage];

        if (this.hasSubtypes)
        {
            for (int i = 0; i < this.subDamage; i++)
            {
                this.iconBuffer[i] = register.registerIcon("aurum:" + this.textures[i]);
            }
        }

        this.itemIcon = register.registerIcon("aurum:" + texture);
    }
}