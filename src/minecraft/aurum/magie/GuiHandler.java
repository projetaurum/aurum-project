package aurum.magie;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import aurum.magie.runes.ContainerNexus;
import aurum.magie.runes.GuiNexus;
import aurum.magie.runes.TileEntityNexus;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

        if (id == 0)
        {
            return new ContainerNexus((TileEntityNexus) tileEntity, player);
        }

        return null;
    }

    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

        if (id == 0)
        {
            return new GuiNexus(new ContainerNexus((TileEntityNexus) tileEntity, player), (TileEntityNexus) tileEntity);
        }

        return null;
    }
}