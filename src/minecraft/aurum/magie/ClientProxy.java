package aurum.magie;

import aurum.core.multiblocks.TESRInventoryRenderer;
import aurum.core.multiblocks.TESRInventoryRenderer.TESRIndex;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumn;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumnSpecialRenderer;
import cpw.mods.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySuperpositionColumn.class, new TileEntitySuperpositionColumnSpecialRenderer());
        TESRInventoryRenderer.blockByTESR.put(new TESRIndex(AurumMagie.blockSuperpositionColumn, 0), new TileEntitySuperpositionColumnSpecialRenderer());
    }
}
