package aurum.magie.runes;

import java.util.List;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.core.PLocation;
import aurum.magie.AurumMagie;

public class BlockNexus extends BlockRuneBase
{
    public Icon[] iconBuffer;

    public BlockNexus(int par1)
    {
        super(par1, Material.rock);
        this.setBlockUnbreakable();
        this.setTickRandomly(true);
    }

    @Override
    public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
    {
        if (player.isSneaking() || !((TileEntityNexus)w.getBlockTileEntity(x, y, z)).isNode())
        {
            return true;
        }

        TileEntityNexus nexus = (TileEntityNexus)w.getBlockTileEntity(x, y, z);
        nexus.setSword(Item.swordDiamond);
        player.openGui(AurumMagie.instance, 0, w, x, y, z);
        return true;
    }

    @Override
    public void randomDisplayTick(World w, int x, int y, int z, Random rand)
    {
        if (rand.nextInt(30) == 27)
        {
            TileEntityNexus nexus = (TileEntityNexus) w.getBlockTileEntity(x, y, z);
            PLocation nodeLoc = nexus.getNodeLoc();

            if (nodeLoc == null)
                return;

            if (w.getBlockId(nodeLoc.getPosX(), nodeLoc.getPosY(), nodeLoc.getPosZ()) == this.blockID)
            {
                if (!nexus.isNode())
                {
                    if (w.getBlockId(nodeLoc.getPosX(), nodeLoc.getPosY(), nodeLoc.getPosZ()) == this.blockID)
                    {
                        TileEntityNexus node = (TileEntityNexus) w.getBlockTileEntity(nodeLoc.getPosX(), nodeLoc.getPosY(), nodeLoc.getPosZ());

                        if (node.isNode())
                            return;
                        else
                            w.destroyBlock(x, y, z, false);
                    }
                    else
                        w.destroyBlock(x, y, z, false);
                }
                else
                    return;
            }
            else
                w.destroyBlock(x, y, z, false);
        }
    }

    @Override
    public int damageDropped(int par1)
    {
        return 0;
    }

    @Override
    public int getRenderBlockPass()
    {
        return 1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[4];
        this.iconBuffer[0] = register.registerIcon("aurummagie:runes/rune_nexus_block");
        this.iconBuffer[1] = register.registerIcon("aurummagie:runes/rune_nexus_node");
        this.iconBuffer[2] = register.registerIcon("aurummagie:runes/rune_nexus_node_arme");
        this.iconBuffer[3] = register.registerIcon("aurummagie:runes/rune_nexus_block_arme");
    }

    @Override
    public Icon getBlockTexture(IBlockAccess access, int x, int y, int z, int side)
    {
        TileEntityNexus nexus = (TileEntityNexus) access.getBlockTileEntity(x, y, z);

        if (nexus.isNode())
        {
            if (nexus.hasSword())
            {
                return iconBuffer[2];
            }

            return iconBuffer[1];
        }
        else
        {
            if (nexus.hasSword())
            {
                return iconBuffer[3];
            }

            return iconBuffer[0];
        }
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityNexus();
    }

    @Override
    public void setBlockBoundsBasedOnState(IBlockAccess access, int x, int y, int z)
    {
        float f;
        float f1;
        int metadata = access.getBlockMetadata(x, y, z);

        if (metadata == 0)
        {
            f = 0.0625F;
            f1 = 0.5F;
            this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f1, 0.5F + f, 1.0F, 0.5F + f1);
        }
        else if (metadata == 1)
        {
            f = 0.5F;
            f1 = 0.0625F;
            this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f1, 0.5F + f, 1.0F, 0.5F + f1);
        }
        else if (metadata == 2)
        {
            f = 0.0625F;
            this.setBlockBounds(0.0F, 0.5F - (f / 2), 0.0F, 1.0F, 0.5F + (f / 2), 1.0F);
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void addCollisionBoxesToList(World w, int x, int y, int z, AxisAlignedBB axis, List list, Entity entity)
    {
        if (entity instanceof EntityPlayer)
        {
        	return;
        }
        else
        {
            AxisAlignedBB axisalignedbb1 = this.getCollisionBoundingBoxFromPool(w, x, y, z);

            if(w.getBlockMetadata(x, y, z) == 0)
            	axisalignedbb1 = AxisAlignedBB.getAABBPool().getAABB((double)x + 0.5f-0.0625f, 
            			(double)y + 0.0f, (double)z + 0.0f, (double)x + 0.5f+0.0625f, 
            			(double)y + 1.0f, (double)z + 1.0f);
            else if(w.getBlockMetadata(x, y, z) == 1)
            	axisalignedbb1 = AxisAlignedBB.getAABBPool().getAABB((double)x + 0.0f, 
            			(double)y + 0.0f, (double)z + 0.5f-0.0625f, (double)x + 1.0f, 
            			(double)y + 1.0f, (double)z + 0.5f+0.0625f);
            else if(w.getBlockMetadata(x, y, z) == 2)
            	axisalignedbb1 = AxisAlignedBB.getAABBPool().getAABB((double)x + 0.0f, 
            			(double)y + 0.5f-(0.0625f/2), (double)z + 0.0f, (double)x + 1.0f, 
            			(double)y + 0.5f+(0.0625f/2), (double)z + 1.0f);
            
            if (axisalignedbb1 != null && axis.intersectsWith(axisalignedbb1))
            {
                list.add(axisalignedbb1);
            }
        }
    }
}