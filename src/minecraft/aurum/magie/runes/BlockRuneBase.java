package aurum.magie.runes;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockRuneBase extends BlockContainer
{
	public BlockRuneBase(int par1, Material m)
	{
		super(par1, m);
		float f = 0.5F;
		float f1 = 0.015625F;
		this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f1, 0.5F + f);
	}

	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z)
	{
		float f = 0.0625F;
		int metadata = world.getBlockMetadata(x, y, z);

		if (metadata == 0)
			return AxisAlignedBB.getAABBPool().getAABB((double)x + this.minX+f, (double)y + 1f-f, (double)z + this.minZ+f, (double)x + this.maxX-f, (double)y + this.maxY, (double)z + this.maxZ-f);
		else if (metadata == 1)
			return AxisAlignedBB.getAABBPool().getAABB((double)x + this.minX+f, (double)y + this.minY, (double)z + this.minZ+f, (double)x + this.maxX-f, (double)y + f, (double)z + this.maxZ-f);
		else if (metadata == 2)
			return AxisAlignedBB.getAABBPool().getAABB((double)x + this.minX+f, (double)y + this.minY+f, (double)z + 1f-f, (double)x + this.maxX-f, (double)y + this.maxY-f, (double)z + this.maxZ);
		else if (metadata == 3)
			return AxisAlignedBB.getAABBPool().getAABB((double)x + this.minX+f, (double)y + this.minY+f, (double)z + this.minZ, (double)x + this.maxX-f, (double)y + this.maxY-f, (double)z + f);
		else if (metadata == 4)
			return AxisAlignedBB.getAABBPool().getAABB((double)x + 1f-f, (double)y + this.minY+f, (double)z + this.minZ+f, (double)x + this.maxX, (double)y + this.maxY-f, (double)z + this.maxZ-f);
		return AxisAlignedBB.getAABBPool().getAABB((double)x + this.minX, (double)y + this.minY+f, (double)z + this.minZ+f, (double)x + f, (double)y + this.maxY-f, (double)z + this.maxZ-f);
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess access, int x, int y, int z)
	{
		float f = 0.001F;
		int metadata = access.getBlockMetadata(x, y, z);

		if (metadata == 0)
		{
			this.setBlockBounds(0.0F, 1.0F - f, 0.0F, 1.0F, 1.0F, 1.0F);
		}
		else if (metadata == 1)
		{
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, f, 1.0F);
		}
		else if (metadata == 2)
		{
			this.setBlockBounds(0.0F, 0.0F, 1.0F - f, 1.0F, 1.0F, 1.0F);
		}
		else if (metadata == 3)
		{
			this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, f);
		}
		else if (metadata == 4)
		{
			this.setBlockBounds(1.0F - f, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}
		else if (metadata == 5)
		{
			this.setBlockBounds(0.0F, 0.0F, 0.0F, f, 1.0F, 1.0F);
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return new TileEntityRuneBase();
	}

	public boolean renderAsNormalBlock()
	{
		return false;
	}

	public boolean isOpaqueCube()
	{
		return false;
	}
}