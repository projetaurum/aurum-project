package aurum.magie.runes;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSoulStealer extends ModelBase
{
    ModelRenderer Block;
    ModelRenderer Block1;
    ModelRenderer Block2;
    ModelRenderer Block3;
    ModelRenderer Block4;
    ModelRenderer Block5;
    ModelRenderer Block6;
    ModelRenderer Block11;
    ModelRenderer Block12;
    ModelRenderer Block13;
    ModelRenderer Block14;
    ModelRenderer Block15;
    ModelRenderer Block16;
    ModelRenderer Block17;
    ModelRenderer Block18;
    ModelRenderer Block19;
    ModelRenderer Block21;
    ModelRenderer Block110;
    ModelRenderer Block111;
    ModelRenderer Block112;
    ModelRenderer Block113;
    ModelRenderer Block114;
    ModelRenderer Block115;
    ModelRenderer Block116;
    ModelRenderer Block117;
    ModelRenderer Block118;
    ModelRenderer Block22;

    public ModelSoulStealer()
    {
        this( 0.0f );
    }

    public ModelSoulStealer( float par1 )
    {
        Block = new ModelRenderer( this, 8, 3 );
        Block.setTextureSize( 64, 32 );
        Block.addBox( -1F, -1.5F, -1F, 2, 4, 2);
        Block.setRotationPoint( 0F, 7F, 0F );
        Block1 = new ModelRenderer( this, 8, 0 );
        Block1.setTextureSize( 64, 32 );
        Block1.addBox( -1F, -1F, -1F, 2, 1, 2);
        Block1.setRotationPoint( 0F, 5.5F, 0F );
        Block2 = new ModelRenderer( this, 8, 0 );
        Block2.setTextureSize( 64, 32 );
        Block2.addBox( -1F, -1F, -1F, 2, 1, 2);
        Block2.setRotationPoint( 0F, 10.5F, 0F );
        Block3 = new ModelRenderer( this, 0, 0 );
        Block3.setTextureSize( 64, 32 );
        Block3.addBox( -1F, -6F, -1F, 2, 12, 2);
        Block3.setRotationPoint( 0F, -1.5F, 0F );
        Block4 = new ModelRenderer( this, 0, 0 );
        Block4.setTextureSize( 64, 32 );
        Block4.addBox( -1F, -6F, -1F, 2, 12, 2);
        Block4.setRotationPoint( 0F, 16.5F, 0F );
        Block5 = new ModelRenderer( this, 0, 2 );
        Block5.setTextureSize( 64, 32 );
        Block5.addBox( -1F, 0F, -1F, 2, 1, 2);
        Block5.setRotationPoint( 0F, 22.5F, 0F );
        Block6 = new ModelRenderer( this, 2, 3 );
        Block6.setTextureSize( 64, 32 );
        Block6.addBox( -0.5F, 0F, -0.5F, 1, 1, 1);
        Block6.setRotationPoint( 0F, 23.5F, 0F );
        Block11 = new ModelRenderer( this, 16, 0 );
        Block11.setTextureSize( 64, 32 );
        Block11.addBox( -2F, -1.5F, -0.5F, 4, 3, 1);
        Block11.setRotationPoint( 0F, -5.985662F, 0F );
        Block12 = new ModelRenderer( this, 18, 0 );
        Block12.setTextureSize( 64, 32 );
        Block12.addBox( -2F, -3F, -0.5F, 2, 3, 1);
        Block12.setRotationPoint( 2F, -4.985662F, 0F );
        Block13 = new ModelRenderer( this, 18, 0 );
        Block13.setTextureSize( 64, 32 );
        Block13.addBox( -2F, -3F, -0.5F, 2, 3, 1);
        Block13.setRotationPoint( 4.120462F, -7.085663F, 0F );
        Block14 = new ModelRenderer( this, 19, 0 );
        Block14.setTextureSize( 64, 32 );
        Block14.addBox( -1.5F, -3F, -0.5F, 1, 3, 1);
        Block14.setRotationPoint( 3.634761F, -9.954403F, 0F );
        Block15 = new ModelRenderer( this, 19, 0 );
        Block15.setTextureSize( 64, 32 );
        Block15.addBox( -1F, -3F, -0.5F, 1, 3, 1);
        Block15.setRotationPoint( 4.13005F, -10.10083F, 0F );
        Block16 = new ModelRenderer( this, 18, 0 );
        Block16.setTextureSize( 64, 32 );
        Block16.addBox( 0F, -3F, -0.5F, 2, 3, 1);
        Block16.setRotationPoint( -2F, -4.985662F, 0F );
        Block17 = new ModelRenderer( this, 17, 0 );
        Block17.setTextureSize( 64, 32 );
        Block17.addBox( 0F, -3F, -0.5F, 2, 3, 1);
        Block17.setRotationPoint( -4.120462F, -7.085663F, 0F );
        Block18 = new ModelRenderer( this, 19, 0 );
        Block18.setTextureSize( 64, 32 );
        Block18.addBox( 0.5F, -3F, -0.5F, 1, 3, 1);
        Block18.setRotationPoint( -3.634761F, -9.954403F, 0F );
        Block19 = new ModelRenderer( this, 18, 0 );
        Block19.setTextureSize( 64, 32 );
        Block19.addBox( 0F, -3F, -0.5F, 1, 3, 1);
        Block19.setRotationPoint( -4.13005F, -10.10083F, 0F );
        Block21 = new ModelRenderer( this, 8, 9 );
        Block21.setTextureSize( 64, 32 );
        Block21.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block21.setRotationPoint( 0F, -11F, 0F );
        Block110 = new ModelRenderer( this, 16, 0 );
        Block110.setTextureSize( 64, 32 );
        Block110.addBox( -2F, -1.5F, -0.5F, 4, 3, 1);
        Block110.setRotationPoint( 0F, -5.98596F, 0F );
        Block111 = new ModelRenderer( this, 18, 0 );
        Block111.setTextureSize( 64, 32 );
        Block111.addBox( -2F, -3F, -0.5F, 2, 3, 1);
        Block111.setRotationPoint( -2.384186E-07F, -4.98596F, -2F );
        Block112 = new ModelRenderer( this, 18, 0 );
        Block112.setTextureSize( 64, 32 );
        Block112.addBox( -2F, -3F, -0.5F, 2, 3, 1);
        Block112.setRotationPoint( -4.911973E-07F, -7.08596F, -4.120462F );
        Block113 = new ModelRenderer( this, 19, 0 );
        Block113.setTextureSize( 64, 32 );
        Block113.addBox( -1.5F, -3F, -0.5F, 1, 3, 1);
        Block113.setRotationPoint( -4.332973E-07F, -9.9547F, -3.634761F );
        Block114 = new ModelRenderer( this, 18, 0 );
        Block114.setTextureSize( 64, 32 );
        Block114.addBox( -1F, -3F, -0.5F, 1, 3, 1);
        Block114.setRotationPoint( -4.923403E-07F, -10.10113F, -4.13005F );
        Block115 = new ModelRenderer( this, 18, 0 );
        Block115.setTextureSize( 64, 32 );
        Block115.addBox( 0F, -3F, -0.5F, 2, 3, 1);
        Block115.setRotationPoint( 2.384186E-07F, -4.98596F, 2F );
        Block116 = new ModelRenderer( this, 18, 0 );
        Block116.setTextureSize( 64, 32 );
        Block116.addBox( 0F, -3F, -0.5F, 2, 3, 1);
        Block116.setRotationPoint( 4.911973E-07F, -7.08596F, 4.120462F );
        Block117 = new ModelRenderer( this, 18, 0 );
        Block117.setTextureSize( 64, 32 );
        Block117.addBox( 0.5F, -3F, -0.5F, 1, 3, 1);
        Block117.setRotationPoint( 4.332973E-07F, -9.9547F, 3.634761F );
        Block118 = new ModelRenderer( this, 18, 0 );
        Block118.setTextureSize( 64, 32 );
        Block118.addBox( 0F, -3F, -0.5F, 1, 3, 1);
        Block118.setRotationPoint( 4.923403E-07F, -10.10113F, 4.13005F );
        Block22 = new ModelRenderer( this, 8, 9 );
        Block22.setTextureSize( 64, 32 );
        Block22.addBox( -1F, -1F, -1F, 2, 2, 2);
        Block22.setRotationPoint( 0F, -11F, 0F );
    }

   public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
   {
        Block.rotateAngleX = 0F;
        Block.rotateAngleY = -0.7853982F;
        Block.rotateAngleZ = 0F;
        Block.renderWithRotation(par7);

        Block1.rotateAngleX = 0F;
        Block1.rotateAngleY = -0.7853982F;
        Block1.rotateAngleZ = 0F;
        Block1.renderWithRotation(par7);

        Block2.rotateAngleX = 0F;
        Block2.rotateAngleY = -0.7853982F;
        Block2.rotateAngleZ = 0F;
        Block2.renderWithRotation(par7);

        Block3.rotateAngleX = 0F;
        Block3.rotateAngleY = -0.7853982F;
        Block3.rotateAngleZ = 0F;
        Block3.renderWithRotation(par7);

        Block4.rotateAngleX = 0F;
        Block4.rotateAngleY = -0.7853982F;
        Block4.rotateAngleZ = 0F;
        Block4.renderWithRotation(par7);

        Block5.rotateAngleX = 0F;
        Block5.rotateAngleY = -0.7853982F;
        Block5.rotateAngleZ = 0F;
        Block5.renderWithRotation(par7);

        Block6.rotateAngleX = 0F;
        Block6.rotateAngleY = -0.7853982F;
        Block6.rotateAngleZ = 0F;
        Block6.renderWithRotation(par7);

        Block11.rotateAngleX = 0F;
        Block11.rotateAngleY = 0F;
        Block11.rotateAngleZ = 0F;
        Block11.renderWithRotation(par7);

        Block12.rotateAngleX = 0F;
        Block12.rotateAngleY = 0F;
        Block12.rotateAngleZ = 0.7853982F;
        Block12.renderWithRotation(par7);

        Block13.rotateAngleX = 0F;
        Block13.rotateAngleY = 0F;
        Block13.rotateAngleZ = 9.448085E-09F;
        Block13.renderWithRotation(par7);

        Block14.rotateAngleX = 0F;
        Block14.rotateAngleY = 0F;
        Block14.rotateAngleZ = 9.448085E-09F;
        Block14.renderWithRotation(par7);

        Block15.rotateAngleX = 0F;
        Block15.rotateAngleY = 0F;
        Block15.rotateAngleZ = -0.3474076F;
        Block15.renderWithRotation(par7);

        Block16.rotateAngleX = 0F;
        Block16.rotateAngleY = 0F;
        Block16.rotateAngleZ = -0.7853982F;
        Block16.renderWithRotation(par7);

        Block17.rotateAngleX = 0F;
        Block17.rotateAngleY = 0F;
        Block17.rotateAngleZ = -9.448085E-09F;
        Block17.renderWithRotation(par7);

        Block18.rotateAngleX = 0F;
        Block18.rotateAngleY = 0F;
        Block18.rotateAngleZ = -9.448085E-09F;
        Block18.renderWithRotation(par7);

        Block19.rotateAngleX = 0F;
        Block19.rotateAngleY = 0F;
        Block19.rotateAngleZ = 0.3474076F;
        Block19.renderWithRotation(par7);

        Block21.rotateAngleX = 0F;
        Block21.rotateAngleY = -0.2617994F;
        Block21.rotateAngleZ = 0F;
        Block21.renderWithRotation(par7);

        Block110.rotateAngleX = 0F;
        Block110.rotateAngleY = 1.570796F;
        Block110.rotateAngleZ = 0F;
        Block110.renderWithRotation(par7);

        Block111.rotateAngleX = 6.680807E-09F;
        Block111.rotateAngleY = 1.570796F;
        Block111.rotateAngleZ = 0.7853982F;
        Block111.renderWithRotation(par7);

        Block112.rotateAngleX = 6.680806E-09F;
        Block112.rotateAngleY = 1.570796F;
        Block112.rotateAngleZ = -4.203828E-09F;
        Block112.renderWithRotation(par7);

        Block113.rotateAngleX = 6.680806E-09F;
        Block113.rotateAngleY = 1.570796F;
        Block113.rotateAngleZ = -4.203828E-09F;
        Block113.renderWithRotation(par7);

        Block114.rotateAngleX = 4.190468E-09F;
        Block114.rotateAngleY = 1.570796F;
        Block114.rotateAngleZ = -0.3474076F;
        Block114.renderWithRotation(par7);

        Block115.rotateAngleX = -6.680807E-09F;
        Block115.rotateAngleY = 1.570796F;
        Block115.rotateAngleZ = -0.7853982F;
        Block115.renderWithRotation(par7);

        Block116.rotateAngleX = -6.680806E-09F;
        Block116.rotateAngleY = 1.570796F;
        Block116.rotateAngleZ = 4.203828E-09F;
        Block116.renderWithRotation(par7);

        Block117.rotateAngleX = -6.680806E-09F;
        Block117.rotateAngleY = 1.570796F;
        Block117.rotateAngleZ = 4.203828E-09F;
        Block117.renderWithRotation(par7);

        Block118.rotateAngleX = -4.190468E-09F;
        Block118.rotateAngleY = 1.570796F;
        Block118.rotateAngleZ = 0.3474076F;
        Block118.renderWithRotation(par7);

        Block22.rotateAngleX = 0F;
        Block22.rotateAngleY = -0.2617994F;
        Block22.rotateAngleZ = 0F;
        Block22.renderWithRotation(par7);

    }

}
