package aurum.magie.runes;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;

public class ContainerNexus extends Container
{
    TileEntityNexus nexus;

    public ContainerNexus(TileEntityNexus nexus, EntityPlayer player)
    {
        this.nexus = nexus;
        bindPlayerInventory(player.inventory);
        bindNexusInventory(nexus);
    }
    
    protected void bindNexusInventory(TileEntityNexus nexus)
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                addSlotToContainer(new Slot(nexus, j + i * 9,
                                            j * 18+8, 32+ i * 18));
            }
        }
    }

    protected void bindPlayerInventory(InventoryPlayer inventoryPlayer)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
                                            j * 18+8, 84 -11 + i * 18));
            }
        }

        for (int i = 0; i < 9; i++)
        {
            addSlotToContainer(new Slot(inventoryPlayer, i, i * 18+8, 142-11));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer)
    {
        return nexus.isUseableByPlayer(entityplayer);
    }
}