package aurum.magie.runes;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;


public class TileEntityRuneBases extends TileEntityRuneBase
{
	private byte nbtData;
	
	public TileEntityRuneBases() {}
	
	public void setNBTData(int data)
	{
		this.nbtData = (byte) data;
	}
	
    @Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet)
    {
        readFromNBT(packet.data);
    }
	
	public int getNBTData()
	{
		return this.nbtData;
	}
	
    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        tag.setByte("NBTData", nbtData);
        super.writeToNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        this.nbtData = tag.getByte("NBTData");
        super.readFromNBT(tag);
    }
}