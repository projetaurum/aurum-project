package aurum.magie.runes.crafters;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.multiblocks.TileEntityGagBase;
import aurum.magie.AurumMagie;

public class ItemBlockSuperpositionColumn extends ItemBlock
{
	public ItemBlockSuperpositionColumn(int par1) 
	{
		super(par1);
        this.setUnlocalizedName("ItemBlockSuperpositionColumn");
	}
	
    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata)
    {
        if (!world.setBlock(x, y, z, AurumMagie.blockSuperpositionColumn.blockID, MathHelper.floor_double((double)(player.rotationYaw * 4.0F / 360.0F) + 2.5D) & 3, 3))
            return false;
        
        if (world.getBlockId(x, y, z) == this.getBlockID())
        {
            Block.blocksList[this.getBlockID()].onBlockPlacedBy(world, x, y, z, player, stack);
            Block.blocksList[this.getBlockID()].onPostBlockPlaced(world, x, y, z, metadata);
        }
        return true;
    }
}