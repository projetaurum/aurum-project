package aurum.magie.runes.crafters;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSuperpositionColumn extends ModelBase
{
    ModelRenderer Base;
    ModelRenderer Corner1;
    ModelRenderer Corner2;
    ModelRenderer Corner3;
    ModelRenderer Corner4;
    ModelRenderer Corner5;
    ModelRenderer Corner6;
    ModelRenderer Corner7;
    ModelRenderer Corner8;
    ModelRenderer Corner9;
    ModelRenderer Corner10;
    ModelRenderer Corner11;
    ModelRenderer Corner12;
    ModelRenderer Lever;
    ModelRenderer LeverRing1;
    ModelRenderer LeverRing4;
    ModelRenderer LeverRing2;
    ModelRenderer LeverRing3;
    ModelRenderer LeverRing5;
    ModelRenderer LeverRing11;
    ModelRenderer LeverRing41;
    ModelRenderer LeverRing21;
    ModelRenderer LeverRing31;
    ModelRenderer LeverRing51;
    ModelRenderer Separator;
    ModelRenderer Column1;
    ModelRenderer Column2;
    ModelRenderer Column3;
    ModelRenderer Column4;
    ModelRenderer Column5;

    public ModelSuperpositionColumn()
    {
        this( 0.0f );
    }

    public ModelSuperpositionColumn( float par1 )
    {
        Base = new ModelRenderer( this, 0, 35 );
        Base.setTextureSize( 128, 64 );
        Base.addBox( -7F, -7.5F, -7F, 14, 15, 14);
        Base.setRotationPoint( 0F, 17F, 0F );
        Corner1 = new ModelRenderer( this, 0, 0 );
        Corner1.setTextureSize( 128, 64 );
        Corner1.addBox( -0.5F, -7.5F, -0.5F, 1, 15, 1);
        Corner1.setRotationPoint( -7F, 17F, -7F );
        Corner2 = new ModelRenderer( this, 0, 0 );
        Corner2.setTextureSize( 128, 64 );
        Corner2.addBox( -0.5F, -7.5F, -0.5F, 1, 15, 1);
        Corner2.setRotationPoint( -7F, 17F, 7F );
        Corner3 = new ModelRenderer( this, 0, 0 );
        Corner3.setTextureSize( 128, 64 );
        Corner3.addBox( -0.5F, -7.5F, -0.5F, 1, 15, 1);
        Corner3.setRotationPoint( 7F, 17F, 7F );
        Corner4 = new ModelRenderer( this, 0, 0 );
        Corner4.setTextureSize( 128, 64 );
        Corner4.addBox( -0.5F, -7.5F, -0.5F, 1, 15, 1);
        Corner4.setRotationPoint( 7F, 17F, -7F );
        Corner5 = new ModelRenderer( this, 3, 0 );
        Corner5.setTextureSize( 128, 64 );
        Corner5.addBox( -0.5F, -0.5F, -6.5F, 1, 1, 13);
        Corner5.setRotationPoint( -7F, 10F, 0F );
        Corner6 = new ModelRenderer( this, 3, 0 );
        Corner6.setTextureSize( 128, 64 );
        Corner6.addBox( -0.5F, -0.5F, -6.5F, 1, 1, 13);
        Corner6.setRotationPoint( 7F, 10F, 0F );
        Corner7 = new ModelRenderer( this, 3, 0 );
        Corner7.setTextureSize( 128, 64 );
        Corner7.addBox( -0.5F, -0.5F, -6.5F, 1, 1, 13);
        Corner7.setRotationPoint( 0F, 10F, -7F );
        Corner8 = new ModelRenderer( this, 3, 0 );
        Corner8.setTextureSize( 128, 64 );
        Corner8.addBox( -0.5F, -0.5F, -6.5F, 1, 1, 13);
        Corner8.setRotationPoint( 0F, 10F, 7F );
        Corner9 = new ModelRenderer( this, 3, 0 );
        Corner9.setTextureSize( 128, 64 );
        Corner9.addBox( -0.5F, -1F, -6.5F, 1, 2, 13);
        Corner9.setRotationPoint( -7F, 23.5F, 0F );
        Corner10 = new ModelRenderer( this, 3, 0 );
        Corner10.setTextureSize( 128, 64 );
        Corner10.addBox( -0.5F, -1F, -6.5F, 1, 2, 13);
        Corner10.setRotationPoint( 7F, 23.5F, 0F );
        Corner11 = new ModelRenderer( this, 3, 0 );
        Corner11.setTextureSize( 128, 64 );
        Corner11.addBox( -0.5F, -1F, -6.5F, 1, 2, 13);
        Corner11.setRotationPoint( 0F, 23.5F, -7F );
        Corner12 = new ModelRenderer( this, 3, 0 );
        Corner12.setTextureSize( 128, 64 );
        Corner12.addBox( -0.5F, -1F, -6.5F, 1, 2, 13);
        Corner12.setRotationPoint( 0F, 23.5F, 7F );
        Lever = new ModelRenderer( this, 14, 14 );
        Lever.setTextureSize( 128, 64 );
        Lever.addBox( -1F, -1.5F, -1F, 2, 3, 2);
        Lever.setRotationPoint( 0F, 16.5F, -6.8F );
        LeverRing1 = new ModelRenderer( this, 15, 15 );
        LeverRing1.setTextureSize( 128, 64 );
        LeverRing1.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing1.setRotationPoint( 0F, 13.6006F, -6.804839F );
        LeverRing4 = new ModelRenderer( this, 15, 15 );
        LeverRing4.setTextureSize( 128, 64 );
        LeverRing4.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing4.setRotationPoint( -1.93F, 15.53001F, -6.852752F );
        LeverRing2 = new ModelRenderer( this, 15, 15 );
        LeverRing2.setTextureSize( 128, 64 );
        LeverRing2.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing2.setRotationPoint( 1.35F, 14.15043F, -6.818493F );
        LeverRing3 = new ModelRenderer( this, 15, 15 );
        LeverRing3.setTextureSize( 128, 64 );
        LeverRing3.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing3.setRotationPoint( -1.35F, 14.15043F, -6.818493F );
        LeverRing5 = new ModelRenderer( this, 15, 15 );
        LeverRing5.setTextureSize( 128, 64 );
        LeverRing5.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing5.setRotationPoint( 1.93F, 15.53001F, -6.852752F );
        LeverRing11 = new ModelRenderer( this, 15, 15 );
        LeverRing11.setTextureSize( 128, 64 );
        LeverRing11.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing11.setRotationPoint( 0F, 19.39882F, -6.948827F );
        LeverRing41 = new ModelRenderer( this, 15, 15 );
        LeverRing41.setTextureSize( 128, 64 );
        LeverRing41.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing41.setRotationPoint( 1.93F, 17.46941F, -6.900914F );
        LeverRing21 = new ModelRenderer( this, 15, 15 );
        LeverRing21.setTextureSize( 128, 64 );
        LeverRing21.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing21.setRotationPoint( -1.35F, 18.84899F, -6.935173F );
        LeverRing31 = new ModelRenderer( this, 15, 15 );
        LeverRing31.setTextureSize( 128, 64 );
        LeverRing31.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing31.setRotationPoint( 1.35F, 18.84899F, -6.935173F );
        LeverRing51 = new ModelRenderer( this, 15, 15 );
        LeverRing51.setTextureSize( 128, 64 );
        LeverRing51.addBox( -1F, -0.5F, -0.5F, 2, 1, 1);
        LeverRing51.setRotationPoint( -1.93F, 17.46941F, -6.900914F );
        Separator = new ModelRenderer( this, 0, 20 );
        Separator.setTextureSize( 128, 64 );
        Separator.addBox( -7F, -0.5F, -7F, 14, 1, 14);
        Separator.setRotationPoint( 0F, 9.5F, 0F );
        Column1 = new ModelRenderer( this, 72, 18 );
        Column1.setTextureSize( 128, 64 );
        Column1.addBox( 0F, -16F, -7F, 0, 32, 14);
        Column1.setRotationPoint( 7F, -7F, 0F );
        Column2 = new ModelRenderer( this, 72, 18 );
        Column2.setTextureSize( 128, 64 );
        Column2.addBox( 0F, -16F, -7F, 0, 32, 14);
        Column2.setRotationPoint( -7F, -7F, 0F );
        Column3 = new ModelRenderer( this, 72, 18 );
        Column3.setTextureSize( 128, 64 );
        Column3.addBox( 0F, -16F, -7F, 0, 32, 14);
        Column3.setRotationPoint( 0F, -7F, 7F );
        Column4 = new ModelRenderer( this, 72, 18 );
        Column4.setTextureSize( 128, 64 );
        Column4.addBox( 0F, -16F, -7F, 0, 32, 14);
        Column4.setRotationPoint( 0F, -7F, -7F );
        Column5 = new ModelRenderer( this, 72, 18 );
        Column5.setTextureSize( 128, 64 );
        Column5.addBox( -7F, 0F, -7F, 14, 0, 14);
        Column5.setRotationPoint( 0F, -23F, 0F );
    }

   public void render(float par7)
   {
        Base.render(par7);
        Corner1.render(par7);
        Corner2.render(par7);
        Corner3.render(par7);
        Corner4.render(par7);
        Corner5.render(par7);
        Corner6.render(par7);

        Corner7.rotateAngleX = 0F;
        Corner7.rotateAngleY = -1.570796F;
        Corner7.rotateAngleZ = 0F;
        Corner7.renderWithRotation(par7);

        Corner8.rotateAngleX = 0F;
        Corner8.rotateAngleY = -1.570796F;
        Corner8.rotateAngleZ = 0F;
        Corner8.renderWithRotation(par7);

        Corner9.render(par7);
        Corner10.render(par7);

        Corner11.rotateAngleX = 0F;
        Corner11.rotateAngleY = -1.570796F;
        Corner11.rotateAngleZ = 0F;
        Corner11.renderWithRotation(par7);

        Corner12.rotateAngleX = 0F;
        Corner12.rotateAngleY = -1.570796F;
        Corner12.rotateAngleZ = 0F;
        Corner12.renderWithRotation(par7);

        LeverRing1.rotateAngleX = -0.02482817F;
        LeverRing1.rotateAngleY = 0F;
        LeverRing1.rotateAngleZ = 0F;
        LeverRing1.renderWithRotation(par7);

        LeverRing4.rotateAngleX = -0.02482817F;
        LeverRing4.rotateAngleY = 5.41337E-10F;
        LeverRing4.rotateAngleZ = -1.570796F;
        LeverRing4.renderWithRotation(par7);

        LeverRing2.rotateAngleX = -0.02482817F;
        LeverRing2.rotateAngleY = 1.771339E-10F;
        LeverRing2.rotateAngleZ = 0.7853981F;
        LeverRing2.renderWithRotation(par7);

        LeverRing3.rotateAngleX = -0.02482817F;
        LeverRing3.rotateAngleY = -1.771339E-10F;
        LeverRing3.rotateAngleZ = -0.7853981F;
        LeverRing3.renderWithRotation(par7);

        LeverRing5.rotateAngleX = -0.02482817F;
        LeverRing5.rotateAngleY = 5.41337E-10F;
        LeverRing5.rotateAngleZ = -1.570796F;
        LeverRing5.renderWithRotation(par7);

        LeverRing11.rotateAngleX = -0.0248282F;
        LeverRing11.rotateAngleY = -2.318643E-15F;
        LeverRing11.rotateAngleZ = -3.141592F;
        LeverRing11.renderWithRotation(par7);

        LeverRing41.rotateAngleX = -0.0248282F;
        LeverRing41.rotateAngleY = -5.053913E-10F;
        LeverRing41.rotateAngleZ = 1.570796F;
        LeverRing41.renderWithRotation(par7);

        LeverRing21.rotateAngleX = -0.0248282F;
        LeverRing21.rotateAngleY = -1.315221E-10F;
        LeverRing21.rotateAngleZ = -2.356194F;
        LeverRing21.renderWithRotation(par7);

        LeverRing31.rotateAngleX = -0.0248282F;
        LeverRing31.rotateAngleY = -7.651247E-10F;
        LeverRing31.rotateAngleZ = 2.356194F;
        LeverRing31.renderWithRotation(par7);

        LeverRing51.rotateAngleX = -0.0248282F;
        LeverRing51.rotateAngleY = -5.053913E-10F;
        LeverRing51.rotateAngleZ = 1.570796F;
        LeverRing51.renderWithRotation(par7);

        Separator.render(par7);
    }
   
   public void renderColumn(float par7)
   {
       Column1.render(par7);
       Column2.render(par7);

       Column3.rotateAngleX = 0F;
       Column3.rotateAngleY = -1.570796F;
       Column3.rotateAngleZ = 0F;
       Column3.renderWithRotation(par7);

       Column4.rotateAngleX = 0F;
       Column4.rotateAngleY = -1.570796F;
       Column4.rotateAngleZ = 0F;
       Column4.renderWithRotation(par7);

       Column5.render(par7);
   }
   
   public void renderLever(float f)
   {
	   Lever.render(f);
   }
}