package aurum.magie.runes.crafters;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import codechicken.lib.raytracer.IndexedCuboid6;
import codechicken.lib.vec.Cuboid6;
import codechicken.lib.vec.Matrix4;
import codechicken.lib.vec.Transformation;
import codechicken.lib.vec.Vector3;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntitySuperpositionColumn extends TileEntity
{
	ArrayList<SuperpositionColumnRuneObject> runes = new ArrayList<SuperpositionColumnRuneObject>();
	public ArrayList<Float> runesY = new ArrayList<Float>();

	public boolean activated = false;
	public boolean hasColumn = false;
	public boolean isSuperposing = false;
	
	public TileEntitySuperpositionColumn()	
	{
		this.runes.add(new SuperpositionColumnRuneObject((byte) 0));
		this.runes.add(new SuperpositionColumnRuneObject((byte) 2));
		this.runes.add(new SuperpositionColumnRuneObject((byte) 14));
	}
	
	public void setSuperposing(boolean b)
	{
		this.isSuperposing = b;
	}
	
	public void setActivated(boolean b)
	{
		this.activated = b;
	}
	
	public void setColumn(boolean b)
	{
		this.hasColumn = b;
	}
	
	public boolean isSuperposing()
	{
		return this.isSuperposing;
	}
	
	public boolean isActivated()
	{
		return this.activated;
	}
	
	public boolean hasColumn()
	{
		return hasColumn;
	}
	
    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.runes.size(); ++i)
        {
            if (this.runes.get(i) != null)
            {
                NBTTagCompound tag2 = new NBTTagCompound();
                nbttaglist.appendTag(tag2);
            }
        }
        
        tag.setTag("Runes", nbttaglist);
        tag.setBoolean("Activated", this.activated);
        tag.setBoolean("Column", this.hasColumn);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.activated = tag.getBoolean("Activated");
        this.hasColumn = tag.getBoolean("Column");
    }
	
    @Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet)
    {
        readFromNBT(packet.data);
    }

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getRenderBoundingBox()
	{
		AxisAlignedBB bb = INFINITE_EXTENT_AABB;

		bb = AxisAlignedBB.getAABBPool().getAABB(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 3, zCoord + 1);
		return bb;
	}

    public void addTraceableCuboids(List<IndexedCuboid6> cuboids, int meta)
    {
    	if(this.hasColumn())
    		cuboids.add(new IndexedCuboid6(1, new Cuboid6(xCoord+1/32D, yCoord, zCoord+1/32D, xCoord+31/32D, yCoord+3, zCoord+31/32D)));
    	else
    		cuboids.add(new IndexedCuboid6(1, new Cuboid6(xCoord+1/32D, yCoord, zCoord+1/32D, xCoord+31/32D, yCoord+30/32D, zCoord+31/32D)));
        
        double decalage = 1;
        double decalage2 = 0;
        
        if(this.isActivated())
        {
        	decalage = 125/128D;
        	decalage2 = 3/128D;
        }
        
        if(meta == 0)
        	cuboids.add(new IndexedCuboid6(0, new Cuboid6(xCoord+14/32D, yCoord+12/32D, zCoord+decalage, 
        			xCoord+18/32D, yCoord+18/32D, zCoord+30/32D)));
        else if(meta == 1)
        	cuboids.add(new IndexedCuboid6(0, new Cuboid6(xCoord+1-decalage, yCoord+12/32D, zCoord+14/32D, 
        			xCoord+16/32D, yCoord+18/32D, zCoord+18/32D)));
        else if(meta == 2)
        	cuboids.add(new IndexedCuboid6(0, new Cuboid6(xCoord+14/32D, yCoord+12/32D, zCoord+1-decalage, 
        			xCoord+18/32D, yCoord+18/32D, zCoord+(2/32D))));
        else if(meta == 3)
        	cuboids.add(new IndexedCuboid6(0, new Cuboid6(xCoord+decalage, yCoord+12/32D, zCoord+14/32D, 
        			xCoord+16/32D, yCoord+18/32D, zCoord+18/32D)));
    }
    
	public class SuperpositionColumnRuneObject
	{
		private byte runeID;
		private float yPos;
		private float angle;
		private boolean upDir;
		
		public SuperpositionColumnRuneObject(byte runeID)
		{
			this.runeID = runeID;
			this.yPos = 0+runes.size()+(new Random().nextInt(10)/20);
			this.angle = 30;
			this.upDir = true;
		}
		
		public void setAngle(float theta)
		{
			this.angle = theta;
		}
		
		public void setPos(float pos)
		{
			this.yPos = pos;
		}
		
		public void setUp(boolean b)
		{
			this.upDir = b;
		}
		
		public float angle()
		{
			return this.angle;
		}
		
		public float yPos()
		{
			return this.yPos;
		}
		
		public boolean upDir()
		{
			return this.upDir;
		}
		
		public byte runeID()
		{
			return this.runeID;
		}
	}
}