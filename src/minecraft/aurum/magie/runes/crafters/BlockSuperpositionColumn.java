package aurum.magie.runes.crafters;

import java.util.LinkedList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.ClientProxy;
import aurum.core.multiblocks.TileEntityGagBase;
import aurum.core.packets.SupColumn07Packet;
import aurum.magie.AurumMagie;
import codechicken.lib.raytracer.IndexedCuboid6;
import codechicken.lib.raytracer.RayTracer;
import codechicken.lib.vec.BlockCoord;
import codechicken.lib.vec.Vector3;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockSuperpositionColumn extends BlockContainer
{
	RayTracer rayTracer = new RayTracer();

	public BlockSuperpositionColumn(int par1) 
	{
		super(par1, Material.rock);
	}

	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ClientProxy.renderInventoryTESRID;
	}

	@Override
	public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
	{
		if(player.isSneaking())
			return true;

		TileEntitySuperpositionColumn column = (TileEntitySuperpositionColumn) w.getBlockTileEntity(x, y, z);

		MovingObjectPosition hit = RayTracer.retraceBlock(w, player, x, y, z);

		if(column != null)
		{
			if(hit != null && hit.subHit == 0)
			{
				if(column.hasColumn())
				{
					if(!column.isActivated())
					{
						w.playSoundEffect((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, "random.click", 0.3F, 0.6F);
						column.setActivated(true);
						column.setSuperposing(true);
					}
					else
					{
						w.playSoundEffect((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, "random.click", 0.3F, 0.6F);
						column.setActivated(false);
						column.setSuperposing(false);
					}

					PacketDispatcher.sendPacketToServer(new SupColumn07Packet(x,y,z,w.provider.dimensionId, column.isActivated(), column.hasColumn()).makePacket());
				}
				else
				{
					if(w.getBlockId(x, y+1, z) == Block.glass.blockID && w.getBlockId(x, y+2, z) == Block.glass.blockID)
					{
						w.playSoundEffect((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, "random.click", 0.3F, 0.6F);
						column.setActivated(true);
						column.setColumn(true);

						w.setBlock(x, y+1, z, AurumCore.blockGagGlass.blockID, 0, 3);

						TileEntityGagBase gag1 = (TileEntityGagBase) w.getBlockTileEntity(x, y+1, z);

						gag1.setMainBlock(AurumMagie.blockSuperpositionColumn);

						gag1.setSourceX(x);
						gag1.setSourceY(y);
						gag1.setSourceZ(z);

						w.setBlock(x, y+2, z, AurumCore.blockGagGlass.blockID, 0, 3);

						TileEntityGagBase gag2 = (TileEntityGagBase) w.getBlockTileEntity(x, y+2, z);

						gag2.setMainBlock(AurumMagie.blockSuperpositionColumn);

						gag2.setSourceX(x);
						gag2.setSourceY(y);
						gag2.setSourceZ(z);
					}
					PacketDispatcher.sendPacketToServer(new SupColumn07Packet(x,y,z,w.provider.dimensionId, column.isActivated(), column.hasColumn()).makePacket());
				}
			}
		}
		return true;
	}

	@Override
	public MovingObjectPosition collisionRayTrace(World world, int x, int y, int z, Vec3 start, Vec3 end)
	{
		TileEntitySuperpositionColumn column = (TileEntitySuperpositionColumn)world.getBlockTileEntity(x, y, z);
		if(column != null)
		{
			List<IndexedCuboid6> cuboids = new LinkedList<IndexedCuboid6>();
			column.addTraceableCuboids(cuboids, world.getBlockMetadata(x, y, z));
			return rayTracer.rayTraceCuboids(new Vector3(start), new Vector3(end), cuboids, new BlockCoord(x, y, z), this);
		}
		return null;
	}

	@Override
	public int getRenderBlockPass()
	{
		return 1;
	}

	@Override
	public boolean canRenderInPass(int pass)
	{
		return true;
	}

	@Override
	public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public void registerIcons(IconRegister register)
	{
		this.blockIcon = register.registerIcon("aurummagie:runes/supcolumn.png");
	}

	@Override
	public TileEntity createNewTileEntity(World world) 
	{
		return new TileEntitySuperpositionColumn();
	}
}