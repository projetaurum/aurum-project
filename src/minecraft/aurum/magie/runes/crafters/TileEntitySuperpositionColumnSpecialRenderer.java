package aurum.magie.runes.crafters;

import java.nio.DoubleBuffer;
import java.util.ArrayList;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import aurum.core.multiblocks.IInventoryRenderer;
import aurum.magie.runes.BlockRuneBases;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumn.SuperpositionColumnRuneObject;

public class TileEntitySuperpositionColumnSpecialRenderer extends TileEntitySpecialRenderer implements IInventoryRenderer
{
	public TileEntitySuperpositionColumnSpecialRenderer()
	{
		this.setTileEntityRenderer(TileEntityRenderer.instance);
	}
	
	ModelSuperpositionColumn sup = new ModelSuperpositionColumn();
	Tessellator t = Tessellator.instance;
	
	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y,
			double z, float f) 
	{
		int metadata = 0;
		
		if(tileentity != null)
			metadata = tileentity.getBlockMetadata();
		
		//Superposition Column
		GL11.glPushMatrix();
		GL11.glTranslatef((float)x + 0.5f, (float)y +1.5f, (float)z + 0.5f);
		/*GL11.glRotatef(180f, 1.0f, 0.0f, 0.0f);
		if(metadata == 1)
			GL11.glRotatef(90f, 0f, 1f, 0f);
		else if(metadata == 2)
			GL11.glRotatef(180f, 0f, 1f, 0f);
		else if(metadata == 3)
			GL11.glRotatef(-90f, 0f, 1f, 0f);
		bindTexture(new ResourceLocation("aurummagie","textures/blocks/runes/model_supcolumn.png"));
		sup.render(0.0625f);
		
		if(tileentity != null)
		{
			TileEntitySuperpositionColumn column = (TileEntitySuperpositionColumn) tileentity;
			
			if(column.hasColumn())
				sup.renderColumn(0.0625f);
			
			if(column.isActivated() && column.hasColumn())
			{
				GL11.glPopMatrix();
				GL11.glPushMatrix();
				if(metadata == 0)
				{
					GL11.glTranslatef((float)x+0.5f, (float)y-0.5615f, (float)z+0.49f);
					GL11.glRotatef(180f, 0f, 1f, 0f);
				}
				if(metadata == 1)
				{
					GL11.glTranslatef((float)x+0.51f, (float)y-0.5615f, (float)z+0.5f);
					GL11.glRotatef(90f, 0f, 1f, 0f);
				}
				else if(metadata == 2)
					GL11.glTranslatef((float)x+0.50f, (float)y-0.5615f, (float)z+0.51f);
				else if(metadata == 3)
				{
					GL11.glTranslatef((float)x+0.49f, (float)y-0.5615f, (float)z+0.5f);
					GL11.glRotatef(-90f, 0f, 1f, 0f);
				}
			}
		}
		sup.renderLever(0.0625f);
		GL11.glPopMatrix();

		if(tileentity != null)
		{
			if(((TileEntitySuperpositionColumn) tileentity).hasColumn)
			{
				//Render Runes
				for(SuperpositionColumnRuneObject rune : ((TileEntitySuperpositionColumn) tileentity).runes)
				{
					GL11.glPushMatrix();
					GL11.glTranslatef((float)x+0.5f, (float)y +0.65f+rune.yPos(), (float)z+0.5f);
					GL11.glRotatef(90f, 1f, 0f, 0f);
					GL11.glRotatef(rune.angle(), 0f, 0f, 1f);
					GL11.glDisable(GL11.GL_LIGHTING);
				
					t.startDrawingQuads();
					bindTexture( new ResourceLocation("aurummagie","textures/blocks/"+BlockRuneBases.iconBuffer[rune.runeID()].getIconName().replace("aurummagie:", "")+".png"));
					t.addVertexWithUV(-0.35, -0.35, -0.35, 0, 0);
					t.addVertexWithUV(-0.35, 0.35, -0.35, 0, 1);
					t.addVertexWithUV(0.35, 0.35, -0.35, 1, 1);
					t.addVertexWithUV(0.35, -0.35, -0.35, 1, 0);

					t.addVertexWithUV(-.35, -.35, -.35, 0, 0);
					t.addVertexWithUV(.35, -.35, -.35, 0, 1);
					t.addVertexWithUV(.35, .35, -.35, 1, 1);
					t.addVertexWithUV(-.35, .35, -.35, 1, 0);
					t.draw();
					GL11.glEnable(GL11.GL_LIGHTING);
					GL11.glPopMatrix();
					
					if(((TileEntitySuperpositionColumn) tileentity).isSuperposing())
					{
						if(rune.angle()<360)
							rune.setAngle(rune.angle()+0.5f);
						else
							rune.setAngle(0);
						
						if(rune.upDir())
						{
							if(rune.yPos()<=1.80f)
								rune.setPos(rune.yPos()+0.006f);
							else
								rune.setUp(false);
						}
						else
						{
							if(rune.yPos()>0f)
								rune.setPos(rune.yPos()-0.006f);
							else
								rune.setUp(true);
						}
					}
				}
			}
		}*/
		
		double[] controlPoints = new double[]{-1,3,-3,1,3,-6,3,0,-3,3,0,0,1,0,0,0};
		
		DoubleBuffer ctrlPointBuffer = BufferUtils.createDoubleBuffer(3*controlPoints.length);

        for(int i = 0; i < controlPoints.length; i++)
                ctrlPointBuffer.put(controlPoints[i]);
        ctrlPointBuffer.rewind();

        GL11.glMap1d(GL11.GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, ctrlPointBuffer);

        GL11.glEnable(GL11.GL_MAP1_VERTEX_3);
        GL11.glBegin(GL11.GL_LINE_STRIP);
        for (int i = 0; i <= 30; i++) {
                GL11.glEvalCoord1f(i / 30.0f);
        }
        GL11.glEnd();
        GL11.glFlush();
        GL11.glPopMatrix();
	}

	@Override
	public void renderInventory(double x, double y, double z, int metadata) 
	{
		this.renderTileEntityAt(null, x, y, z, 0.0f);
	}
}