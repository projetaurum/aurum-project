package aurum.magie.runes;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityRuneBase extends TileEntity
{
    protected String owner;

    public TileEntityRuneBase()
    {
        this.owner = "";
    }

    public String getOwner()
    {
        return this.owner;
    }

    public void setOwner(String str)
    {
        this.owner = str;
    }

    public boolean isOwner(String str)
    {
        return str.equals(this.owner);
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        tag.setString("Owner", owner);
        super.writeToNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        this.owner = tag.getString("Owner");
        super.readFromNBT(tag);
    }
}