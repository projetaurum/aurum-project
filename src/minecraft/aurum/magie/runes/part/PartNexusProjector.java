package aurum.magie.runes.part;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import aurum.magie.AurumMagie;
import codechicken.lib.data.MCDataInput;
import codechicken.lib.data.MCDataOutput;
import codechicken.lib.lighting.LazyLightMatrix;
import codechicken.lib.vec.BlockCoord;
import codechicken.lib.vec.Cuboid6;
import codechicken.lib.vec.Vector3;
import codechicken.multipart.minecraft.McBlockPart;
import codechicken.multipart.minecraft.PartMetaAccess;

public class PartNexusProjector extends PartRuneBases
{
	/**
	 * Determinons un meta alternatif pour le renderbounds
	 * 0 : Liaisons face
	 * 1 : Liaisons diagonales -> carre
	 */
	private byte nexusMeta;
	
	/**
	 * Un meta deux fois plus alternatif qui correspond a l'orientation du nexus
	 * 0 : x+ x-
	 * 1 : z+ z-
	 * 2 : y+ y-
	 */
	private byte nexusOrientationMeta;
	
	private boolean isActivated = false;
	
	public PartNexusProjector() {}
	
	public PartNexusProjector(byte meta)
	{
		super(meta);
	}
    
    public void setActivated(boolean b)
    {
    	this.isActivated = b;
    }
    
    public boolean isActivated()
    {
    	return this.isActivated;
    }
    
    public byte getNexusMeta()
    {
    	return this.nexusMeta;
    }
    
    public byte getNexusOrientation()
    {
    	return this.nexusOrientationMeta;
    }
    
    public void setNexusMeta(byte meta)
    {
    	this.nexusMeta = meta;
    }
    
    public void setNexusOrientation(byte orien)
    {
    	this.nexusOrientationMeta = orien;
    }
    
    @Override
    public void save(NBTTagCompound tag)
    {
        tag.setByte("nexusMeta", this.nexusMeta);
        tag.setByte("nexusOrien", this.nexusOrientationMeta);
        tag.setBoolean("isActive", this.isActivated);
        super.save(tag);
    }
    
	@Override
	public float getStrength(MovingObjectPosition hit, EntityPlayer player)
	{
        if (isOwner(player.username))
        {
            return 0.0F;
        }
        else
        {
            return -1.0F;
        }
	}
    
	@Override
	public void renderStatic(Vector3 pos, LazyLightMatrix olm, int pass)
	{
        if(pass == 0)
            new RenderBlocks(new PartMetaAccess(this)).renderBlockByRenderType(getBlock(), x(), y(), z());
	}

    @Override
    public void load(NBTTagCompound tag)
    {
        this.nexusMeta = tag.getByte("nexusMeta");
        this.nexusOrientationMeta = tag.getByte("nexusOrien");
        this.isActivated = tag.getBoolean("isActive");

        super.load(tag);
    }
    
    @Override
    public void writeDesc(MCDataOutput packet)
    {
        packet.writeByte(nexusMeta);
        packet.writeByte(nexusOrientationMeta);
        packet.writeBoolean(isActivated);
        super.writeDesc(packet);
    }
    
    @Override
    public void readDesc(MCDataInput packet)
    {
    	nexusMeta = packet.readByte();
        nexusOrientationMeta = packet.readByte();
        isActivated = packet.readBoolean();
        super.readDesc(packet);
    }
	
    public Cuboid6 getBounds(int meta)
    {
        double d = 0.0625;
        if (meta == 1)
            return new Cuboid6(0.0F+d, 0.0F, 0.0F+d, 1.0F-d, d, 1.0F-d);
        if (meta == 2)
            return new Cuboid6(0.0F+d, 0.0F+d, 1.0F - d, 1.0F-d, 1.0F-d, 1.0F);
        if (meta == 3)
            return new Cuboid6(0.0F+d, 0.0F+d, 0.0F, 1.0F-d, 1.0F-d, d);
        if (meta == 4)
            return new Cuboid6(1.0F - d, 0.0F+d, 0.0F+d, 1.0F, 1.0F-d, 1.0F-d);
        if(meta == 5)
        	return new Cuboid6(0.0F+d, 0.0F+d, 0.0F, d, 1.0F-d, 1.0F-d);
        return new Cuboid6(0.0F+d, 1.0F - d, 0.0F+d, 1.0F-d, 1.0F, 1.0F-d);
    }

    @Override
    public int sideForMeta(int meta)
    {
        return meta;
    }

	@Override
	public String getType() 
	{
		return "aurummagie_nexusprojector";
	}

	@Override
	public Block getBlock()
	{
		return AurumMagie.runeNexusProjector;
	}
	
	public static McBlockPart placement(World world, BlockCoord pos, int side)
    {
        pos = pos.copy().offset(side^1);
        
        if(!world.isBlockSolidOnSide(pos.x, pos.y, pos.z, ForgeDirection.getOrientation(side)))
            return null;
        
        return new PartNexusProjector((byte) side);
    }

	@Override
	public Cuboid6 getBounds() 
	{
		return this.getBounds(getMetadata());
	}
	
	@Override
    public boolean canStay()
    {
    	return true;
    }
}
