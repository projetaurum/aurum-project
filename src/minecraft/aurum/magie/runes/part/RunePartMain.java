package aurum.magie.runes.part;

import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.magie.AurumMagie;
import aurum.magie.runes.TileEntityRuneBases;
import codechicken.lib.vec.BlockCoord;
import codechicken.multipart.MultiPartRegistry;
import codechicken.multipart.MultiPartRegistry.IPartConverter;
import codechicken.multipart.MultiPartRegistry.IPartFactory;
import codechicken.multipart.TMultiPart;
import codechicken.multipart.TileMultipart;

public class RunePartMain implements IPartFactory, IPartConverter
{
	@Override
	public TMultiPart createPart(String name, boolean client)
	{
		if(name.equals("aurummagie_runebases")) return new PartRuneBases();
		if(name.equals("aurummagie_nexusprojector")) return new PartNexusProjector();
		
		return null;
	}

	public void init()
	{
		MultiPartRegistry.registerConverter(this);
		MultiPartRegistry.registerParts(this, new String[]{"aurummagie_runebases", "aurummagie_nexusprojector"});
	}

	@Override
	public boolean canConvert(int blockID)
	{
		return blockID == AurumMagie.runeBases.blockID || blockID == AurumMagie.runeNexusProjector.blockID;
	}

	@Override
	public TMultiPart convert(World world, BlockCoord pos)
	{
		int id = world.getBlockId(pos.x, pos.y, pos.z);
		int meta = world.getBlockMetadata(pos.x, pos.y, pos.z);
		if(id == AurumMagie.runeBases.blockID)
			return new PartRuneBases(meta, (byte) ((TileEntityRuneBases)world.getBlockTileEntity(pos.x, pos.y, pos.z)).getNBTData());
		if(id == AurumMagie.runeNexusProjector.blockID)
			return new PartNexusProjector((byte) meta);
		return null;
	}
	
	public static final boolean isPart(World w, int x, int y, int z)
	{
		if(TileMultipart.getTile(w, new BlockCoord(x,y,z)) != null)
			return true;
		else
			return false;
	}
	
	public static final boolean isPart(IBlockAccess access, int x, int y, int z)
	{
		if(!(access.getBlockTileEntity(x, y, z) instanceof TileMultipart))
			return false;
		return true;
	}
}
