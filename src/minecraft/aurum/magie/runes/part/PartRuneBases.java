package aurum.magie.runes.part;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import aurum.magie.AurumMagie;
import aurum.magie.runes.BlockRuneBases;
import codechicken.lib.data.MCDataInput;
import codechicken.lib.data.MCDataOutput;
import codechicken.lib.lighting.LazyLightMatrix;
import codechicken.lib.vec.BlockCoord;
import codechicken.lib.vec.Cuboid6;
import codechicken.lib.vec.Vector3;
import codechicken.multipart.minecraft.McBlockPart;
import codechicken.multipart.minecraft.McSidedMetaPart;

public class PartRuneBases extends McSidedMetaPart
{
	byte subMeta = 0;
	String owner = "";
	
	public PartRuneBases() {}
	
	public void setSubMeta(byte meta)
	{
		this.subMeta = meta;
	}
	
    public String getOwner()
    {
        return this.owner;
    }

    public void setOwner(String str)
    {
        this.owner = str;
    }

    public boolean isOwner(String str)
    {
        return str.equals(this.owner);
    }
	
    @Override
    public void writeDesc(MCDataOutput packet)
    {
        packet.writeByte(subMeta);
        packet.writeString(owner);
        super.writeDesc(packet);
    }
    
    @Override
    public void readDesc(MCDataInput packet)
    {
        subMeta = packet.readByte();
        owner = packet.readString();
        super.readDesc(packet);
    }
	
	@Override
	public void save(NBTTagCompound tag)
	{
		tag.setByte("NBTData",subMeta);
		tag.setString("Owner", owner);
		super.save(tag);
	}
	
	@Override
	public void load(NBTTagCompound tag)
	{
		this.subMeta = tag.getByte("NBTData");
		this.owner = tag.getString("Owner");
		super.load(tag);
	}
	
	@Override
	public void renderStatic(Vector3 pos, LazyLightMatrix olm, int pass)
	{
		if(this.getMetadata() == 0)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0F, 0.999F, 0F, 1F, 1F, 1F);
		if(this.getMetadata() == 1)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0F, 0F, 0F, 1F, 0.001F, 1F);
		if(this.getMetadata() == 2)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0F, 0F, 0.999F, 1F, 1F, 1F);
		if(this.getMetadata() == 3)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0F, 0F, 0F, 1F, 1F, 0.001F);
		if(this.getMetadata() == 4)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0.999F, 0F, 0F, 1f, 1F, 1F);
		if(this.getMetadata() == 5)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setRenderBounds(0F, 0F, 0F, 0.001F, 1F, 1F);
		
		if(BlockRuneBases.iconBuffer[this.subMeta] != null)
			Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.setOverrideBlockTexture(BlockRuneBases.iconBuffer[this.subMeta]);
		Minecraft.getMinecraft().renderGlobal.globalRenderBlocks.renderStandardBlock(this.getBlock(), this.x(), this.y(), this.z());
	}
	
	public PartRuneBases(int meta)
	{
		super(meta);
	}
	
	public PartRuneBases(int meta, byte subMeta)
	{
		super(meta);
		this.subMeta = subMeta;
	}
	
	@Override
    public Cuboid6 getBounds()
    {
        return getBounds(meta);
    }
	
	@Override
    public boolean canStay()
    {
    	return true;
    }
    
    public Cuboid6 getBounds(int meta)
    {
        double d = 0.0625;
        if (meta == 1)
            return new Cuboid6(0+d, 0, 0+d, 1-d, d, 1-d);
        if (meta == 2)
            return new Cuboid6(0+d, 0+d, 1-d, 1-d, 1-d, 1);
        if (meta == 3)
            return new Cuboid6(0+d, 0+d, 0, 1-d, 1-d, d);
        if (meta == 4)
            return new Cuboid6(1 - d, 0+d, 0+d, 1, 1-d, 1-d);
        if(meta == 5)
        	return new Cuboid6(0, 0+d, 0+d, d, 1-d, 1-d);
        return new Cuboid6(0+d, 1-d, 0+d, 1-d, 1, 1-d);
    }
    
    @Override
    public int sideForMeta(int meta)
    {
        return meta;
    }

	@Override
	public String getType() 
	{
		return "aurummagie_runebases";
	}

	@Override
	public Block getBlock()
	{
		return AurumMagie.runeBases;
	}
	
	public static McBlockPart placement(World world, BlockCoord pos, int side)
    {
        pos = pos.copy().offset(side^1);
        
        if(!world.isBlockSolidOnSide(pos.x, pos.y, pos.z, ForgeDirection.getOrientation(side)))
            return null;
        
        return new PartRuneBases(side);
    }
}