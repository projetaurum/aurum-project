package aurum.magie.runes;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import aurum.core.BlockPlacements;
import aurum.core.PLocation;
import aurum.magie.AurumMagie;
import aurum.magie.runes.part.RunePartMain;

public class BlockNexusProjector extends BlockRuneBase
{
    public BlockNexusProjector(int par1)
    {
        super(par1, Material.rock);
        this.setTickRandomly(true);
    }
    
    @Override
    public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
    {
    	return true;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }
    
    @Override
    public void setBlockBoundsForItemRender()
    {
        float f = 0.0625F;
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, f, 1.0F);
    }

    @Override
    public float getPlayerRelativeBlockHardness(EntityPlayer player, World w, int x, int y, int z)
    {
    	if(!RunePartMain.isPart(w, x, y, z))
    	{
            TileEntityNexusProjector projector = (TileEntityNexusProjector) w.getBlockTileEntity(x, y, z);

            if (projector.isOwner(player.username))
            {
                return 0.0F;
            }
            else
            {
                return -1.0F;
            }
    	}
    	return -1.0f;
    }

    @Override
    public int damageDropped(int par1)
    {
        return 0;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.blockIcon = register.registerIcon("aurummagie:runes/rune_nexus_projector");
    }

    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityNexusProjector();
    }

    @Override
    public void onBlockPlacedBy(World w, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
        if (entity instanceof EntityPlayer)
        {
            TileEntityNexusProjector projector = (TileEntityNexusProjector) w.getBlockTileEntity(x, y, z);
            projector.setOwner(((EntityPlayer) entity).username);
            searchForward(w, x, y, z, w.getBlockMetadata(x, y, z), ((EntityPlayer) entity).username);
        }
    }

    public void searchForward(World w, int x, int y, int z, int meta, String owner)
    {
        if (meta == 0)
        {
            if (w.getBlockId(x, y - 2, z) == this.blockID && w.getBlockMetadata(x, y - 2, z) == 1
                    && w.getBlockId(x, y - 1, z) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x, y, z, meta, owner);
            }
            else
            {
                if (w.getBlockId(x, y - 1, z) == AurumMagie.runeNexusBlock.blockID &&
                        (w.getBlockMetadata(x, y - 1, z) >= 3 || w.getBlockMetadata(x, y - 2, z) <= 8)
                        && (w.getBlockId(x, y - 2, z) == this.blockID && w.getBlockMetadata(x, y - 2, z) == 1))
                {
                    createNexusPlate(w, x, y, z, meta);
                }
            }
        }
        else if (meta == 1)
        {
            if (w.getBlockId(x, y + 2, z) == this.blockID && w.getBlockMetadata(x, y + 2, z) == 1
                    && w.getBlockId(x, y + 1, z) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x, y + 2, z, 0, owner);
            }
            else
            {
                if (w.getBlockId(x, y + 1, z) == AurumMagie.runeNexusBlock.blockID &&
                        (w.getBlockMetadata(x, y + 1, z) >= 3 || w.getBlockMetadata(x, y + 2, z) <= 8)
                        && (w.getBlockId(x, y + 2, z) == this.blockID && w.getBlockMetadata(x, y + 2, z) == 1))
                {
                    createNexusPlate(w, x, y + 2, z, 0);
                }
            }
        }
        else if (meta == 2)
        {
            if (w.getBlockId(x, y, z - 2) == this.blockID && w.getBlockMetadata(x, y, z - 2) == 2
                    && w.getBlockId(x, y, z - 1) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x, y, z - 2, 3, owner);
            }
        }
        else if (meta == 3)
        {
            if (w.getBlockId(x, y, z + 2) == this.blockID && w.getBlockMetadata(x, y, z + 2) == 2
                    && w.getBlockId(x, y, z + 1) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x, y, z, meta, owner);
            }
        }
        else if (meta == 4)
        {
            if (w.getBlockId(x - 2, y, z) == this.blockID && w.getBlockMetadata(x - 2, y, z) == 4
                    && w.getBlockId(x - 1, y, z) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x - 2, y, z, 5, owner);
            }
        }
        else if (meta == 5)
        {
            if (w.getBlockId(x + 2, y, z) == this.blockID && w.getBlockMetadata(x + 2, y, z) == 4
                    && w.getBlockId(x + 1, y, z) != AurumMagie.runeNexusBlock.blockID)
            {
                createNexusFlux(w, x, y, z, meta, owner);
            }
        }
    }

    public void createNexusPlate(World w, int x, int y, int z, int meta)
    {
        TileEntityNexus nexus1 = null;
        TileEntityNexus nexus2 = null;
        TileEntityNexus nexus3 = null;
        TileEntityNexus nexus4 = null;
        TileEntityNexus nexus5 = null;
        TileEntityNexus nexus6 = null;
        TileEntityNexus node = (TileEntityNexus) w.getBlockTileEntity(x, y - 1, z);
        int nexusMeta = w.getBlockMetadata(x, y - 1, z);

        if (nexusMeta == 0)
        {
            if (BlockPlacements.spaceIsFree(w, x, y, z + 1)
                    && BlockPlacements.spaceIsFree(w, x, y, z - 1)
                    && BlockPlacements.spaceIsFree(w, x, y - 2, z + 1)
                    && BlockPlacements.spaceIsFree(w, x, y - 2, z - 1))
            {
                w.setBlock(x, y, z + 1, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x, y, z - 1, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x, y - 2, z + 1, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x, y - 2, z - 1, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                nexus3 = (TileEntityNexus) w.getBlockTileEntity(x, y, z + 1);
                nexus4 = (TileEntityNexus) w.getBlockTileEntity(x, y, z - 1);
                nexus5 = (TileEntityNexus) w.getBlockTileEntity(x, y - 2, z + 1);
                nexus6 = (TileEntityNexus) w.getBlockTileEntity(x, y - 2, z - 1);
            }
            else
            {
                return;
            }
        }
        else if (nexusMeta == 1)
        {
            if (BlockPlacements.spaceIsFree(w, x + 1, y, z)
                    && BlockPlacements.spaceIsFree(w, x - 1, y, z)
                    && BlockPlacements.spaceIsFree(w, x + 1, y - 2, z)
                    && BlockPlacements.spaceIsFree(w, x - 1, y - 2, z))
            {
                w.setBlock(x + 1, y, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x - 1, y, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x + 1, y - 2, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                w.setBlock(x - 1, y - 2, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
                nexus3 = (TileEntityNexus) w.getBlockTileEntity(x + 1, y, z);
                nexus4 = (TileEntityNexus) w.getBlockTileEntity(x - 1, y, z);
                nexus5 = (TileEntityNexus) w.getBlockTileEntity(x + 1, y - 2, z);
                nexus6 = (TileEntityNexus) w.getBlockTileEntity(x - 1, y - 2, z);
            }
            else
            {
                return;
            }
        }

        w.setBlockToAir(x, y, z);
        w.removeBlockTileEntity(x, y, z);
        w.setBlockToAir(x, y - 2, z);
        w.removeBlockTileEntity(x, y - 2, z);
        w.setBlock(x, y - 2, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
        w.setBlock(x, y, z, AurumMagie.runeNexusBlock.blockID, nexusMeta, 2);
        nexus1 = (TileEntityNexus) w.getBlockTileEntity(x, y - 2, z);
        nexus2 = (TileEntityNexus) w.getBlockTileEntity(x, y, z);
        nexus1.setNodeLoc(x, y - 1, z);
        nexus2.setNodeLoc(x, y - 1, z);
        nexus3.setNodeLoc(x, y - 1, z);
        nexus4.setNodeLoc(x, y - 1, z);
        nexus5.setNodeLoc(x, y - 1, z);
        nexus6.setNodeLoc(x, y - 1, z);

        if (node.hasSword())
        {
            nexus1.setSword(node.getSword());
            nexus2.setSword(node.getSword());
            nexus3.setSword(node.getSword());
            nexus4.setSword(node.getSword());
            nexus5.setSword(node.getSword());
            nexus6.setSword(node.getSword());
        }
    }

    public void createNexusFlux(World w, int x, int y, int z, int meta, String owner)
    {
        TileEntityNexus node = null;
        TileEntityNexus nexus1 = null;
        TileEntityNexus nexus2 = null;
        PLocation loc1 = new PLocation(x, y, z);
        PLocation loc2 = null;

        if (meta == 0)
        {
            if (!BlockPlacements.spaceIsFree(w, x, y - 1, z))
            {
                return;
            }

            w.setBlock(x, y - 1, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            w.setBlockToAir(x, y, z);
            w.removeBlockTileEntity(x, y, z);
            w.setBlockToAir(x, y - 2, z);
            w.removeBlockTileEntity(x, y - 2, z);
            w.setBlock(x, y, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            w.setBlock(x, y - 2, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            node = (TileEntityNexus)w.getBlockTileEntity(x, y - 1, z);
            nexus2 = (TileEntityNexus)w.getBlockTileEntity(x, y - 2, z);
            loc2 = new PLocation(x, y - 2, z);
        }
        else if (meta == 3)
        {
            if (!BlockPlacements.spaceIsFree(w, x, y, z + 1))
            {
                return;
            }

            w.setBlock(x, y, z + 1, AurumMagie.runeNexusBlock.blockID, 0, 3);
            w.setBlockToAir(x, y, z);
            w.removeBlockTileEntity(x, y, z);
            w.setBlockToAir(x, y, z + 2);
            w.removeBlockTileEntity(x, y, z + 2);
            w.setBlock(x, y, z, AurumMagie.runeNexusBlock.blockID, 0, 3);
            w.setBlock(x, y, z + 2, AurumMagie.runeNexusBlock.blockID, 0, 3);
            node = (TileEntityNexus)w.getBlockTileEntity(x, y, z + 1);
            nexus2 = (TileEntityNexus)w.getBlockTileEntity(x, y, z + 2);
            loc2 = new PLocation(x, y, z + 2);
        }
        else if (meta == 5)
        {
            if (!BlockPlacements.spaceIsFree(w, x + 1, y, z))
            {
                return;
            }

            w.setBlock(x + 1, y, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            w.setBlockToAir(x, y, z);
            w.removeBlockTileEntity(x, y, z);
            w.setBlockToAir(x + 2, y, z);
            w.removeBlockTileEntity(x + 2, y, z);
            w.setBlock(x, y, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            w.setBlock(x + 2, y, z, AurumMagie.runeNexusBlock.blockID, 1, 3);
            node = (TileEntityNexus)w.getBlockTileEntity(x + 1, y, z);
            nexus2 = (TileEntityNexus)w.getBlockTileEntity(x + 2, y, z);
            loc2 = new PLocation(x + 2, y, z);
        }

        nexus1 = (TileEntityNexus)w.getBlockTileEntity(x, y, z);
        node.setNode(true, new PLocation[] {loc1, loc2});
        node.setOwner(owner);
        nexus1.setNodeLoc(node.xCoord, node.yCoord, node.zCoord);
        nexus2.setNodeLoc(node.xCoord, node.yCoord, node.zCoord);
    }

    @Override
    public void randomDisplayTick(World w, int x, int y, int z, Random rand)
    {
        float f = (float)x + 0.5F;
        float f1 = (float)y + 0.0F + rand.nextFloat() * 6.0F / 16.0F;
        float f2 = (float)z + 0.5F;
        float f4 = rand.nextFloat() * 0.6F - 0.3F;
        int meta = w.getBlockMetadata(x, y, z);

        if (meta == 0)
        {
            w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
            w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
            w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
        }
        else if (meta == 1)
        {
            w.spawnParticle("portal", (double)(f), (double)f1 + 0.0625F, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
            w.spawnParticle("portal", (double)(f), (double)f1 + 0.0625F, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
            w.spawnParticle("portal", (double)(f), (double)f1 + 0.0625F, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
        }
    }
}