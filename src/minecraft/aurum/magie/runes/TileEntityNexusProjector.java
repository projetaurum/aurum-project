package aurum.magie.runes;

import net.minecraft.nbt.NBTTagCompound;

public class TileEntityNexusProjector extends TileEntityRuneBase
{
	/**
	 * Determinons un meta alternatif pour le renderbounds
	 * 0 : Liaisons face
	 * 1 : Liaisons diagonales -> carre
	 */
	private byte nexusMeta;
	
	/**
	 * Un meta deux fois plus alternatif qui correspond a l'orientation du nexus
	 * 0 : x+ x-
	 * 1 : z+ z-
	 * 2 : y+ y-
	 */
	private byte nexusOrientationMeta;
	
	private boolean isActivated = false;
	
    public TileEntityNexusProjector()
    {
    }
    
    public void setActivated(boolean b)
    {
    	this.isActivated = b;
    }
    
    public boolean isActivated()
    {
    	return this.isActivated;
    }
    
    public byte getNexusMeta()
    {
    	return this.nexusMeta;
    }
    
    public byte getNexusOrientation()
    {
    	return this.nexusOrientationMeta;
    }
    
    public void setNexusMeta(byte meta)
    {
    	this.nexusMeta = meta;
    }
    
    public void setNexusOrientation(byte orien)
    {
    	this.nexusOrientationMeta = orien;
    }
    
    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        tag.setByte("nexusMeta", this.nexusMeta);
        tag.setByte("nexusOrien", this.nexusOrientationMeta);
        tag.setBoolean("isActive", this.isActivated);

        super.writeToNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        this.nexusMeta = tag.getByte("nexusMeta");
        this.nexusOrientationMeta = tag.getByte("nexusOrien");
        this.isActivated = tag.getBoolean("isActive");

        super.readFromNBT(tag);
    }
}