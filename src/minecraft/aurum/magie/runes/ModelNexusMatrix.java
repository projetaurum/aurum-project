package aurum.magie.runes;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelNexusMatrix extends ModelBase
{
    ModelRenderer Centre_1;
    ModelRenderer Centre_2;
    ModelRenderer Centre_3;
    ModelRenderer Centre_4;
    ModelRenderer Centre_5;
    ModelRenderer Centre_6;
    ModelRenderer OrbitalBlock1;
    ModelRenderer OrbitalBlock2;
    ModelRenderer OrbitalBlock3;
    ModelRenderer OrbitalBlock4;
    ModelRenderer OrbitalBlock5;
    ModelRenderer OrbitalBlock6;
    ModelRenderer OrbitalBlock7;
    ModelRenderer OrbitalBlock8;
    ModelRenderer OrbitalBlock11;
    ModelRenderer OrbitalBlock21;
    ModelRenderer OrbitalBlock31;
    ModelRenderer OrbitalBlock41;
    ModelRenderer OrbitalBlock51;
    ModelRenderer OrbitalBlock61;
    ModelRenderer OrbitalBlock71;
    ModelRenderer OrbitalBlock81;

    public ModelNexusMatrix()
    {
        this( 0.0f );
    }

    public ModelNexusMatrix( float par1 )
    {
        Centre_1 = new ModelRenderer( this, 16, 0 );
        Centre_1.setTextureSize( 128, 32 );
        Centre_1.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_1.setRotationPoint( 0F, -8F, 0F );
        Centre_2 = new ModelRenderer( this, 0, 0 );
        Centre_2.setTextureSize( 128, 32 );
        Centre_2.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_2.setRotationPoint( 0F, -8F, 0F );
        Centre_3 = new ModelRenderer( this, 0, 0 );
        Centre_3.setTextureSize( 128, 32 );
        Centre_3.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_3.setRotationPoint( 0F, -8F, 0F );
        Centre_4 = new ModelRenderer( this, 16, 0 );
        Centre_4.setTextureSize( 128, 32 );
        Centre_4.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_4.setRotationPoint( 0F, -8F, 0F );
        Centre_5 = new ModelRenderer( this, 16, 0 );
        Centre_5.setTextureSize( 128, 32 );
        Centre_5.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_5.setRotationPoint( 0F, -8F, 0F );
        Centre_6 = new ModelRenderer( this, 0, 0 );
        Centre_6.setTextureSize( 128, 32 );
        Centre_6.addBox( -4F, -4F, -4F, 8, 8, 8);
        Centre_6.setRotationPoint( 0F, -8F, 0F );
        OrbitalBlock1 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock1.setTextureSize( 128, 32 );
        OrbitalBlock1.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock1.setRotationPoint( 0F, -8F, 0F );
        OrbitalBlock2 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock2.setTextureSize( 128, 32 );
        OrbitalBlock2.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock2.setRotationPoint( -2.384186E-07F, -8F, 1.490116E-08F );
        OrbitalBlock3 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock3.setTextureSize( 128, 32 );
        OrbitalBlock3.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock3.setRotationPoint( -2.384186E-07F, -8F, 1.490116E-08F );
        OrbitalBlock4 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock4.setTextureSize( 128, 32 );
        OrbitalBlock4.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock4.setRotationPoint( -2.384186E-07F, -8F, 1.490116E-08F );
        OrbitalBlock5 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock5.setTextureSize( 128, 32 );
        OrbitalBlock5.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock5.setRotationPoint( -5.098902E-08F, -8F, 9.778915E-07F );
        OrbitalBlock6 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock6.setTextureSize( 128, 32 );
        OrbitalBlock6.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock6.setRotationPoint( -8.24115E-07F, -7.999998F, 1.929719E-06F );
        OrbitalBlock7 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock7.setTextureSize( 128, 32 );
        OrbitalBlock7.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock7.setRotationPoint( -6.464999E-07F, -7.999996F, 2.087652E-06F );
        OrbitalBlock8 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock8.setTextureSize( 128, 32 );
        OrbitalBlock8.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock8.setRotationPoint( 3.16808E-07F, -7.999996F, 2.868681E-06F );
        OrbitalBlock11 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock11.setTextureSize( 128, 32 );
        OrbitalBlock11.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock11.setRotationPoint( 0F, -8F, 0F );
        OrbitalBlock21 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock21.setTextureSize( 128, 32 );
        OrbitalBlock21.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock21.setRotationPoint( -3.576279E-07F, -8F, 5.960464E-08F );
        OrbitalBlock31 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock31.setTextureSize( 128, 32 );
        OrbitalBlock31.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock31.setRotationPoint( -3.576279E-07F, -8F, 5.960464E-08F );
        OrbitalBlock41 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock41.setTextureSize( 128, 32 );
        OrbitalBlock41.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock41.setRotationPoint( -3.576279E-07F, -8F, 5.960464E-08F );
        OrbitalBlock51 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock51.setTextureSize( 128, 32 );
        OrbitalBlock51.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock51.setRotationPoint( -3.014337E-07F, -8F, -1.113028E-06F );
        OrbitalBlock61 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock61.setTextureSize( 128, 32 );
        OrbitalBlock61.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock61.setRotationPoint( -1.058371E-06F, -7.999998F, -1.827903E-06F );
        OrbitalBlock71 = new ModelRenderer( this, 0, 20 );
        OrbitalBlock71.setTextureSize( 128, 32 );
        OrbitalBlock71.addBox( 8.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock71.setRotationPoint( -1.463778E-06F, -7.999996F, -3.538383E-06F );
        OrbitalBlock81 = new ModelRenderer( this, 0, 26 );
        OrbitalBlock81.setTextureSize( 128, 32 );
        OrbitalBlock81.addBox( -11.5F, -1.5F, -1.5F, 3, 3, 3);
        OrbitalBlock81.setRotationPoint( -1.904986E-07F, -7.999996F, -3.413242E-06F );
    }

   public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
   {
        Centre_1.rotateAngleX = 0F;
        Centre_1.rotateAngleY = 0F;
        Centre_1.rotateAngleZ = 0F;
        Centre_1.renderWithRotation(par7);

        Centre_2.rotateAngleX = -0.7853982F;
        Centre_2.rotateAngleY = 0F;
        Centre_2.rotateAngleZ = 0F;
        Centre_2.renderWithRotation(par7);

        Centre_3.rotateAngleX = -0.7853982F;
        Centre_3.rotateAngleY = 0.7853982F;
        Centre_3.rotateAngleZ = -1.177136E-08F;
        Centre_3.renderWithRotation(par7);

        Centre_4.rotateAngleX = -0.7853982F;
        Centre_4.rotateAngleY = 1.570796F;
        Centre_4.rotateAngleZ = 4.561942E-08F;
        Centre_4.renderWithRotation(par7);

        Centre_5.rotateAngleX = -0.7853982F;
        Centre_5.rotateAngleY = 2.356194F;
        Centre_5.rotateAngleZ = 4.774594E-08F;
        Centre_5.renderWithRotation(par7);

        Centre_6.rotateAngleX = 0F;
        Centre_6.rotateAngleY = -0.7853982F;
        Centre_6.rotateAngleZ = 0F;
        Centre_6.renderWithRotation(par7);

        OrbitalBlock1.rotateAngleX = 0.5235988F;
        OrbitalBlock1.rotateAngleY = 1.173462E-09F;
        OrbitalBlock1.rotateAngleZ = -0.09669121F;
        OrbitalBlock1.renderWithRotation(par7);

        OrbitalBlock2.rotateAngleX = 0.5235988F;
        OrbitalBlock2.rotateAngleY = 1.173462E-09F;
        OrbitalBlock2.rotateAngleZ = -0.09669121F;
        OrbitalBlock2.renderWithRotation(par7);

        OrbitalBlock3.rotateAngleX = -0.08370435F;
        OrbitalBlock3.rotateAngleY = -1.522337F;
        OrbitalBlock3.rotateAngleZ = -0.5256284F;
        OrbitalBlock3.renderWithRotation(par7);

        OrbitalBlock4.rotateAngleX = 0.08370435F;
        OrbitalBlock4.rotateAngleY = 1.619255F;
        OrbitalBlock4.rotateAngleZ = 0.5256284F;
        OrbitalBlock4.renderWithRotation(par7);

        OrbitalBlock5.rotateAngleX = 0.4253857F;
        OrbitalBlock5.rotateAngleY = 0.883027F;
        OrbitalBlock5.rotateAngleZ = 0.3291534F;
        OrbitalBlock5.renderWithRotation(par7);

        OrbitalBlock6.rotateAngleX = 0.2988637F;
        OrbitalBlock6.rotateAngleY = -0.8278069F;
        OrbitalBlock6.rotateAngleZ = -0.4465027F;
        OrbitalBlock6.renderWithRotation(par7);

        OrbitalBlock7.rotateAngleX = 0.4253857F;
        OrbitalBlock7.rotateAngleY = 0.883027F;
        OrbitalBlock7.rotateAngleZ = 0.3291534F;
        OrbitalBlock7.renderWithRotation(par7);

        OrbitalBlock8.rotateAngleX = 0.2988637F;
        OrbitalBlock8.rotateAngleY = -0.8278069F;
        OrbitalBlock8.rotateAngleZ = -0.4465027F;
        OrbitalBlock8.renderWithRotation(par7);

        OrbitalBlock11.rotateAngleX = -0.5235988F;
        OrbitalBlock11.rotateAngleY = 0.6532767F;
        OrbitalBlock11.rotateAngleZ = -0.4007276F;
        OrbitalBlock11.renderWithRotation(par7);

        OrbitalBlock21.rotateAngleX = -0.5235988F;
        OrbitalBlock21.rotateAngleY = 0.6532767F;
        OrbitalBlock21.rotateAngleZ = -0.4007276F;
        OrbitalBlock21.renderWithRotation(par7);

        OrbitalBlock31.rotateAngleX = -0.3446066F;
        OrbitalBlock31.rotateAngleY = -1.126259F;
        OrbitalBlock31.rotateAngleZ = 0.560054F;
        OrbitalBlock31.renderWithRotation(par7);

        OrbitalBlock41.rotateAngleX = 0.3446066F;
        OrbitalBlock41.rotateAngleY = 2.015333F;
        OrbitalBlock41.rotateAngleZ = -0.560054F;
        OrbitalBlock41.renderWithRotation(par7);

        OrbitalBlock51.rotateAngleX = -0.1149268F;
        OrbitalBlock51.rotateAngleY = 1.368004F;
        OrbitalBlock51.rotateAngleZ = -0.6389674F;
        OrbitalBlock51.renderWithRotation(par7);

        OrbitalBlock61.rotateAngleX = -0.6340752F;
        OrbitalBlock61.rotateAngleY = -0.2877829F;
        OrbitalBlock61.rotateAngleZ = 0.1428277F;
        OrbitalBlock61.renderWithRotation(par7);

        OrbitalBlock71.rotateAngleX = -0.1149268F;
        OrbitalBlock71.rotateAngleY = 1.368004F;
        OrbitalBlock71.rotateAngleZ = -0.6389674F;
        OrbitalBlock71.renderWithRotation(par7);

        OrbitalBlock81.rotateAngleX = -0.6340752F;
        OrbitalBlock81.rotateAngleY = -0.2877829F;
        OrbitalBlock81.rotateAngleZ = 0.1428277F;
        OrbitalBlock81.renderWithRotation(par7);

    }

}
