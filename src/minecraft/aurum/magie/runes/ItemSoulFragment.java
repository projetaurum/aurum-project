package aurum.magie.runes;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumChatFormatting;

public class ItemSoulFragment extends Item
{
    public ItemSoulFragment(int par1)
    {
        super(par1);
        this.setMaxStackSize(1);
    }
    
    public EnumRarity getRarity(ItemStack stack)
    {
    	return EnumRarity.uncommon;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.itemIcon = register.registerIcon("aurummagie:soulfragment");
    }

    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack par1ItemStack)
    {
        return true;
    }

    @Override
    public boolean getShareTag()
    {
        return true;
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void addInformation(ItemStack stack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
    {
        if (stack.hasTagCompound())
        {
            NBTTagCompound nbttagcompound = stack.getTagCompound();
            NBTTagString soul = (NBTTagString)nbttagcompound.getTag("soul");

            if (soul != null)
            {
                if (soul.data.startsWith("P&"))
                {
                    par3List.add("["+EnumChatFormatting.AQUA+EnumChatFormatting.ITALIC + soul.data.replace("P&", "")+EnumChatFormatting.GRAY+"]");
                }
                else if(soul.data.startsWith("N&"))
                {
                	if(soul.data.contains("%"))
                	{
                    	String[] datas = soul.data.split("%");
                        par3List.add("["+EnumChatFormatting.GOLD+EnumChatFormatting.ITALIC + datas[0].replace("N&", "")+EnumChatFormatting.GRAY+"]");
                        par3List.add(EnumChatFormatting.AQUA + datas[1]);
                	}
                }
                else
                {
                    par3List.add("["+EnumChatFormatting.GREEN+EnumChatFormatting.ITALIC+ soul.data.replace("M&", "")+EnumChatFormatting.GRAY+"]");
                }
            }
        }
    }
}