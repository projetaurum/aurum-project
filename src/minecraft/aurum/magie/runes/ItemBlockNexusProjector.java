package aurum.magie.runes;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBlockNexusProjector extends ItemBlock
{
    public ItemBlockNexusProjector(int par1)
    {
        super(par1);
        this.setUnlocalizedName("ItemBlockNexusProjector");
    }

    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World w, int x, int y, int z, int side,
                                float hitX, float hitY, float hitZ, int metadata)
    {
        int newmeta = side;
        super.placeBlockAt(stack, player, w, x, y, z, side, hitX, hitY, hitZ, newmeta);
        return true;
    }
}