package aurum.magie.runes;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class GuiNexus extends GuiContainer
{
    TileEntityNexus nexus;

    public GuiNexus(Container par1Container, TileEntityNexus tileEntity)
    {
        super(par1Container);
        this.nexus = tileEntity;
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int i, int j)
    {
        this.fontRenderer.drawString("Nexus Rune", 8, this.ySize - 170, 4210752);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(new ResourceLocation("aurummagie","textures/gui/rune_nexus.png"));
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y - 11, 0, 0, xSize, ySize + 20);
    }
}