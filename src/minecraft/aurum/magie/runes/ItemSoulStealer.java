package aurum.magie.runes;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import aurum.magie.AurumMagie;
import aurum.npc.entity.EntityNPC;

public class ItemSoulStealer extends Item
{
    public ItemSoulStealer(int par1)
    {
        super(par1);
    }
    
    public EnumRarity getRarity(ItemStack stack)
    {
    	return EnumRarity.uncommon;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.itemIcon = register.registerIcon("aurummagie:soul_stealer");
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity)
    {
        if (entity instanceof EntityPlayer)
        {
            EntityPlayer player2 = (EntityPlayer) entity;
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setString("soul", "P&Human " + player2.username);
            ItemStack soulFragment = new ItemStack(AurumMagie.itemSoulFragment, 1);
            soulFragment.setTagCompound(nbt);

            if (!player.inventory.addItemStackToInventory(soulFragment))
            {
                player.dropPlayerItem(soulFragment);
            }
        }
        else if(entity instanceof EntityNPC)
        {
        	EntityNPC npc = (EntityNPC)entity;
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setString("soul", "N&"+npc.getFunction()+"%"+npc.getPersonnalName());
            ItemStack soulFragment = new ItemStack(AurumMagie.itemSoulFragment, 1);
            soulFragment.setTagCompound(nbt);

            if (!player.inventory.addItemStackToInventory(soulFragment))
            {
                player.dropPlayerItem(soulFragment);
            }
        }
        else
        {
            NBTTagCompound nbt = new NBTTagCompound();
            nbt.setString("soul", "M&" + entity.getEntityName());
            ItemStack soulFragment = new ItemStack(AurumMagie.itemSoulFragment, 1);
            soulFragment.setTagCompound(nbt);

            if (!player.inventory.addItemStackToInventory(soulFragment))
            {
                player.dropPlayerItem(soulFragment);
            }
        }

        super.onLeftClickEntity(stack, player, entity);
        return true;
    }
}