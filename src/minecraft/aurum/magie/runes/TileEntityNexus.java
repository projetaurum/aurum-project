package aurum.magie.runes;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import aurum.core.PLocation;

public class TileEntityNexus extends TileEntityRuneBase implements IInventory
{
	/**
	 * Determinons un meta alternatif pour le renderbounds
	 * 0 : X
	 * 1 : Z
	 * 2 : Y
	 */
	private byte nexusMeta;
	
	//Est il un noeud ?
    private boolean isNode;

    //La location du noeud
    private PLocation node = null;

    //Les locations de ses composants (si c'est un noeud)
    private PLocation[] components;

    //L'id de l'epee qu'il utilise pour taper sur les abrutis
    private int swordID;

    //Inventory
    private ItemStack[] stacks;

    public TileEntityNexus()
    {
        super();
        this.isNode = false;
        this.node = null;
        this.swordID = 0;
        this.stacks = new ItemStack[19];
    }

    public boolean hasSword()
    {
        return swordID != 0;
    }

    public void setNodeLoc(int x, int y, int z)
    {
        this.node = new PLocation(x, y, z);
    }

    public void setNode(boolean node, PLocation[] components)
    {
        this.isNode = node;
        this.components = components;
    }

    public void setSword(Item item)
    {
        this.swordID = item.itemID;
    }

    public boolean isNode()
    {
        return isNode;
    }

    public PLocation getNodeLoc()
    {
        return node;
    }

    public ItemSword getSword()
    {
        if (Item.itemsList[this.swordID] instanceof ItemSword)
        {
            return (ItemSword) Item.itemsList[this.swordID];
        }
        else
        {
            return null;
        }
    }

    public int getSwordID()
    {
        return this.swordID;
    }

    public int getSwordDamage(Entity e)
    {
        return (int) getSword().getDamageVsEntity(e, new ItemStack(getSword(),1));
    }

    public PLocation[] getComponents()
    {
        return this.components;
    }

    @Override
    public Packet getDescriptionPacket()
    {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, nbtTag);
    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData packet)
    {
        readFromNBT(packet.data);
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        tag.setBoolean("isNode", this.isNode);
        tag.setInteger("Sword", this.swordID);
        tag.setByte("nexusMeta", this.nexusMeta);

        if (this.node != null)
        {
            tag.setBoolean("NodeOK", true);
            this.node.writeToNBT(tag, "NodeLocation");
        }
        else
        {
            tag.setBoolean("NodeOK", false);
        }

        if (isNode)
        {
            int i = 0;

            for (PLocation loc : this.components)
            {
                loc.writeToNBT(tag, "PLoc" + i);
                i = i + 1;
            }

            tag.setInteger("CompoLen", this.components.length);
        }

        //Inventory
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.stacks.length; ++i)
        {
            if (this.stacks[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.stacks[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        tag.setTag("Items", nbttaglist);
        super.writeToNBT(tag);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        this.isNode = tag.getBoolean("isNode");
        this.swordID = tag.getInteger("Sword");
        this.nexusMeta = tag.getByte("nexusMeta");

        if (tag.getBoolean("NodeOK"))
        {
            this.node = PLocation.loadFromNBT(tag, "NodeLocation");
        }

        if (isNode)
        {
            this.components = new PLocation[tag.getInteger("CompoLen")];

            for (int i = 0; i < tag.getInteger("CompoLen"); i++)
            {
                this.components[i] = PLocation.loadFromNBT(tag, "PLoc" + i);
            }
        }
        else
        {
            this.node = null;
        }

        //Inventory
        NBTTagList nbttaglist = tag.getTagList("Items");
        this.stacks = new ItemStack[19];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.stacks.length)
            {
                this.stacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        super.readFromNBT(tag);
    }

    /*
     * INVENTORY MANAGMENT
     */

    @Override
    public int getSizeInventory()
    {
        return this.stacks.length;
    }

    @Override
    public ItemStack getStackInSlot(int i)
    {
        return this.stacks[i];
    }

    @Override
    public ItemStack decrStackSize(int slotIndex, int amount)
    {
        ItemStack stack = getStackInSlot(slotIndex);

        if (stack != null)
        {
            if (stack.stackSize <= amount)
            {
                setInventorySlotContents(slotIndex, null);
            }
            else
            {
                stack = stack.splitStack(amount);

                if (stack.stackSize == 0)
                {
                    setInventorySlotContents(slotIndex, null);
                }
            }
        }

        return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slotIndex)
    {
        ItemStack stack = getStackInSlot(slotIndex);

        if (stack != null)
        {
            setInventorySlotContents(slotIndex, null);
        }

        return stack;
    }
    
    @Override
    public void onInventoryChanged()
    {
    	super.onInventoryChanged();
    	
    	if(FMLCommonHandler.instance().getEffectiveSide().isServer())
    	{
    		
    	}
    }

    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack stack)
    {
        stacks[slotIndex] = stack;

        if (stack != null && stack.stackSize > getInventoryStackLimit())
        {
            stack.stackSize = getInventoryStackLimit();
        }
        this.onInventoryChanged();
    }

    @Override
    public String getInvName()
    {
        return "Nexus Node";
    }

    @Override
    public boolean isInvNameLocalized()
    {
        return false;
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer entityplayer)
    {
    	return true;
    }

    @Override
    public void openChest() {}

    @Override
    public void closeChest() {}

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemstack)
    {
        return true;
    }
}