package aurum.magie.runes;

import java.util.ArrayList;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.magie.AurumMagie;
import aurum.magie.runes.part.RunePartMain;
import codechicken.lib.vec.BlockCoord;
import codechicken.multipart.TileMultipart;

public class BlockRuneBases extends BlockRuneBase
{
	public static Icon[] iconBuffer;
	
	public BlockRuneBases(int par1) 
	{
		super(par1, Material.rock);
	}
	
    @Override
    public int getRenderBlockPass()
    {
        return 0;
    }
    
	@Override
	public boolean shouldSideBeRendered(IBlockAccess iblockaccess, int i, int j, int k, int l)
	{
		return true;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
    public ArrayList<ItemStack> getBlockDropped(World w, int x, int y, int z, int metadata, int fortune)
    {
		ItemStack stack = new ItemStack(AurumMagie.itemBlockRuneBases.itemID,1,0);
		NBTTagCompound tag = new NBTTagCompound();
		ArrayList<ItemStack> stacks = new ArrayList<ItemStack>();
		
		tag.setByte("NBTData", (byte) ((TileEntityRuneBases)w.getBlockTileEntity(x, y, z)).getNBTData());
		stack.setTagCompound(tag);
		stacks.add(stack);
		
		return stacks;
    }
	
	@Override
	public ItemStack getPickBlock(MovingObjectPosition object, World w, int x, int y, int z)
	{
		ItemStack stack = new ItemStack(AurumMagie.itemBlockRuneBases.itemID,1,0);
		NBTTagCompound tag = new NBTTagCompound();
		
		tag.setByte("NBTData", (byte) ((TileEntityRuneBases)w.getBlockTileEntity(x, y, z)).getNBTData());
		stack.setTagCompound(tag);
		
		return stack;
	}
	
    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }
    
    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[21];
        
        this.iconBuffer[0] = register.registerIcon("aurummagie:runes/rune_magicalli");
        this.iconBuffer[1] = register.registerIcon("aurummagie:runes/rune_regeneratio");
        this.iconBuffer[2] = register.registerIcon("aurummagie:runes/rune_intelli");
        
        this.iconBuffer[3] = register.registerIcon("aurummagie:runes/rune_materia");
        this.iconBuffer[4] = register.registerIcon("aurummagie:runes/rune_energia");
        this.iconBuffer[5] = register.registerIcon("aurummagie:runes/rune_percepti");
        
        this.iconBuffer[6] = register.registerIcon("aurummagie:runes/rune_forcia");
        this.iconBuffer[7] = register.registerIcon("aurummagie:runes/rune_transmutatio");
        this.iconBuffer[8] = register.registerIcon("aurummagie:runes/rune_spacia");
        
        this.iconBuffer[9] = register.registerIcon("aurummagie:runes/rune_aspect_armement");
        this.iconBuffer[10] = register.registerIcon("aurummagie:runes/rune_aspect_malefico");
        this.iconBuffer[11] = register.registerIcon("aurummagie:runes/rune_aspect_complexillia");
        
        this.iconBuffer[12] = register.registerIcon("aurummagie:runes/rune_aspect_benefica");
        this.iconBuffer[13] = register.registerIcon("aurummagie:runes/rune_aspect_invisibilis");
        this.iconBuffer[14] = register.registerIcon("aurummagie:runes/rune_aspect_nexus");
        
        this.iconBuffer[15] = register.registerIcon("aurummagie:runes/rune_aspect_scrutatio");
        this.iconBuffer[16] = register.registerIcon("aurummagie:runes/rune_element_aeria");
        this.iconBuffer[17] = register.registerIcon("aurummagie:runes/rune_element_aqua");
        
        this.iconBuffer[18] = register.registerIcon("aurummagie:runes/rune_element_ignis");
        this.iconBuffer[19] = register.registerIcon("aurummagie:runes/rune_element_terra");
        this.iconBuffer[20] = register.registerIcon("aurummagie:runes/rune_element_lux");
    }
    
    @Override
    public Icon getBlockTexture(IBlockAccess access, int x, int y, int z, int side)
    {
    	if(!RunePartMain.isPart(access, x, y, z))
    	{
            TileEntityRuneBases rune = (TileEntityRuneBases) access.getBlockTileEntity(x, y, z);
            
            if(rune != null)
            	return iconBuffer[rune.getNBTData()];
    	}
    	return iconBuffer[0];
    }
    
    @Override
    public TileEntity createNewTileEntity(World world)
    {
        return new TileEntityRuneBases();
    }
}