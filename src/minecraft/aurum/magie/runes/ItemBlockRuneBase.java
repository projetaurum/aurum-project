package aurum.magie.runes;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBlockRuneBase extends ItemBlock
{
    public ItemBlockRuneBase(int par1)
    {
        super(par1);
        this.setUnlocalizedName("ItemBlockRuneBase");
    }

    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World w, int x, int y, int z, int side,
                                float hitX, float hitY, float hitZ, int metadata)
    {
        int newmeta = side;
        super.placeBlockAt(stack, player, w, x, y, z, side, hitX, hitY, hitZ, newmeta);
        return true;
    }
}