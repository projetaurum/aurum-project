package aurum.magie.runes;

import java.util.List;

import aurum.magie.AurumMagie;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class ItemBlockRuneBases extends Item
{
	public Icon[] iconBuffer;
	public static final String[] names = new String[]{"Rune Base Magicalli","Rune Base Regeneratio","Rune Base Intelli","Rune Base Materia","Rune Base Energia","Rune Base Percepti","Rune Base Forcia","Rune Base Transmutatio","Rune Base Spacia","Rune Aspect Armement","Rune Aspect Malefico","Rune Aspect Complexillia","Rune Aspect Benefica","Rune Aspect Invisibilis","Rune Aspect Nexus","Rune Aspect Scrutatio","Rune Element Aeria","Rune Element Aqua","Rune Element Ignis","Rune Element Terra","Rune Element Lux"};

	public ItemBlockRuneBases(int par1) 
	{
		super(par1);
	}
	
	@Override
	public String getItemDisplayName(ItemStack stack)
	{
		if(stack.hasTagCompound())
		{
			return names[stack.getTagCompound().getByte("NBTData")];
		}
		return "";
	}

	@Override
	public int getSpriteNumber()
	{
		return 0;
	}

	@Override
	public void getSubItems(int id, CreativeTabs tab, List list)
	{
		ItemStack[] stacks = new ItemStack[21];

		for(int i =0;i<21;i++)
		{
			stacks[i] = new ItemStack(id,1,0);

			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setByte("NBTData", (byte) i);
			stacks[i].setTagCompound(nbt);
		}

		for(ItemStack stack : stacks)
		{
			list.add(stack);
		}
	}

	@Override
	public Icon getIconIndex(ItemStack stack)
	{
		return this.iconBuffer[stack.getTagCompound().getByte("NBTData")];
	}

	@Override
	public void registerIcons(IconRegister register)
	{
		this.iconBuffer = new Icon[21];

		this.iconBuffer[0] = register.registerIcon("aurummagie:runes/rune_magicalli");
		this.iconBuffer[1] = register.registerIcon("aurummagie:runes/rune_regeneratio");
		this.iconBuffer[2] = register.registerIcon("aurummagie:runes/rune_intelli");

		this.iconBuffer[3] = register.registerIcon("aurummagie:runes/rune_materia");
		this.iconBuffer[4] = register.registerIcon("aurummagie:runes/rune_energia");
		this.iconBuffer[5] = register.registerIcon("aurummagie:runes/rune_percepti");

		this.iconBuffer[6] = register.registerIcon("aurummagie:runes/rune_forcia");
		this.iconBuffer[7] = register.registerIcon("aurummagie:runes/rune_transmutatio");
		this.iconBuffer[8] = register.registerIcon("aurummagie:runes/rune_spacia");

		this.iconBuffer[9] = register.registerIcon("aurummagie:runes/rune_aspect_armement");
		this.iconBuffer[10] = register.registerIcon("aurummagie:runes/rune_aspect_malefico");
		this.iconBuffer[11] = register.registerIcon("aurummagie:runes/rune_aspect_complexillia");

		this.iconBuffer[12] = register.registerIcon("aurummagie:runes/rune_aspect_benefica");
		this.iconBuffer[13] = register.registerIcon("aurummagie:runes/rune_aspect_invisibilis");
		this.iconBuffer[14] = register.registerIcon("aurummagie:runes/rune_aspect_nexus");

		this.iconBuffer[15] = register.registerIcon("aurummagie:runes/rune_aspect_scrutatio");
		this.iconBuffer[16] = register.registerIcon("aurummagie:runes/rune_element_aeria");
		this.iconBuffer[17] = register.registerIcon("aurummagie:runes/rune_element_aqua");

		this.iconBuffer[18] = register.registerIcon("aurummagie:runes/rune_element_ignis");
		this.iconBuffer[19] = register.registerIcon("aurummagie:runes/rune_element_terra");
		this.iconBuffer[20] = register.registerIcon("aurummagie:runes/rune_element_lux");
	}

	@Override
	public boolean getShareTag()
	{
		return true;
	}

	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
	{
		int i1 = par3World.getBlockId(par4, par5, par6);

		if (i1 == Block.snow.blockID && (par3World.getBlockMetadata(par4, par5, par6) & 7) < 1)
		{
			par7 = 1;
		}
		else if (i1 != Block.vine.blockID && i1 != Block.tallGrass.blockID && i1 != Block.deadBush.blockID
				&& (Block.blocksList[i1] == null || !Block.blocksList[i1].isBlockReplaceable(par3World, par4, par5, par6)))
		{
			if (par7 == 0)
			{
				--par5;
			}

			if (par7 == 1)
			{
				++par5;
			}

			if (par7 == 2)
			{
				--par6;
			}

			if (par7 == 3)
			{
				++par6;
			}

			if (par7 == 4)
			{
				--par4;
			}

			if (par7 == 5)
			{
				++par4;
			}
		}

		if (par1ItemStack.stackSize == 0)
		{
			return false;
		}
		else if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6, par7, par1ItemStack))
		{
			return false;
		}
		else if (par5 == 255 && Block.blocksList[AurumMagie.runeBases.blockID].blockMaterial.isSolid())
		{
			return false;
		}
		else if (par3World.canPlaceEntityOnSide(AurumMagie.runeBases.blockID, par4, par5, par6, false, par7, par2EntityPlayer, par1ItemStack))
		{
			Block block = Block.blocksList[AurumMagie.runeBases.blockID];
			int j1 = this.getMetadata(par1ItemStack.getItemDamage());
			int k1 = Block.blocksList[AurumMagie.runeBases.blockID].onBlockPlaced(par3World, par4, par5, par6, par7, par8, par9, par10, j1);

			if (placeBlockAt(par1ItemStack, par2EntityPlayer, par3World, par4, par5, par6, par7, par8, par9, par10, k1))
			{
				par3World.playSoundEffect((double)((float)par4 + 0.5F), (double)((float)par5 + 0.5F), (double)((float)par6 + 0.5F), block.stepSound.getPlaceSound(), (block.stepSound.getVolume() + 1.0F) / 2.0F, block.stepSound.getPitch() * 0.8F);
				--par1ItemStack.stackSize;
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World w, int x, int y, int z, int side,
			float hitX, float hitY, float hitZ, int metadata)
	{	
		if (!w.setBlock(x, y, z, AurumMagie.runeBases.blockID, side, 3))
		{
			return false;
		}

		if (w.getBlockId(x, y, z) == AurumMagie.runeBases.blockID)
		{
			Block.blocksList[AurumMagie.runeBases.blockID].onBlockPlacedBy(w, x, y, z, player, stack);
			Block.blocksList[AurumMagie.runeBases.blockID].onPostBlockPlaced(w, x, y, z, side);
		}

		TileEntityRuneBases rune = (TileEntityRuneBases) w.getBlockTileEntity(x, y, z);

		if (stack.hasTagCompound())
		{
			NBTTagCompound nbttagcompound = stack.getTagCompound();
			rune.setNBTData(nbttagcompound.getByte("NBTData"));
		}
		return true;
	}
}