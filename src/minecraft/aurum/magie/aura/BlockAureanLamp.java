package aurum.magie.aura;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;

public class BlockAureanLamp extends Block
{
    /*  Metadata :
     *  0 Eteinte
     *  1 Torche de redstone
     *  2 +1
     *  3 +1
     *  4 +1
     *  5 +1
     */

    Icon iconBuffer[];

    public BlockAureanLamp(int par1)
    {
        super(par1, Material.glass);
    }

    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z)
    {
        switch (world.getBlockMetadata(x, y, z))
        {
            case 0:
                return 0;

            case 1:
                return 3;

            case 2:
                return 6;

            case 3:
                return 9;

            case 4:
                return 12;

            case 5:
                return 14;
        }

        return 0;
    }

    @Override
    public Icon getIcon(int i, int meta)
    {
        return iconBuffer[meta];
    }

    @SuppressWarnings( { "rawtypes", "unchecked" })
    @Override
    public void getSubBlocks(int i, CreativeTabs tab, List list)
    {
        list.add(new ItemStack(this.blockID, 1, 0));
        list.add(new ItemStack(this.blockID, 1, 1));
        list.add(new ItemStack(this.blockID, 1, 2));
        list.add(new ItemStack(this.blockID, 1, 3));
        list.add(new ItemStack(this.blockID, 1, 4));
        list.add(new ItemStack(this.blockID, 1, 5));
        list.add(new ItemStack(this.blockID, 1, 6));
    }

    @Override
    public int damageDropped(int par1)
    {
        return par1;
    }

    @Override
    public void registerIcons(IconRegister register)
    {
        this.iconBuffer = new Icon[7];
        iconBuffer[0] = register.registerIcon("aurummagie:lamp_aurean_off");
        iconBuffer[1] = register.registerIcon("aurummagie:lamp_aurean1");
        iconBuffer[2] = register.registerIcon("aurummagie:lamp_aurean2");
        iconBuffer[3] = register.registerIcon("aurummagie:lamp_aurean3");
        iconBuffer[4] = register.registerIcon("aurummagie:lamp_aurean4");
        iconBuffer[5] = register.registerIcon("aurummagie:lamp_aurean5");
        iconBuffer[6] = register.registerIcon("aurummagie:lamp_aurean_dmg");
    }
}