package aurum.magie.aura;

import net.minecraft.item.ItemBlock;

public class ItemBlockAureanLamp extends ItemBlock
{
    public ItemBlockAureanLamp(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
        this.setUnlocalizedName("ItemBlockAureanLamp");
    }

    @Override
    public int getMetadata(int damageValue)
    {
        return damageValue;
    }
}