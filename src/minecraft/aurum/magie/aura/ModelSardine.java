package aurum.magie.aura;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSardine extends ModelBase
{
    ModelRenderer Block;
    ModelRenderer Block1;
    ModelRenderer Block11;
    ModelRenderer Block12;
    ModelRenderer Block13;
    ModelRenderer Block14;
    ModelRenderer Block15;
    ModelRenderer Block2;
    ModelRenderer Block21;
    ModelRenderer Block22;
    ModelRenderer Block23;
    ModelRenderer Block16;
    ModelRenderer Block17;
    ModelRenderer Block18;
    ModelRenderer Block19;
    ModelRenderer Block110;
    ModelRenderer Block111;
    ModelRenderer Block112;
    ModelRenderer Block113;
    ModelRenderer Block24;
    ModelRenderer Block25;
    ModelRenderer Block26;
    ModelRenderer Block27;
    ModelRenderer Block114;
    ModelRenderer Block115;
    ModelRenderer Block116;
    ModelRenderer Block117;
    ModelRenderer Block118;
    ModelRenderer Block119;
    ModelRenderer Block120;
    ModelRenderer Block121;
    ModelRenderer Block122;
    ModelRenderer Block123;
    ModelRenderer Block124;
    ModelRenderer Block125;
    ModelRenderer Block28;
    ModelRenderer Block29;
    ModelRenderer Block210;
    ModelRenderer Block211;
    ModelRenderer Block126;
    ModelRenderer Block127;
    ModelRenderer Block128;
    ModelRenderer Block129;
    ModelRenderer Block130;
    ModelRenderer Block131;
    ModelRenderer Block212;
    ModelRenderer Block213;
    ModelRenderer Block214;
    ModelRenderer Block215;
    ModelRenderer Block3;

    public ModelSardine()
    {
        this( 0.0f );
    }

    public ModelSardine( float par1 )
    {
        Block = new ModelRenderer( this, 32, 23 );
        Block.setTextureSize( 64, 32 );
        Block.addBox( -4F, -0.5F, -4F, 8, 1, 8);
        Block.setRotationPoint( 0F, 23F, 0F );
        Block1 = new ModelRenderer( this, 12, 0 );
        Block1.setTextureSize( 64, 32 );
        Block1.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block1.setRotationPoint( 5F, 22F, 0F );
        Block11 = new ModelRenderer( this, 13, 0 );
        Block11.setTextureSize( 64, 32 );
        Block11.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block11.setRotationPoint( 6F, 21F, 0F );
        Block12 = new ModelRenderer( this, 0, 16 );
        Block12.setTextureSize( 64, 32 );
        Block12.addBox( 0F, -4.5F, -4F, 1, 8, 8);
        Block12.setRotationPoint( 7F, 17F, 0F );
        Block13 = new ModelRenderer( this, 26, 1 );
        Block13.setTextureSize( 64, 32 );
        Block13.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block13.setRotationPoint( 6F, 12F, 0F );
        Block14 = new ModelRenderer( this, 24, 1 );
        Block14.setTextureSize( 64, 32 );
        Block14.addBox( -1F, -0.5F, -4F, 3, 1, 8);
        Block14.setRotationPoint( 4F, 11F, 0F );
        Block15 = new ModelRenderer( this, 0, 0 );
        Block15.setTextureSize( 64, 32 );
        Block15.addBox( 0F, -0.5F, -2F, 1, 1, 4);
        Block15.setRotationPoint( 2F, 9F, 0F );
        Block2 = new ModelRenderer( this, 17, 20 );
        Block2.setTextureSize( 64, 32 );
        Block2.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block2.setRotationPoint( 5F, 21F, 5F );
        Block21 = new ModelRenderer( this, 20, 23 );
        Block21.setTextureSize( 64, 32 );
        Block21.addBox( -1F, -4.5F, 0F, 2, 8, 1);
        Block21.setRotationPoint( 5F, 17F, 6F );
        Block22 = new ModelRenderer( this, 26, 22 );
        Block22.setTextureSize( 64, 32 );
        Block22.addBox( 0F, -4.5F, -1F, 1, 8, 2);
        Block22.setRotationPoint( 6F, 17F, 5F );
        Block23 = new ModelRenderer( this, 24, 19 );
        Block23.setTextureSize( 64, 32 );
        Block23.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block23.setRotationPoint( 5F, 12F, 5F );
        Block16 = new ModelRenderer( this, 12, 12 );
        Block16.setTextureSize( 64, 32 );
        Block16.addBox( -1F, -0.5F, -2F, 2, 1, 4);
        Block16.setRotationPoint( 3F, 8F, 0F );
        Block17 = new ModelRenderer( this, 12, 12 );
        Block17.setTextureSize( 64, 32 );
        Block17.addBox( -1F, -0.5F, -2F, 2, 1, 4);
        Block17.setRotationPoint( -3F, 8F, 0F );
        Block18 = new ModelRenderer( this, 12, 0 );
        Block18.setTextureSize( 64, 32 );
        Block18.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block18.setRotationPoint( 0F, 22F, 5F );
        Block19 = new ModelRenderer( this, 13, 0 );
        Block19.setTextureSize( 64, 32 );
        Block19.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block19.setRotationPoint( 5.960464E-08F, 21F, 6F );
        Block110 = new ModelRenderer( this, 0, 16 );
        Block110.setTextureSize( 64, 32 );
        Block110.addBox( 0F, -4.5F, -4F, 1, 8, 8);
        Block110.setRotationPoint( 1.192093E-07F, 17F, 7F );
        Block111 = new ModelRenderer( this, 26, 1 );
        Block111.setTextureSize( 64, 32 );
        Block111.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block111.setRotationPoint( 5.960464E-08F, 12F, 6F );
        Block112 = new ModelRenderer( this, 24, 1 );
        Block112.setTextureSize( 64, 32 );
        Block112.addBox( -1F, -0.5F, -4F, 3, 1, 8);
        Block112.setRotationPoint( -5.960464E-08F, 11F, 4F );
        Block113 = new ModelRenderer( this, 0, 0 );
        Block113.setTextureSize( 64, 32 );
        Block113.addBox( 0F, -0.5F, -3F, 1, 1, 6);
        Block113.setRotationPoint( -1.788139E-07F, 9F, 2F );
        Block24 = new ModelRenderer( this, 17, 20 );
        Block24.setTextureSize( 64, 32 );
        Block24.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block24.setRotationPoint( -5F, 21F, 5F );
        Block25 = new ModelRenderer( this, 20, 23 );
        Block25.setTextureSize( 64, 32 );
        Block25.addBox( -1F, -4.5F, 0F, 2, 8, 1);
        Block25.setRotationPoint( -6F, 17F, 5F );
        Block26 = new ModelRenderer( this, 26, 22 );
        Block26.setTextureSize( 64, 32 );
        Block26.addBox( 0F, -4.5F, -1F, 1, 8, 2);
        Block26.setRotationPoint( -5F, 17F, 6F );
        Block27 = new ModelRenderer( this, 24, 19 );
        Block27.setTextureSize( 64, 32 );
        Block27.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block27.setRotationPoint( -5F, 12F, 5F );
        Block114 = new ModelRenderer( this, 8, 8 );
        Block114.setTextureSize( 64, 32 );
        Block114.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block114.setRotationPoint( -1.192093E-07F, 8F, 3F );
        Block115 = new ModelRenderer( this, 8, 8 );
        Block115.setTextureSize( 64, 32 );
        Block115.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block115.setRotationPoint( -2.714659E-07F, 8F, -3F );
        Block116 = new ModelRenderer( this, 8, 8 );
        Block116.setTextureSize( 64, 32 );
        Block116.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block116.setRotationPoint( -1.192093E-07F, 10F, 3F );
        Block117 = new ModelRenderer( this, 8, 8 );
        Block117.setTextureSize( 64, 32 );
        Block117.addBox( -1F, -0.5F, -4F, 1, 1, 8);
        Block117.setRotationPoint( -3.995276E-07F, 10F, -4F );
        Block118 = new ModelRenderer( this, 10, 10 );
        Block118.setTextureSize( 64, 32 );
        Block118.addBox( -1F, -0.5F, -3F, 1, 1, 6);
        Block118.setRotationPoint( 4F, 10F, 5.960464E-08F );
        Block119 = new ModelRenderer( this, 10, 10 );
        Block119.setTextureSize( 64, 32 );
        Block119.addBox( -1F, -0.5F, -3F, 1, 1, 6);
        Block119.setRotationPoint( -3F, 10F, 4.768372E-07F );
        Block120 = new ModelRenderer( this, 12, 0 );
        Block120.setTextureSize( 64, 32 );
        Block120.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block120.setRotationPoint( -5F, 22F, 0F );
        Block121 = new ModelRenderer( this, 13, 0 );
        Block121.setTextureSize( 64, 32 );
        Block121.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block121.setRotationPoint( -6F, 21F, 0F );
        Block122 = new ModelRenderer( this, 0, 16 );
        Block122.setTextureSize( 64, 32 );
        Block122.addBox( 0F, -4.5F, -4F, 1, 8, 8);
        Block122.setRotationPoint( -7F, 17F, 0F );
        Block123 = new ModelRenderer( this, 26, 1 );
        Block123.setTextureSize( 64, 32 );
        Block123.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block123.setRotationPoint( -6F, 12F, 0F );
        Block124 = new ModelRenderer( this, 24, 1 );
        Block124.setTextureSize( 64, 32 );
        Block124.addBox( -1F, -0.5F, -4F, 3, 1, 8);
        Block124.setRotationPoint( -4F, 11F, 0F );
        Block125 = new ModelRenderer( this, 0, 0 );
        Block125.setTextureSize( 64, 32 );
        Block125.addBox( 0F, -0.5F, -2F, 1, 1, 4);
        Block125.setRotationPoint( -2.000001F, 9F, 0F );
        Block28 = new ModelRenderer( this, 17, 20 );
        Block28.setTextureSize( 64, 32 );
        Block28.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block28.setRotationPoint( -5F, 21F, -4.999999F );
        Block29 = new ModelRenderer( this, 20, 23 );
        Block29.setTextureSize( 64, 32 );
        Block29.addBox( -1F, -4.5F, 0F, 2, 8, 1);
        Block29.setRotationPoint( -5F, 17F, -5.999999F );
        Block210 = new ModelRenderer( this, 26, 22 );
        Block210.setTextureSize( 64, 32 );
        Block210.addBox( 0F, -4.5F, -1F, 1, 8, 2);
        Block210.setRotationPoint( -6F, 17F, -4.999999F );
        Block211 = new ModelRenderer( this, 24, 19 );
        Block211.setTextureSize( 64, 32 );
        Block211.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block211.setRotationPoint( -5F, 12F, -4.999999F );
        Block126 = new ModelRenderer( this, 12, 0 );
        Block126.setTextureSize( 64, 32 );
        Block126.addBox( -1F, -0.5F, -4F, 2, 1, 8);
        Block126.setRotationPoint( 0F, 22F, -5F );
        Block127 = new ModelRenderer( this, 13, 0 );
        Block127.setTextureSize( 64, 32 );
        Block127.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block127.setRotationPoint( 1.788139E-07F, 21F, -6F );
        Block128 = new ModelRenderer( this, 0, 16 );
        Block128.setTextureSize( 64, 32 );
        Block128.addBox( 0F, -4.5F, -4F, 1, 8, 8);
        Block128.setRotationPoint( 3.576279E-07F, 17F, -7F );
        Block129 = new ModelRenderer( this, 26, 1 );
        Block129.setTextureSize( 64, 32 );
        Block129.addBox( 0F, -0.5F, -4F, 1, 1, 8);
        Block129.setRotationPoint( 1.788139E-07F, 12F, -6F );
        Block130 = new ModelRenderer( this, 24, 1 );
        Block130.setTextureSize( 64, 32 );
        Block130.addBox( -1F, -0.5F, -4F, 3, 1, 8);
        Block130.setRotationPoint( -1.788139E-07F, 11F, -4F );
        Block131 = new ModelRenderer( this, 0, 0 );
        Block131.setTextureSize( 64, 32 );
        Block131.addBox( 0F, -0.5F, -3F, 1, 1, 6);
        Block131.setRotationPoint( -5.364418E-07F, 9F, -2F );
        Block212 = new ModelRenderer( this, 17, 20 );
        Block212.setTextureSize( 64, 32 );
        Block212.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block212.setRotationPoint( 4.999999F, 21F, -4.999999F );
        Block213 = new ModelRenderer( this, 20, 23 );
        Block213.setTextureSize( 64, 32 );
        Block213.addBox( -1F, -4.5F, 0F, 2, 8, 1);
        Block213.setRotationPoint( 5.999999F, 17F, -4.999999F );
        Block214 = new ModelRenderer( this, 26, 22 );
        Block214.setTextureSize( 64, 32 );
        Block214.addBox( 0F, -4.5F, -1F, 1, 8, 2);
        Block214.setRotationPoint( 4.999999F, 17F, -5.999999F );
        Block215 = new ModelRenderer( this, 24, 19 );
        Block215.setTextureSize( 64, 32 );
        Block215.addBox( -1F, -0.5F, -1F, 2, 1, 2);
        Block215.setRotationPoint( 4.999999F, 12F, -4.999999F );
        Block3 = new ModelRenderer( this, 20, -2 );
        Block3.setTextureSize( 64, 32 );
        Block3.addBox( -6F, -0.5F, -6F, 12, 1, 12);
        Block3.setRotationPoint( 0F, 15F, 0F );
    }

   public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
   {
        Block.rotateAngleX = 0F;
        Block.rotateAngleY = 0F;
        Block.rotateAngleZ = 0F;
        Block.renderWithRotation(par7);

        Block1.rotateAngleX = 0F;
        Block1.rotateAngleY = 0F;
        Block1.rotateAngleZ = 0F;
        Block1.renderWithRotation(par7);

        Block11.rotateAngleX = 0F;
        Block11.rotateAngleY = 0F;
        Block11.rotateAngleZ = 0F;
        Block11.renderWithRotation(par7);

        Block12.rotateAngleX = 0F;
        Block12.rotateAngleY = 0F;
        Block12.rotateAngleZ = 0F;
        Block12.renderWithRotation(par7);

        Block13.rotateAngleX = 0F;
        Block13.rotateAngleY = 0F;
        Block13.rotateAngleZ = 0F;
        Block13.renderWithRotation(par7);

        Block14.rotateAngleX = 0F;
        Block14.rotateAngleY = 0F;
        Block14.rotateAngleZ = 0F;
        Block14.renderWithRotation(par7);

        Block15.rotateAngleX = 0F;
        Block15.rotateAngleY = 0F;
        Block15.rotateAngleZ = 0F;
        Block15.renderWithRotation(par7);

        Block2.rotateAngleX = 0F;
        Block2.rotateAngleY = 0F;
        Block2.rotateAngleZ = 0F;
        Block2.renderWithRotation(par7);

        Block21.rotateAngleX = 0F;
        Block21.rotateAngleY = 0F;
        Block21.rotateAngleZ = 0F;
        Block21.renderWithRotation(par7);

        Block22.rotateAngleX = 0F;
        Block22.rotateAngleY = 0F;
        Block22.rotateAngleZ = 0F;
        Block22.renderWithRotation(par7);

        Block23.rotateAngleX = 0F;
        Block23.rotateAngleY = 0F;
        Block23.rotateAngleZ = 0F;
        Block23.renderWithRotation(par7);

        Block16.rotateAngleX = 0F;
        Block16.rotateAngleY = 0F;
        Block16.rotateAngleZ = 0F;
        Block16.renderWithRotation(par7);

        Block17.rotateAngleX = 0F;
        Block17.rotateAngleY = 0F;
        Block17.rotateAngleZ = 0F;
        Block17.renderWithRotation(par7);

        Block18.rotateAngleX = 0F;
        Block18.rotateAngleY = -1.570796F;
        Block18.rotateAngleZ = 0F;
        Block18.renderWithRotation(par7);

        Block19.rotateAngleX = 0F;
        Block19.rotateAngleY = -1.570796F;
        Block19.rotateAngleZ = 0F;
        Block19.renderWithRotation(par7);

        Block110.rotateAngleX = 0F;
        Block110.rotateAngleY = -1.570796F;
        Block110.rotateAngleZ = 0F;
        Block110.renderWithRotation(par7);

        Block111.rotateAngleX = 0F;
        Block111.rotateAngleY = -1.570796F;
        Block111.rotateAngleZ = 0F;
        Block111.renderWithRotation(par7);

        Block112.rotateAngleX = 0F;
        Block112.rotateAngleY = -1.570796F;
        Block112.rotateAngleZ = 0F;
        Block112.renderWithRotation(par7);

        Block113.rotateAngleX = 0F;
        Block113.rotateAngleY = -1.570796F;
        Block113.rotateAngleZ = 0F;
        Block113.renderWithRotation(par7);

        Block24.rotateAngleX = 0F;
        Block24.rotateAngleY = -1.570796F;
        Block24.rotateAngleZ = 0F;
        Block24.renderWithRotation(par7);

        Block25.rotateAngleX = 0F;
        Block25.rotateAngleY = -1.570796F;
        Block25.rotateAngleZ = 0F;
        Block25.renderWithRotation(par7);

        Block26.rotateAngleX = 0F;
        Block26.rotateAngleY = -1.570796F;
        Block26.rotateAngleZ = 0F;
        Block26.renderWithRotation(par7);

        Block27.rotateAngleX = 0F;
        Block27.rotateAngleY = -1.570796F;
        Block27.rotateAngleZ = 0F;
        Block27.renderWithRotation(par7);

        Block114.rotateAngleX = 0F;
        Block114.rotateAngleY = -1.570796F;
        Block114.rotateAngleZ = 0F;
        Block114.renderWithRotation(par7);

        Block115.rotateAngleX = 0F;
        Block115.rotateAngleY = 1.570796F;
        Block115.rotateAngleZ = 0F;
        Block115.renderWithRotation(par7);

        Block116.rotateAngleX = 0F;
        Block116.rotateAngleY = -1.570796F;
        Block116.rotateAngleZ = 0F;
        Block116.renderWithRotation(par7);

        Block117.rotateAngleX = 0F;
        Block117.rotateAngleY = 1.570796F;
        Block117.rotateAngleZ = 0F;
        Block117.renderWithRotation(par7);

        Block118.rotateAngleX = 0F;
        Block118.rotateAngleY = 0F;
        Block118.rotateAngleZ = 0F;
        Block118.renderWithRotation(par7);

        Block119.rotateAngleX = 0F;
        Block119.rotateAngleY = 0F;
        Block119.rotateAngleZ = 0F;
        Block119.renderWithRotation(par7);

        Block120.rotateAngleX = 0F;
        Block120.rotateAngleY = -3.141593F;
        Block120.rotateAngleZ = 0F;
        Block120.renderWithRotation(par7);

        Block121.rotateAngleX = 0F;
        Block121.rotateAngleY = -3.141593F;
        Block121.rotateAngleZ = 0F;
        Block121.renderWithRotation(par7);

        Block122.rotateAngleX = 0F;
        Block122.rotateAngleY = -3.141593F;
        Block122.rotateAngleZ = 0F;
        Block122.renderWithRotation(par7);

        Block123.rotateAngleX = 0F;
        Block123.rotateAngleY = -3.141593F;
        Block123.rotateAngleZ = 0F;
        Block123.renderWithRotation(par7);

        Block124.rotateAngleX = 0F;
        Block124.rotateAngleY = -3.141593F;
        Block124.rotateAngleZ = 0F;
        Block124.renderWithRotation(par7);

        Block125.rotateAngleX = 0F;
        Block125.rotateAngleY = -3.141593F;
        Block125.rotateAngleZ = 0F;
        Block125.renderWithRotation(par7);

        Block28.rotateAngleX = 0F;
        Block28.rotateAngleY = -3.141593F;
        Block28.rotateAngleZ = 0F;
        Block28.renderWithRotation(par7);

        Block29.rotateAngleX = 0F;
        Block29.rotateAngleY = -3.141593F;
        Block29.rotateAngleZ = 0F;
        Block29.renderWithRotation(par7);

        Block210.rotateAngleX = 0F;
        Block210.rotateAngleY = -3.141593F;
        Block210.rotateAngleZ = 0F;
        Block210.renderWithRotation(par7);

        Block211.rotateAngleX = 0F;
        Block211.rotateAngleY = -3.141593F;
        Block211.rotateAngleZ = 0F;
        Block211.renderWithRotation(par7);

        Block126.rotateAngleX = 0F;
        Block126.rotateAngleY = 1.570796F;
        Block126.rotateAngleZ = 0F;
        Block126.renderWithRotation(par7);

        Block127.rotateAngleX = 0F;
        Block127.rotateAngleY = 1.570796F;
        Block127.rotateAngleZ = 0F;
        Block127.renderWithRotation(par7);

        Block128.rotateAngleX = 0F;
        Block128.rotateAngleY = 1.570796F;
        Block128.rotateAngleZ = 0F;
        Block128.renderWithRotation(par7);

        Block129.rotateAngleX = 0F;
        Block129.rotateAngleY = 1.570796F;
        Block129.rotateAngleZ = 0F;
        Block129.renderWithRotation(par7);

        Block130.rotateAngleX = 0F;
        Block130.rotateAngleY = 1.570796F;
        Block130.rotateAngleZ = 0F;
        Block130.renderWithRotation(par7);

        Block131.rotateAngleX = 0F;
        Block131.rotateAngleY = 1.570796F;
        Block131.rotateAngleZ = 0F;
        Block131.renderWithRotation(par7);

        Block212.rotateAngleX = 0F;
        Block212.rotateAngleY = 1.570796F;
        Block212.rotateAngleZ = 0F;
        Block212.renderWithRotation(par7);

        Block213.rotateAngleX = 0F;
        Block213.rotateAngleY = 1.570796F;
        Block213.rotateAngleZ = 0F;
        Block213.renderWithRotation(par7);

        Block214.rotateAngleX = 0F;
        Block214.rotateAngleY = 1.570796F;
        Block214.rotateAngleZ = 0F;
        Block214.renderWithRotation(par7);

        Block215.rotateAngleX = 0F;
        Block215.rotateAngleY = 1.570796F;
        Block215.rotateAngleZ = 0F;
        Block215.renderWithRotation(par7);

        Block3.rotateAngleX = 0F;
        Block3.rotateAngleY = 0F;
        Block3.rotateAngleZ = 0F;
        Block3.renderWithRotation(par7);

    }

}
