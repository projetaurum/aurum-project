package aurum.magie;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import aurum.core.AurumCore;
import aurum.core.AurumRegistry;
import aurum.magie.aura.BlockAureanLamp;
import aurum.magie.aura.ItemBlockAureanLamp;
import aurum.magie.monolithes.BlockMonolithe;
import aurum.magie.monolithes.BlockMonolithePortal;
import aurum.magie.monolithes.BlockMonolitheSpawner;
import aurum.magie.monolithes.TileEntityMonolitheBlock;
import aurum.magie.runes.BlockNexus;
import aurum.magie.runes.BlockNexusProjector;
import aurum.magie.runes.BlockRuneBases;
import aurum.magie.runes.ItemBlockNexusProjector;
import aurum.magie.runes.ItemBlockRuneBases;
import aurum.magie.runes.ItemSoulFragment;
import aurum.magie.runes.ItemSoulStealer;
import aurum.magie.runes.TileEntityNexus;
import aurum.magie.runes.TileEntityNexusProjector;
import aurum.magie.runes.TileEntityRuneBases;
import aurum.magie.runes.crafters.BlockSuperpositionColumn;
import aurum.magie.runes.crafters.ItemBlockSuperpositionColumn;
import aurum.magie.runes.crafters.TileEntitySuperpositionColumn;
import aurum.magie.runes.part.AMMultipartCPH;
import aurum.magie.runes.part.AMMultipartSPH;
import aurum.magie.runes.part.RuneEventHandler;
import aurum.magie.runes.part.RunePartMain;
import codechicken.lib.packet.PacketCustom;
import codechicken.lib.packet.PacketCustom.CustomTinyPacketHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumMagie.ID, version = AurumMagie.VERSION, name = "AurumMagie", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false, tinyPacketHandler=CustomTinyPacketHandler.class)
public class AurumMagie
{
    @Instance("AurumMagie")
    public static AurumMagie instance;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumMagieMod";

    @SidedProxy(clientSide = "aurum.magie.ClientProxy", serverSide = "aurum.magie.CommonProxy")
    public static CommonProxy proxy;

    public static final CreativeTabs magieTab = AurumCore.aurumMagieTab;

    public static Logger logger;

    public static MinecraftServer server;

    public static final Block blockMonolithe = new BlockMonolithe(2551, Material.rock)
    .setCreativeTab(magieTab)
    .setUnlocalizedName("BlockMonolithe")
    .setResistance(2000.0F)
    .setStepSound(Block.soundStoneFootstep);

    public static final Block blockMonolithePortal = new BlockMonolithePortal(2552, Material.portal)
    .setLightValue(1.0F)
    .setUnlocalizedName("MonolithePortal");

    public static final Block blockMonolitheSpawner = new BlockMonolitheSpawner(2553, Material.grass)
    .setUnlocalizedName("MonolitheSpawner")
    .setHardness(50.0F).setResistance(2000.0F)
    .setStepSound(Block.soundGravelFootstep);

    public static final Block blockAureanLamp = new BlockAureanLamp(2554)
    .setCreativeTab(magieTab)
    .setUnlocalizedName("BlockAureanLamp")
    .setHardness(1.0F)
    .setResistance(10.0F)
    .setStepSound(Block.soundGlassFootstep);

    public static final Block runeNexusBlock = new BlockNexus(2601)
    .setUnlocalizedName("RuneNexusBlock")
    .setCreativeTab(magieTab);

    public static final Block runeNexusProjector = new BlockNexusProjector(2602)
    .setUnlocalizedName("RuneNexusProjector")
    .setCreativeTab(magieTab);
    
    public static final Block runeBases = new BlockRuneBases(2603)
    .setUnlocalizedName("RuneBases");
    
    public static final Block blockSuperpositionColumn = new BlockSuperpositionColumn(2604)
    .setUnlocalizedName("SuperpositionColumn")
    .setCreativeTab(magieTab);

    public static final Item itemSoulFragment = new ItemSoulFragment(2901)
    .setUnlocalizedName("SoulFragment")
    .setCreativeTab(magieTab);

    public static final Item itemSoulStealer = new ItemSoulStealer(2902)
    .setUnlocalizedName("SoulStealer")
    .setCreativeTab(magieTab);
    
    public static final Item itemBlockRuneBases = new ItemBlockRuneBases(2903)
    .setUnlocalizedName("ItemBlockRuneBases")
    .setCreativeTab(magieTab);

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        instance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        //Blocks
        AurumRegistry.registerBlock(blockMonolithe, "Monolithe Part");
        AurumRegistry.registerBlock(blockMonolithePortal, "Monolithe Portal");
        AurumRegistry.registerBlock(blockMonolitheSpawner, "Corrupted Dirt");
        AurumRegistry.registerBlock(runeNexusBlock, "Nexus Block");
        AurumRegistry.registerBlock(runeBases, "Block Rune");
        //ItemBlocks & Blocks
        AurumRegistry.registerBlock(blockAureanLamp, ItemBlockAureanLamp.class, "Aurean Lamp");
        AurumRegistry.registerBlock(runeNexusProjector, ItemBlockNexusProjector.class, "Rune Nexus Projector");
        AurumRegistry.registerBlock(blockSuperpositionColumn, ItemBlockSuperpositionColumn.class, "Superposition Column");
        //Adding TileEntities
        GameRegistry.registerTileEntity(TileEntityNexus.class, "TileEntityNexus");
        GameRegistry.registerTileEntity(TileEntityNexusProjector.class, "TileEntityNexusProjector");
        GameRegistry.registerTileEntity(TileEntityMonolitheBlock.class, "TileEntityBlockMonolithe");
        GameRegistry.registerTileEntity(TileEntityRuneBases.class, "TileEntityRuneBases");
        GameRegistry.registerTileEntity(TileEntitySuperpositionColumn.class, "TileEntitySuperpositionColumn");
        //Adding Items
        AurumRegistry.registerItem(itemSoulFragment, "Soul Fragment");
        AurumRegistry.registerItem(itemSoulStealer, "Soul Stealer");
        AurumRegistry.registerItem(itemBlockRuneBases, "Item Rune");
        //Tab Name
        LanguageRegistry.instance().addStringLocalization("itemGroup.tabMagie", "en_US", "Aurum Magie");
        //GuiHandler
        NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());
        //Events
        MinecraftForge.EVENT_BUS.register(new RuneEventHandler());
        //Packets
        new RunePartMain().init();
        PacketCustom.assignHandler(this, new AMMultipartSPH());
        if(FMLCommonHandler.instance().getSide().isClient())
            PacketCustom.assignHandler(this, new AMMultipartCPH());
    }

    /*
     * Particules :
     * hugeexplosion
     * largeexplode
     * fireworksSpark
     * bubble
     * suspended
     * depthsuspend
     * townaura
     * crit
     * magicCrit
     * smoke
     * mobSpell
     * mobSpellAmbient
     * spell
     * instantSpell
     * witchMagic
     * note
     * portal
     * enchantmenttable
     * explode
     * flame
     * lava
     * footstep
     * splash
     * largesmoke
     * cloud
     * reddust
     * snowballpoof
     * dripWater
     * dripLava
     * snowshovel
     * slime
     * heart
     * angryVillager
     * happyVillager
     */
}