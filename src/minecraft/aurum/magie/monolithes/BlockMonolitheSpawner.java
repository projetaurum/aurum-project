package aurum.magie.monolithes;

import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.world.World;
import aurum.magie.AurumMagie;

public class BlockMonolitheSpawner extends Block
{
    public BlockMonolitheSpawner(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    @Override
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.blockIcon = par1IconRegister.registerIcon("aurummagie:monolithe_dirt");
    }

    @Override
    public void onBlockAdded(World par1World, int par2, int par3, int par4)
    {
        MonolithesManager.addSpawner(par1World, par2, par3, par4);
    }

    @Override
    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
    {
        if (MonolithesManager.isSpawner(par1World, par2, par3, par4))
        {
            MonolithesManager.removeSpawner(par1World, par2, par3, par4);
        }

        super.breakBlock(par1World, par2, par3, par4, par5, par6);
    }

    public static final boolean spawnMonolithe(World par1World, int par2, int par3, int par4)
    {
        for (int i = 0; i <= 13; i++)
        {
            if (par1World.getBlockId(par2, par3 + i + 2, par4) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 + 1, par3 + i + 2, par4) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 - 1, par3 + i + 2, par4) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2, par3 + i + 2, par4 + 1) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2, par3 + i + 2, par4 - 1) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 + 1, par3 + i + 2, par4 - 1) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 - 1, par3 + i + 2, par4 - 1) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 + 1, par3 + i + 2, par4 + 1) != 0)
            {
                return false;
            }

            if (par1World.getBlockId(par2 - 1, par3 + i + 2, par4 + 1) != 0)
            {
                return false;
            }
        }

        AurumMagie.logger.log(Level.INFO, "Apparition portail monolithe !");
        par1World.setBlock(par2, par3 + 15, par4, AurumMagie.blockMonolithePortal.blockID, 0, 2);
        par1World.setBlock(par2, par3, par4, Block.dirt.blockID, 0, 2);
        return true;
    }
}