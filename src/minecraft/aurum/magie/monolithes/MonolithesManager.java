package aurum.magie.monolithes;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.PLocation;
import aurum.magie.AurumMagie;

public class MonolithesManager
{
    public static final ArrayList<PLocation> locs = new ArrayList<PLocation>();

    public static final void addSpawner(World w, int posX, int posY, int posZ)
    {
        locs.add(new PLocation(posX, posY, posZ, w.getWorldInfo().getVanillaDimension()));
    }

    public static final void removeSpawner(World w, int posX, int posY, int posZ)
    {
        locs.remove(new PLocation(posX, posY, posZ, w.getWorldInfo().getVanillaDimension()));
    }

    public static final boolean isSpawner(World w, int posX, int posY, int posZ)
    {
        return locs.contains(new PLocation(posX, posY, posZ, w.getWorldInfo().getVanillaDimension()));
    }

    public static final void launchSpawner()
    {
        Random rand = new Random();
        int i = rand.nextInt(locs.size());
        PLocation loc = locs.get(i);
        World w = AurumCore.server.worldServerForDimension(loc.getWorld());

        if (w.getBlockMetadata((int)loc.getPosX(), (int)loc.getPosY(), (int)loc.getPosZ()) != 0)
        {
            AurumCore.logger.log(Level.INFO, "Un monolithe spawner est occupe a : " + loc.getPosX() + " | " + loc.getPosY() + " | " + loc.getPosZ() + " ; Recherche d'un nouveau...");
            return;
        }

        w.setBlock((int)loc.getPosX(), (int)loc.getPosY(), (int)loc.getPosZ(), AurumMagie.blockMonolitheSpawner.blockID, 1, 2);
        AurumCore.logger.log(Level.INFO, "Generation de monolithe a : " + loc.getPosX() + " | " + loc.getPosY() + " | " + loc.getPosZ());
        
        if (!BlockMonolitheSpawner.spawnMonolithe(w, loc.getPosX(), loc.getPosY(), loc.getPosZ()))
        {
            AurumCore.logger.log(Level.WARNING, "Impossible de generer un monolithe a : " + loc.getPosX() + " | " + loc.getPosY() + " | " + loc.getPosZ() + " !");
        }
        
        locs.remove(i);
        return;
    }
}