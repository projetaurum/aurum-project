package aurum.magie.monolithes;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.PLocation;
import aurum.magie.AurumMagie;

public class ThreadMonolithe extends Thread
{
    World ws;
    int posX;
    int posY;
    int posZ;
    Random rand;

    public static final ArrayList<PLocation> cacheLocs = new ArrayList<PLocation>();

    public ThreadMonolithe(World ws, int posX, int posY, int posZ)
    {
        this.ws = ws;
        this.posX = posX;
        this.posY = posY;
        this.posZ = posZ;
        this.rand = new Random();
    }

    @SuppressWarnings("static-access")
    public void run()
    {
        int hauteur = rand.nextInt(9) + 8;
        int nbrColonne = rand.nextInt(5) + 4;

        for (int i = 0; hauteur >= i; i++)
        {
            cacheLocs.add(new PLocation(posX, posY - 1, posZ));
        }

        if (nbrColonne == 4)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }
        }
        else if (nbrColonne == 5)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ - 1));
            }
        }
        else if (nbrColonne == 6)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ + 1));
            }
        }
        else if (nbrColonne == 7)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ - 1));
            }
        }
        else if (nbrColonne == 8)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ - 1));
            }
        }
        else if (nbrColonne == 9)
        {
            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(7) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ + 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX + 1, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ - 1));
            }

            for (int i = 0; rand.nextInt(5) + 4 >= i; i++)
            {
                cacheLocs.add(new PLocation(posX - 1, posY - 1, posZ + 1));
            }
        }

        ArrayList<PLocation> cacheLocs1 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs2 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs3 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs4 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs5 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs6 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs7 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs8 = new ArrayList<PLocation>();
        ArrayList<PLocation> cacheLocs9 = new ArrayList<PLocation>();

        for (PLocation loc : cacheLocs)
        {
            if (loc.getPosX() == posX - 1 && loc.getPosZ() == posZ - 1)
            {
                cacheLocs1.add(loc);
            }
            else if (loc.getPosX() == posX && loc.getPosZ() == posZ - 1)
            {
                cacheLocs2.add(loc);
            }
            else if (loc.getPosX() == posX + 1 && loc.getPosZ() == posZ - 1)
            {
                cacheLocs3.add(loc);
            }
            else if (loc.getPosX() == posX - 1 && loc.getPosZ() == posZ)
            {
                cacheLocs4.add(loc);
            }
            else if (loc.getPosX() == posX && loc.getPosZ() == posZ)
            {
                cacheLocs5.add(loc);
            }
            else if (loc.getPosX() == posX + 1 && loc.getPosZ() == posZ)
            {
                cacheLocs6.add(loc);
            }
            else if (loc.getPosX() == posX - 1 && loc.getPosZ() == posZ + 1)
            {
                cacheLocs7.add(loc);
            }
            else if (loc.getPosX() == posX && loc.getPosZ() == posZ + 1)
            {
                cacheLocs8.add(loc);
            }
            else if (loc.getPosX() == posX + 1 && loc.getPosZ() == posZ + 1)
            {
                cacheLocs9.add(loc);
            }
        }

        int hauteurMax = 0;
        hauteurMax = cacheLocs1.size();

        if (cacheLocs1.size() < cacheLocs2.size())
        {
            hauteurMax = cacheLocs2.size();
        }

        if (cacheLocs2.size() < cacheLocs3.size())
        {
            hauteurMax = cacheLocs3.size();
        }

        if (cacheLocs3.size() < cacheLocs4.size())
        {
            hauteurMax = cacheLocs4.size();
        }

        if (cacheLocs4.size() < cacheLocs5.size())
        {
            hauteurMax = cacheLocs5.size();
        }

        if (cacheLocs5.size() < cacheLocs6.size())
        {
            hauteurMax = cacheLocs6.size();
        }

        if (cacheLocs6.size() < cacheLocs7.size())
        {
            hauteurMax = cacheLocs7.size();
        }

        if (cacheLocs7.size() < cacheLocs8.size())
        {
            hauteurMax = cacheLocs8.size();
        }

        if (cacheLocs8.size() < cacheLocs9.size())
        {
            hauteurMax = cacheLocs9.size();
        }

        for (int j = 0; hauteurMax >= j; j++)
        {
            int i = j + 1;

            if (cacheLocs1.size() > i)
                ws.setBlock((int)cacheLocs1.get(i).getPosX(),
                            (int)cacheLocs1.get(i).getPosY(), (int)cacheLocs1.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs2.size() > i)
                ws.setBlock((int)cacheLocs2.get(i).getPosX(),
                            (int)cacheLocs2.get(i).getPosY(), (int)cacheLocs2.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs3.size() > i)
                ws.setBlock((int)cacheLocs3.get(i).getPosX(),
                            (int)cacheLocs3.get(i).getPosY(), (int)cacheLocs3.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs4.size() > i)
                ws.setBlock((int)cacheLocs4.get(i).getPosX(),
                            (int)cacheLocs4.get(i).getPosY(), (int)cacheLocs4.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs5.size() > i)
                ws.setBlock((int)cacheLocs5.get(i).getPosX(),
                            (int)cacheLocs5.get(i).getPosY(), (int)cacheLocs5.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs6.size() > i)
                ws.setBlock((int)cacheLocs6.get(i).getPosX(),
                            (int)cacheLocs6.get(i).getPosY(), (int)cacheLocs6.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs7.size() > i)
                ws.setBlock((int)cacheLocs7.get(i).getPosX(),
                            (int)cacheLocs7.get(i).getPosY(), (int)cacheLocs7.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs8.size() > i)
                ws.setBlock((int)cacheLocs8.get(i).getPosX(),
                            (int)cacheLocs8.get(i).getPosY(), (int)cacheLocs8.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            if (cacheLocs9.size() > i)
                ws.setBlock((int)cacheLocs9.get(i).getPosX(),
                            (int)cacheLocs9.get(i).getPosY(), (int)cacheLocs9.get(i).getPosZ(),
                            AurumMagie.blockMonolithe.blockID, 1, 2);

            try
            {
                this.sleep(1000L);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        cacheLocs.clear();
        cacheLocs1.clear();
        cacheLocs2.clear();
        cacheLocs3.clear();
        cacheLocs4.clear();
        cacheLocs5.clear();
        cacheLocs6.clear();
        cacheLocs7.clear();
        cacheLocs8.clear();
        cacheLocs9.clear();

        try
        {
            this.sleep(5000L);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        //Elimination portail
        this.ws.setBlock(posX - 1, posY, posZ - 1, 0);
        this.ws.setBlock(posX - 1, posY, posZ, 0);
        this.ws.setBlock(posX - 1, posY, posZ + 1, 0);
        this.ws.setBlock(posX, posY, posZ - 1, 0);
        this.ws.setBlock(posX, posY, posZ, 0);
        this.ws.setBlock(posX, posY, posZ + 1, 0);
        this.ws.setBlock(posX + 1, posY, posZ - 1, 0);
        this.ws.setBlock(posX + 1, posY, posZ, 0);
        this.ws.setBlock(posX + 1, posY, posZ + 1, 0);
        
        //Elimination contours
        this.ws.setBlock(posX + 2, posY, posZ, 0);
        this.ws.setBlock(posX + 2, posY, posZ + 1, 0);
        this.ws.setBlock(posX + 2, posY, posZ - 1, 0);
        this.ws.setBlock(posX - 2, posY, posZ, 0);
        this.ws.setBlock(posX - 2, posY, posZ + 1, 0);
        this.ws.setBlock(posX - 2, posY, posZ - 1, 0);
        this.ws.setBlock(posX + 2, posY, posZ + 2, 0);
        this.ws.setBlock(posX + 2, posY, posZ - 2, 0);
        this.ws.setBlock(posX - 2, posY, posZ + 2, 0);
        this.ws.setBlock(posX - 2, posY, posZ - 2, 0);
        this.ws.setBlock(posX, posY, posZ + 2, 0);
        this.ws.setBlock(posX + 1, posY, posZ + 2, 0);
        this.ws.setBlock(posX - 1, posY, posZ + 2, 0);
        this.ws.setBlock(posX, posY, posZ - 2, 0);
        this.ws.setBlock(posX + 1, posY, posZ - 2, 0);
        this.ws.setBlock(posX - 1, posY, posZ - 2, 0);

        try
        {
            this.sleep(3000L);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        AurumCore.logger.log(Level.INFO, "Attention! Desactivation de la protection d'un monolithe! Il a ete generer il y a 300,000 secondes.");
        ArrayList<PLocation> changes = new ArrayList<PLocation>();

        for (int i = 0; i <= posY; i++)
        {
            if (ws.getBlockId(posX - 1, i, posZ - 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX - 1, i, posZ - 1) == 3)
            {
                changes.add(new PLocation(posX - 1, i, posZ - 1));
            }

            if (ws.getBlockId(posX - 1, i, posZ) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX - 1, i, posZ) == 3)
            {
                changes.add(new PLocation(posX - 1, i, posZ));
            }

            if (ws.getBlockId(posX - 1, i, posZ + 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX - 1, i, posZ + 1) == 3)
            {
                changes.add(new PLocation(posX - 1, i, posZ + 1));
            }

            if (ws.getBlockId(posX, i, posZ - 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX, i, posZ - 1) == 3)
            {
                changes.add(new PLocation(posX, i, posZ - 1));
            }

            if (ws.getBlockId(posX, i, posZ) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX, i, posZ) == 3)
            {
                changes.add(new PLocation(posX, i, posZ));
            }

            if (ws.getBlockId(posX, i, posZ + 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX, i, posZ + 1) == 3)
            {
                changes.add(new PLocation(posX, i, posZ + 1));
            }

            if (ws.getBlockId(posX + 1, i, posZ - 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX + 1, i, posZ - 1) == 3)
            {
                changes.add(new PLocation(posX + 1, i, posZ - 1));
            }

            if (ws.getBlockId(posX + 1, i, posZ) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX + 1, i, posZ) == 3)
            {
                changes.add(new PLocation(posX + 1, i, posZ));
            }

            if (ws.getBlockId(posX + 1, i, posZ + 1) == AurumMagie.blockMonolithe.blockID && ws.getBlockMetadata(posX + 1, i, posZ + 1) == 3)
            {
                changes.add(new PLocation(posX + 1, i, posZ + 1));
            }
        }

        for (PLocation loc : changes)
        {
            ws.setBlock((int)loc.getPosX(), (int)loc.getPosY(), (int)loc.getPosZ(), AurumMagie.blockMonolithe.blockID, 0, 2);
        }
    }
}