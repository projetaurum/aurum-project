package aurum.magie.monolithes;

import java.util.Random;

import net.minecraft.block.BlockSand;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.item.EntityFallingSand;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import aurum.magie.AurumMagie;

public class BlockMonolithe extends BlockSand implements ITileEntityProvider
{
	public Icon[] iconBuffer;

	/**
	 * Metadata :
	 * 0 : Destructible
	 * 1 : Falling Block
	 * 2 : ?
	 * 3 : Finish falling
	 */
	public BlockMonolithe(int par1, Material par2Material)
	{
		super(par1, par2Material);
		this.setTickRandomly(true);
		this.isBlockContainer = true;
	}

	@Override
	public int getLightValue(IBlockAccess world, int x, int y, int z)
	{
		switch (world.getBlockMetadata(x, y, z))
		{
		case 4:
			return 14;
		}

		return 0;
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		iconBuffer = new Icon[2];
		iconBuffer[0] = par1IconRegister.registerIcon("aurummagie:obsidian_rune");
		iconBuffer[1] = par1IconRegister.registerIcon("aurummagie:obsidian_rune_powered");
	}

	@Override
	public float getBlockHardness(World par1World, int par2, int par3, int par4)
	{
		if (par1World.getBlockMetadata(par2, par3, par4) == 0)
		{
			return 50.0F;
		}
		else
		{
			return 6000000.0F;
		}
	}

	@Override
	public Icon getIcon(int i, int meta)
	{
		if (meta == 3 || meta == 1)
		{
			return iconBuffer[1];
		}
		else
		{
			return iconBuffer[0];
		}
	}

	@Override
	public void updateTick(World par1World, int par2, int par3, int par4, Random par5Random)
	{
		if (!par1World.isRemote)
		{
			if (par1World.getBlockMetadata(par2, par3, par4) == 1)
			{
				this.tryToFall(par1World, par2, par3, par4);
			}
			else if(par1World.getBlockMetadata(par2, par3, par4) == 0)
			{
				if(par5Random.nextInt(2) == 1)
				{
					EntityZombie z = new EntityZombie(par1World);
					z.setPositionAndUpdate(par2+par5Random.nextInt(4)-2, par3, par4+par5Random.nextInt(4)-2);
					par1World.spawnEntityInWorld(z);
				}
			}
		}
	}

	private void tryToFall(World par1World, int par2, int par3, int par4)
	{
		if (canFallBelow(par1World, par2, par3 - 1, par4) && par3 >= 0)
		{
			byte b0 = 32;

			if (!fallInstantly && par1World.checkChunksExist(par2 - b0, par3 - b0, par4 - b0, par2 + b0, par3 + b0, par4 + b0))
			{
				if (!par1World.isRemote)
				{
					EntityFallingSand entityfallingsand = new EntityFallingSand(par1World, (double)((float)par2 + 0.5F), (double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), this.blockID, par1World.getBlockMetadata(par2, par3, par4));
					this.onStartFalling(entityfallingsand);
					par1World.spawnEntityInWorld(entityfallingsand);
				}
			}
			else
			{
				par1World.setBlockToAir(par2, par3, par4);

				while (canFallBelow(par1World, par2, par3 - 1, par4) && par3 > 0)
				{
					--par3;
				}

				if (par3 > 0)
				{
					par1World.setBlock(par2, par3, par4, this.blockID);
				}
			}
		}
	}

	@Override
	public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
	{
		if (par1World.getBlockMetadata(par2, par3, par4) == 0)
		{
			if (par5Random.nextInt(100) == 0)
			{
				par1World.playSound((double)par2 + 0.5D, (double)par3 + 0.5D, (double)par4 + 0.5D, "monolithe", 0.5F, par5Random.nextFloat() * 0.4F + 0.8F, false);
			}
		}

		/*for (int l = 0; l < 4; ++l)
		{
			double d0 = (double)((float)par2 + par5Random.nextFloat());
			double d1 = (double)((float)par3 + par5Random.nextFloat());
			double d2 = (double)((float)par4 + par5Random.nextFloat());
			double d3 = 0.0D;
			double d4 = 0.0D;
			double d5 = 0.0D;
			int i1 = par5Random.nextInt(2) * 2 - 1;
			d3 = ((double)par5Random.nextFloat() - 0.5D) * 0.5D;
			d4 = ((double)par5Random.nextFloat() - 0.5D) * 0.5D;
			d5 = ((double)par5Random.nextFloat() - 0.5D) * 0.5D;

			if (par1World.getBlockId(par2 - 1, par3, par4) != this.blockID && par1World.getBlockId(par2 + 1, par3, par4) != this.blockID)
			{
				d0 = (double)par2 + 0.5D + 0.25D * (double)i1;
				d3 = (double)(par5Random.nextFloat() * 2.0F * (float)i1);
			}
			else
			{
				d2 = (double)par4 + 0.5D + 0.25D * (double)i1;
				d5 = (double)(par5Random.nextFloat() * 2.0F * (float)i1);
			}

			par1World.spawnParticle("portal", d0, d1, d2, d3, d4, d5);
		}*/
	}

	@Override
	public void onFinishFalling(World par1World, int par2, int par3, int par4, int par5)
	{
		par1World.setBlock(par2, par3, par4, AurumMagie.blockMonolithe.blockID, 3, 3);
	}

	//=============================
	// TileEntity Methods begin
	//=============================

	@Override
	public TileEntity createNewTileEntity(World world) 
	{
		return new TileEntityMonolitheBlock();
	}

	/**
	 * Called whenever the block is added into the world. Args: world, x, y, z
	 */
	@Override
	public void onBlockAdded(World par1World, int par2, int par3, int par4)
	{
		super.onBlockAdded(par1World, par2, par3, par4);
	}

	/**
	 * ejects contained items into the world, and notifies neighbours of an update, as appropriate
	 */
	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
	{
		super.breakBlock(par1World, par2, par3, par4, par5, par6);
		par1World.removeBlockTileEntity(par2, par3, par4);
	}

	/**
	 * Called when the block receives a BlockEvent - see World.addBlockEvent. By default, passes it on to the tile
	 * entity at this location. Args: world, x, y, z, blockID, EventID, event parameter
	 */
	@Override
	public boolean onBlockEventReceived(World par1World, int par2, int par3, int par4, int par5, int par6)
	{
		super.onBlockEventReceived(par1World, par2, par3, par4, par5, par6);
		TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);
		return tileentity != null ? tileentity.receiveClientEvent(par5, par6) : false;
	}
}