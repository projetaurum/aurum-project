package aurum.magie.monolithes;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import aurum.magie.AurumMagie;

public class BlockMonolithePortal extends Block
{
	public Icon[] iconBuffer;

	public BlockMonolithePortal(int par1, Material par2Material)
	{
		super(par1, par2Material);
		float f = 0.5F;
		this.setBlockBounds(0.5F - f, f, 0.5F - f, 0.5F + f, 0.9F, 0.5F + f);
		this.setTickRandomly(true);
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void onBlockAdded(World par1World, int par2, int par3, int par4)
	{
		if (par1World.getBlockId(par2 + 1, par3, par4 + 1) != 0
				|| par1World.getBlockId(par2, par3, par4 + 1) != 0
				|| par1World.getBlockId(par2 - 1, par3, par4 + 1) != 0
				|| par1World.getBlockId(par2 + 1, par3, par4) != 0
				|| par1World.getBlockId(par2 - 1, par3, par4) != 0
				|| par1World.getBlockId(par2 + 1, par3, par4 - 1) != 0
				|| par1World.getBlockId(par2, par3, par4 - 1) != 0
				|| par1World.getBlockId(par2 - 1, par3, par4 - 1) != 0)
		{
			return;
		}

		//Portail
		par1World.setBlock(par2 + 1, par3, par4 + 1, blockID, 8, 2);
		par1World.setBlock(par2, par3, par4 + 1, blockID, 7, 2);
		par1World.setBlock(par2 - 1, par3, par4 + 1, blockID, 6, 3);
		par1World.setBlock(par2 + 1, par3, par4, blockID, 5, 3);
		par1World.setBlock(par2, par3, par4, blockID, 4, 3);
		par1World.setBlock(par2 - 1, par3, par4, blockID, 3, 3);
		par1World.setBlock(par2 + 1, par3, par4 - 1, blockID, 2, 3);
		par1World.setBlock(par2, par3, par4 - 1, blockID, 1, 3);
		par1World.setBlock(par2 - 1, par3, par4 - 1, blockID, 0, 3);
		//Contour
		par1World.setBlock(par2 + 2, par3, par4, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 2, par3, par4 + 1, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 2, par3, par4 - 1, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 2, par3, par4, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 2, par3, par4 + 1, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 2, par3, par4 - 1, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 2, par3, par4 + 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 2, par3, par4 - 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 2, par3, par4 + 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 2, par3, par4 - 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2, par3, par4 + 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 1, par3, par4 + 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 1, par3, par4 + 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2, par3, par4 - 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 + 1, par3, par4 - 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		par1World.setBlock(par2 - 1, par3, par4 - 2, AurumMagie.blockMonolithe.blockID, 3, 2);
		Thread t = new ThreadMonolithe((WorldServer) par1World, par2, par3, par4);
		t.start();
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
	{
		return AxisAlignedBB.getAABBPool().getAABB((double)par2 + this.minX, (double)par3 + this.minY, (double)par4 + this.minZ, (double)par2 + this.maxX, (double)par3 + this.maxY, (double)par4 + this.maxZ);
	}

	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, @SuppressWarnings("rawtypes") List par6List, Entity par7Entity)
	{
		if (par7Entity == null || !(par7Entity instanceof EntityBoat))
		{
			super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
		}
	}

	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		iconBuffer = new Icon[9];
		iconBuffer[0] = par1IconRegister.registerIcon("aurummagie:monolithe_portal1");
		iconBuffer[1] = par1IconRegister.registerIcon("aurummagie:monolithe_portal2");
		iconBuffer[2] = par1IconRegister.registerIcon("aurummagie:monolithe_portal3");
		iconBuffer[3] = par1IconRegister.registerIcon("aurummagie:monolithe_portal4");
		iconBuffer[4] = par1IconRegister.registerIcon("aurummagie:monolithe_portal5");
		iconBuffer[5] = par1IconRegister.registerIcon("aurummagie:monolithe_portal6");
		iconBuffer[6] = par1IconRegister.registerIcon("aurummagie:monolithe_portal7");
		iconBuffer[7] = par1IconRegister.registerIcon("aurummagie:monolithe_portal8");
		iconBuffer[8] = par1IconRegister.registerIcon("aurummagie:monolithe_portal9");
	}

	@Override
	public Icon getBlockTexture(IBlockAccess world, int x, int y, int z, int blockSide)
	{
		int blockMeta = world.getBlockMetadata(x, y, z);
		return iconBuffer[blockMeta];
	}

	@Override
	public Icon getIcon(int i, int j)
	{
		return iconBuffer[j];
	}

	@Override
	public void randomDisplayTick(World w, int x, int y, int z, Random rand)
	{
		float f = (float)x + 0.5F;
		float f1 = (float)y + 0.0F + rand.nextFloat() * 6.0F / 16.0F;
		float f2 = (float)z + 0.5F;
		float f4 = rand.nextFloat() * 0.6F - 0.3F;

		w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
		w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
		w.spawnParticle("portal", (double)(f), (double)f1 - 1, (double)(f2 + f4), 0.0D, 0.5D, 0.0D);
	}
}