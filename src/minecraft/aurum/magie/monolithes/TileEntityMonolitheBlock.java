package aurum.magie.monolithes;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMonolitheBlock extends TileEntity
{
	private boolean isUnbreakable;
	private boolean isIlluminated;
	private boolean isActivated;
	
	public TileEntityMonolitheBlock()
	{
		
	}
	
	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		
		tag.setBoolean("BreakProp", isUnbreakable);
		tag.setBoolean("LightProp", isIlluminated);
		tag.setBoolean("ActivProp", isActivated);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		
		this.isUnbreakable = tag.getBoolean("BreakProp");
		this.isIlluminated = tag.getBoolean("LightProp");
		this.isActivated = tag.getBoolean("ActivProp");
	}
}