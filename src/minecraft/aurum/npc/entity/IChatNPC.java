package aurum.npc.entity;

import java.util.ArrayList;


public abstract interface IChatNPC 
{
	void addPhrase(String str);
	
	void setPhrase(String str, int index);
	
	void removePhrase(String str);
	
	void removePhrase(int index);
	
	String getPhrase(int index);
	
	ArrayList<String> getPhrases();
	
	int getPhraseNumber(String str);
}
