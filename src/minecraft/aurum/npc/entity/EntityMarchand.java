package aurum.npc.entity;

import java.util.ArrayList;

import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.packets.NPC05Packet;
import aurum.npc.gui.InventoryMarchand;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

public class EntityMarchand extends EntityNPC implements IChatNPC
{
	ArrayList<String> phrases = new ArrayList<String>();
	
	public InventoryMarchand inv = new InventoryMarchand(this);

	public String owner = "";

	public EntityMarchand(World par1World) 
	{
		super(par1World);
		//this.get = "/mods/aurum/npc/textures/entity/marchand.png";
		this.tasks.addTask(0, new EntityAISwimming(this));
		this.tasks.addTask(1, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
		this.tasks.addTask(1, new EntityAILookIdle(this));
		this.needLabel = true;
		this.function = "Marchand";
		
		if(this.phrases.isEmpty() && FMLCommonHandler.instance().getEffectiveSide().isServer())
		{
			this.phrases.add("Bonjour !");
			this.phrases.add("Bienvenue !");
			this.phrases.add("Coucou tu veut voir ma ... marchandise ? Vous imaginiez quoi d'autre hein ?");
		}
	}

	public boolean hasOwner()
	{
		return !this.owner.equals("");
	}

	public String getOwner()
	{
		return this.owner;
	}

	public void setOwner(String str)
	{
		this.owner = str;
	}

	@Override
	public String getNameColor()
	{
		return "Yellow";
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound tag)
	{
		super.writeEntityToNBT(tag);
		tag.setString("OWNER", this.owner);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound tag)
	{
		super.readEntityFromNBT(tag);
		this.owner = tag.getString("OWNER");
	}

	@Override
	public boolean isEntityInvulnerable()
	{
		return true;
	}

	public boolean interact(EntityPlayer player)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isServer())
		{
			if(!this.hasOwner())
				this.setOwner(player.username.toLowerCase());
			
			StringBuilder b = new StringBuilder();
			
			for(String s : this.phrases)
				b.append("#*%"+s);
			
			b.replace(0, 3, "");
			
			PacketDispatcher.sendPacketToPlayer(new NPC05Packet(1,b.toString()).makePacket(), (Player) player);
		}

		if(player.isSneaking())
			return false;
		player.openGui(AurumCore.instance, 1, this.worldObj, this.entityId, (int)this.posY, (int)this.posZ);
		return true;
	}

	@Override
	public void addPhrase(String str) 
	{
		this.phrases.add(str);
	}

	@Override
	public void removePhrase(String str) 
	{
		this.phrases.remove(str);
		
	}

	@Override
	public void removePhrase(int index) 
	{
		this.phrases.remove(index);
	}

	@Override
	public String getPhrase(int index) 
	{
		return this.phrases.get(index);
	}

	@Override
	public int getPhraseNumber(String str) 
	{
		return this.phrases.indexOf(str);
	}

	@Override
	public ArrayList<String> getPhrases() 
	{
		return this.phrases;
	}

	@Override
	public void setPhrase(String str, int index) 
	{
		this.phrases.set(index, str);
	}
}