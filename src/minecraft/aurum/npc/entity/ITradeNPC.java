package aurum.npc.entity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface ITradeNPC
{
    NPCTrade[] echanges = new NPCTrade[3];

    void addTrade(ItemStack[] want, ItemStack sell);

    void removeTrade(int index);

    void performTrade(int tradeIndex, EntityPlayer player);
}