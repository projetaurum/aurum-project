package aurum.npc.entity;

import net.minecraft.entity.DataWatcher;
import net.minecraft.entity.EntityCreature;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityNPC extends EntityCreature
{
	protected String function;
	protected boolean needLabel;

	public EntityNPC(World par1World)
	{
		super(par1World);
		this.dataWatcher.addObject(20, String.valueOf("Hammer Le Forgeron"));
		this.function = "";
	}

	public void setPersonnalName(String str)
	{
		DataWatcher dw = this.getDataWatcher();
		dw.updateObject(20, String.valueOf(str));
	}

	public boolean hasPersonnalName()
	{
		return !this.getDataWatcher().getWatchableObjectString(20).equals("");
	}

	public String getFunction()
	{
		return this.function;
	}

	public String getNameColor()
	{
		return "White";
	}

	public String getPersonnalName()
	{
		return this.getDataWatcher().getWatchableObjectString(20);
	}

	public boolean needLabel()
	{
		return this.needLabel;
	}

	@Override
	protected boolean isAIEnabled()
	{
		return true;
	}

	@Override
	public String getEntityName()
	{
		return this.function;
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound tag)
	{
		super.writeEntityToNBT(tag);
		tag.setString("NAME", this.getDataWatcher().getWatchableObjectString(20));
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound tag)
	{
		super.readEntityFromNBT(tag);
		this.setPersonnalName(tag.getString("NAME"));
	}
}