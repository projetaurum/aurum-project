package aurum.npc.entity;

import net.minecraft.item.ItemStack;

public class NPCTrade
{
    protected ItemStack[] wants;
    protected ItemStack sell = null;

    public NPCTrade(ItemStack[] want, ItemStack sell)
    {
        this.wants = want;
        this.sell = sell;
    }

    public ItemStack[] getWants()
    {
        return this.wants;
    }

    public ItemStack getSell()
    {
        return this.sell;
    }

    public void addWants(ItemStack stack)
    {
        ItemStack[] tempWant = wants;
        this.wants = new ItemStack[tempWant.length + 1];
        int i = 0;

        for (ItemStack stack2 : tempWant)
        {
            this.wants[i] = stack2;
            i = i + 1;
        }

        this.wants[i] = stack;
    }

    public void setWant(ItemStack stack, int slot)
    {
        this.wants[slot] = stack;
    }

    public void setSell(ItemStack stack)
    {
        this.sell = stack;
    }
}