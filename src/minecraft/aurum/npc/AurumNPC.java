package aurum.npc;

import java.awt.Color;
import java.util.logging.Logger;

import net.minecraft.server.MinecraftServer;
import aurum.npc.entity.EntityMarchand;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = AurumNPC.ID, version = AurumNPC.VERSION, name = "AurumNPC")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumNPC 
{
    @Instance(value = "AurumNPCMod")
    public static AurumNPC instance;

    public static final String ID = "AurumNPCMod";
    public static final String VERSION = "1.0";

    @SidedProxy(clientSide = "aurum.npc.ClientProxy", serverSide = "aurum.npc.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    public static MinecraftServer server;
    
    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
        instance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRendering();
    }

    @Init
    public void init(FMLInitializationEvent event)
    {
        //Entity
        int marchandID = EntityRegistry.findGlobalUniqueEntityId();
        EntityRegistry.registerGlobalEntityID(EntityMarchand.class, "marchand", marchandID, new Color(30, 1, 1).getRGB(), new Color(30, 5, 1).getRGB());
        EntityRegistry.registerModEntity(EntityMarchand.class, "marchand", marchandID, this, 60, 20, true);
        LanguageRegistry.instance().addStringLocalization("entity.marchand.name", "Marchand"); 
    }

    @PostInit
    public void postInit(FMLPostInitializationEvent event) { }
}