package aurum.npc;

import aurum.npc.entity.EntityMarchand;
import aurum.npc.render.ModelNPC;
import aurum.npc.render.RenderNPC;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy
{
    @Override
    public void registerRendering()
    {
        RenderingRegistry.registerEntityRenderingHandler(EntityMarchand.class, new RenderNPC(new ModelNPC(),0.5f, EntityMarchand.class));
    }
}