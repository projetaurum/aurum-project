package aurum.npc.render;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import aurum.npc.entity.EntityNPC;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderNPC extends RenderLiving
{
	public static final ResourceLocation texture = new ResourceLocation("aurumnpc:textures/entity/marchand.png");
	
    public RenderNPC(ModelBase par1ModelBase, float par2, Class <? extends EntityNPC > entityClass)
    {
        super(par1ModelBase, par2);
    }
    
    @Override
    public void doRender(Entity e, double par2, double par4, double par6, float par8, float distanceSq)
    {
    	EntityNPC npc = (EntityNPC) e;
    	
    	if(npc.needLabel())
    	{
    		this.renderNPCLabel(npc, npc.getFunction(), par2, par4+0.25d, par6, 32, npc.getNameColor());
    		if(npc.hasPersonnalName())
    			this.renderNPCLabel(npc, npc.getPersonnalName(), par2, par4, par6, 32, npc.getNameColor());
    	}
        this.doRenderLiving((EntityLiving)e, par2, par4, par6, par8, distanceSq);
    }
    
    public void renderNPCLabel(EntityNPC entity, String text, double x, double y, double z, int distanceSq, String color)
    {
        double d3 = entity.getDistanceSqToEntity(this.renderManager.livingPlayer);

        if (d3 <= (double)(distanceSq * distanceSq))
        {
            FontRenderer fontrenderer = this.getFontRendererFromRenderManager();
            float f = 1.6F;
            float f1 = 0.016666668F * f;
            GL11.glPushMatrix();
            GL11.glTranslatef((float)x + 0.0F, (float)y + entity.height + 0.5F, (float)z);
            GL11.glNormal3f(0.0F, 1.0F, 0.0F);
            GL11.glRotatef(-this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
            GL11.glScalef(-f1, -f1, f1);
            GL11.glDisable(GL11.GL_LIGHTING);
            GL11.glDepthMask(false);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            Tessellator tessellator = Tessellator.instance;
            byte b0 = 0;

            if (text.equals("deadmau5"))
            {
                b0 = -10;
            }

            GL11.glDisable(GL11.GL_TEXTURE_2D);
            tessellator.startDrawingQuads();
            int j = fontrenderer.getStringWidth(text) / 2;
        	tessellator.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
            tessellator.addVertex((double)(-j - 1), (double)(-1 + b0), 0.0D);
            tessellator.addVertex((double)(-j - 1), (double)(8 + b0), 0.0D);
            tessellator.addVertex((double)(j + 1), (double)(8 + b0), 0.0D);
            tessellator.addVertex((double)(j + 1), (double)(-1 + b0), 0.0D);
            tessellator.draw();
            GL11.glEnable(GL11.GL_TEXTURE_2D);
            
            int colorInt = -1;
            
            if(color.equals("Green"))
            	colorInt = 30007;
            else if(color.equals("Yellow"))
            	colorInt = 11910950;
            else if(color.equals("Red"))
            	colorInt = 29048127;
            else if(color.equals("Blue"))
            	colorInt = 150007;
            else if(color.equals("LightBlue"))
            	colorInt = 20048127;
            else if(color.equals("Purple"))
            	colorInt = 40648127;
            else if(color.equals("Orange"))
            	colorInt = 31748127;
            else if(color.equals("Pink"))
            	colorInt = 14455750;
            
            fontrenderer.drawString(text, -fontrenderer.getStringWidth(text) / 2, b0, 553648127);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthMask(true);
            fontrenderer.drawString(text, -fontrenderer.getStringWidth(text) / 2, b0, colorInt);
            GL11.glEnable(GL11.GL_LIGHTING);
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glPopMatrix();
        }
    }

	protected ResourceLocation getNPCTextures(EntityNPC drone)
    {
        return texture;
    }

    protected ResourceLocation getEntityTexture(Entity par1Entity)
    {
        return this.getNPCTextures((EntityNPC)par1Entity);
    }
}