package aurum.npc.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

public class GuiButtonMarchand extends GuiButton
{
	boolean visible = true;
	public boolean isSelected = this.field_82253_i;
	
	public GuiButtonMarchand(int buttonID, int x, int y, int width, int height) 
	{
		super(buttonID, x, y, width, height, "");
	}
	
	@Override
    public void drawButton(Minecraft par1Minecraft, int x, int y)
    {
        this.isSelected = x >= this.xPosition && y >= this.yPosition && x < this.xPosition + this.width && y < this.yPosition + this.height;

		return;
    }
}