package aurum.npc.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import aurum.stockage.AurumStockage;

public class SlotBourse extends Slot
{
    public SlotBourse(IInventory par1iInventory, int par2, int par3, int par4)
    {
        super(par1iInventory, par2, par3, par4);
    }

    public boolean isItemValid(ItemStack is)
    {
        return is.getItem().itemID == AurumStockage.itemBourse.itemID;
    }
}