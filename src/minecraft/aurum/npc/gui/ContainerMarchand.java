package aurum.npc.gui;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import aurum.monnaie.gui.IContainerBourse;
import aurum.monnaie.gui.InventoryBourse;
import aurum.monnaie.gui.SlotEcu;
import aurum.stockage.AurumStockage;

public class ContainerMarchand extends ContainerNPC implements IContainerBourse
{
	public EntityPlayer player;
	public boolean hasBourse;
	
	public InventoryMarchand inv;
	public InventoryBourse bourse;
	
	public int tab = 0;

	public ContainerMarchand(InventoryMarchand inv, InventoryPlayer invPlayer, EntityPlayer player)
	{
		this.inv = inv;
		this.player = player;

		bindPlayerInventory(invPlayer,-4,79);
		bindMarchandChest(inv);
		bindMarchandEchanges(inv);
		bindMarchandBourse(inv);
		
		System.out.println(this.inv.marchand.getPhrases());
	}
	
	@Override
	public void updateBourse()
	{
		if(((Slot)this.inventorySlots.get(72)).getStack() != null && ((Slot)this.inventorySlots.get(72)).getStack().itemID == AurumStockage.itemBourse.itemID)
		{
			addBourse(new InventoryBourse(player, ((Slot)this.inventorySlots.get(72)).getStack()));
		}
	}
	
	@Override
	public ItemStack slotClick(int slotIndex, int par2, int par3, EntityPlayer player)
	{
		if(slotIndex == 72)
		{
			if(player.inventory.getItemStack() != null && player.inventory.getItemStack().itemID == AurumStockage.itemBourse.itemID)
			{
				addBourse(new InventoryBourse(player,player.inventory.getItemStack()));
			}
			else if(player.inventory.getItemStack() == null)
			{
				if(((Slot)this.inventorySlots.get(slotIndex)).getStack() != null)
					removeBourse();
			}
		}
		return super.slotClick(slotIndex, par2, par3, player);
	}
	
	@Override
	public void onContainerClosed(EntityPlayer player)
	{
		this.saveBourse();
		this.saveToNBT();
	}

	public void setTab0()
	{
		this.tab = 0;
		bindMarchandChest(inv);
		bindMarchandEchanges(inv);
		bindMarchandBourse(inv);
	}

	public void removeTab0()
	{
		this.tab = 1;
		ArrayList<Slot> slots = new ArrayList<Slot>();
		for(int i=0;i<27;i++)
		{
			slots.add((Slot) this.inventorySlots.get(i+36));
		}
		for(int i =0;i<9;i++)
		{
			slots.add((Slot) this.inventorySlots.get(i+36+27));
		}
		
		if(this.inventorySlots.size()>73)
		{
			for(int i =0;i<8;i++)
			{
				slots.add((Slot) this.inventorySlots.get(i+73));
			}
		}
		slots.add((Slot) this.inventorySlots.get(72));

		for(Slot s : slots)
		{
			this.inventorySlots.remove(s);
		}
	}

	public void bindMarchandBourse(InventoryMarchand inv)
	{
		addSlotToContainer(new SlotBourse(inv, 0, 94, 76));
	}
	
	@Override
	public void addBourse(InventoryBourse inv)
	{
		this.bourse = inv;
		
		this.addSlotToContainer(new SlotEcu(inv,0,94,52-18));
		this.addSlotToContainer(new SlotEcu(inv,1,94+18,52-18));
		this.addSlotToContainer(new SlotEcu(inv,2,94+36,52-18));
		this.addSlotToContainer(new SlotEcu(inv,3,94+18+36,52-18));
		
		this.addSlotToContainer(new SlotEcu(inv,4,94,52));
		this.addSlotToContainer(new SlotEcu(inv,5,94+18,52));
		this.addSlotToContainer(new SlotEcu(inv,6,94+36,52));
		this.addSlotToContainer(new SlotEcu(inv,7,94+18+36,52));
	}
	
	@Override
	public void saveBourse()
	{
		if(this.inventorySlots.size()>36)
		{
			if(((Slot)this.inventorySlots.get(72)).getStack() != null)
			{
				ItemStack itemStack = ((Slot)this.inventorySlots.get(72)).getStack();
				
		        if (!itemStack.hasTagCompound())
		        {
		            itemStack.setTagCompound(new NBTTagCompound());
		        }

		        if(this.bourse != null)
		        	this.bourse.writeToNBT(itemStack.getTagCompound());
			}
		}
	}
	
	@Override
	public void removeBourse()
	{
		if(this.inventorySlots.size()>73)
		{
			saveBourse();
			
			ArrayList<Slot> slots = new ArrayList<Slot>();
			for(int i=0;i<8;i++)
			{
				slots.add((Slot) this.inventorySlots.get(i+73));
			}

			for(Slot s : slots)
			{
				this.inventorySlots.remove(s);
			}
		}
	}

	public void bindMarchandEchanges(InventoryMarchand inv)
	{
		for (int i = 0; i < 3; i++)
		{
			addSlotToContainer(new Slot(inv, i+1, 4, 25 + 9+i*21));
			addSlotToContainer(new Slot(inv, i+3+1, 4+19, 25 + 9+i*21));
			addSlotToContainer(new Slot(inv, i+6+1, 65, 25 + 9+i*21));
		}
	}

	public void bindMarchandChest(InventoryMarchand inv)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				addSlotToContainer(new Slot(inv, j + i * 9 + 9+1,
						8 + j * 18-4, 84 + 9 + i * 18+9));
			}
		}
	}

	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer, int sourceX, int sourceY)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
						8 + j * 18 + sourceX, 85 + i * 18 + sourceY));
			}
		}

		for (int i = 0; i < 9; i++)
		{
			addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18 + sourceX, 143 + sourceY));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) 
	{
		return inv.isUseableByPlayer(entityplayer);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
	{
		ItemStack stack = null;
		Slot slotObject = (Slot) inventorySlots.get(slot);

		int i = 27;
		
		if(this.tab == 1)
			i=0;
		
		ItemStack stackInSlot = slotObject.getStack();
		
		if(stackInSlot != null)
		{
			stack = stackInSlot.copy();

			if (slot >= 36)
			{
				if (!this.mergeItemStack(stackInSlot, 0, 36, true))
				{
					return null;
				}
			}
			else if (!this.mergeItemStack(stackInSlot, 36, 36+i, false))
			{
				return null;
			}

			if (stackInSlot.stackSize == 0)
			{
				slotObject.putStack(null);
			}
			else
			{
				slotObject.onSlotChanged();
			}

			if (stackInSlot.stackSize == stack.stackSize)
			{
				return null;
			}

			slotObject.onPickupFromSlot(player, stackInSlot);
		}
		return stack;
	}
	
    public void saveToNBT()
    {
        this.inv.writeToNBT(this.inv.marchand.getEntityData());
    }

	@Override
	void setNPCName(String name) 
	{
		this.inv.marchand.setPersonnalName(name);
	}

	@Override
	void setNPCPhrase(int pIndex, String phrase) 
	{
		this.inv.marchand.setPhrase(phrase, pIndex);
	}

	@Override
	void addNPCPhrase(String phrase) 
	{
		this.inv.marchand.addPhrase(phrase);
	}

	@Override
	void removeNPCPhrase(String phrase) 
	{
		this.inv.marchand.removePhrase(phrase);
	}

	@Override
	ArrayList<String> getAllPhrases() 
	{
		return this.inv.marchand.getPhrases();
	}
}