package aurum.npc.gui;

import java.util.ArrayList;

import net.minecraft.inventory.Container;

public abstract class ContainerNPC extends Container
{	
	public ContainerNPC(){}
	
	abstract void setNPCName(String name);
	
	abstract void setNPCPhrase(int pIndex, String phrase);
	
	abstract void addNPCPhrase(String phrase);
	
	abstract void removeNPCPhrase(String phrase);
	
	abstract ArrayList<String> getAllPhrases();
}
