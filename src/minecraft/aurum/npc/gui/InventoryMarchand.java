package aurum.npc.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import aurum.npc.entity.EntityMarchand;

public class InventoryMarchand implements IInventory
{
	/**
	 * 0 : BourseSlot
	 * 1-10 : Echanges
	 * 11-37 : Chest
	 */
	public ItemStack[] items;
	public EntityMarchand marchand;
	
	public boolean hasBourse;
	
	public byte onglet;
	
	public InventoryMarchand(EntityMarchand marchand)
	{
		this.onglet = 0;
		this.marchand = marchand;
		this.items = new ItemStack[38];
		this.hasBourse = false;
        readFromNBT(marchand.getEntityData());
	}
	
	@Override
	public int getSizeInventory() 
	{
		return items.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) 
	{
		return items[i];
	}

	@Override
	public ItemStack decrStackSize(int slotIndex, int amount) 
	{
        ItemStack stack = getStackInSlot(slotIndex);

        if (stack != null)
        {
            if (stack.stackSize <= amount)
            {
                setInventorySlotContents(slotIndex, null);
            }
            else
            {
                stack = stack.splitStack(amount);

                if (stack.stackSize == 0)
                {
                    setInventorySlotContents(slotIndex, null);
                }
            }
        }

        return stack;
	}

    @Override
    public ItemStack getStackInSlotOnClosing(int slotIndex)
    {
        ItemStack stack = getStackInSlot(slotIndex);

        if (stack != null)
        {
            setInventorySlotContents(slotIndex, null);
        }

        return stack;
    }

    @Override
    public void setInventorySlotContents(int slotIndex, ItemStack stack)
    {
        items[slotIndex] = stack;

        if (stack != null && stack.stackSize > getInventoryStackLimit())
        {
            stack.stackSize = getInventoryStackLimit();
        }
    }

	@Override
	public String getInvName() 
	{
		return "MarchandGui";
	}

	@Override
	public boolean isInvNameLocalized() 
	{
		return false;
	}

	@Override
	public int getInventoryStackLimit() 
	{
		return 64;
	}

    @Override
    public void onInventoryChanged() {}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) 
	{
		return true;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) 
	{
		return true;
	}
	
	@Override
	public void openChest() {}
	@Override
	public void closeChest() {}
	
    public void writeToNBT(NBTTagCompound tag)
    {
        NBTTagList nbttaglist = new NBTTagList();
        
        tag.setBoolean("Bourse", hasBourse);

        for (int i = 0; i < this.items.length; ++i)
        {
            if (this.items[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.items[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        tag.setTag("Items", nbttaglist);
    }

    public void readFromNBT(NBTTagCompound tag)
    {
        NBTTagList nbttaglist = tag.getTagList("Items");

        this.hasBourse = tag.getBoolean("Bourse");
        
        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.items.length)
            {
                this.items[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }
    }
}