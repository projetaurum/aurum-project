package aurum.npc.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import aurum.core.packets.NPC05Packet;
import aurum.core.packets.SlotRemoved04Packet;
import cpw.mods.fml.common.network.PacketDispatcher;

public class GuiMarchand extends GuiContainer implements IChatNPCGui 
{
	private ContainerMarchand marchand;
	
	private GuiButtonMarchand onglet0;
	private GuiButtonMarchand onglet1;
	
	private GuiButtonMarchand nameValid;
	private GuiButtonMarchand phraseValid;
	private GuiButtonMarchand phraseAdd;
	private GuiButtonMarchand phraseRemove;
	
	private ArrayList<GuiTextField> phrases = new ArrayList<GuiTextField>();
	private GuiTextField namePNJ;
	
	public GuiMarchand(ContainerMarchand containerMarchand) 
	{
		super(containerMarchand);
		this.marchand = containerMarchand;
        this.ySize = 256;
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		
        int x = (this.width - this.xSize) / 2;
        int y = (this.height - this.ySize) / 2;
		
		if(!this.marchand.getAllPhrases().isEmpty())
		{	
	        System.out.println("ALIVE '2");
			int i = 0;
			for(String s : this.marchand.getAllPhrases())
			{
		        this.phrases.get(i).setText(s);
				i=i+1;
			}
			this.marchand.getAllPhrases().clear();
		}
	}
	
    @SuppressWarnings("unchecked")
	public void initGui()
    {
        super.initGui();
        Keyboard.enableRepeatEvents(true);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.onglet0 = new GuiButtonMarchand(1, i, j+1, 85,15));
        this.buttonList.add(this.onglet1 = new GuiButtonMarchand(2, i+84, j+1,85,15));
        this.onglet0.enabled = false;
        this.onglet0.visible = false;
        this.onglet1.visible = false;
        
        this.namePNJ = new GuiTextField(this.fontRenderer, i+4, j + 144, 83, 19);
        this.namePNJ.setTextColor(-1);
        this.namePNJ.setDisabledTextColour(-1);
        this.namePNJ.setEnableBackgroundDrawing(false);
        this.namePNJ.setMaxStringLength(30);
        this.namePNJ.setText(this.marchand.inv.marchand.getPersonnalName());
        
        this.buttonList.add(this.nameValid =  new GuiButtonMarchand(3,i+89,j+136, 20, 19));
        this.nameValid.visible = false;
        this.nameValid.enabled = false;
        
        for(int i2 =0;i2<5;i2++)
        {
        	GuiTextField text = new GuiTextField(this.fontRenderer,i+4,j+40+(i2*19),136,19);
			text.setTextColor(-1);
	        text.setDisabledTextColour(-1);
	        text.setEnableBackgroundDrawing(false);
	        text.setMaxStringLength(90);
	        
	        phrases.add(text);
        }
    }
    
    @Override
    public void onGuiClosed()
    {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents(false);
    }
    
    @Override
    public void drawScreen(int par1, int par2, float par3)
    {
        super.drawScreen(par1, par2, par3);
        
        if(this.marchand.tab == 1)
        {
            GL11.glDisable(GL11.GL_LIGHTING);
            this.namePNJ.drawTextBox();
            for(GuiTextField text : this.phrases)
            	text.drawTextBox();
        }
    }
	
	@Override
    protected void actionPerformed(GuiButton button)
    {
    	if(button == this.onglet0 && this.onglet0.enabled && this.marchand.tab == 1)
    	{
    		this.onglet0.enabled = false;
    		this.onglet1.enabled = true;
    		
    		((ContainerMarchand)this.inventorySlots).setTab0();
        	PacketDispatcher.sendPacketToServer(new SlotRemoved04Packet(false).makePacket());
    	}
    	else if(button == this.onglet1 && this.onglet1.enabled && this.marchand.tab == 0)
    	{
    		this.onglet1.enabled = false;
    		this.onglet0.enabled = true;
    		
        	((ContainerMarchand)this.inventorySlots).removeTab0();
        	PacketDispatcher.sendPacketToServer(new SlotRemoved04Packet(true).makePacket());
    	}
    	else if(button == this.nameValid)
    	{
    		PacketDispatcher.sendPacketToServer(new NPC05Packet(0,this.namePNJ.getText()).makePacket());
    	}
    }

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRenderer.drawString("Echanges", 5, this.ySize - 232, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) 
	{
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;

		if(this.marchand.tab == 0)
			this.mc.renderEngine.bindTexture(new ResourceLocation("/mods/aurum/npc/textures/gui/marchand_owner1.png"));
		else if(this.marchand.tab == 1)
			this.mc.renderEngine.bindTexture(new ResourceLocation("/mods/aurum/npc/textures/gui/marchand_owner2.png"));
		this.drawTexturedModalRect(x - 4, y, 0, 0, xSize, ySize);
		
		if(!this.nameValid.enabled)
			this.drawTexturedModalRect(x+89, y+136, 176, 20, 20, 20);
		else if(this.nameValid.isSelected)
			this.drawTexturedModalRect(x+89, y+136, 176, 0, 20, 20);
	}
	
	@Override
    protected void mouseClicked(int par1, int par2, int par3)
    {
        super.mouseClicked(par1, par2, par3);
        
        if(this.marchand.tab == 1)
        {
        	this.namePNJ.mouseClicked(par1, par2, par3);
        	
        	for(GuiTextField text : this.phrases)
        	{
        		text.mouseClicked(par1, par2, par3);
        	}
        }
    }
	
	@Override
    protected void keyTyped(char par1, int par2)
    {
		if(!this.phrases.isEmpty())
		{
			for(GuiTextField text : phrases)
			{
				if(text.textboxKeyTyped(par1, par2))
				{
		        	/*if(!this.phraseValid.enabled)
		        		this.phraseValid.enabled = true;
		        	else
		        		this.phraseValid.enabled = false;*/
				}
			}
		}
		
        if (this.namePNJ.textboxKeyTyped(par1, par2))
        {
        	if(!this.namePNJ.getText().equals(marchand.inv.marchand.getPersonnalName()))
        		this.nameValid.enabled = true;
        	else if(this.nameValid.enabled)
        		this.nameValid.enabled = false;
        }
        else
        {
            super.keyTyped(par1, par2);
        }
    }
}