package aurum.monnaie;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class ItemEcu extends Item
{
    public ItemEcu(int par1)
    {
        super(par1);
    }

    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        itemIcon = iconRegister.registerIcon("aurummonnaie:ecu");
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void getSubItems(int par1, CreativeTabs par2, List par3)
    {
        par3.add(new ItemStack(par1, 1, 0));
        par3.add(new ItemStack(par1, 1, 1));
    }

    @SuppressWarnings( { "unchecked", "rawtypes" })
    @Override
    public void addInformation(ItemStack par1, EntityPlayer par2, List par3, boolean par4)
    {
        if (par1.getItemDamage() == 0)
        {
            par3.add(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "Aura");
        }
        else if (par1.getItemDamage() == 1)
        {
            par3.add(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "VousSaurezBienVite");
        }
    }
}