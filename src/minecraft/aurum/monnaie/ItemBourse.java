package aurum.monnaie;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import aurum.backpack.ItemBag;
import aurum.core.AurumCore;

public class ItemBourse extends ItemBag
{
    public ItemBourse(int par1)
    {
        super(par1);
        this.setHasSubtypes(true);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack is, World w, EntityPlayer player)
    {
        player.openGui(AurumCore.instance, 0, w, (int)player.posX, (int)player.posY, (int)player.posZ);
        return is;
    }

    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        this.itemIcon = iconRegister.registerIcon("aurummonnaie:bourse_brune");
    }
}