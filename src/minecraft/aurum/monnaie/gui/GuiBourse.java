package aurum.monnaie.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class GuiBourse extends GuiContainer
{
    public ContainerBourse container;

    public GuiBourse(ContainerBourse par1Container)
    {
        super(par1Container);
        this.container = par1Container;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
    {
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.mc.renderEngine.bindTexture(new ResourceLocation("aurummonnaie","textures/gui/bourse.png"));
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
    }

    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    {
        this.fontRenderer.drawString("Bourse", 8, this.ySize - 160, 4210752);
        this.fontRenderer.drawString(this.container.bourse.nbrEcus + " Ecus", 38, 60, 4210752);
    }
}