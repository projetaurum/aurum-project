package aurum.monnaie.gui;


/**
 * An Interface for Inventory who implements a Bourse
 * @author Ourten
 *
 */
public abstract interface IContainerBourse 
{
	/**
	 * Add Bourse to Container & IInventory
	 * 
	 * @param IInventoryBourse
	 **/
	abstract void addBourse(InventoryBourse bourse);
	
	/**
	 * Remove the Bourse from the Container (When the ItemBourse is picked up for example)
	 */
	abstract void removeBourse();
	
	/**
	 * Save BourseIInventory into the Bourse ItemStack
	 */
	abstract void saveBourse();
	
	/**
	 * When a slot is clicked use updateBourse() to add the Bourse into the container
	 */
	abstract void updateBourse();
}