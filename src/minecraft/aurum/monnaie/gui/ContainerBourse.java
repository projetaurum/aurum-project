package aurum.monnaie.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import aurum.backpack.ContainerBag;
import aurum.backpack.InventoryBag;
import aurum.core.AurumCore;

public class ContainerBourse extends ContainerBag
{
    int invRow, invCol, colPlayer, invSize, rest;
    InventoryBag containerInv;
    boolean updateNotification;
    InventoryBourse bourse;

    public ContainerBourse(InventoryBourse invBourse, InventoryPlayer invPlayer)
    {
        super(invBourse, invPlayer);
        updateNotification = false;
        containerInv = invBourse;
        invSize = containerInv.getSizeInventory();
        bourse = invBourse;
        addSlotToContainer(new SlotEcu(invBourse, 0, 53, 12));
        addSlotToContainer(new SlotEcu(invBourse, 1, 71, 12));
        addSlotToContainer(new SlotEcu(invBourse, 2, 89, 12));
        addSlotToContainer(new SlotEcu(invBourse, 3, 107, 12));
        addSlotToContainer(new SlotEcu(invBourse, 4, 53, 30));
        addSlotToContainer(new SlotEcu(invBourse, 5, 71, 30));
        addSlotToContainer(new SlotEcu(invBourse, 6, 89, 30));
        addSlotToContainer(new SlotEcu(invBourse, 7, 107, 30));
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack stack = null;
        Slot slotObject = (Slot) inventorySlots.get(slot);

        if (slotObject != null && slotObject.getHasStack())
        {
            ItemStack stackInSlot = slotObject.getStack();
            stack = stackInSlot.copy();

            if (stack.itemID == AurumCore.itemEcu.itemID)
            {
                if (slot >= 36)
                {
                    if (!this.mergeItemStack(stackInSlot, 0, 36, true))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(stackInSlot, 36, 44, false))
                {
                    return null;
                }

                if (stackInSlot.stackSize == 0)
                {
                    slotObject.putStack(null);
                }
                else
                {
                    slotObject.onSlotChanged();
                }

                if (stackInSlot.stackSize == stack.stackSize)
                {
                    return null;
                }

                slotObject.onPickupFromSlot(player, stackInSlot);
            }
        }

        return stack;
    }
}