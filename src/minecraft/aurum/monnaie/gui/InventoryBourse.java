package aurum.monnaie.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import aurum.backpack.InventoryBag;

public class InventoryBourse extends InventoryBag
{
    int nbrEcus = 0;

    public InventoryBourse(EntityPlayer entityPlayer, ItemStack is)
    {
        super(is, entityPlayer, 9);
    }

    @Override
    public void onInventoryChanged()
    {
        super.onInventoryChanged();
        updateEcus();
    }

    public void updateEcus()
    {
        this.nbrEcus = 0;

        for (int i = 0; i < this.myInventory.length; i++)
        {
            if (this.getStackInSlot(i) != null)
            {
                this.nbrEcus += this.getStackInSlot(i).stackSize;
            }
        }
    }

    @Override
    public int readInvSizeFromNBT(NBTTagCompound myCompound)
    {
        return 8;
    }

    @Override
    public String getInvName()
    {
        return "Bourse";
    }
}