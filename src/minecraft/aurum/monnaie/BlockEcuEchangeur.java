package aurum.monnaie;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import aurum.core.AurumCore;
import aurum.core.ClientProxy;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockEcuEchangeur extends Block
{
    public Icon[] iconBuffer;

    public BlockEcuEchangeur(int par1, Material par2Material)
    {
        super(par1, par2Material);
    }

    @Override
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        System.out.println("Side: " + par6);

        if (par5EntityPlayer.getCurrentEquippedItem() != null)
        {
            ItemStack is = par5EntityPlayer.getCurrentEquippedItem();

            if (is.itemID == Item.emerald.itemID && par6 == 1)
            {
                par5EntityPlayer.inventory.setInventorySlotContents(par5EntityPlayer.inventory.currentItem, new ItemStack(AurumCore.itemEcu, par5EntityPlayer.inventory.getCurrentItem().stackSize));
            }
        }

        return true;
    }

    @Override
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
    {
        super.onBlockPlacedBy(par1World, par2, par3, par4, par5EntityLiving, par6ItemStack);
        par1World.setBlockMetadataWithNotify(par2, par3, par4, determineOrientation(par1World, par2, par3, par4, par5EntityLiving), 2);
    }

    public static int determineOrientation(World par0World, int par1, int par2, int par3, EntityLivingBase par4EntityLiving)
    {
        if (MathHelper.abs((float)par4EntityLiving.posX - (float)par1) < 2.0F && MathHelper.abs((float)par4EntityLiving.posZ - (float)par3) < 2.0F)
        {
            double d0 = par4EntityLiving.posY + 1.82D - (double)par4EntityLiving.yOffset;

            if (d0 - (double)par2 > 2.0D)
            {
                return 1;
            }

            if ((double)par2 - d0 > 0.0D)
            {
                return 0;
            }
        }

        int l = MathHelper.floor_double((double)(par4EntityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
        return l == 0 ? 2 : (l == 1 ? 5 : (l == 2 ? 3 : (l == 3 ? 4 : 0)));
    }

    @SideOnly(Side.CLIENT)
    @Override
    public int getRenderType()
    {
        return ClientProxy.ecuEchangeurRenderType;
    }

    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @Override
    public void registerIcons(IconRegister par1IconRegister)
    {
        iconBuffer = new Icon[4];
        iconBuffer[0] = par1IconRegister.registerIcon("aurummonnaie:tirelire_bas");
        iconBuffer[1] = par1IconRegister.registerIcon("aurummonnaie:tirelire_haut");
        iconBuffer[2] = par1IconRegister.registerIcon("aurummonnaie:tirelire_face");
        iconBuffer[3] = par1IconRegister.registerIcon("aurummonnaie:tirelire_cotes");
    }

    @Override
    public Icon getIcon(int par1, int par2)
    {
        return par1 == 1 ? iconBuffer[1] : (par1 == 0 ? iconBuffer[0] : (par1 != par2 ? iconBuffer[3] : iconBuffer[2]));
    }
}