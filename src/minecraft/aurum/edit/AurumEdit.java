package aurum.edit;

import java.util.logging.Logger;

import aurum.core.AurumRegistry;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = AurumEdit.ID, version = AurumEdit.VERSION, name = "AurumEdit", dependencies = "required-after:AurumCoreMod")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class AurumEdit 
{
    @Instance("AurumEdit")
    public static AurumEdit instance;

    public static final String VERSION = "1.0";
    public static final String ID = "AurumEditMod";
    
    @SidedProxy(clientSide = "aurum.edit.ClientProxy", serverSide = "aurum.edit.CommonProxy")
    public static CommonProxy proxy;
    
    public static Logger logger;

    public static MinecraftServer server;
    
    public static final Item itemWand = new ItemWand(11000)
    .setUnlocalizedName("aurumcore:wand");
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        instance = this;
        logger = Logger.getLogger(ID);
        logger.setParent(FMLLog.getLogger());
        proxy.registerRender();
    }

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        //AurumRegistry.registerItem(itemWand, "Aurum Edit Wand");
    }
}